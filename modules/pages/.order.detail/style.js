import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		backgroundColor: Colors.solid.grey.palette(1),
		paddingTop: 0,
		paddingLeft: 0,
		paddingRight: 0,
		paddingBottom: 32,
	},

	header: {
		borderTopWidth: 0,
	},

	section: {
		paddingTop: 16,
		paddingBottom: 16,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
		backgroundColor: Colors.white.primary,
		borderWidth: 0,
		borderColor: Colors.coal.palette(2),
		borderStyle: 'solid',
		borderTopWidth: StyleSheet.hairlineWidth,
		borderBottomWidth: StyleSheet.hairlineWidth,
	},

	summary: {
		paddingBottom: 24,
	},

	productCount: {
		color: Colors.black.palette(4),
		marginBottom: 16,
	},

	product: {
		paddingBottom: 24,
	},

	image: {
		width: 72,
		height: 72,
		marginRight: 16,
	},

	action: {
		marginTop: 24,
		marginBottom: 24,
		marginLeft: Sizes.margin.thick,
		marginRight: Sizes.margin.thick,
	},

	prices: {
		paddingTop: 16,
		borderWidth: 0,
		borderStyle: 'solid',
		borderTopWidth: StyleSheet.hairlineWidth,
		borderTopColor: Colors.coal.palette(2),
	},

	footer: {
		marginTop: 8,
		padding: 24,
		backgroundColor: Colors.coal.primary,
	},

	footerHeader: {
		color: Colors.white.primary,
		marginBottom: 8,
	},

	footerNote: {
		color: Colors.white.palette(4),
		marginBottom: 16,
	},
})
