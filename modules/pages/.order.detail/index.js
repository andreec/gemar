import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

import BatchManager from 'app/managers/batch'

import TrackingHandler from 'app/handlers/tracking';

import CheckoutHelper from 'utils/helpers/checkout';
import EmitterHelper from 'coeur/helpers/emitter';
import Linking from 'coeur/libs/linking';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import SourceSansBit from 'modules/bits/source.sans';
import HeaderBit from 'modules/bits/header';
import LoaderBit from 'modules/bits/loader';
import PageBit from 'modules/bits/page';

import ListHeaderLego from 'modules/legos/list.header';

import CardOrderComponent from 'modules/components/card.order'

import FourOFourPagelet from 'modules/pagelets/four.o.four'

import AddressPart from './_address'
import OrderPart from './_order'
import PricesPart from './_prices'

import Styles from './style';

export {
	OrderPart,
	PricesPart,
}


export default ConnectHelper(
	class OrderDetailPage extends PageModel {

		static routeName = 'order.detail'

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				batch: PropTypes.object,
				me: PropTypes.object,
			}
		}

		static stateToProps(state, oP) {
			const id = parseInt(oP.id, 10)
				, batch = BatchManager.get(id)

			return batch._isInvalid ? {
				id,
				me: state.me,
				batch,
				isCrashing: true,
			} : {
				id,
				me: state.me,
				batch,
			}
		}

		header = {
			leftActions: [{
				type: HeaderBit.TYPES.BACK,
			}],
			title: 'ORDER DETAILS',
			rightActions: [{
				type: HeaderBit.TYPES.BLANK,
			}],
		}

		constructor(p) {
			super(p, {
				isGettingData: false,
			}, [
				'onContactCustomerCare',
				'onProceedToPayment',
				'orderRenderer',
				'onPageVisibilityChange',
			])

			this._visibilityListener = undefined
		}

		componentWillMount() {
			if(!this.props.batch._isGetting) {
				BatchManager.dataGetter(this.props.id)
			}
		}

		componentDidMount() {
			TrackingHandler.trackPageView(this.routeName)

			this._visibilityListener = EmitterHelper.addListener('app:visibility', this.onPageVisibilityChange)
		}

		componentWillUnmount() {
			this._visibilityListener &&
			this._visibilityListener.remove()
		}

		onPageVisibilityChange(isVisible) {
			if(isVisible && !this.state.isGettingData) {
				this.setState({
					isGettingData: true,
				}, () => {
					BatchManager
						.dataGetter(this.props.id)
						.catch(() => {
							return true
						})
						.then(() => {
							this.setState({
								isGettingData: false,
							})
						})
				})
			}
		}

		onContactCustomerCare() {
			Linking.open('mailto:care@helloyuna.io')
		}

		onProceedToPayment() {
			BatchManager
				.getCheckoutLink(this.props.batch.id)
				.then(({
					link,
					data,
				}) => {
					CheckoutHelper.generateFormAutoSender(link, data)
				})
		}

		__errorRenderer() {
			return (
				<FourOFourPagelet />
			)
		}

		orderRenderer(order, i) {
			return (
				<BoxBit unflex key={i}>
					<ListHeaderLego title="SHIPPING ADDRESS" />
					<AddressPart
						pic={ order.to.pic }
						phone={ order.to.phone }
						address={ order.to.address }
						district={ order.to.district }
						postal={ order.to.postal }
						style={Styles.section}
					/>
					<ListHeaderLego title="ORDER SUMMARY" />
					<BoxBit unflex style={Styles.section}>
						<OrderPart
							order={order}
						/>
						<PricesPart
							price={ this.props.batch.totalAmount }
							discount={ this.props.batch.totalDiscount }
							shipping={ this.props.batch.totalShipping }
							total={ this.props.batch.total }
							style={ Styles.prices }
						/>
					</BoxBit>
				</BoxBit>
			)
		}

		render() {
			return this.state.isGettingData ? super.render(
				<PageBit scroll={false}
					header={<HeaderBit { ...this.header } />}
					contentContainerStyle={ Styles.container }
				>
					<BoxBit centering>
						<LoaderBit />
					</BoxBit>
				</PageBit>
			) : super.render(
				<PageBit
					header={<HeaderBit { ...this.header } />}
					contentContainerStyle={ Styles.container }
				>
					<CardOrderComponent unPressable
						id={ this.props.id }
						data={ this.props.batch }
						style={ Styles.header }
					/>
					{ this.props.batch.status.code === 'UNPAID' && (
						<ButtonBit
							title="PROCEED TO PAYMENT"
							onPress={ this.onProceedToPayment }
							style={Styles.action}
						/>
					) }
					{ this.props.batch.orders.map(this.orderRenderer) }
					<BoxBit unflex style={Styles.footer}>
						<SourceSansBit type={SourceSansBit.TYPES.HEADER_5} style={Styles.footerHeader}>Having problem?</SourceSansBit>
						<SourceSansBit type={SourceSansBit.TYPES.PARAGRAPH_2} style={Styles.footerNote}>Please feel free to contact our customer service department.</SourceSansBit>
						<ButtonBit
							title="Contact"
							width={ButtonBit.TYPES.WIDTHS.FIT}
							size={ButtonBit.TYPES.SIZES.MINI}
							theme={ButtonBit.TYPES.THEMES.INVERTED}
							onPress={ this.onContactCustomerCare }
						/>
					</BoxBit>
				</PageBit>
			)
		}
	}
)
