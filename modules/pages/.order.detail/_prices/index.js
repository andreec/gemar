import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import FormatHelper from 'coeur/helpers/format';

import BoxBit from 'modules/bits/box';
import BoxLoadingBit from 'modules/bits/box.loading';
import SourceSansBit from 'modules/bits/source.sans';
// import LoaderBit from 'modules/bits/loader';

import Styles from './style';


export default ConnectHelper(
	class PricesPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				price: PropTypes.number.isRequired,
				discount: PropTypes.number.isRequired,
				shipping: PropTypes.number.isRequired,
				total: PropTypes.number.isRequired,
				isLoading: PropTypes.bool,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			price: 0,
			discount: 0,
			shipping: 0,
			total: 0,
		}

		render() {
			return (
				<BoxBit unflex row style={this.props.style}>
					<BoxBit>
						<SourceSansBit type={SourceSansBit.TYPES.SUBHEADER_3} weight="medium" style={[Styles.prices, Styles.pale]}>SUBTOTAL</SourceSansBit>
						<SourceSansBit type={SourceSansBit.TYPES.SUBHEADER_3} weight="medium" style={[Styles.prices, Styles.pale]}>DISCOUNT</SourceSansBit>
						<SourceSansBit type={SourceSansBit.TYPES.SUBHEADER_3} weight="medium" style={[Styles.prices, Styles.pale]}>SHIPPING</SourceSansBit>
						<SourceSansBit type={SourceSansBit.TYPES.SUBHEADER_3} weight="medium" style={[Styles.prices, Styles.bold]}>TOTAL</SourceSansBit>
					</BoxBit>
					<BoxBit style={Styles.price}>
						<SourceSansBit type={SourceSansBit.TYPES.PARAGRAPH_3} weight="normal" style={Styles.prices}>IDR { FormatHelper.currencyFormat(this.props.price) }</SourceSansBit>

						{ this.props.isLoading ? [
							(
								<BoxLoadingBit key={0} unflex width={72} height={14} style={Styles.loader} />
							), (
								<BoxLoadingBit key={1} unflex width={72} height={14} style={Styles.loader} />
							),
						] : [
							(
								<SourceSansBit key={0} type={SourceSansBit.TYPES.PARAGRAPH_3} weight="normal" style={Styles.prices}>-IDR { FormatHelper.currencyFormat(this.props.discount) }</SourceSansBit>
							), (
								<SourceSansBit key={1} type={SourceSansBit.TYPES.PARAGRAPH_3} weight="normal" style={Styles.prices}>IDR { FormatHelper.currencyFormat(this.props.shipping) }</SourceSansBit>
							),
						] }
						<SourceSansBit type={SourceSansBit.TYPES.PARAGRAPH_3} weight="medium" style={Styles.prices}>IDR { FormatHelper.currencyFormat(this.props.total) }</SourceSansBit>
					</BoxBit>
				</BoxBit>
			)
		}
	}
)
