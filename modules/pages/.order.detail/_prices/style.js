import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({

	price: {
		alignItems: 'flex-end',
	},

	prices: {
		lineHeight: 24,
	},

	pale: {
		color: Colors.coal.palette(5),
	},

	bold: {
		color: Colors.coal.primary,
	},

	loader: {
		marginTop: 5,
		marginBottom: 5,
		marginRight: 0,
		marginLeft: 0,
	},
})
