import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

import BoxBit from 'modules/bits/box';
import SourceSansBit from 'modules/bits/source.sans';

import Styles from './style';


export default ConnectHelper(
	class AddressPart extends PageModel {

		static propTypes(PropTypes) {
			return {
				pic: PropTypes.string,
				phone: PropTypes.string,
				address: PropTypes.string,
				district: PropTypes.string,
				postal: PropTypes.string,
				style: PropTypes.style,
			}
		}

		render() {
			return super.render(
				<BoxBit unflex style={this.props.style}>
					<SourceSansBit type={SourceSansBit.TYPES.PARAGRAPH_3} style={Styles.color} weight="medium">
						{ this.props.pic }
					</SourceSansBit>
					<SourceSansBit type={SourceSansBit.TYPES.PARAGRAPH_3} style={Styles.color}>
						{ this.props.phone }
					</SourceSansBit>
					<SourceSansBit type={SourceSansBit.TYPES.PARAGRAPH_3} style={Styles.color}>
						{ this.props.address }
					</SourceSansBit>
					<SourceSansBit type={SourceSansBit.TYPES.PARAGRAPH_3} style={Styles.color}>
						{ this.props.district }
					</SourceSansBit>
					<SourceSansBit type={SourceSansBit.TYPES.PARAGRAPH_3} style={Styles.color}>
						{ this.props.postal }
					</SourceSansBit>
				</BoxBit>
			)
		}
	}
)
