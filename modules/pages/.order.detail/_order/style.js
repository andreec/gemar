import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	header: {
		color: Colors.black.palette(4),
		marginBottom: 16,
	},

	product: {
		paddingBottom: 24,
	},

	image: {
		width: 72,
		height: 72,
		marginRight: 16,
	},

	estimation: {
		color: Colors.green.palette(3),
	},
})
