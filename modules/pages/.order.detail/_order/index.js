import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import BatchRecord from 'utils/records/batch';

import FormatHelper from 'coeur/helpers/format';
import StringHelper from 'coeur/helpers/string';
import TimeHelper from 'coeur/helpers/time';

import BoxBit from 'modules/bits/box';
import SourceSansBit from 'modules/bits/source.sans';
import ImageBit from 'modules/bits/image';

import Styles from './style';


export default ConnectHelper(
	class OrderPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				order: PropTypes.object,
				isLoading: PropTypes.bool,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			order: new BatchRecord.orderRecord(),
		}

		constructor(p) {
			super(p, {}, [
				'productRenderer',
			])

			this._imageTransformation = {
				crop: 'fill',
			}
		}

		getProductTitle(detail) {
			switch(detail.type) {
			case 'matchbox':
				return `${detail.product.title} Matchbox`
			default:
				return detail.product.title
			}
		}

		productRenderer(detail, i) {
			return (
				<BoxBit unflex row key={i} style={Styles.product}>
					<ImageBit
						transform={this._imageTransformation}
						source={detail.image}
						style={Styles.image}
					/>
					<BoxBit>
						<SourceSansBit type={SourceSansBit.TYPES.PARAGRAPH_2} weight="medium">{ this.getProductTitle(detail) }</SourceSansBit>
						<SourceSansBit type={SourceSansBit.TYPES.PARAGRAPH_3} weight="normal">IDR { FormatHelper.currencyFormat(detail.price) }</SourceSansBit>
						<BoxBit />
						{ detail.metadata && detail.metadata.shipmentStartDate && detail.metadata.shipmentEndDate ? (
							<SourceSansBit type={SourceSansBit.TYPES.CAPTION_1} style={Styles.estimation}>Est. shipment { TimeHelper.getRange(detail.metadata.shipmentStartDate, detail.metadata.shipmentEndDate) }</SourceSansBit>
						) : false }
					</BoxBit>
				</BoxBit>
			)
		}

		render() {
			return (
				<BoxBit unflex style={this.props.style}>
					<SourceSansBit type={SourceSansBit.TYPES.PARAGRAPH_3} style={Styles.header}>{ StringHelper.pluralize(this.props.order.productCount, 'item', '', 's') }</SourceSansBit>
					{ this.props.order.details.map(this.productRenderer) }
				</BoxBit>
			)
		}
	}
)
