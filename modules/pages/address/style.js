import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	footer: {
		width: Sizes.app.width,
		backgroundColor: Colors.new.white.primary,
	},
	page: {
		paddingBottom: 17,
	},
	button: {
		paddingTop: 11,
		paddingBottom: 11,
		paddingLeft: 17,
		paddingRight: 17,
	},
	empty: {
		marginTop: 15,
	},
})
