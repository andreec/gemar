import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

// import PageManager from 'app/managers/page';
// import MeManager from 'app/managers/me';

import Colors from 'coeur/constants/color';
// import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
// import BoxImageBit from 'modules/bits/box.image';
import ButtonBit from 'modules/bits/button';
import PageBit from 'modules/bits/page';
import HeaderBit from 'modules/bits/header';
import ShadowBit from 'modules/bits/shadow';
import SourceSansBit from 'modules/bits/source.sans';

import HeaderLego from 'modules/legos/header';

import CardAddressComponent from 'modules/components/card.address'

import Styles from './style';

// import {
// 	BoxesPart,
// 	CountdownPart,
// 	PromoPart,
// } from '../matchbox'
// import FooterPart from './_footer';
// import HowPart from './_how';
// import IntroductionPart from './_introduction';


export default ConnectHelper(
	class AddressPage extends PageModel {

		static routeName = 'address'

		static stateToProps(state) {
			return {
				me: state.me,
				addresses: state.address.valueSeq().toJS(),
			}
		}

		constructor(p) {
			super(p, {
			}, [
				'onNavigateToAddressDetail',
				'onAddAddress',
				'addressRenderer',
			]);

			this.__userData = {}
		}

		header = {
			// title: 'CHANGE PASSWORD',
			leftActions: [{
				type: HeaderBit.TYPES.BACK,
			}],
			rightActions: [{
				type: HeaderBit.TYPES.BLANK,
			}],
		}

		onNavigateToAddressDetail(addressId) {
			// TODO
			this.navigator.navigate('address.edit', {
				id: addressId,
			})
		}

		onAddAddress() {
			// TODO
			this.navigator.navigate('address.edit')
		}

		addressRenderer({
			id,
			title,
			pic,
			phone,
			address,
			district,
			postal,
		}, i) {
			console.log('hy', id, this.props.me.defaultAddressId)
			return (
				<CardAddressComponent
					key={i}
					id={id}
					title={title}
					pic={pic}
					phone={phone}
					address={address}
					district={district}
					postal={postal}
					onPress={this.onNavigateToAddressDetail}
					// divider: PropTypes.bool,
					// selectionMode: PropTypes.bool,
					// isActive: PropTypes.bool,
					isDefaultAddress={ this.props.me.defaultAddressId === id }
				/>
			)
		}

		render() {
			console.log(this.props.addresses)
			return super.render(
				<PageBit type={PageBit.TYPES.THICK} header={(
					<HeaderBit { ...this.header } />
				)} footer={(
					<ShadowBit
						y={-1}
						blur={8}
						color={Colors.new.grey.palette(2)}
						style={ Styles.footer }
					>
						<BoxBit unflex style={Styles.button}>
							<ButtonBit
								type={ButtonBit.TYPES.SHAPES.PROGRESS}
								title="ADD ADDRESS"
								onPress={ this.onAddAddress }
							/>
						</BoxBit>
					</ShadowBit>
				)} contentContainerStyle={Styles.page} >
					<HeaderLego
						title="My addresses"
						description="Never forgot the address of your office or friend's house ever again. We'll keep them for you forever."
					/>
					{ this.props.addresses.length ? this.props.addresses.map(this.addressRenderer) : (
						<BoxBit unflex style={Styles.empty}>
							<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_2}>No address found</SourceSansBit>
						</BoxBit>
					) }
					{/* <CardAddressComponent
						id={1}
						title="My home"
						pic="Mbak Warni"
						phone="085959302007"
						address="Jalan Graha Elok II Blox H1 no 8, Margahayu, Bekasi Timur"
						district="Bekasi Timur / Margahayu"
						postal="17113"
						onPress={this.onNavigateToAddressDetail}
					// divider: PropTypes.bool,
					// selectionMode: PropTypes.bool,
					// isActive: PropTypes.bool,
					// isDefaultAddress: PropTypes.bool,
					/> */}
				</PageBit>
			)
		}
	}
)
