import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

import MatchboxManager from 'app/managers/matchbox';

import TrackingHandler from 'app/handlers/tracking';

import FormatHelper from 'coeur/helpers/format';
import TimeHelper from 'coeur/helpers/time';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import SourceSansBit from 'modules/bits/source.sans';
import HeaderBit from 'modules/bits/header';
import LineBit from 'modules/bits/line';
import LoaderBit from 'modules/bits/loader';
import PageImageBit from 'modules/bits/page.image';

import ListContainerLego from 'modules/legos/list.container';

import {
	ColumnPart,
	ColumnCheckmarkPart,
	TitlePart,
} from 'modules/components/card.matchbox';

import FourOFourPagelet from 'modules/pagelets/four.o.four';

import Styles from './style';


export default ConnectHelper(
	class MatchboxDetailPage extends PageModel {

		static routeName = 'matchbox.detail'

		static propTypes(PropTypes) {
			return {
				id: PropTypes.string,
				matchbox: PropTypes.object,
			}
		}

		static stateToProps(state, oP) {
			const matchbox = MatchboxManager.get(oP.id)
				, returned = {
					id: oP.id,
					me: state.me,
				}

			if(!matchbox._isInvalid) {
				returned.matchbox = matchbox
			} else {
				returned.isCrashing = true
				returned.matchbox = matchbox
				returned.crashError = new Error(`No matchbox found with id:${oP.id} supplied`)
			}

			return returned
		}

		static defaultProps = {
			matchbox: {},
		}

		header = {
			leftActions: [{
				type: HeaderBit.TYPES.BACK,
			}],
			background: Colors.transparent,
		}

		footer = [{
			title: 'Terms of Service',
			onPress: this.onNavigateToTerms,
		}, {
			title: 'Return & Exchange Policy',
			onPress: this.onNavigateToFAQ,
		}]

		constructor(p) {
			super(p, {}, [
				'onNavigateToTerms',
				'onNavigateToFAQ',
				'onNavigateToOrderMatchbox',
			]);
		}

		componentDidMount() {
			TrackingHandler.trackPageView(this.routeName)
		}

		// componentWillMount() {
		// 	if(!this.state.timer) {
		// 		MatchboxManager.getTimer(timer => {
		// 			this.setState({
		// 				timer: {
		// 					lastOrderDate: TimeHelper.moment(timer.lastOrderDate),
		// 					shipmentEndDate: TimeHelper.moment(timer.shipmentEndDate),
		// 					shipmentStartDate: TimeHelper.moment(timer.shipmentStartDate),
		// 				},
		// 			})
		// 		})
		// 	}
		// }

		onNavigateToTerms() {
			this.navigator.navigate('webview', {
				title: 'TERMS OF SERVICE',
				url: '/term.html',
			})
		}

		onNavigateToFAQ() {
			this.navigator.navigate('webview', {
				title: 'RETURN POLICY',
				url: '/faq.html#matchbox-order-return-&-exchange',
			})
		}

		onNavigateToOrderMatchbox() {
			TrackingHandler.trackEvent('matchbox-order-intent', this.props.id)
			if(this.props.me.token) {
				this.navigator.navigate(`matchbox/${this.props.id}/order`)
			} else {
				this.navigator.navigate('signup', {
					redirect: `matchbox/${this.props.id}/order`,
				})
			}
		}

		__errorRenderer() {
			return (
				<FourOFourPagelet />
			)
		}

		render() {
			return this.props.matchbox._isGetting ? super.render(
				<PageImageBit
					header={ <HeaderBit { ...this.header } /> }
					// headerStyle={Styles.header}
				>
					<BoxBit centering>
						<LoaderBit />
					</BoxBit>
				</PageImageBit>
			) : super.render(
				<PageImageBit
					header={ <HeaderBit { ...this.header } /> }
					source={ this.props.matchbox.image }
					// headerStyle={Styles.header}
				>
					<BoxBit unflex style={[Styles.content, Styles.top24]}>
						<TitlePart
							type={this.props.matchbox.type}
							title={this.props.matchbox.title}
							isDiscounted={this.props.matchbox.isDiscounted}
							basePrice={this.props.matchbox.basePrice}
							price={this.props.matchbox.price}
						/>
						<LineBit style={Styles.divider} />
					</BoxBit>
					<BoxBit unflex style={[Styles.content, Styles.bottom32]}>
						<SourceSansBit type={SourceSansBit.TYPES.PARAGRAPH_2} style={Styles.description}>
							{ this.props.matchbox.description }
						</SourceSansBit>
						<BoxBit unflex style={Styles.padder} />
					</BoxBit>
					<BoxBit unflex style={[Styles.content, Styles.top24, Styles.bottom32, Styles.emphasize]}>
						<ColumnPart
							data={[{
								title: 'LAST ORDER',
								content: TimeHelper.moment(this.props.matchbox.metadata.lastOrderDate).format('DD MMM \'YY'),
							}, {
								title: 'NEXT SHIPMENT',
								content: this.props.matchbox.metadata.nextShipmentDate,
							}]}
						/>
						<ButtonBit
							title="GET MATCHBOX"
							onPress={ this.onNavigateToOrderMatchbox }
							style={Styles.top16}
						/>
					</BoxBit>
					<BoxBit unflex style={[Styles.content, Styles.top24, Styles.bottom32]}>
						<ColumnPart
							data={[{
								title: 'CONTENT',
								content: `${this.props.matchbox.count} pcs`,
							}, {
								title: 'APPROX. VALUE',
								content: `IDR ${FormatHelper.currencyFormat(this.props.matchbox.value)}`,
							}]}
						/>
						<ColumnCheckmarkPart
							title="INSIDE THE BOX"
							data={ this.props.matchbox.feature }
							column={2}
							note="*May be included occasionally"
							style={Styles.mTop24}
						/>
						<ColumnCheckmarkPart
							title="WARRANTY"
							data={ this.props.matchbox.warranty }
							note="*Terms and conditions apply"
							style={Styles.mTop24}
						/>
					</BoxBit>
					<BoxBit unflex style={Styles.footer}>
						<ListContainerLego data={this.footer} />
					</BoxBit>
				</PageImageBit>
			)
		}
	}
)
