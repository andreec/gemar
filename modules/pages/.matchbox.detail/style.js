import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({

	// header: {
	// 	backgroundColor: Colors.coal.primary,
	// },

	content: {
		paddingTop: 16,
		paddingBottom: 16,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
	},

	divider: {
		borderBottomWidth: 2,
		borderBottomColor: Colors.red.primary,
		width: Sizes.margin.thick,
	},

	description: {
		color: Colors.black.palette(4),
	},

	emphasize: {
		backgroundColor: Colors.pink.palette(1),
	},

	footer: {
		backgroundColor: Colors.solid.grey.palette(1),
		paddingBottom: 24,
	},

	top16: {
		marginTop: 16,
	},

	mTop24: {
		marginTop: 24,
	},

	top24: {
		paddingTop: 24,
	},

	bottom32: {
		paddingBottom: 32,
	},
})
