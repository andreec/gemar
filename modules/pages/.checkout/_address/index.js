import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import CardComponentModel from 'coeur/models/components/card';

import AddressManager from 'app/managers/address';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import IconBit from 'modules/bits/icon';
import SourceSansBit from 'modules/bits/source.sans';
import LoaderBit from 'modules/bits/loader';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style';


export default ConnectHelper(
	class AddressPart extends CardComponentModel(AddressManager) {

		static propTypes(PropTypes) {
			return {
				...super.propTypes(PropTypes),
				id: PropTypes.id,
			}
		}

		shouldComponentUpdate(nP, nS) {
			return this.shallowCompare(nP, nS)
		}

		render() {
			return (
				<TouchableBit unflex style={Styles.container} onPress={this.onPress}>
					{ this.props.data._isGetting ? (
						<BoxBit style={Styles.loader}>
							<LoaderBit />
						</BoxBit>
					) : this.props.id && (
						<BoxBit unflex row>
							<BoxBit>
								<SourceSansBit type={SourceSansBit.TYPES.SECONDARY_3} weight="medium" style={Styles.colorBlack}>{ this.props.data.title || this.props.data.pic }</SourceSansBit>
								<SourceSansBit type={SourceSansBit.TYPES.SECONDARY_3} weight="normal" style={Styles.colorBlack}>{ this.props.data.phone }</SourceSansBit>
								<SourceSansBit type={SourceSansBit.TYPES.SECONDARY_3} weight="normal" style={Styles.colorBlack}>{ this.props.data.address }</SourceSansBit>
							</BoxBit>
							<IconBit
								name="arrow-right"
								color={ Colors.coal.palette(4) }
							/>
						</BoxBit>
					) || (
						<BoxBit unflex row>
							<BoxBit style={Styles.text}>
								<SourceSansBit type={SourceSansBit.TYPES.SECONDARY_3} weight="medium" style={Styles.colorRed}>Select a shipping address</SourceSansBit>
							</BoxBit>
							<IconBit
								name="arrow-right"
								color={ Colors.coal.palette(4) }
							/>
						</BoxBit>
					) }
				</TouchableBit>
			)
		}
	}
)
