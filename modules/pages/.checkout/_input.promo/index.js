import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import IconBit from 'modules/bits/icon';
import LoaderBit from 'modules/bits/loader';
import TextInputBit from 'modules/bits/text.input';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style';


export default ConnectHelper(
	class InputPromoPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				onSubmit: PropTypes.func,
				isLoading: PropTypes.bool,
				isValid: PropTypes.bool,
				value: PropTypes.string,
			}
		}

		constructor(p) {
			super(p, {
			}, [
				'onChange',
				'onSubmit',
			])

			this._data = undefined
		}

		onChange(e, val) {
			this._data = val
		}

		onSubmit() {
			this.props.onSubmit &&
			this.props.onSubmit(this._data)
		}

		render() {
			return (
				<BoxBit unflex row style={Styles.container} onPress={this.onPress}>
					<BoxBit>
						<TextInputBit
							key={ this.props.value }
							defaultValue={ this.props.value }
							placeholder="Add promo code"
							onChange={ this.onChange }
							onSubmitEditing={ this.onSubmit }
							onBlur={ this.onSubmit }
							style={Styles.input}
						/>
					</BoxBit>
					{ this.props.isLoading ? (
						<BoxBit unflex centering>
							<LoaderBit style={Styles.loader} size={16} color={ Colors.coal.palette(3) } />
						</BoxBit>
					) : this.props.isValid && (
						<BoxBit unflex centering>
							<IconBit
								name="checkmark"
								color={ Colors.green.palette(3) }
							/>
						</BoxBit>
					) || (
						<TouchableBit unflex onPress={ this.onSubmit }>
							<IconBit
								name="expand"
								color={ Colors.coal.palette(4) }
							/>
						</TouchableBit>
					) }
				</BoxBit>
			)
		}
	}
)
