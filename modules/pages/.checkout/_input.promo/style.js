import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({

	container: {
		paddingTop: 16,
		paddingBottom: 16,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
		backgroundColor: Colors.white.primary,
		borderWidth: 0,
		borderStyle: 'solid',
		borderColor: Colors.coal.palette(2),
		borderTopWidth: StyleSheet.hairlineWidth,
		borderBottomWidth: StyleSheet.hairlineWidth,
	},

	input: {
		border: 0,
		height: 24,
	},

	loader: {
		height: 24,
	},

	colorBlack: {
		color: Colors.black.palette(4),
		marginRight: 16,
	},

	colorRed: {
		color: Colors.red.primary,
		marginRight: 16,
	},
})
