import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

import BatchManager from 'app/managers/batch';
import MeManager from 'app/managers/me';
import VolatileManager from 'app/managers/volatile';

import TrackingHandler from 'app/handlers/tracking';

import CheckoutHelper from 'utils/helpers/checkout';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import SourceSansBit from 'modules/bits/source.sans';
import HeaderBit from 'modules/bits/header';
import LoaderBit from 'modules/bits/loader';
import PageBit from 'modules/bits/page';

import ListHeaderLego from 'modules/legos/list.header';

import AddressPagelet from 'modules/pagelets/address';

import {
	OrderPart,
	PricesPart,
} from '../order.detail';
import AddressPart from './_address';
import InputPromoPart from './_input.promo';

import Styles from './style';


export default ConnectHelper(
	class CheckoutPage extends PageModel {

		static routeName = 'checkout'

		static propTypes(PropTypes) {
			return {
				me: PropTypes.object,
				cart: PropTypes.array,
			}
		}

		static stateToProps(state) {
			return {
				me: state.me,
				cart: state.me.cartItems,
			}
		}

		header = {
			title: 'CHECKOUT',
			leftActions: [{
				type: HeaderBit.TYPES.CLOSE,
			}],
			rightActions: [{
				type: HeaderBit.TYPES.BLANK,
			}],
		}

		constructor(p) {
			super(p, {
				selectedAddressId: p.me.defaultAddressId,
				promoCode: undefined,
				isLoading: true,
				isEmpty: !p.cart.length,
				isSending: false,
				isUpdating: false,
				isCouponValid: false,
				isClearing: false,

				mockBatch: new BatchManager.record(),
			}, [
				'canSend',
				'onSelectAddress',
				'onUpdateSelectedAddress',
				'onModalRequestClose',
				'onProceedToPayment',
				'onValidateCoupon',
				'onRedirectToPayment',
				'orderRenderer',
			])
		}

		componentWillMount() {
			if(VolatileManager.isExpired('me')) {
				MeManager.authenticate(this.props.me.token).then(() => {
					VolatileManager.mark('me')
				}).catch(() => {
					this.utilities.notification.show({
						title: 'Oops…',
						message: 'Looks like you\'ve been logged out of your session. Try logging in.',
					})

					this.navigator.top()

					MeManager.logout()
				})
			}

			if(!this.state.isEmpty) {
				this.getOrderDetail()
			}
		}

		componentDidMount() {
			TrackingHandler.trackPageView(this.routeName)
		}

		shouldComponentUpdate(nP, nS) {
			if(nS.isClearing) return false

			return super.shouldComponentUpdate(nP, nS)
		}

		canSend() {
			return !this.state.isSending && this.state.selectedAddressId && this.state.selectedAddressId !== -1
		}

		getOrderDetail(validatingCoupon) {
			this.setState({
				isUpdating: true,
			}, () => {
				BatchManager.getMockFromCart({
					addressId: this.state.selectedAddressId,
					products: this.props.cart,
					couponCode: this.state.promoCode,
				}).then(({
					coupon,
					orders,
					discount,
					additionalDiscount,
				}) => {
					const additionalState = {}

					if(validatingCoupon) {
						if(coupon && !coupon.error) {
							if(discount === 0) {
								this.utilities.notification.show({
									title: 'Oops…',
									message: 'Your coupon is valid but not applicable for current item(s).',
									timeout: 3000,
								})

								additionalState.promoCode = ''
							} else {
								this.utilities.notification.show({
									title: 'Yeay!',
									message: 'Coupon successfully applied',
									type: this.utilities.notification.TYPES.SUCCESS,
									timeout: 3000,
								})
							}
						} else {
							this.utilities.notification.show({
								title: 'Oops…',
								message: (coupon.error.detail && `${coupon.error.message}. ${coupon.error.detail}`) || coupon.error.message,
								timeout: 3000,
							})

							additionalState.promoCode = ''
						}
					}

					this.setState({
						isLoading: false,
						isUpdating: false,
						isCouponValid: coupon && !coupon.error && discount > 0,
						mockBatch: BatchManager.getRecordFromConfig({
							orders,
							discount,
							additionalDiscount,
						}),
						...additionalState,
					})
				}).catch(err => {
					this.warn(err)

					this.utilities.notification.show({
						title: 'Oops…',
						message: 'There\'s something wrong.',
					})

					this.setState({
						isLoading: false,
						isUpdating: false,
					})
				})
			})
		}

		onSelectAddress() {
			// TODO: update order shipping fee
			this.utilities.alert.modal({
				component: (
					<AddressPagelet
						selectionMode
						selectedAddressId={ this.state.selectedAddressId }
						onRequestClose={ this.onModalRequestClose }
						onSubmit={ this.onUpdateSelectedAddress }
					/>
				),
			})
		}

		onModalRequestClose() {
			this.utilities.alert.hide()
		}

		onUpdateSelectedAddress(aId) {
			this.setState({
				selectedAddressId: aId,
			}, () => {
				this.onModalRequestClose()
				this.getOrderDetail()
			})
		}

		onValidateCoupon(couponCode) {
			this.setState({
				promoCode: couponCode,
			}, () => {
				this.getOrderDetail(true)
			})
		}

		onProceedToPayment() {
			TrackingHandler.trackEvent('matchbox-checkout', this.state.mockBatch.total)

			this.utilities.alert.show({
				title: 'Proceed to Payment',
				message: 'You will be redirected to third-party secure payment page.',
				actions: [{
					title: 'OK',
					type: 'OK',
					onPress: this.onRedirectToPayment,
				}, {
					title: 'Don\'t Allow',
					type: 'CANCEL',
				}],
			})
		}

		onRedirectToPayment() {
			this.setState({
				isSending: true,
			}, () => {
				BatchManager.create({
					addressId: this.state.selectedAddressId,
					products: this.props.cart,
					couponCode: this.state.promoCode,
				}).then(batch => {
					// TODO: REDIRECT

					BatchManager
						.getCheckoutLink(batch.id)
						.then(({
							link,
							data,
						}) => {
							CheckoutHelper.generateFormAutoSender(link, data)
						})

					// clear cart
					MeManager.clearCart()
				}).catch(err => {
					this.warn(err)

					this.utilities.notification.show({
						title: 'Oops…',
						message: 'There\'s something wrong. Please try again later.',
					})

					this.setState({
						isSending: false,
					})
				})
			})
		}

		loadingRenderer() {
			return this.state.isLoading && !this.state.isEmpty && (
				<BoxBit centering>
					<LoaderBit color={Colors.coal.palette(3)} />
					<SourceSansBit type={SourceSansBit.TYPES.CAPTION_2} style={Styles.loader}>
						Fetching your order detail...
					</SourceSansBit>
				</BoxBit>
			)
		}

		emptyRenderer() {
			return this.state.isEmpty && (
				<BoxBit style={Styles.empty}>
					<SourceSansBit type={SourceSansBit.TYPES.HEADER_4} style={Styles.padder}>
						You currently have no items in your cart.
					</SourceSansBit>
					<SourceSansBit type={SourceSansBit.TYPES.PARAGRAPH_2}>
						Try to order some more items
					</SourceSansBit>
				</BoxBit>
			)
		}

		orderRenderer(order, i) {
			return (
				<OrderPart
					key={i}
					order={order}
					isLoading={ this.state.isUpdating }
				/>
			)
		}

		render() {
			return super.render(
				<PageBit
					header={<HeaderBit { ...this.header } />}
					contentContainerStyle={ Styles.container }
				>
					{ this.loadingRenderer() }
					{ this.emptyRenderer() }
					{ !this.state.isLoading && !this.state.isEmpty && (
						<BoxBit unflex accessible={ !this.state.isSending }>
							<ListHeaderLego title="SHIPPING ADDRESS" />
							<AddressPart
								id={ this.state.selectedAddressId }
								onPress={ this.onSelectAddress }
							/>
							<ListHeaderLego title="OPTIONS" />
							<InputPromoPart
								value={ this.state.promoCode }
								onSubmit={ this.onValidateCoupon }
								isLoading={ this.state.isUpdating }
								isValid={ this.state.isCouponValid }
							/>
							<ListHeaderLego title="ORDER SUMMARY" />
							<BoxBit unflex style={ Styles.summary }>
								{ this.state.mockBatch.orders.map(this.orderRenderer) }
								<PricesPart
									price={ this.state.mockBatch.totalAmount }
									discount={ this.state.mockBatch.totalDiscount }
									shipping={ this.state.mockBatch.totalShipping }
									total={ this.state.mockBatch.total }
									isLoading={ this.state.isUpdating }
									style={ Styles.prices }
								/>
							</BoxBit>
							<ButtonBit
								title="PROCEED TO PAYMENT"
								type={ ButtonBit.TYPES.SHAPES.PROGRESS }
								state={ this.canSend() ? ButtonBit.TYPES.STATES.NORMAL : this.state.isSending && ButtonBit.TYPES.STATES.LOADING || ButtonBit.TYPES.STATES.DISABLED }
								onPress={ this.onProceedToPayment }
								style={Styles.button}
							/>
						</BoxBit>
					) }
				</PageBit>
			)
		}
	}
)
