import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		backgroundColor: Colors.solid.grey.palette(1),
		paddingTop: 0,
		paddingRight: 0,
		paddingLeft: 0,
		paddingBottom: 64,
	},

	empty: {
		paddingTop: 24,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
		paddingBottom: 0,
	},

	padder: {
		marginBottom: 8,
	},

	loader: {
		marginTop: 24,
		color: Colors.coal.palette(3),
	},

	prices: {
		paddingTop: 16,
		borderWidth: 0,
		borderStyle: 'solid',
		borderTopWidth: StyleSheet.hairlineWidth,
		borderTopColor: Colors.coal.palette(2),
	},

	summary: {
		paddingTop: 16,
		paddingBottom: 16,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
		backgroundColor: Colors.white.primary,
		borderColor: Colors.coal.palette(2),
		borderWidth: 0,
		borderStyle: 'solid',
		borderTopWidth: StyleSheet.hairlineWidth,
		borderBottomWidth: StyleSheet.hairlineWidth,
	},

	button: {
		marginTop: 24,
		marginLeft: Sizes.margin.thick,
		marginRight: Sizes.margin.thick,
		marginBottom: 0,
	},
})
