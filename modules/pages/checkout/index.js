import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';
import TimeHelper from 'coeur/helpers/time';

// import PageManager from 'app/managers/page';

import TrackingHandler from 'app/handlers/tracking';

// import FormatHelper from 'coeur/helpers/format';
// import Colors from 'coeur/constants/color';
// import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
import SourceSansBit from 'modules/bits/source.sans';
import PageBit from 'modules/bits/page';
import IconBit from 'modules/bits/icon';
import HeaderBit from 'modules/bits/header';
import LoaderBit from 'modules/bits/loader';
import TouchableBit from 'modules/bits/touchable';
// import ButtonBit from 'modules/bits/button';
// import ImageBit from 'modules/bits/image';
// import GalleryBit from 'modules/bits/gallery';
// import ShadowBit from 'modules/bits/shadow';
// import TextBit from 'modules/bits/text';

import NotifyPart from './_notify'

import Styles from './style'

import {
	shuffle,
	take,
} from 'lodash'

// import {
// 	BoxesPart,
// 	CountdownPart,
// 	PromoPart,
// } from '../matchbox'
// import FooterPart from './_footer';
// import HowPart from './_how';
// import IntroductionPart from './_introduction';


export default ConnectHelper(
	class CheckoutPage extends PageModel {

		static routeName = 'checkout'

		header = {
			title: 'CHECKOUT',
			leftActions: [{
				type: HeaderBit.TYPES.BACK,
			}],
			rightActions: [{
				type: HeaderBit.TYPES.BLANK,
			}],
		}

		constructor(p) {
			super(p, {
				isLoading: true,
				notifications: [],
			}, [
			]);
		}

		componentWillMount() {
			// TODO: get data from server
			this.setTimeout(() => {
				Promise.resolve().then(res => {
					const mockImages = [
						'https://res.cloudinary.com/hiandree/image/upload/v1539438197/dummies/dummy-user1-img3.jpg',
						'https://res.cloudinary.com/hiandree/image/upload/v1539438197/dummies/dummy-user1-img2.jpg',
						'https://res.cloudinary.com/hiandree/image/upload/v1539438197/dummies/dummy-user1-img1.jpg',
						'https://res.cloudinary.com/hiandree/image/upload/v1539438204/dummies/dummy-user2-img1.jpg',
						'https://res.cloudinary.com/hiandree/image/upload/v1539438204/dummies/dummy-user2-img2.jpg',
						'https://res.cloudinary.com/hiandree/image/upload/v1539438204/dummies/dummy-user2-img3.jpg',
						'https://res.cloudinary.com/hiandree/image/upload/v1539438205/dummies/dummy-user3-img1.jpg',
					]

					const mockUsers = [{
						id: 1,
						name: 'Anthony Diorgo',
						image: 'https://res.cloudinary.com/hiandree/image/upload/v1539438198/dummies/dummy-user1.jpg',
					}, {
						id: 2,
						name: 'Herman Pradipta',
						image: 'https://res.cloudinary.com/hiandree/image/upload/v1539438201/dummies/dummy-user2.jpg',
					}, {
						id: 3,
						name: 'Andrea Yukana',
						image: 'https://res.cloudinary.com/hiandree/image/upload/v1539438201/dummies/dummy-user3.jpg',
					}]

					const generateRandom = (num = 7, total = []) => {
						if (num === 0) {
							return total
						}

						return generateRandom(num - 1, [
							...total,
							{
								type: Object.values(NotifyPart.TYPES)[Math.floor(Math.random() * 7)],
								timestamp: +TimeHelper.moment().subtract(Math.random() * 100000, 'm').toDate(),
								postId: Math.floor(Math.random() * 5),
								postImage: mockImages[Math.floor(Math.random() * 7)],
								users: take(shuffle(mockUsers), Math.floor(Math.random() * 2) + 1),
							},
						])
					}

					this.setState({
						isLoading: false,
						notifications: generateRandom(Math.round(Math.random() * 10)),
					})
				})
			}, 2000)
		}

		notificationRenderer(config, i) {
			return (
				<NotifyPart key={i} { ...config } />
			)
		}

		headerRenderer(index, title, onPress, isActive) {
			return (
				<BoxBit unflex style={[Styles.header, isActive && Styles.headerActive]}>
					<TouchableBit accessible={isActive} row onPress={ onPress }>
						<BoxBit unflex style={Styles.index}>
							<SourceSansBit type={SourceSansBit.TYPES.NEW_HEADER_2}>
								{ index }
							</SourceSansBit>
						</BoxBit>
						<BoxBit>
							<SourceSansBit type={SourceSansBit.TYPES.NEW_HEADER_2}>
								{ title }
							</SourceSansBit>
						</BoxBit>
						<IconBit
							name="arrow-right"
						/>
					</TouchableBit>
				</BoxBit>
			)
		}

		render() {
			return super.render(
				<PageBit header={ <HeaderBit { ...this.header } /> } contentContainerStyle={Styles.container}>
					{ this.state.isLoading ? (
						<BoxBit centering>
							<LoaderBit />
						</BoxBit>
					) : (
						<BoxBit>
							{ this.state.notifications.map(this.notificationRenderer)}
						</BoxBit>
					) }
				</PageBit>
			)
		}
	}
)
