import StyleSheet from 'coeur/libs/style.sheet'

// import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	container: {
		paddingTop: 24,
		paddingLeft: 15,
		paddingRight: 15,
		justifyContent: 'center',
	},

	header: {
		paddingLeft: 15,
		paddingRight: 15,
		paddingTop: 7,
		paddingBottom: 7,
		opacity: .5,
	},

	headerActive: {
		opacity: 1,
	},

	index: {
		borderWidth: 2,
		width: 24,
		height: 24,
		marginRight: 15,
	},

})
