import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	container: {
		marginBottom: 24,
		justifyContent: 'center',
	},
	images: {
		width: 41,
		height: 41,
		marginRight: 8,
	},
	image: {
		width: 41,
		height: 41,
		borderRadius: 21,
		overflow: 'hidden',
	},
	imageSmall1: {
		alignSelf: 'flex-start',
		width: 30,
		height: 30,
		borderRadius: 15,
		borderWidth: 2,
		borderColor: Colors.new.white.palette(1),
		overflow: 'hidden',
	},
	imageSmall2: {
		alignSelf: 'flex-end',
		width: 30,
		height: 30,
		borderRadius: 15,
		borderWidth: 2,
		borderColor: Colors.new.white.palette(1),
		overflow: 'hidden',
		marginBottom: -19,
	},
	postImage: {
		marginLeft: 8,
		width: 45,
		height: 45,
	},
	time: {
		color: Colors.new.black.palette(4),
	},
	title: {
		justifyContent: 'center',
	},
	buttons: {
		alignItems: 'center',
		marginLeft: 8,
		paddingTop: 5,
	},
	button: {
		marginRight: 8,
	},
})
