import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	container: {
		paddingLeft: 15,
		paddingRight: 15,
	},

	padder: {
		height: 28,
	},

	footer: {
		width: Sizes.app.width,
		backgroundColor: Colors.white.primary,
		paddingTop: 11,
		paddingBottom: 9,
		paddingRight: 15,
		paddingLeft: 15,
		justifyContent: 'space-between',
	},

	footerText: {
		marginRight: 40,
		justifyContent: 'center',
		flexGrow: 1.5,
	},

	price: {
		marginTop: 4,
		color: Colors.new.yellow.palette(3),
	},
})
