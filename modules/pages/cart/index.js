import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

// import PageManager from 'app/managers/page';

import TrackingHandler from 'app/handlers/tracking';

import FormatHelper from 'coeur/helpers/format';

import Colors from 'coeur/constants/color';
import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import ImageBit from 'modules/bits/image';
import SourceSansBit from 'modules/bits/source.sans';
import PageBit from 'modules/bits/page';
// import IconBit from 'modules/bits/icon';
// import GalleryBit from 'modules/bits/gallery';
import HeaderBit from 'modules/bits/header';
import ShadowBit from 'modules/bits/shadow';
// import TouchableBit from 'modules/bits/touchable';
// import TextBit from 'modules/bits/text';

import CardCartComponent from 'modules/components/card.cart';

import Styles from './style'

// import {
// 	BoxesPart,
// 	CountdownPart,
// 	PromoPart,
// } from '../matchbox'
// import FooterPart from './_footer';
// import HowPart from './_how';
// import IntroductionPart from './_introduction';


export default ConnectHelper(
	class CartPage extends PageModel {

		static routeName = 'cart'

		header = {
			title: 'My Cart',
			// leftActions: [{
			// 	type: HeaderBit.TYPES.BACK,
			// }],
			// rightActions: [{
			// 	type: HeaderBit.TYPES.BLANK,
			// }],
		}
		
		static stateToProps(state) {
			return {
				cartItems: state.me.cartItems,
				total: state.me.cartItems.reduce((sum, cartItem) => {
					const post = state.posts.get(cartItem.id)
						, product = state.products.get(post.refId) || { price: 0 }
					return sum + (product.price * cartItem.quantity)
				}, 0),
			}
		}
		
		constructor(p) {
			super(p, {
			}, []);
		}
		
		cartCardRenderer(cartItem) {
			return (
				<CardCartComponent key={cartItem.id} id={cartItem.id} />
			)
		}
		onNavigateToCheckout = () => {
			this.navigator.navigate('checkout')
		}
		
		footerRenderer() {
			return this.props.cartItems.length ? (
				<ShadowBit x={0} y={-1} blur={8} color={Colors.new.grey.palette(2)}>
					<BoxBit unflex row style={Styles.footer}>
						<BoxBit style={Styles.footerText}>
							<SourceSansBit
								type={SourceSansBit.TYPES.NEW_SUBHEADER_1_ALT_2}
								weight="normal"
							>
								Cart Amount
							</SourceSansBit>
							<SourceSansBit
								type={SourceSansBit.TYPES.NEW_HEADER_2}
								weight="semibold"
								style={Styles.price}
							>
								Rp { FormatHelper.currencyFormat(this.props.total) }
							</SourceSansBit>
						</BoxBit>
						<ButtonBit
							title="CHECKOUT"
							type={ButtonBit.TYPES.SHAPES.PROGRESS}
							width={ ButtonBit.TYPES.WIDTHS.FORCE_BLOCK }
							onPress={this.onNavigateToCheckout}
						/>
					</BoxBit>
				</ShadowBit>
			) : false
		}

		render() {
			return super.render(
				<PageBit
					header={ <HeaderBit { ...this.header } /> }
					footer={ this.footerRenderer() }
					contentContainerStyle={Styles.container}
				>
					<BoxBit unflex style={Styles.padder} />
					{ this.props.cartItems.length ? this.props.cartItems.map(this.cartCardRenderer) : (
						<BoxBit centering>
							<SourceSansBit
								type={SourceSansBit.TYPES.NEW_SUBHEADER_1_ALT_2}
								weight="normal"
							>
								Your cart is empty
							</SourceSansBit>
						</BoxBit>
					)}
					{/* <CardCartComponent
						seller={ '@test.test' }
						image={ 'https://www.pianetaoutlet.it/82174-large_default/adidas-swift-run-j-sport-shoes-ac8443.jpg' }
						title={ 'Adidas Sport J GT -32' }
						price={ 499000 }
						quantity={ 1 }
					/> */}
					
					{/* { this.props.cartItems.map(cartItem => {
						return (
							<CardCartComponent
								seller={ '@test.test' }
								image: PropTypes.string,
								title={ product.title }
								price={ product.price }
								// note
								quantity={ 1 }
							/>
						)
					}) }
				*/}
				</PageBit>
			)
		}
	}
)
