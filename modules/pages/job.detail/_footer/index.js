import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import ConnectedStatefulModel from 'coeur/models/stateful';
import TouchableBit from 'modules/bits/touchable';
import UtilitiesContext from 'coeur/contexts/utilities';
import PageContext from 'coeur/contexts/page';


import BoxBit from 'modules/bits/box';
// import ImageBit from 'modules/bits/image';
import ButtonBit from 'modules/bits/button';
import SourceSansBit from 'modules/bits/source.sans';

import Styles from './style'

export default ConnectHelper(
	class FooterPart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
			}
		}

		static contexts = [
			PageContext,
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
			})

			this._action = [{
				title: 'DECLINE',
			}, {
				title: 'ACCEPT',
			}]
		}

		onPress = (title) => {
			this.props.utilities.alert.show({
				title,
				message: 'Are you sure want to continue?',
				actions: [{
					title,
					type: 'OK',
					onPress: () => {},
				}, {
					title: 'Cancel',
					type: 'CANCEL',
				}],
			})
		}

		actionRenderer = ({
			title,
		}, i) => {
			return (
				<TouchableBit
					key={i}
					centering
					style={[Styles.button, i === 0 && Styles.decline]}
					onPress={ () => this.onPress(title) }
				>
					<SourceSansBit type={SourceSansBit.TYPES.NEW_PARAGRAPH_3} weight="semibold">{ title }</SourceSansBit>
				</TouchableBit>
			)
		}
		render() {
			return (
				<BoxBit row style={Styles.container}>
					{ this._action.map(this.actionRenderer)}
				</BoxBit>
			)
		}
	}
)
