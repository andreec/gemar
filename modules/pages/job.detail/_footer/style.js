import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	container: {
		paddingTop: 0,
		paddingBottom: 0,
		paddingLeft: 0,
		paddingRight: 0,
		backgroundColor: Colors.new.black.palette(3),
	},

	header: {
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderBottomColor: Colors.new.black.palette(3),
	},

	jobId: {
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderTopWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.new.black.palette(3),
	},

	button: {
		paddingTop: Sizes.margin.thick,
		paddingBottom: Sizes.margin.thick,
		backgroundColor: Colors.primary,
	},

	decline: {
		backgroundColor: Colors.white.primary,
	},
})
