import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';
import TimeHelper from 'coeur/helpers/time';
import FormatHelper from 'coeur/helpers/format';
// import PageManager from 'app/managers/page';

// import TrackingHandler from 'app/handlers/tracking';

// import Animated from 'coeur/libs/animated';

import BoxBit from 'modules/bits/box';
// import ImageBit from 'modules/bits/image';
import LoaderBit from 'modules/bits/loader';
import ButtonBit from 'modules/bits/button';
import SourceSansBit from 'modules/bits/source.sans';

import FormHeaderLego from 'modules/legos/form.header';

import Styles from './style'

export default ConnectHelper(
	class BodyPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				minSalary: PropTypes.string,
				maxSalary: PropTypes.string,
				company: PropTypes.string,
				position: PropTypes.string,
				availability: PropTypes.number,
				date: PropTypes.string,

				content: PropTypes.string,

				style: PropTypes.style,
			}
		}

		static defaultProps = {
			minSalary: 4000000,
			maxSalary: 8000000,
		}

		constructor(p) {
			super(p, {
			});

			this.data = [{
				title: 'Applying To',
				answer: this.props.applyTo || 'HR Manager',
			}, {
				title: 'Company Name',
				answer: this.props.company || 'PT. Interior Superior',
			}, {
				title: 'Position',
				answer: this.props.position || 'Senior Interior Designer',
			}, {
				title: 'Application Date',
				answer: this.props.date && TimeHelper.format(this.props.date),
			}, {
				title: 'Offered Sallary',
				answer: `IDR ${FormatHelper.currencyFormat(this.props.minSalary)} - IDR ${FormatHelper.currencyFormat(this.props.maxSalary)}`,
			}]
		}

		contentRenderer({
			title,
			answer,
		}, i) {
			return (
				<BoxBit key={i} unflex>
					<SourceSansBit type={SourceSansBit.TYPES.NEW_PARAGRAPH_3}>{ title }:</SourceSansBit>
					<SourceSansBit type={SourceSansBit.TYPES.NEW_PARAGRAPH_3} weight="semibold">{ answer || '-'}</SourceSansBit>
				</BoxBit>
			)
		}

		headerRenderer() {
			return (
				<FormHeaderLego
					data={this.data}
					style={Styles.header}
				/>
			)
		}

		bodyRenderer() {
			return (
				<BoxBit style={Styles.body}>
					<SourceSansBit type={SourceSansBit.TYPES.NEW_PARAGRAPH_3}>Application letter:</SourceSansBit>
					<SourceSansBit type={SourceSansBit.TYPES.NEW_PARAGRAPH_3}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla efficitur, tortor et hendrerit aliquet, nisl tellus fermentum lorem, a ullamcorper augue est eu tellus. Proin dictum libero libero, vel commodo orci maximus id. Nam vehicula, lorem ultrices varius placerat, nunc eros lobortis ex, in semper nisi augue eget ipsum. Duis eu tellus at sapien semper malesuada nec ut lacus. Vestibulum rhoncus congue turpis in ultricies. Mauris non justo aliquet, lobortis eros non, venenatis dui. In tristique justo massa, eget ullamcorper leo molestie a. Fusce ultricies mattis mi, feugiat lacinia urna bibendum quis. Sed accumsan sed magna sit amet iaculis. Integer aliquam elit eu libero ultrices eleifend. Curabitur a convallis neque, vitae molestie tortor. Integer posuere, felis ac dictum ornare, arcu augue accumsan lectus, in iaculis erat ligula ac nisl.</SourceSansBit>
				</BoxBit>
			)
		}

		footerRenderer() {
			return (
				<BoxBit row style={Styles.footer}>
					<BoxBit>
						<SourceSansBit type={SourceSansBit.TYPES.NEW_PARAGRAPH_3}>Attached CV or Portfolio</SourceSansBit>
					</BoxBit>
					<ButtonBit
						title="Download"
						state={ButtonBit.TYPES.STATES.NORMAL}
						width={ButtonBit.TYPES.WIDTHS.FIT}
					/>
				</BoxBit>
			)
		}

		render() {
			return (
				<BoxBit style={[Styles.container, this.props.style]}>
					{ this.headerRenderer() }
					{ this.bodyRenderer() }
					{ this.footerRenderer() }
				</BoxBit>
			)
		}
	}
)
