import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';
import TimeHelper from 'coeur/helpers/time';
// import PageManager from 'app/managers/page';

// import TrackingHandler from 'app/handlers/tracking';

// import Animated from 'coeur/libs/animated';

import BoxBit from 'modules/bits/box';
// import ImageBit from 'modules/bits/image';
import LoaderBit from 'modules/bits/loader';
import SourceSansBit from 'modules/bits/source.sans';

import Styles from './style'

export default ConnectHelper(
	class HeaderPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				status: PropTypes.string,
				date: PropTypes.string,
			}
		}

		static defaultProps = {
			status: 'UNREAD',
		}

		constructor(p) {
			super(p, {
			});
		}

		render() {
			return (
				<BoxBit unflex style={Styles.container}>
					<SourceSansBit type={SourceSansBit.TYPES.NEW_PARAGRAPH_3} style={Styles.darkGrey}>Status :</SourceSansBit>
					<SourceSansBit type={SourceSansBit.TYPES.NEW_HEADER_1} weight="semibold" style={Styles.darkGrey60}>{ this.props.status }</SourceSansBit>
					<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_1} style={Styles.darkGrey60}>{TimeHelper.moment(this.props.date).format('MMMM DD, YYYY') }</SourceSansBit>
				</BoxBit>
			)
		}
	}
)
