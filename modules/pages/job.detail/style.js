import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	header: {
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderBottomColor: Colors.new.black.palette(3),
	},

	jobId: {
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderTopWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.new.black.palette(3),
		backgroundColor: Colors.white.primary,
		paddingTop: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
	},

	content: {
		backgroundColor: Colors.new.grey.palette(1),
	},

	darkGrey: {
		color: Colors.new.black.palette(3),
	},
})
