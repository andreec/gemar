import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

// import PageManager from 'app/managers/page';
import JobManager from 'app/managers/job';

// import FormatHelper from 'coeur/helpers/format';

// import Colors from 'coeur/constants/color';
// import Sizes from 'coeur/constants/size';

// import BoxImageBit from 'modules/bits/box.image';
// import ButtonBit from 'modules/bits/button';
// import ImageBit from 'modules/bits/image';
// import SourceSansBit from 'modules/bits/source.sans';
// import IconBit from 'modules/bits/icon';
// import GalleryBit from 'modules/bits/gallery';
import PageBit from 'modules/bits/page';
import BoxBit from 'modules/bits/box';
import HeaderBit from 'modules/bits/header';
// import TouchableBit from 'modules/bits/touchable';
// import TextBit from 'modules/bits/text';
// import HeaderLego from 'modules/legos/header';

import ListHeaderLego from 'modules/legos/list.header';

import ContentPart from './_content'
import HeaderPart from './_header';
import BodyPart from './_body';
import FooterPart from './_footer';

import Styles from './style';

// import {
// 	BoxesPart,
// 	CountdownPart,
// 	PromoPart,
// } from '../matchbox'
// import FooterPart from './_footer';
// import HowPart from './_how';
// import IntroductionPart from './_introduction';


export default ConnectHelper(
	class JobApplicationDetailPage extends PageModel {

		static routeName = 'job.detail'

		static stateToProps(state) {
			return {
				me: state.me,
			}
		}

		constructor(p) {
			super(p, {
				isLoading: false,
				detail: {},
			}, [
			]);
		}

		componentDidMount() {
			if(this.props.id) {
				this.getData()
			}
		}

		header = {
			title: 'APPLICATION DETAIL',
			leftActions: [{
				type: HeaderBit.TYPES.BACK,
			}],
			rightActions: [{
				type: HeaderBit.TYPES.BLANK,
			}],
		}

		getData = () => {
			this.setState({
				isLoading: true,
			}, () => {
				setTimeout(() => {
					this.setState({
						isLoading: false,
						detail: JobManager.get(this.props.id),
					})
				}, 1500);
			})
		}

		render() {
			return super.render(
				<PageBit header={ ( <HeaderBit { ...this.header } style={Styles.header}/> )} contentContainerStyle={Styles.content}>
					<ListHeaderLego title={`JOB ID: #JOB-${this.props.id}`} style={Styles.jobId} inputStyle={Styles.darkGrey}/>
					<HeaderPart />
					<BodyPart />
					<FooterPart />
					{/* <ContentPart
						isLoading={ this.state.isLoading }
						jobIds={ this.state.jobIds }
						onEndReached={ this.getData }
					/> */}
				</PageBit>
			)
		}
	}
)
