import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

import PageManager from 'app/managers/page';

import TrackingHandler from 'app/handlers/tracking';

import BoxBit from 'modules/bits/box';
import SourceSansBit from 'modules/bits/source.sans';
import HeaderBit from 'modules/bits/header';
import LoaderBit from 'modules/bits/loader';
import PageBit from 'modules/bits/page';

import CardOrderComponent from 'modules/components/card.order'

import Styles from './style';


export default ConnectHelper(
	class OrderPage extends PageModel {

		static routeName = 'order'

		static propTypes(PropTypes) {
			return {
				batchIds: PropTypes.array,
				isLoading: PropTypes.bool,
			}
		}

		static stateToProps() {
			const order = PageManager.get('order')

			return {
				batchIds: order.ids,
				isLoading: order._isGetting,
			}
		}

		static defaultProps = {
			batchIds: [],
			isLoading: true,
		}

		header = {
			leftActions: [{
				type: HeaderBit.TYPES.BACK,
				onPress: this.onNavigateToBack,
			}],
			title: 'ORDER HISTORY',
			rightActions: [{
				type: HeaderBit.TYPES.BLANK,
			}],
		}

		constructor(p) {
			super(p, {}, [
				'orderCardRenderer',
				'onNavigateToOrderDetail',
				'onNavigateToBack',
			])
		}

		componentWillMount() {
			PageManager.get('order')
		}

		componentDidMount() {
			TrackingHandler.trackPageView(this.routeName)
		}

		onNavigateToBack() {
			this.navigator.navigate('profile')
		}

		onNavigateToOrderDetail(id) {
			this.navigator.navigate(`profile/order/${id}`)
		}

		orderCardRenderer(bId) {
			return (
				<CardOrderComponent
					id={bId}
					key={bId}
					onPress={ this.onNavigateToOrderDetail }
					style={Styles.divider}
				/>
			)
		}


		render() {
			return super.render(
				<PageBit
					header={<HeaderBit { ...this.header } />}
					contentContainerStyle={ Styles.container }
				>
					{ this.props.isLoading ? (
						<BoxBit centering>
							<LoaderBit />
						</BoxBit>
					) : this.props.batchIds.length && this.props.batchIds.map(this.orderCardRenderer) || (
						<BoxBit centering>
							<SourceSansBit type={SourceSansBit.TYPES.SUBHEADER_1} weight="normal" style={Styles.empty}>No order found</SourceSansBit>
						</BoxBit>
					) }
				</PageBit>
			)
		}
	}
)
