import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		backgroundColor: Colors.solid.grey.palette(1),
		paddingTop: 12,
		paddingRight: 0,
		paddingLeft: 0,
		paddingBottom: 60,
	},

	divider: {
		marginTop: 4,
		marginBottom: 4,
		marginLeft: 0,
		marginRight: 0,
	},

	empty: {
		color: Colors.black.palette(2),
	},
})
