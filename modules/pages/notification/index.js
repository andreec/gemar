import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

// import PageManager from 'app/managers/page';

import TrackingHandler from 'app/handlers/tracking';

// import FormatHelper from 'coeur/helpers/format';
import TimeHelper from 'coeur/helpers/time';

// import Colors from 'coeur/constants/color';
// import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
// import ButtonBit from 'modules/bits/button';
// import ImageBit from 'modules/bits/image';
import SourceSansBit from 'modules/bits/source.sans';
import PageBit from 'modules/bits/page';
// import IconBit from 'modules/bits/icon';
// import GalleryBit from 'modules/bits/gallery';
import HeaderBit from 'modules/bits/header';
import LoaderBit from 'modules/bits/loader';
// import ShadowBit from 'modules/bits/shadow';
// import TouchableBit from 'modules/bits/touchable';
// import TextBit from 'modules/bits/text';

import NotifyPart from './_notify'

import Styles from './style'

import {
	shuffle,
	take,
} from 'lodash'

// import {
// 	BoxesPart,
// 	CountdownPart,
// 	PromoPart,
// } from '../matchbox'
// import FooterPart from './_footer';
// import HowPart from './_how';
// import IntroductionPart from './_introduction';


export default ConnectHelper(
	class NotificationPage extends PageModel {

		static routeName = 'notification'

		header = {
			title: 'Notification',
			leftActions: [{
				type: HeaderBit.TYPES.BACK,
			}],
			rightActions: [{
				type: HeaderBit.TYPES.BLANK,
			}],
		}

		constructor(p) {
			super(p, {
				isLoading: true,
				notifications: [],
			}, [
			]);
		}

		componentWillMount() {
			// TODO: get data from server
			this.setTimeout(() => {
				Promise.resolve().then(res => {
					const mockImages = [
						'https://res.cloudinary.com/hiandree/image/upload/v1539438197/dummies/dummy-user1-img3.jpg',
						'https://res.cloudinary.com/hiandree/image/upload/v1539438197/dummies/dummy-user1-img2.jpg',
						'https://res.cloudinary.com/hiandree/image/upload/v1539438197/dummies/dummy-user1-img1.jpg',
						'https://res.cloudinary.com/hiandree/image/upload/v1539438204/dummies/dummy-user2-img1.jpg',
						'https://res.cloudinary.com/hiandree/image/upload/v1539438204/dummies/dummy-user2-img2.jpg',
						'https://res.cloudinary.com/hiandree/image/upload/v1539438204/dummies/dummy-user2-img3.jpg',
						'https://res.cloudinary.com/hiandree/image/upload/v1539438205/dummies/dummy-user3-img1.jpg',
					]

					const mockUsers = [{
						id: 1,
						name: 'Anthony Diorgo',
						image: 'https://res.cloudinary.com/hiandree/image/upload/v1539438198/dummies/dummy-user1.jpg',
					}, {
						id: 2,
						name: 'Herman Pradipta',
						image: 'https://res.cloudinary.com/hiandree/image/upload/v1539438201/dummies/dummy-user2.jpg',
					}, {
						id: 3,
						name: 'Andrea Yukana',
						image: 'https://res.cloudinary.com/hiandree/image/upload/v1539438201/dummies/dummy-user3.jpg',
					}]

					const generateRandom = (num = 7, total = []) => {
						if (num === 0) {
							return total
						}

						return generateRandom(num - 1, [
							...total,
							{
								type: Object.values(NotifyPart.TYPES)[Math.floor(Math.random() * 7)],
								timestamp: +TimeHelper.moment().subtract(Math.random() * 100000, 'm').toDate(),
								postId: Math.floor(Math.random() * 5),
								postImage: mockImages[Math.floor(Math.random() * 7)],
								users: take(shuffle(mockUsers), Math.floor(Math.random() * 2) + 1),
							},
						])
					}

					this.setState({
						isLoading: false,
						notifications: generateRandom(Math.round(Math.random() * 10)),
					})
				})
			}, 2000)
		}

		componentDidMount() {
			TrackingHandler.trackPageView(this.routeName)
		}

		notificationRenderer(config, i) {
			return (
				<NotifyPart key={i} { ...config } />
			)
		}

		render() {
			return super.render(
				<PageBit header={ <HeaderBit { ...this.header } /> } contentContainerStyle={Styles.container}>
					{ this.state.isLoading ? (
						<BoxBit centering>
							<LoaderBit />
						</BoxBit>
					) : this.state.notifications.length && this.state.notifications.map(this.notificationRenderer) || (
						<BoxBit centering>
							<SourceSansBit type={SourceSansBit.TYPES.NEW_PARAGRAPH_1}>No notifications</SourceSansBit>
						</BoxBit>
					) }
				</PageBit>
			)
		}
	}
)
