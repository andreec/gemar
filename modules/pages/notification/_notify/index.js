import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import PageContext from 'coeur/contexts/page';
import UtilitiesContext from 'coeur/contexts/utilities';

// import HobbyManager from 'app/managers/hobby';
// import UserManager from 'app/managers/user';

import FormatHelper from 'coeur/helpers/format';
import TimeHelper from 'coeur/helpers/time';

// import Colors from 'coeur/constants/color';
// import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import ImageBit from 'modules/bits/image';
import SourceSansBit from 'modules/bits/source.sans';
import TextBit from 'modules/bits/text';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style'


export default ConnectHelper(
	class NotifyPart extends StatefulModel {

		static TYPES = {
			LIKE: 'LIKE',
			MENTION: 'MENTION',
			REQUEST: 'REQUEST',
			FOLLOW: 'FOLLOW',
			ADDED_TO_CART: 'ADDED_TO_CART',
			SOLD: 'SOLD',
			APPLIED: 'APPLIED',
		}

		static propTypes(PropTypes) {
			return {
				type: PropTypes.oneOf(this.TYPES),
				timestamp: PropTypes.number.isRequired,
				postId: PropTypes.id.isRequired,
				postImage: PropTypes.image,
				users: PropTypes.arrayOf(PropTypes.shape({
					id: PropTypes.id,
					image: PropTypes.image,
					name: PropTypes.string,
				})),
			}
		}

		static contexts = [
			PageContext,
			UtilitiesContext,
		]

		static defaultProps = {
			type: 'LIKE',
			users: [],
		}

		constructor(p) {
			super(p, {}, [
			])
		}

		onPress() {
			switch(this.props.type) {
			case this.TYPES.LIKE:
			case this.TYPES.MENTION:
			case this.TYPES.ADDED_TO_CART:
			case this.TYPES.SOLD:
			case this.TYPES.APPLIED:
			case this.TYPES.APPLICATION_FULL:
				// go to post
				this.props.page.navigator.navigate('post', {
					id: this.props.postId,
				})
				break
			case this.TYPES.REQUEST:
			case this.TYPES.FOLLOW:
				// go to profile
				this.props.page.navigator.navigate('profile', {
					id: this.props.users[0].id,
				})
			}
		}

		onNavigateToProfile(userId) {
			this.props.page.navigator.navigate('profile', {
				id: userId,
			})
		}

		imageRenderer() {
			if(this.props.users.length > 1) {
				// many
				return (
					<BoxBit unflex style={Styles.images}>
						<ImageBit
							resizeMode={ImageBit.TYPES.COVER}
							source={this.props.users[1].image}
							style={Styles.imageSmall2}
						/>
						<ImageBit
							resizeMode={ImageBit.TYPES.COVER}
							source={this.props.users[0].image}
							style={Styles.imageSmall1}
						/>
					</BoxBit>
				)
			} else {
				// one
				return (
					<TouchableBit unflex onPress={this.onNavigateToProfile.bind(this, this.props.users[0].id)} style={Styles.images}>
						<ImageBit
							resizeMode={ImageBit.TYPES.COVER}
							source={this.props.users[0].image}
							style={Styles.image}
						/>
					</TouchableBit>
				)
			}
		}

		titleRenderer() {
			switch (this.props.type) {
			default:
				return false
			case this.TYPES.LIKE:
				return (
					<BoxBit style={Styles.title}>
						<SourceSansBit ellipsis numberOfLines={2} type={SourceSansBit.TYPES.NEW_PARAGRAPH_1}>
							{ this.peopleRenderer() } liked your comment. { this.timeRenderer() }
						</SourceSansBit>
					</BoxBit>
				)
			case this.TYPES.MENTION:
				return (
					<BoxBit style={Styles.title}>
						<SourceSansBit ellipsis numberOfLines={2} type={SourceSansBit.TYPES.NEW_PARAGRAPH_1}>
							{ this.peopleRenderer() } mentioned you in a post. {this.timeRenderer()}
						</SourceSansBit>
					</BoxBit>
				)
			case this.TYPES.ADDED_TO_CART:
				return (
					<BoxBit style={Styles.title}>
						<SourceSansBit ellipsis numberOfLines={2} type={SourceSansBit.TYPES.NEW_PARAGRAPH_1}>
							An item in your marketplace is added to cart. {this.timeRenderer()}
						</SourceSansBit>
					</BoxBit>
				)
			case this.TYPES.SOLD:
				return (
					<BoxBit style={Styles.title}>
						<SourceSansBit ellipsis numberOfLines={2} type={SourceSansBit.TYPES.NEW_PARAGRAPH_1}>
							An item in your marketplace is sold. {this.timeRenderer()}
						</SourceSansBit>
					</BoxBit>
				)
			case this.TYPES.APPLIED:
				return (
					<BoxBit style={Styles.title}>
						<SourceSansBit ellipsis numberOfLines={2} type={SourceSansBit.TYPES.NEW_PARAGRAPH_1}>
							{ this.peopleRenderer() } has just apply for position in your job posting. {this.timeRenderer()}
						</SourceSansBit>
					</BoxBit>
				)
			case this.TYPES.REQUEST:
				return (
					<BoxBit style={Styles.title}>
						<SourceSansBit ellipsis numberOfLines={2} type={SourceSansBit.TYPES.NEW_PARAGRAPH_1}>
							{ this.peopleRenderer() } requesting to follow you. {this.timeRenderer()}
						</SourceSansBit>
					</BoxBit>
				)
			case this.TYPES.FOLLOW:
				return (
					<BoxBit style={Styles.title}>
						<SourceSansBit ellipsis numberOfLines={2} type={SourceSansBit.TYPES.NEW_PARAGRAPH_1}>
							{ this.peopleRenderer() } following you. {this.timeRenderer()}
						</SourceSansBit>
					</BoxBit>
				)
			}
		}

		peopleRenderer() {
			const users = this.props.users.slice()
			if(users.length > 3) {
				const length = users.length - 3

				return (
					<TextBit>
						{ users.splice(0, 3).map((user, i) => {
							return (
								<TextBit key={user.id} weight="medium" onPress={this.onNavigateToProfile.bind(this, user.id)}>
									{user.name}{users.length - 1 === i ? '' : ', '}
								</TextBit>
							)
						}) }
						and {FormatHelper.currency(length)} others
					</TextBit>
				)
			} else if(users.length > 1) {
				const last = users.pop()
				return (
					<TextBit>
						{ users.map((user, i) => {
							return (
								<TextBit key={user.id} weight="medium" onPress={this.onNavigateToProfile.bind(this, user.id)}>
									{user.name}{users.length - 1 === i ? '' : ', '}
								</TextBit>
							)
						}) }
						 and <TextBit weight="medium" onPress={this.onNavigateToProfile.bind(this, last.id)}>
							{ last.name }
						</TextBit>
					</TextBit>
				)
			} else {
				return (
					<TextBit weight="medium" onPress={this.onNavigateToProfile.bind(this, users[0].id)}>
						{ users[0].name }
					</TextBit>
				)
			}
		}

		timeRenderer() {
			return (
				<TextBit style={Styles.time}>{TimeHelper.shortAgo(this.props.timestamp)}</TextBit>
			)
		}

		actionRenderer() {
			switch (this.props.type) {
			case this.TYPES.FOLLOW:
			default:
				return false
			case this.TYPES.LIKE:
			case this.TYPES.MENTION:
			case this.TYPES.ADDED_TO_CART:
			case this.TYPES.SOLD:
			case this.TYPES.APPLIED:
			case this.TYPES.APPLICATION_FULL:
				return (
					<ImageBit
						resizeMode={ImageBit.TYPES.COVER}
						source={ this.props.postImage }
						style={Styles.postImage}
					/>
				)
			case this.TYPES.REQUEST:
				return (
					<BoxBit row unflex style={Styles.buttons}>
						<ButtonBit
							title="Ignore"
							type={ButtonBit.TYPES.SHAPES.BADGE}
							theme={ButtonBit.TYPES.THEMES.ACTIVATED}
							size={ButtonBit.TYPES.SIZES.COMPACT}
							width={ButtonBit.TYPES.WIDTHS.FIT}
							onPress={ this.onIgnoreRequest }
							style={Styles.button}
						/>
						<ButtonBit
							title="Accept"
							type={ButtonBit.TYPES.SHAPES.BADGE}
							width={ButtonBit.TYPES.WIDTHS.FIT}
							size={ButtonBit.TYPES.SIZES.COMPACT}
							onPress={ this.onAcceptRequest }
						/>
					</BoxBit>
				)
			}
		}

		render() {
			return (
				<TouchableBit onPress={this.onPress} unflex row style={[Styles.container, this.props.style]}>
					{ this.props.users.length ? this.imageRenderer() : false }
					{ this.titleRenderer() }
					{ this.actionRenderer() }
				</TouchableBit>
			)
		}
	}
)
