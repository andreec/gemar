import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

import TrackingHandler from 'app/handlers/tracking';

import TipsPagelet from 'modules/pagelets/tips';


export default ConnectHelper(
	class TipsPage extends PageModel {

		static routeName = 'tips'

		static propTypes() {
			return TipsPagelet.propTypes
		}

		constructor(p) {
			super(p, {}, [
				'onNavigateToBack',
			]);

			if(!p.data.length) {
				this.navigator.back()
			}
		}

		componentDidMount() {
			TrackingHandler.trackPageView(this.routeName, this.props.title)
		}

		onNavigateToBack() {
			this.navigator.back()
		}

		shouldComponentUpdate() {
			return false
		}

		render() {
			return super.render(
				<TipsPagelet
					title={ this.props.title }
					image={ this.props.image }
					data={ this.props.data }
					onRequestClose={ this.onNavigateToBack }
				/>
			)
		}
	}
)
