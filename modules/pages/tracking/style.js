import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({

	section: {
		paddingTop: Sizes.margin.default,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
		paddingBottom: Sizes.margin.default,
		backgroundColor: Colors.white.primary,
		borderWidth: 0,
	},
	empty: {
		paddingTop: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		color: Colors.new.black.palette(3),
	},
	box: {
		paddingBottom: 16,
		paddingTop: 16,
	},
	capital: {
		textTransform: 'capitalize',
		paddingLeft: 12,
	},

	meter: {
		marginLeft: 8,
		width: 2,
		height: 72,
		position: 'absolute',
	},

	activeColor: {
		backgroundColor: Colors.primary,
	},

	inactiveColor: {
		backgroundColor: Colors.new.black.palette(3),
		opacity: .5,
	},

	progressTop: {
		paddingTop: 0,
		height: 47,
		top: 10,
	},

	progressBottom: {
		paddingBottom: 0,
		height: 26,
	},

	status: {
		flexGrow: 3,
		paddingLeft: 21,
	},

	noTop: {
		paddingTop: 0,
	},

	noBottom: {
		paddingBottom: 0,
	},

	marginTop16: {
		marginTop: 16,
	},

	paddingBottom0: {
		paddingBottom: 0,
	},

	padderLeft: {
		paddingLeft: 12,
	},
	titleWrapper: {
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingTop: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.new.black.palette(3),
	},
	title: {
		paddingLeft: Sizes.margin.default,
	},

	mainText: {
		color: Colors.new.black.palette(3),
	},

	trackText: {
		color: Colors.new.black.palette(3),
		flexGrow: 1,
		paddingLeft: 8,
	},
	resi: {
		color: Colors.new.black.palette(3),
	},
	padderRight: {
		marginRight: Sizes.margin.default,
	},
	statusPrimary: {
		color: Colors.primary,
	},

	statusGrey: {
		color: Colors.new.black.palette(3),
	},

	dark40: {
		color: Colors.new.black.palette(3),
	},

	seemore: {
		color: Colors.new.black.palette(3),
		paddingTop: 13,
		paddingBottom: 13,
	},

	dotActive: {
		backgroundColor: Colors.primary,
		borderRadius: 6,
		height: 12,
		width: 12,
		position: 'absolute',
		left: -18,
		top: 5,
	},
	dotInactive: {
		backgroundColor: Colors.new.black.palette(3),
		zIndex: 2,
		opacity: .5,
		borderRadius: 6,
		height: 12,
		width: 12,
		position: 'absolute',
		left: -18,
		top: 5,
	},

	track: {
		paddingTop: 16,
		paddingBottom: 16,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
		backgroundColor: Colors.white.primary,
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.new.black.palette(3),
		alignItems: 'center',
	},
})
