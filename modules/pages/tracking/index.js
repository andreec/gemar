import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';
import TimeHelper from 'coeur/helpers/time';


import PageBit from 'modules/bits/page';
import BoxBit from 'modules/bits/box';
import SourceSansBit from 'modules/bits/source.sans';
import HeaderBit from 'modules/bits/header';

import ListContainerLego from 'modules/legos/list.container';

import { isEmpty } from 'lodash';
import Styles from './style';
import { Source } from 'graphql';

export default ConnectHelper(
	class TrackingPage extends PageModel {

		static routeName = 'tracking'

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				
				statuses: PropTypes.array,
			}
		}

		// static stateToProps(state, oP) {
		// 	const id = oP.id
		// 		, order = state.orders.get(id) || OrderManager.get(id)
		// 		, seller = order.seller_id && (state.users.get(order.seller_id) || UserManager.get(order.seller_id)) || new UserManager._record()
		// 		, products = order.productIds && order.productIds.map(pId => state.products.get(pId) || ProductManager.get(pId) || new ProductManager._record()) || []
		// 	return {
		// 		id: oP.id,
		// 		order,
		// 		seller,
		// 		products,
		// 		me: state.me,
		// 	}
		// }
		
		static defaultProps = {
			statuses: [{
				status: 'CREATED',
				date: TimeHelper.moment(),
			}, {
				status: 'PAYMENT_PENDING',
				date: TimeHelper.moment(),
			}, {
				status: 'PENDING',
				date: TimeHelper.moment(),
			}, {
				status: 'PROCESSING',
				date: TimeHelper.moment(),
			}, {
				status: 'SHIPMENT_DELIVERY_CONFIRMED',
				date: TimeHelper.moment(),
			}, {
				status: 'RESOLVED',
				date: TimeHelper.moment(),
			}],
		}

		header = {
			title: 'TRACKING',
			leftActions: [{
				type: HeaderBit.TYPES.BACK,
			}],
			rightActions: [{
				type: HeaderBit.TYPES.BLANK,
			}],
		}

		constructor(p) {
			super(p, {
			}, [
				'onNavigateToBack',
			]);
		}

		onNavigateToBack() {
			this.navigator.back()
		}

		getOrderStatusTitle(status) {
			switch(status) {
			case 'CREATED':
				return 'Order confirmed and checked out'
			case 'PAYMENT_PENDING':
				return 'Waiting for payment or verification'
			case 'PAYMENT_FAILED':
				return 'Payment Failed'
			case 'PAYMENT_SUCCESS':
				return 'Payment Success'
			case 'PAYMENT_EXCEPTION':
				return 'Payment Exceptioned'
			case 'PENDING':
				return 'Payment verifed and forwarded to seller.'
			case 'PROCESSING':
				return 'Your order is being processed by seller.'
			case 'PRIMED':
				return 'Packed'
			case 'RESOLVED':
				return 'Order Completed'
			case 'EXCEPTION':
				return 'Order Exceptioned'
			case 'SHIPMENT_PENDING':
				return 'Awaiting Pickup'
			case 'SHIPMENT_PROCESSING':
				return 'In Transit'
			case 'SHIPMENT_ON_COURIER':
				return 'Out for Delivery'
			case 'SHIPMENT_DELIVERED':
				return 'Delivered'
			case 'SHIPMENT_DELIVERY_CONFIRMED':
				return 'Package is received by Ijah Warni'
			case 'SHIPMENT_FAILED':
				return 'Shipment Attempt Failed'
			case 'SHIPMENT_EXCEPTION':
				return 'Shipment Exceptioned'
			case 'EXCHANGE_CREATED':
				return 'Exchange Created'
			case 'EXCHANGE_SUCCESS':
				return 'Exchange Approved'
			case 'EXCHANGE_FAILED':
				return 'Exchange Rejected'
			case 'EXCHANGE_EXCEPTION':
				return 'Exchange Partially Approved'
			default:
				return status.toLowerCase()
			}
		}
		
		emptyRenderer() {
			return (
				<BoxBit unflex centering>
					<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_1} style={Styles.empty}>
						ORDER ID: #{ this.props.order.number }
					</SourceSansBit>
				</BoxBit>
			)
		}

		render() {
			const statusLen = this.props.statuses.length - 1

			return super.render(
				<PageBit header={( <HeaderBit { ...this.header } /> )} contentContainerStyle={Styles.content}>
					<BoxBit unflex style={Styles.titleWrapper}>
						<SourceSansBit type={SourceSansBit.TYPES.SUBHEADER_1} style={Styles.title}>Tracking History</SourceSansBit>
					</BoxBit>
					<BoxBit style={Styles.section}>
						{ isEmpty(this.props.statuses) ? this.emptyRenderer() : this.props.statuses.map(({
							date,
							status,
						}, i) => {
							return (
								<BoxBit row unflex key={i}>
									<BoxBit unflex style={[
										Styles.box,
										Styles.meter, i === 0
											? Styles.progressTop
											: false,
										i === statusLen
											? Styles.progressBottom
											: false,
										i === 0 ? Styles.activeColor : Styles.inactiveColor,
									]}/>
									<BoxBit row
										style={[Styles.box, Styles.status, i === 0
											? Styles.noTop
											: false,
										i === statusLen
											? Styles.noBottom
											: false]}
									>
										<BoxBit style={Styles.padderRight}>
											<BoxBit>
												{/* <SourceSansBit
													type={SourceSansBit.TYPES.NEW_SUBHEADER_1}
													style={[Styles.capital, i === 0 && Styles.statusGreen || Styles.statusGrey]}
													weight="medium"
												>
													{ TimeHelper.format(date)}
												</SourceSansBit> */}
												<SourceSansBit
													type={SourceSansBit.TYPES.NEW_SUBHEADER_1}
													style={[Styles.capital, i === 0 && Styles.statusPrimary || Styles.statusGrey]}
													weight="medium"
												>
													{ this.getOrderStatusTitle(status).split('_').join(' ')}
												</SourceSansBit>
											</BoxBit>

											{ i === 0
												? <BoxBit unflex style={Styles.dotActive}/>
												: <BoxBit unflex style={Styles.dotInactive}/>
											}
										</BoxBit>
										{ date && (
											<SourceSansBit
												type={SourceSansBit.TYPES.NEW_SUBHEADER_1}
												align="right"
												style={Styles.mainText}
											>
												{ TimeHelper.format(date, 'DD-MM-YYYY') }
												{ '\n' }
												{ TimeHelper.format(date, 'HH.mm') }
											</SourceSansBit>
										)}
									</BoxBit>
								</BoxBit>
							)
						}) }
					</BoxBit>
					{/* <BoxBit unflex>
						<ListContainerLego
							title="Tracking History"
							children={(
							)}
						/>

					</BoxBit> */}
				</PageBit>
			)
		}
	}
)
