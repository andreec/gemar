import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	header: {
		textAlign: 'center',
		paddingBottom: 12,
	},

	section: {
		paddingTop: 24,
		paddingBottom: 24,
		paddingLeft: 20,
		paddingRight: 20,
		backgroundColor: Colors.grey.palette(1),
	},

	row: {
		backgroundColor: Colors.grey.palette(2),
		marginTop: 15,
		marginBottom: 15,
		marginRight: 0,
		marginLeft: 0,
	},

	size: {
		backgroundColor: Colors.red.palette(6),
	},

	inner: {
		backgroundColor: Colors.red.palette(6),
	},
})
