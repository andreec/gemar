import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

import BoxBit from 'modules/bits/box';
import SourceSansBit from 'modules/bits/source.sans';
import PageBit from 'modules/bits/page';

import Styles from './style';


export default ConnectHelper(
	class TypographyPage extends PageModel {

		static routeName = 'typography'

		render() {
			return super.render(
				<PageBit>


					<BoxBit unflex className={Styles.section}>
						<SourceSansBit type={SourceSansBit.TYPES.HEADER_1} className={Styles.header}>
							HEADERS
						</SourceSansBit>
						<BoxBit unflex className={Styles.row} style={{
							height: 40,
						}}>
							<BoxBit unflex className={Styles.size} style={{
								height: 56,
								marginTop: -8,
								marginBottom: -8,
							}}>
								<BoxBit unflex className={Styles.inner} style={{
									height: 62,
									marginTop: -3,
									marginBottom: -3,
								}}>
									<SourceSansBit type={SourceSansBit.TYPES.HERO}>
										Lorem ipsum.
									</SourceSansBit>
								</BoxBit>
							</BoxBit>
						</BoxBit>
						<BoxBit unflex className={Styles.row} style={{
							height: 31,
						}}>
							<BoxBit unflex className={Styles.size} style={{
								height: 45,
								marginTop: -7,
								marginBottom: -7,
							}}>
								<BoxBit unflex className={Styles.inner} style={{
									height: 55,
									marginTop: -5,
									marginBottom: -5,
								}}>
									<SourceSansBit type={SourceSansBit.TYPES.HEADER_1}>
										Lorem ipsum.
									</SourceSansBit>
								</BoxBit>
							</BoxBit>
						</BoxBit>
						<BoxBit unflex className={Styles.row} style={{
							height: 24,
						}}>
							<BoxBit unflex className={Styles.size} style={{
								height: 34,
								marginTop: -5,
								marginBottom: -5,
							}}>
								<BoxBit unflex className={Styles.inner} style={{
									height: 42,
									marginTop: -4,
									marginBottom: -4,
								}}>
									<SourceSansBit type={SourceSansBit.TYPES.HEADER_2}>
										Lorem ipsum.
									</SourceSansBit>
								</BoxBit>
							</BoxBit>
						</BoxBit>
						<BoxBit unflex className={Styles.row} style={{
							height: 20,
						}}>
							<BoxBit unflex className={Styles.size} style={{
								height: 28,
								marginTop: -4,
								marginBottom: -4,
							}}>
								<BoxBit unflex className={Styles.inner} style={{
									height: 36,
									marginTop: -4,
									marginBottom: -4,
								}}>
									<SourceSansBit type={SourceSansBit.TYPES.HEADER_3}>
										Lorem ipsum.
									</SourceSansBit>
								</BoxBit>
							</BoxBit>
						</BoxBit>
						<BoxBit unflex className={Styles.row} style={{
							height: 17,
						}}>
							<BoxBit unflex className={Styles.size} style={{
								height: 24,
								marginTop: -3.5,
								marginBottom: -3.5,
							}}>
								<BoxBit unflex className={Styles.inner} style={{
									height: 31,
									marginTop: -3.5,
									marginBottom: -3.5,
								}}>
									<SourceSansBit type={SourceSansBit.TYPES.HEADER_4}>
										Lorem ipsum.
									</SourceSansBit>
								</BoxBit>
							</BoxBit>
						</BoxBit>
						<BoxBit unflex className={Styles.row} style={{
							height: 14,
						}}>
							<BoxBit unflex className={Styles.size} style={{
								height: 20,
								marginTop: -3,
								marginBottom: -3,
							}}>
								<BoxBit unflex className={Styles.inner} style={{
									height: 26,
									marginTop: -3,
									marginBottom: -3,
								}}>
									<SourceSansBit type={SourceSansBit.TYPES.HEADER_5}>
										Lorem ipsum.
									</SourceSansBit>
								</BoxBit>
							</BoxBit>
						</BoxBit>
					</BoxBit>

					<BoxBit unflex className={Styles.section}>
						<SourceSansBit type={SourceSansBit.TYPES.HEADER_1} className={Styles.header}>
							SUB HEADERS
						</SourceSansBit>
						<BoxBit unflex className={Styles.row} style={{
							height: 11,
						}}>
							<BoxBit unflex className={Styles.size} style={{
								height: 15,
								marginTop: -2,
								marginBottom: -2,
							}}>
								<BoxBit unflex className={Styles.inner} style={{
									height: 18,
									marginTop: -1.5,
									marginBottom: -1.5,
								}}>
									<SourceSansBit type={SourceSansBit.TYPES.SUBHEADER_1}>
										Lorem ipsum dolor sit amet.
									</SourceSansBit>
								</BoxBit>
							</BoxBit>
						</BoxBit>
						<BoxBit unflex className={Styles.row} style={{
							height: 8,
						}}>
							<BoxBit unflex className={Styles.size} style={{
								height: 12,
								marginTop: -2,
								marginBottom: -2,
							}}>
								<BoxBit unflex className={Styles.inner} style={{
									height: 14,
									marginTop: -1,
									marginBottom: -1,
								}}>
									<SourceSansBit type={SourceSansBit.TYPES.SUBHEADER_2}>
										Lorem ipsum dolor sit amet.
									</SourceSansBit>
								</BoxBit>
							</BoxBit>
						</BoxBit>
						<BoxBit unflex className={Styles.row} style={{
							height: 9,
						}}>
							<BoxBit unflex className={Styles.size} style={{
								height: 11,
								marginTop: -1,
								marginBottom: -1,
							}}>
								<BoxBit unflex className={Styles.inner} style={{
									height: 13,
									marginTop: -1,
									marginBottom: -1,
								}}>
									<SourceSansBit type={SourceSansBit.TYPES.SUBHEADER_3}>
										Lorem ipsum dolor sit amet.
									</SourceSansBit>
								</BoxBit>
							</BoxBit>
						</BoxBit>
					</BoxBit>

					<BoxBit unflex className={Styles.section}>
						<SourceSansBit type={SourceSansBit.TYPES.HEADER_1} className={Styles.header}>
							PARAGRAPH
						</SourceSansBit>
						<BoxBit unflex className={Styles.row}>
							<SourceSansBit type={SourceSansBit.TYPES.PARAGRAPH_1}>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis in sem congue, vestibulum erat a, tempor mi. Duis id lorem blandit, cursus mi tincidunt, placerat enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vivamus porttitor tellus quis erat varius fermentum sed id odio. Nulla sed enim vitae dui aliquam lobortis. Vivamus eu vehicula ipsum. Curabitur bibendum ipsum ut sem feugiat, id pharetra felis ullamcorper.
							</SourceSansBit>
						</BoxBit>
						<BoxBit unflex className={Styles.row}>
							<SourceSansBit type={SourceSansBit.TYPES.PARAGRAPH_2}>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis in sem congue, vestibulum erat a, tempor mi. Duis id lorem blandit, cursus mi tincidunt, placerat enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vivamus porttitor tellus quis erat varius fermentum sed id odio. Nulla sed enim vitae dui aliquam lobortis. Vivamus eu vehicula ipsum. Curabitur bibendum ipsum ut sem feugiat, id pharetra felis ullamcorper.
							</SourceSansBit>
						</BoxBit>
						<BoxBit unflex className={Styles.row}>
							<SourceSansBit type={SourceSansBit.TYPES.PARAGRAPH_3}>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis in sem congue, vestibulum erat a, tempor mi. Duis id lorem blandit, cursus mi tincidunt, placerat enim. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vivamus porttitor tellus quis erat varius fermentum sed id odio. Nulla sed enim vitae dui aliquam lobortis. Vivamus eu vehicula ipsum. Curabitur bibendum ipsum ut sem feugiat, id pharetra felis ullamcorper.
							</SourceSansBit>
						</BoxBit>
					</BoxBit>

					<BoxBit unflex className={Styles.section}>
						<SourceSansBit type={SourceSansBit.TYPES.HEADER_1} className={Styles.header}>
							CAPTION
						</SourceSansBit>
						<BoxBit unflex className={Styles.row}>
							<SourceSansBit type={SourceSansBit.TYPES.CAPTION_1}>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis in sem congue, vestibulum erat a, tempor mi.
							</SourceSansBit>
						</BoxBit>
						<BoxBit unflex className={Styles.row}>
							<SourceSansBit type={SourceSansBit.TYPES.CAPTION_2}>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis in sem congue, vestibulum erat a, tempor mi.
							</SourceSansBit>
						</BoxBit>
					</BoxBit>

					<BoxBit unflex className={Styles.section}>
						<SourceSansBit type={SourceSansBit.TYPES.HEADER_1} className={Styles.header}>
							NOTE
						</SourceSansBit>
						<BoxBit unflex className={Styles.row}>
							<SourceSansBit type={SourceSansBit.TYPES.NOTE_1}>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis in sem congue, vestibulum erat a, tempor mi.
							</SourceSansBit>
						</BoxBit>
						<BoxBit unflex className={Styles.row}>
							<SourceSansBit type={SourceSansBit.TYPES.NOTE_2}>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis in sem congue, vestibulum erat a, tempor mi.
							</SourceSansBit>
						</BoxBit>
					</BoxBit>
				</PageBit>
			);
		}
	}
)
