import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

import MeManager from 'app/managers/me';

import TrackingHandler from 'app/handlers/tracking';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import HeaderBit from 'modules/bits/header';
import PageBit from 'modules/bits/page';

import ListContainerLego from 'modules/legos/list.container';

import FormPagelet from 'modules/pagelets/form';

import Styles from './style';


export default ConnectHelper(
	class ProfileEditPage extends PageModel {

		static routeName = 'profile.edit'

		static propTypes(PropTypes) {
			return {
				me: PropTypes.object,
			}
		}

		static stateToProps(state) {
			return {
				me: state.me,
			}
		}

		header = {
			title: 'MY ACCOUNT DETAILS',
			leftActions: [{
				type: HeaderBit.TYPES.CLOSE,
			}],
			rightActions: [{
				type: HeaderBit.TYPES.TEXT,
				data: 'SAVE',
				isActive: this.canSave,
				onPress: this.onSave,
			}],
		}

		constructor(p) {
			super(p, {
				canSave: false,
				isSaving: false,
				data: {},
			}, [
				'canSave',
				'onNavigateToChangePassword',
				'onUpdate',
				'onSave',
			]);

			this._formConfig = [{
				id: 'firstName',
				title: 'First Name',
				isRequired: true,
				type: FormPagelet.TYPES.NAME,
				value: p.me.firstName,
				testOnMount: true,
			}, {
				id: 'lastName',
				title: 'Last Name',
				isRequired: true,
				type: FormPagelet.TYPES.NAME,
				value: p.me.lastName,
				testOnMount: true,
			}, {
				id: 'email',
				isRequired: true,
				type: FormPagelet.TYPES.EMAIL,
				value: p.me.email,
				readonly: true,
			}, {
				id: 'phoneNumber',
				type: FormPagelet.TYPES.PHONE,
				value: p.me.phone && p.me.phone.replace('+62', ''),
				testOnMount: true,
				description: 'We will only contact you by phone if there is a problem with your order',
			}]

			this._footer = [{
				title: 'Change password',
				icon: 'lock',
				onPress: this.onNavigateToChangePassword,
			}]
		}

		componentDidMount() {
			TrackingHandler.trackPageView(this.routeName)
		}

		onNavigateToChangePassword() {
			this.navigator.navigate('profile/edit/password')
		}

		canSave() {
			return this.state.canSave &&
			(
				this.state.data.firstName !== (this.props.me.firstName || '')
				|| this.state.data.lastName !== (this.props.me.lastName || '')
				|| this.state.data.phone !== (this.props.me.phone || '')
			)
			&& !this.state.isSaving
		}

		onUpdate(isDone, {
			firstName,
			lastName,
			phoneNumber,
		}) {
			this.setState({
				canSave: isDone,
				data: {
					firstName: firstName.value,
					lastName: lastName.value,
					phone: phoneNumber.value && ('+62' + phoneNumber.value) || '',
				},
			})
		}

		onSave() {
			if(this.canSave()) {
				this.setState({
					isSaving: true,
				}, () => {
					MeManager.updateProfile(this.state.data).then(() => {
						this.utilities.notification.show({
							title: '🎉',
							message: 'Your profile has been updated successfuly',
							type: this.utilities.notification.TYPES.SUCCESS,
						})

						this.navigator.back()
					})
				})
			}
		}

		render() {
			return super.render(
				<PageBit
					header={( <HeaderBit { ...this.header } updater={ this.canSave() } /> )}
					contentContainerStyle={Styles.container}
				>
					<FormPagelet ignoreNull
						config={this._formConfig}
						style={Styles.form}
						onUpdate={ this.onUpdate }
						onSubmit={ this.onSave }
					/>
					<ListContainerLego data={this._footer} />
					<BoxBit unflex style={Styles.footer}>
						<ButtonBit
							title="SAVE"
							type={ ButtonBit.TYPES.SHAPES.PROGRESS }
							state={ this.canSave() ? ButtonBit.TYPES.STATES.NORMAL : this.state.isSaving && ButtonBit.TYPES.STATES.LOADING || ButtonBit.TYPES.STATES.DISABLED }
							onPress={ this.onSave }
						/>
					</BoxBit>
				</PageBit>
			)
		}
	}
)
