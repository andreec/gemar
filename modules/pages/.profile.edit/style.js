import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({

	container: {
		backgroundColor: Colors.solid.grey.palette(1),
	},

	form: {
		marginTop: 16,
		backgroundColor: Colors.white.primary,
		paddingTop: 0,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
		paddingBottom: 24,
	},

	footer: {
		paddingTop: 16,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
		paddingBottom: 64,
	},

})
