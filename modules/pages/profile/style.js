import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	header: {
		paddingTop: 25,
		paddingLeft: 15,
		paddingRight: 15,
		borderBottomWidth: 1,
		borderBottomColor: Colors.new.grey.palette(1),
	},
	counts: {
		marginTop: 24,
		marginBottom: 40,
	},
	bell: {
		marginRight: 10,
	},
})
