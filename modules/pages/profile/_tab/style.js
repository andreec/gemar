import StyleSheet from 'coeur/libs/style.sheet'
// import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	container: {
		height: 56,
		justifyContent: 'space-around',
		paddingLeft: 15,
		paddingRight: 15,
	},
})
