import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

// import PageManager from 'app/managers/page';

// import TrackingHandler from 'app/handlers/tracking';

// import Animated from 'coeur/libs/animated';

// import Colors from 'coeur/constants/color';
// import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
// import IconBit from 'modules/bits/icon';
// import ImageBit from 'modules/bits/image';
// import LoaderBit from 'modules/bits/loader';
import SourceSansBit from 'modules/bits/source.sans';
// import TouchableBit from 'modules/bits/touchable';

import Styles from './style'


export default ConnectHelper(
	class StatusPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				status: PropTypes.string,
			}
		}

		static defaultProps = {
			status: 'I\'m ready for adventure',
		}

		constructor(p) {
			super(p, {
			}, [
			]);
		}

		render() {
			return (
				<BoxBit unflex centering style={this.props.style}>
					<BoxBit unflex style={Styles.line} />
					<BoxBit unflex style={Styles.container}>
						<SourceSansBit type={SourceSansBit.TYPES.NEW_NOTE} style={Styles.text}>
							{ this.props.status }
						</SourceSansBit>
					</BoxBit>
				</BoxBit>
			)
		}
	}
)
