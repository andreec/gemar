import StyleSheet from 'coeur/libs/style.sheet'
// import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	image: {
		height: 85,
		width: 85,
		borderRadius: 43,
		overflow: 'hidden',
		marginRight: 14,
	},
	name: {
		marginBottom: 7,
	},
	hobby: {
		marginBottom: 11,
	},
})
