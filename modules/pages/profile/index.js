import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

// import PageManager from 'app/managers/page';
import UserManager from 'app/managers/user';
import ProfileService from 'app/services/profile';

// import FormatHelper from 'coeur/helpers/format';

// import Colors from 'coeur/constants/color';
// import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
// import BoxImageBit from 'modules/bits/box.image';
// import ButtonBit from 'modules/bits/button';
// import ImageBit from 'modules/bits/image';
// import SourceSansBit from 'modules/bits/source.sans';
import PageBit from 'modules/bits/page';
// import IconBit from 'modules/bits/icon';
// import GalleryBit from 'modules/bits/gallery';
import HeaderBit from 'modules/bits/header';
// import TouchableBit from 'modules/bits/touchable';
// import TextBit from 'modules/bits/text';

import BioPart from './_bio'
import ContentPart from './_content'
import CountsPart from './_counts'
import HeaderPart from './_header'
import StatusPart from './_status'
import TabPart from './_tab';

import Styles from './style';

// import {
// 	BoxesPart,
// 	CountdownPart,
// 	PromoPart,
// } from '../matchbox'
// import FooterPart from './_footer';
// import HowPart from './_how';
// import IntroductionPart from './_introduction';

const IMAGE = [
	'https://res.cloudinary.com/hiandree/image/upload/v1539438198/dummies/dummy-user1.jpg',
	'https://res.cloudinary.com/hiandree/image/upload/v1539438201/dummies/dummy-user2.jpg',
	'https://res.cloudinary.com/hiandree/image/upload/v1539438201/dummies/dummy-user3.jpg',
]

export default ConnectHelper(
	class ProfilePage extends PageModel {

		static routeName = 'user.profile'

		static stateToProps(state, oP) {
			const user = oP.id && state.users.get(oP.id) || state.me

			return {
				user,
				me: state.me,
				isMe: state.me.id === user.id,
				hobbies: state.me.hobbies,
				// hobbies: user.hobby.get(hobbyId)
			}
		}

		constructor(p) {
			super(p, {
				tabIndex: 0,
				offset: 1,
				// postIds: [],
				posts: [],
				isLoading: false,
				isNext: true,
			}, [
				'onNavigateToNotification',
				'onNavigateToSetting',
				'getData',
				'onChangeTabIndex',
			]);

			this._filters = ['', 'SOCIAL', 'MARKET', 'JOB']

			this.header = this.props.isMe ? {
				// TODO: config by if me or not
				leftActions: [{
					type: HeaderBit.TYPES.BLANK,
				}],
				rightActions: [{
					type: HeaderBit.TYPES.BELL,
					style: Styles.bell,
					onPress: this.onNavigateToNotification,
				}, {
					type: HeaderBit.TYPES.MENU,
					onPress: this.onNavigateToSetting,
				}],
			} : {
				// TODO: config by if me or not
				leftActions: [{
					type: HeaderBit.TYPES.BACK,
				}],
				title: `@${this.props.me.username}`,
				rightActions: [{
					type: HeaderBit.TYPES.BLANK,
				}],
			}
		}

		componentDidMount() {
			if(this.state.isNext) {
				this.getData()
			}
		}

		getData() {
			this.setState({
				isLoading: true,
			}, () => {
				return ProfileService.getPost({
					// TODO CHANGE STATIC ID
					// id: this.props.user.id,
					id: 1,
					page: this.state.tabIndex + 1,
				}).then(res => {
					if(res.status === 1) {
						this.setState({
							isLoading: false,
							isNext: res.data.next,
							posts: !res.data.post ? [] : res.data.post.map((post, i) => {
								// this.log(post, i)
								return {
									id: post.id,
									type: post.post_type,
									userId: post.user_id,
									// TODO CHANGE STATIC IMAGE, BECAUSE IMAGE LINK BROKEN
									// image: post.main_image,
									image: post.main_image,
									likeCount: post.like_count,
									totalImage: post.totalimage,
									status: post.status,
									createdDate: post.create_date,
									caption: post.caption,
								}
							}),
						})
					}
				}).catch(err => this.log(err))
			})

			// const filter = this._filters[this.state.tabIndex]
			// 	, tabIndex = this.state.tabIndex

			// UserManager.getPost({
			// 	filter,
			// 	id: this.props.id,
			// 	offset: this.state.postIds.length,
			// }).then(posts => {
			// 	if(tabIndex === this.state.tabIndex) {
			// 		this.setState({
			// 			isLoading: false,
			// 			postIds: [...this.state.postIds, ...posts.map(post => post.id)],
			// 		})
			// 	}
			// })
			
			// if(tabIndex === this.state.tabIndex) {
			// 		postIds: [...this.state.postIds, ...posts.map(post => post.id)],
			// postIds: [...this.state.postIds, ...posts.map(post => post.id)],
			// }
		// })
		}

		onNavigateToNotification() {
			this.navigator.navigate('notification')
		}

		onNavigateToSetting() {
			this.navigator.navigate('setting')
		}

		onChangeTabIndex(tabIndex) {
			if(tabIndex !== this.state.tabIndex) {
				this.setState({
					tabIndex,
					postIds: [],
					isLoading: false,
				}, this.getData)
			}
		}

		profileRenderer() {
			return (
				<BoxBit unflex>
					<BoxBit unflex style={Styles.header}>
						<HeaderPart
							// image={ this.props.user.profile }
							// name={ this.props.user.name || `@${this.props.me.username}` }
							// isMe={ this.props.isMe }
							// isFollowing={ !this.props.isMe && this.props.me.followingUserIds.indexOf(this.props.user.id) > -1 }
							name={ `@${this.props.me.username}` }
							hobbies={ this.props.hobbies.map(hobby => {
								return hobby.title
							}).join(' & ') }
							isMe
							isFollowing
						/>

						<CountsPart
							posts={ this.state.posts.length }
							followers={ this.props.me.followerCount }
							following={ this.props.me.followingCount }
							// likes={ this.props.me.likeCount }
							likes={ this.props.me.likedPostIds.length }
							style={ Styles.counts }
						/>

						{/* <StatusPart
							status={ this.props.user.status }
						/> */}

						{/* <BioPart
							job={ this.props.user.job || '-' }
							bio={ this.props.user.bio || 'Your bio is currently empty' }
						/> */}

					</BoxBit>
					<TabPart
						onPress={ this.onChangeTabIndex }
					/>
				</BoxBit>
			)
		}

		render() {
			return super.render(
				<PageBit scroll={false} header={ ( <HeaderBit { ...this.header } /> ) } >
					<ContentPart
						header={ this.profileRenderer() }
						isLoading={ this.state.isLoading }
						// postIds={ this.state.postIds }
						data={ this.state.posts }
						onEndReached={ this.state.isNext
							? this.getData
							: false
						}
					/>
				</PageBit>
			)
		}
	}
)
