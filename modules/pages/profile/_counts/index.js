import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

// import PageManager from 'app/managers/page';

import PageContexts from 'coeur/contexts/page';

import FormatHelper from 'coeur/helpers/format';

// import TrackingHandler from 'app/handlers/tracking';

// import Animated from 'coeur/libs/animated';

// import Colors from 'coeur/constants/color';
// import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
// import IconBit from 'modules/bits/icon';
// import ImageBit from 'modules/bits/image';
// import LoaderBit from 'modules/bits/loader';
import SourceSansBit from 'modules/bits/source.sans';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style'


export default ConnectHelper(
	class CountsPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				// posts: PropTypes.number,
				// followers: PropTypes.number,
				// following: PropTypes.number,
				// likes: PropTypes.number,
			}
		}

		static contexts = [
			PageContexts,
		]

		static defaultProps = {
			posts: 0,
			followers: 0,
			following: 0,
			likes: 0,
		}

		constructor(p) {
			super(p, {
			}, [
			]);
		}

		countRenderer({
			title,
			count,
			onPress,
		}) {
			return onPress ? (
				<TouchableBit key={title} centering unflex style={Styles.box} onPress={onPress}>
					<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_1} style={Styles.title}>{title}</SourceSansBit>
					<SourceSansBit type={SourceSansBit.TYPES.NEW_HEADER_2} weight="semibold">{FormatHelper.currencyFormat(count)}</SourceSansBit>
				</TouchableBit>
			) : (
				<BoxBit key={title} centering unflex style={Styles.box}>
					<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_1} style={Styles.title}>{ title }</SourceSansBit>
					<SourceSansBit type={SourceSansBit.TYPES.NEW_HEADER_2} weight="semibold">{ FormatHelper.currencyFormat(count) }</SourceSansBit>
				</BoxBit>
			)
		}

		onNavigateToLikes = () => {
			this.props.page.navigator.navigate('profile.like')
		}

		onNavigateToFollowers = () => {
			this.props.page.navigator.navigate('profile.follower')
		}

		onNavigateToFollowing = () => {
			this.props.page.navigator.navigate('profile.following')
		}

		render() {
			return (
				<BoxBit unflex row style={[Styles.container, this.props.style]}>
					{ this.countRenderer({ title: 'Post', count: this.props.posts}) }
					{ this.countRenderer({ title: 'Followers', count: this.props.followers, onPress: this.onNavigateToFollowers }) }
					{ this.countRenderer({ title: 'Following', count: this.props.following, onPress: this.onNavigateToFollowing }) }
					{ this.countRenderer({ title: 'Likes', count: this.props.likes, onPress: this.onNavigateToLikes }) }
				</BoxBit>
			)
		}
	}
)
