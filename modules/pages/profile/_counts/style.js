import StyleSheet from 'coeur/libs/style.sheet'
// import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	container: {
		justifyContent: 'space-around',
	},
	box: {
		width: 70,
	},
	title: {
		marginBottom: 7,
	},
})
