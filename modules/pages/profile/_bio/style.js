import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	container: {
		padding: 16,
		paddingTop: 24,
		paddingBottom: 24,
	},
	job: {
		marginBottom: 11,
	},
	bio: {
		color: Colors.new.black.palette(4),
	},
	more: {
		color: Colors.new.yellow.palette(3),
	},
})
