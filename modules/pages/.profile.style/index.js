import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

import MeManager from 'app/managers/me';
import VolatileManager from 'app/managers/volatile';

import TrackingHandler from 'app/handlers/tracking';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import SourceSansBit from 'modules/bits/source.sans';
import HeaderBit from 'modules/bits/header';
import PageBit from 'modules/bits/page';

import ListHeaderLego from 'modules/legos/list.header';
import ListIconLego from 'modules/legos/list.icon';

import EmptyPagelet from 'modules/pagelets/empty';


import Styles from './style';


export default ConnectHelper(
	class ProfileStylePage extends PageModel {

		static routeName = 'profile.style'

		static stateToProps(state) {
			return {
				me: state.me,
				isCrashing: !state.me.token,
			}
		}

		header = {
			title: 'STYLE PROFILE',
			leftActions: [{
				type: HeaderBit.TYPES.BACK,
			}],
			rightActions: [{
				type: HeaderBit.TYPES.BLANK,
			}],
		}

		constructor(p) {
			super(p, {
				shouldDisplaySelection: false,
			}, [
				'onFillStyleProfile',
				'onShouldDisplaySelection',
			])

			if(!p.me.token) {
				this.navigator.top()
			}
		}

		componentWillMount() {
			if(VolatileManager.isExpired('me')) {
				MeManager.authenticate(this.props.me.token).then(() => {
					VolatileManager.mark('me')
				}).catch(() => {
					this.utilities.notification.show({
						title: 'Oops…',
						message: 'Looks like you\'ve been logged out of your session. Try logging in.',
					})

					this.navigator.top()

					MeManager.logout()
				})
			}
		}

		componentDidMount() {
			TrackingHandler.trackPageView(this.routeName)
		}

		onShouldDisplaySelection() {
			this.setState({
				shouldDisplaySelection: true,
			})
		}

		onFillStyleProfile(lang) {
			switch(lang) {
			case 'bahasa':
				this.navigator.navigate('webview', {
					title: 'STYLE PROFILE QUIZ',
					url: `https://helloyuna.typeform.com/to/${process.env.SPQ_ID_ID}?firstname=${ this.props.me.firstName }&userid=${ this.props.me.id }`,
				})
				break;
			case 'english':
			default:
				this.navigator.navigate('webview', {
					title: 'STYLE PROFILE QUIZ',
					url: `https://helloyuna.typeform.com/to/${process.env.SPQ_ID_EN}?firstname=${ this.props.me.firstName }&userid=${ this.props.me.id }`,
				})
				break;
			}
		}

		render() {
			return super.render(
				<PageBit
					header={<HeaderBit { ...this.header } />}
					contentContainerStyle={Styles.container}
				>
					{ this.props.me.isStyleProfileCompleted && !this.state.shouldDisplaySelection ? (
						<EmptyPagelet
							title="🎉 Congratulations…"
							description="You’ve completed your style profile! You’re now ready to be styled by a personal stylist-one step short of looking fabulous."
						>
							<ButtonBit
								title="UPDATE STYLE PROFILE"
								onPress={ this.onShouldDisplaySelection }
							/>
							<SourceSansBit type={SourceSansBit.TYPES.NOTE_1} style={Styles.note}>*requires you to fill out from beginning</SourceSansBit>
						</EmptyPagelet>
					) : (
						<BoxBit unflex>
							<ListHeaderLego title="SELECT LANGUAGE" />
							<ListIconLego isFirst
								title="Bahasa Indonesia"
								onPress={ this.onFillStyleProfile.bind(this, 'bahasa') }
							/>
							<ListIconLego
								title="English"
								onPress={ this.onFillStyleProfile.bind(this, 'english') }
							/>
						</BoxBit>
					) }
				</PageBit>
			)
		}
	}
)
