import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		backgroundColor: Colors.solid.grey.palette(1),
		paddingBottom: 24,
	},
	complete: {
		paddingTop: 0,
		paddingBottom: 0,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
	},
	header: {
		marginBottom: 8,
	},
	description: {
		textAlign: 'center',
	},
	note: {
		marginTop: 12,
	},
})
