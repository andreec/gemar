import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

import PostManager from 'app/managers/post';

import TrackingHandler from 'app/handlers/tracking';

// import Animated from 'coeur/libs/animated';

// import Colors from 'coeur/constants/color';
// import Defaults from 'coeur/constants/default';
import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
// import ImageBit from 'modules/bits/image';
// import SourceSansBit from 'modules/bits/source.sans';
import PageBit from 'modules/bits/page';
// import IconBit from 'modules/bits/icon';
// import GalleryBit from 'modules/bits/gallery';
import HeaderBit from 'modules/bits/header';
// import TouchableBit from 'modules/bits/touchable';
// import TextBit from 'modules/bits/text';
// import TextInputBit from 'modules/bits/text.input';

import ContentPart from './_content';
import TabPart from './_tab';

// import _ from 'lodash';

// import {
// 	BoxesPart,
// 	CountdownPart,
// 	PromoPart,
// } from '../matchbox'
// import FooterPart from './_footer';
// import HowPart from './_how';
// import IntroductionPart from './_introduction';

import Styles from './style';
import COLORS from '../../../utils/constants/color';


export default ConnectHelper(
	class PostsPage extends PageModel {

		static routeName = 'posts'

		constructor(p) {
			super(p, {
				isLoading: false,
				tabIndex: 0,
				postIds: [],
				hashtag: p.hashtag,
			}, [
				'getData',
				'onSearch',
				'onChangeTabIndex',
			]);

			this._pageHeaderHeight = 45 + 48 + Sizes.safe.top
			this._filters = ['', 'SOCIAL', 'MARKET', 'JOB']
			this._tabs = ['All', 'Social', 'Market Place', 'Job']
			this._header = {
				title: `#${p.hashtag && p.hashtag.toUpperCase()}`,
				leftActions: [{
					type: HeaderBit.TYPES.BACK,
				}],
				rightActions: [{
					type: HeaderBit.TYPES.BLANK,
				}],
			}
		}

		componentWillMount() {
			this.getData()
		}

		componentDidMount() {
			TrackingHandler.trackPageView(this.routeName)
		}

		getData() {
			if(!this.state.isLoading) {
				this.setState({
					isLoading: true,
				})

				const filter = this._filters[this.state.tabIndex]
					, tabIndex = this.state.tabIndex

				PostManager.explorePost({
					filter,
					offset: this.state.postIds.length,
				}).then(posts => {
					if(tabIndex === this.state.tabIndex) {
						this.setState({
							isLoading: false,
							postIds: [...this.state.postIds, ...posts.map(post => post.id)],
						})
					}
				})
			}
		}

		onSearch(e, val) {
			this.setState({
				search: val,
			})
		}

		onChangeTabIndex(tabIndex) {
			if(tabIndex !== this.state.tabIndex) {
				this.setState({
					tabIndex,
					postIds: [],
					isLoading: false,
				}, this.getData)
			}
		}

		headerRenderer() {
			return (
				<BoxBit unflex style={{
					width: Sizes.app.width,
					backgroundColor: COLORS.new.white.primary,
				}}>
					<HeaderBit { ...this._header } />
					<TabPart
						index={this.state.tabIndex}
						tabs={this._tabs}
						onChangeIndex={this.onChangeTabIndex}
						style={Styles.tabs}
					/>
				</BoxBit>
			)
		}

		render() {
			return super.render(
				<PageBit scroll={false}
					header={this.headerRenderer()}
					headerHeight={this._pageHeaderHeight}
				>
					<ContentPart
						isLoading={this.state.isLoading}
						postIds={this.state.postIds}
						onEndReached={this.getData}
					/>
				</PageBit>
			)
		}
	}
)
