import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	tabs: {
		paddingLeft: 15,
		paddingRight: 15,
		justifyContent: 'space-between',
		borderBottomWidth: 1,
		borderBottomColor: Colors.new.grey.palette(1),
		width: Sizes.app.width,
	},

	tab: {
		height: 47,
		flexShrink: 0,
	},

	tabActive: {
		height: 4,
		position: 'absolute',
		bottom: 0,
		left: 0,
		right: 0,
		backgroundColor: Colors.new.yellow.palette(3),
	},

	textActive: {
		color: Colors.new.yellow.palette(3),
	},

	textInactive: {
		color: Colors.new.black.palette(4),
	},
})
