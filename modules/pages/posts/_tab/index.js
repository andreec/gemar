import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

// import PageManager from 'app/managers/page';

// import TrackingHandler from 'app/handlers/tracking';

// import Animated from 'coeur/libs/animated';

// import Colors from 'coeur/constants/color';
// import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
// import IconBit from 'modules/bits/icon';
// import ImageBit from 'modules/bits/image';
// import GalleryBit from 'modules/bits/gallery';
// import PageBit from 'modules/bits/page';
import SourceSansBit from 'modules/bits/source.sans';
import TouchableBit from 'modules/bits/touchable';
// import TextBit from 'modules/bits/text';
// import TextInputBit from 'modules/bits/text.input';

import Styles from './style'


export default ConnectHelper(
	class TabPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				tabs: PropTypes.array,
				index: PropTypes.number,
				onChangeIndex: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			index: 0,
			headers: [],
		}

		constructor(p) {
			super(p, {
			}, [
				'onChangeIndex',
				'tabRenderer',
			]);
		}

		onChangeIndex(i) {
			this.props.onChangeIndex &&
			this.props.onChangeIndex(i)
		}

		tabRenderer(tab, i) {
			return (
				<TouchableBit enlargeHitSlop unflex key={i} centering style={Styles.tab} onPress={ this.onChangeIndex.bind(this, i) }>
					<BoxBit centering>
						<SourceSansBit type={SourceSansBit.TYPES.NEW_HEADER_2} weight={ this.props.index === i ? 'semibold' : 'normal' } style={ this.props.index === i ? Styles.textActive : Styles.textInactive }>{ tab }</SourceSansBit>
						{ this.props.index === i && (
							<BoxBit unflex style={Styles.tabActive} />
						) }
					</BoxBit>
				</TouchableBit>
			)
		}

		render() {
			return (
				<BoxBit unflex row style={[Styles.tabs, this.props.style]}>
					{ this.props.tabs.map(this.tabRenderer) }
				</BoxBit>
			)
		}
	}
)
