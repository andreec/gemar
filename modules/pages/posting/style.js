import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

const size = Sizes.app.width - (15 * 2)

export default StyleSheet.create({
	container: {
		width: Sizes.app.width,
		overflow: 'hidden',
	},
	mainImage: {
		width: size,
		height: size,
	},
	bigImage: {
		width: Math.round(size * .7),
		height: Math.round(size * .7),
	},
	effect: {
		width: 80,
		opacity: .7,
	},
	effectSelected: {
		opacity: 1,
	},
	effectImage: {
		width: 80,
		height: 80,
	},
	effectText: {
		color: Colors.new.black.palette(5),
		marginTop: 5,
	},
	effectGallery: {
		marginBottom: 35,
	},
	imageGallery: {
		marginTop: 15,
		marginBottom: 7,
	},
	progress: {
		marginTop: 50,
		marginBottom: 30,
	},
	title: {
		marginBottom: 15,
		marginLeft: 15,
	},
	tag: {
		paddingTop: 0,
		paddingBottom: 0,
	},
	input: {
		marginTop: 0,
	},
	inputContainer: {
		paddingRight: 15,
	},
	textInput: {
		paddingLeft: 15,
		paddingRight: 15,
	},
	form: {
		borderStyle: 'solid',
		borderColor: Colors.coal.palette(2),
		borderWidth: 0,
		borderTopWidth: StyleSheet.hairlineWidth,
	},
})
