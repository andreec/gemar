import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

import PostManager from 'app/managers/post';

import Animated from 'coeur/libs/animated';

import CloudinaryHandler from 'app/handlers/cloudinary';
// import TrackingHandler from 'app/handlers/tracking';

import Colors from 'coeur/constants/color';
import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
import GalleryBit from 'modules/bits/gallery';
import HeaderBit from 'modules/bits/header';
import ImageBit from 'modules/bits/image';
import PageBit from 'modules/bits/page';
import ProgressBit from 'modules/bits/progress';
import LoaderBit from 'modules/bits/loader';
import ScrollSnapBit from 'modules/bits/scroll.snap';
import ScrollViewBit from 'modules/bits/scroll.view';
import SourceSansBit from 'modules/bits/source.sans';
import TouchableBit from 'modules/bits/touchable';

import ListLego from 'modules/legos/list';
import ListContainerLego from 'modules/legos/list.container';
// import ListInputLego from 'modules/legos/list.input';

import CardPostComponent from 'modules/components/card.post'

import FormPagelet from 'modules/pagelets/form';

// import CloudinaryUploader from 'react-native-cloudinary-unsigned';

import EditorPart from './_editor'

import Styles from './style'

// const CLOUDINARY_CLOUD_NAME = 'hiandree';
// const CLOUDINARY_UPLOAD_PROFILE_NAME = 'h3wsaxnr';

// CloudinaryUploader.init(CLOUDINARY_CLOUD_NAME, CLOUDINARY_UPLOAD_PROFILE_NAME)

const size = Sizes.app.width - (15 * 2)
	, bigSize = Math.round(size * .7)


export default ConnectHelper(
	class PostingPage extends PageModel {

		static routeName = 'posting'

		static propTypes(PropTypes) {
			return {
				images: PropTypes.array,
			}
		}

		constructor(p) {
			super(p, {
				isDone: false,
				activeIndex: 0,
				images: [],
				effects: {},
				effectKeys: [],
				selectedEffectIndex: 0,
				overrideEffectIndex: [],
				postType: undefined,
				peopleTags: [],
				productTags: [],
			}, [
				'onNavigateToBack',
				'onNext',
				'onChangeEffect',
				'onChangeIndividualEffect',
				'onModalRequestClose',
				'onTagPeople',
				'onTagProduct',
				'onUpdate',
				'onSubmit',
				'bigImageRenderer',
				'effectRenderer',
				'thumbnailRenderer',
			]);

			this.header = {
				leftActions: [{
					type: HeaderBit.TYPES.BACK,
					onPress: this.onNavigateToBack,
				}],
			}

			this.__uploadWritten = []
			this.__downloadWritten = []
			this.__uploadTotal = []
			this.__downloadTotal = []
			this.__progress = new Animated.Value(0)

			this.__userData = {}
			this.__socialForm = [{
				id: 'caption',
				placeholder: 'Write your caption...',
				required: true,
				type: FormPagelet.TYPES.TEXTAREA,
				style: Styles.input,
				inputStyle: Styles.textInput,
				inputContainerStyle: Styles.inputContainer,
				value: this.__userData.caption,
			}]

			this.__marketForm = [{
				id: 'productName',
				required: true,
				placeholder: 'Product Name',
				type: FormPagelet.TYPES.INPUT,
				style: Styles.input,
				inputStyle: Styles.textInput,
				inputContainerStyle: Styles.inputContainer,
				value: this.__userData.productName,
			}, {
				id: 'productPrice',
				placeholder: 'Product Price',
				required: true,
				type: FormPagelet.TYPES.CURRENCY,
				style: Styles.input,
				inputStyle: Styles.textInput,
				inputContainerStyle: Styles.inputContainer,
				value: this.__userData.productPrice,
			}, {
				id: 'productWeight',
				placeholder: 'Product Weight (in Kg)',
				required: true,
				type: FormPagelet.TYPES.CURRENCY,
				style: Styles.input,
				inputStyle: Styles.textInput,
				inputContainerStyle: Styles.inputContainer,
				value: this.__userData.productWeight,
			}, {
				id: 'productWidth',
				placeholder: 'Product Width (in Cm)',
				required: true,
				type: FormPagelet.TYPES.CURRENCY,
				style: Styles.input,
				inputStyle: Styles.textInput,
				inputContainerStyle: Styles.inputContainer,
				value: this.__userData.productWidth,
			}, {
				id: 'productHeight',
				placeholder: 'Product Height (in Cm)',
				required: true,
				type: FormPagelet.TYPES.CURRENCY,
				style: Styles.input,
				inputStyle: Styles.textInput,
				inputContainerStyle: Styles.inputContainer,
				value: this.__userData.productHeight,
			}, {
				id: 'productLength',
				placeholder: 'Product Length (in Cm)',
				required: true,
				type: FormPagelet.TYPES.CURRENCY,
				style: Styles.input,
				inputStyle: Styles.textInput,
				inputContainerStyle: Styles.inputContainer,
				value: this.__userData.productLength,
			}, {
				id: 'productAvailability',
				placeholder: 'Product Availability',
				required: true,
				type: FormPagelet.TYPES.CURRENCY,
				style: Styles.input,
				inputStyle: Styles.textInput,
				inputContainerStyle: Styles.inputContainer,
				value: this.__userData.productAvailability,
			}, {
				id: 'caption',
				placeholder: 'Write your caption...',
				required: true,
				type: FormPagelet.TYPES.TEXTAREA,
				style: Styles.input,
				inputStyle: Styles.textInput,
				inputContainerStyle: Styles.inputContainer,
				value: this.__userData.caption,
			}, {
				id: 'productDescription',
				placeholder: 'Describe your product...',
				required: true,
				type: FormPagelet.TYPES.TEXTAREA,
				style: Styles.input,
				inputStyle: Styles.textInput,
				inputContainerStyle: Styles.inputContainer,
				value: this.__userData.productDescription,
			}]

			this.__jobForm = [{
				id: 'jobPosition',
				placeholder: 'Job Position',
				required: true,
				type: FormPagelet.TYPES.INPUT,
				style: Styles.input,
				inputStyle: Styles.textInput,
				inputContainerStyle: Styles.inputContainer,
				value: this.__userData.jobPosition,
			}, {
				id: 'jobCompany',
				placeholder: 'Company Name',
				required: true,
				type: FormPagelet.TYPES.INPUT,
				style: Styles.input,
				inputStyle: Styles.textInput,
				inputContainerStyle: Styles.inputContainer,
				value: this.__userData.jobCompany,
			}, {
				id: 'jobMinimumSallary',
				placeholder: 'Minimum Sallary',
				required: true,
				type: FormPagelet.TYPES.CURRENCY,
				style: Styles.input,
				inputStyle: Styles.textInput,
				inputContainerStyle: Styles.inputContainer,
				value: this.__userData.jobMinimumSallary,
			}, {
				id: 'jobMaximumSallary',
				placeholder: 'Maximum Sallary',
				required: true,
				type: FormPagelet.TYPES.CURRENCY,
				style: Styles.input,
				inputStyle: Styles.textInput,
				inputContainerStyle: Styles.inputContainer,
				value: this.__userData.jobMaximumSallary,
			}, {
				id: 'jobAvailability',
				placeholder: 'Available Position',
				required: true,
				type: FormPagelet.TYPES.CURRENCY,
				style: Styles.input,
				inputStyle: Styles.textInput,
				inputContainerStyle: Styles.inputContainer,
				value: this.__userData.jobAvailability,
			}, {
				id: 'caption',
				placeholder: 'Write your caption...',
				required: true,
				type: FormPagelet.TYPES.TEXTAREA,
				style: Styles.input,
				inputStyle: Styles.textInput,
				inputContainerStyle: Styles.inputContainer,
				value: this.__userData.caption,
			}]

			// TODO: textarea mention and hashtag detection
		}

		componentWillMount() {
		}

		componentDidMount() {
			// TrackingHandler.trackPageView(this.routeName)

			this.log(this.props.images)

			Promise.all(this.props.images.map((image, i) => {
				return CloudinaryHandler.upload({
					type: 'image/jpeg',
					uri: image,
					name: `image_${Date.now()}`,
				}).uploadProgress((written, total) => {
					this.onUploadProgress(i, written, total)
				}).progress((received, total) => {
					this.onDownloadProgress(i, received, total)
				})
			})).then(res => res.map(result => result.json())).then(results => {
				const effects = CloudinaryHandler.effects(`${results[0].public_id}.jpg`)

				this.setState({
					activeIndex: 1,
					images: results,
					effects,
					effectKeys: Object.keys(effects),
				})
			}).catch(err => {
				this.warn(err)

				this.utilities.notification.show({
					title: 'Oops..',
					message: 'Something went wrong. Please try again later.',
				})

				this.navigator.back()
			})

			// Promise.all(this.props.images.map(image => {
			// 	return CloudinaryUploader.upload(image)
			// })).then(result => {
			// 	console.log(result)
			// }).catch(err => {
			// 	console.warn(err)
			// })
		}

		onNavigateToBack() {
			if(this.state.activeIndex > 1) {
				this.setState({
					activeIndex: this.state.activeIndex - 1,
				})
			} else {
				this.navigator.back()
			}
		}

		onNext() {
			this.setState({
				activeIndex: this.state.activeIndex + 1,
			})
		}

		getHeader() {
			switch(this.state.activeIndex) {
			case 0:
			default:
				return {
					title: '',
					rightActions: [],
				}
			case 1:
				return {
					title: '',
					rightActions: [{
	   					type: HeaderBit.TYPES.TEXT,
	   					data: 'NEXT',
	   					isActive: true,
						onPress: this.onNext,
	   				}],
				}
			case 2:
				return {
					title: 'SHARE AS',
					rightActions: [{
						type: HeaderBit.TYPES.BLANK,
					}],
				}
			case 3:
				return {
					title: 'FINALISE',
					rightActions: [{
						type: HeaderBit.TYPES.TEXT,
						data: 'SHARE',
	   					isActive: this.state.isDone,
						onPress: this.onSubmit,
					}],
				}
			}
		}

		updateValue() {
			this.__progress.setValue(
				(
					this.__uploadWritten.reduce((sum, w) => {
						return sum + w
					}, 0)
					+
					this.__downloadWritten.reduce((sum, w) => {
						return sum + w
					}, 0)
				)
				/
				(
					this.__uploadTotal.reduce((sum, t) => {
						return sum + t
					}, 0)
					+
					this.__downloadTotal.reduce((sum, t) => {
						return sum + t
					}, 0)
				)
				*
				100
			)
		}

		onUploadProgress(index, written, total) {
			this.__uploadTotal[index] = parseInt(total, 10)
			this.__uploadWritten[index] = parseInt(written, 10)

			this.updateValue()
		}

		onDownloadProgress(index, written, total) {
			this.__downloadTotal[index] = parseInt(total, 10)
			this.__downloadWritten[index] = parseInt(written, 10)

			this.updateValue()
		}

		onChangeEffect(index) {
			this.setState({
				selectedEffectIndex: index,
			})
		}

		onChangeIndividualEffect(index, effectIndex) {
			const newEffects = this.state.overrideEffectIndex.slice()

			newEffects[index] = effectIndex

			this.setState({
				overrideEffectIndex: newEffects,
			}, this.onModalRequestClose)
		}

		onModalRequestClose() {
			this.utilities.alert.hide()
		}

		onEditImage(image, index) {
			this.utilities.alert.modal({
				component: (
					<EditorPart
						image={image}
						effects={ this.state.effects }
						effectKeys={ this.state.effectKeys }
						selectedEffectIndex={ this.state.overrideEffectIndex[index] || this.state.selectedEffectIndex }
						onDone={ this.onChangeIndividualEffect.bind(this, index) }
						onCancel={ this.onModalRequestClose }
					/>
				),
			})
		}

		onShareAs(type) {
			this.setState({
				activeIndex: this.state.activeIndex + 1,
				postType: type,
			})
		}

		onUpdate(isDone, {
			caption,
			productName,
			productPrice,
			productWeight,
			productWidth,
			productHeight,
			productLength,
			productAvailability,
			productDescription,
			jobPosition,
			jobCompany,
			jobMinimumSallary,
			jobMaximumSallary,
			jobAvailability,
		}) {

			this.__userData = {
				...this.__userData,
				caption: caption && caption.value,
				productName: productName && productName.value,
				productPrice: productPrice && productPrice.value,
				productWeight: productWeight && productWeight.value,
				productWidth: productWidth && productWidth.value,
				productHeight: productHeight && productHeight.value,
				productLength: productLength && productLength.value,
				productAvailability: productAvailability && productAvailability.value,
				productDescription: productDescription && productDescription.value,
				jobPosition: jobPosition && jobPosition.value,
				jobCompany: jobCompany && jobCompany.value,
				jobMinimumSallary: jobMinimumSallary && jobMinimumSallary.value,
				jobMaximumSallary: jobMaximumSallary && jobMaximumSallary.value,
				jobAvailability: jobAvailability && jobAvailability.value,
			}

			// TODO: mention detection

			this.setState({
				isDone,
			})
		}

		onTagPeople() {
			this.navigator.navigate('tag.people', {
				images: this.state.images.map((image, i) => {
					return CloudinaryHandler.effect(this.state.effectKeys[this.state.overrideEffectIndex[i] || this.state.selectedEffectIndex], `${image.public_id}.jpg`, {
						height: size,
						width: size,
					})
				}),
				tags: this.state.peopleTags,
				onDone: (tags) => {
					this.setState({
						peopleTags: tags,
					})
				},
			})
		}

		onTagProduct() {
			this.navigator.navigate('tag.product', {
				images: this.state.images.map((image, i) => {
					return CloudinaryHandler.effect(this.state.effectKeys[this.state.overrideEffectIndex[i] || this.state.selectedEffectIndex], `${image.public_id}.jpg`, {
						height: size,
						width: size,
					})
				}),
				tags: this.state.productTags,
				onDone: (tags) => {
					this.setState({
						productTags: tags,
					})
				},
				title: this.__userData.productName || 'Your Product Name',
			})
		}

		onSubmit() {
			// TODO
			if(this.state.activeIndex === 3) {
				if(this.state.isDone) {
					// final submit
					// Upload to server,
					this.setState({
						activeIndex: this.state.activeIndex + 1,
					})

					this.setState({
						activeIndex: this.state.activeIndex + 1,
					})

					PostManager.insert({
						type: this.state.postType,
						images: this.state.images.map((image, i) => {
							return CloudinaryHandler.effect(this.state.effectKeys[this.state.overrideEffectIndex[i] || this.state.selectedEffectIndex], `${image.public_id}.jpg`, {
								height: size * 3,
								width: size * 3,
							})
						}),
						tags: this.state.postType === 'MARKET' ? this.state.productTags : this.state.peopleTags,
						product: {
							title: this.__userData.productName,
							description: this.__userData.productDescription,
							price: this.__userData.productPrice,
							weight: this.__userData.productWeight,
							width: this.__userData.productWidth,
							length: this.__userData.productLength,
							height: this.__userData.productHeight,
							stock: this.__userData.productAvailability,
						},
						job: {
							position: this.__userData.jobPosition,
							company: this.__userData.jobCompany,
							minSallary: this.__userData.jobMinimumSallary,
							maxSallary: this.__userData.jobMaximumSallary,
							availability: this.__userData.jobAvailability,
						},
						caption: this.__userData.caption,
					})

					this.setTimeout(() => {
						this.navigator.reset('home')
					}, 2000)
				}
			} else {
				this.setState({
					activeIndex: this.state.activeIndex + 1,
				})
			}
		}

		loadingRenderer(isFinal) {
			return (
				<BoxBit unflex style={Styles.container}>
					<BoxBit centering>
						<LoaderBit />
						<BoxBit unflex style={Styles.effectGallery} />
						{ isFinal && (
							<SourceSansBit type={ SourceSansBit.TYPES.NEW_NOTE }>
								Please wait while we are uploading your post
							</SourceSansBit>
						) }
					</BoxBit>
				</BoxBit>
			)
		}

		postRenderer(postId) {
			return (
				<CardPostComponent
					id={postId}
					key={postId}
					type={ CardPostComponent.TYPES.MARKET }
				/>
			)
		}

		loaderRenderer() {
			return (
				<BoxBit unflex style={Styles.container}>
					<BoxBit centering>
						<LoaderBit />
					</BoxBit>
					<ProgressBit progress={ this.__progress } style={Styles.progress} />
				</BoxBit>
			)
		}

		bigImageRenderer(image, i) {
			return (
				<TouchableBit unflex onPress={ this.onEditImage.bind(this, image, i) }>
					<ImageBit key={this.state.overrideEffectIndex[i] || this.state.selectedEffectIndex} resize={ImageBit.TYPES.COVER}
						source={ CloudinaryHandler.effect(this.state.effectKeys[this.state.overrideEffectIndex[i] || this.state.selectedEffectIndex], `${image.public_id}.jpg`, {
							width: bigSize,
							height: bigSize,
						}) }
						style={Styles.bigImage}
					/>
				</TouchableBit>
			)
		}

		effectRenderer(key, i) {
			return (
				<TouchableBit unflex onPress={this.onChangeEffect.bind(this, i)} style={[Styles.effect, this.state.selectedEffectIndex === i && Styles.effectSelected]}>
					<ImageBit resize={ImageBit.TYPES.COVER} source={ this.state.effects[key] } style={Styles.effectImage} />
					<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_2} align="center" style={Styles.effectText}>{ key }</SourceSansBit>
				</TouchableBit>
			)
		}

		thumbnailRenderer(image, i) {
			return (
				<ImageBit key={this.state.overrideEffectIndex[i] || this.state.selectedEffectIndex} resize={ImageBit.TYPES.COVER}
					source={ CloudinaryHandler.effect(this.state.effectKeys[this.state.overrideEffectIndex[i] || this.state.selectedEffectIndex], `${image.public_id}.jpg`) }
					style={Styles.effectImage}
				/>
			)
		}

		multipleImageEditorRenderer() {
			// TODO
			return (
				<BoxBit unflex style={Styles.container}>
					<GalleryBit unflex
						gutter={8}
						padding={15}
						data={ this.state.images }
						renderItem={ this.bigImageRenderer }
					/>
					<BoxBit />
					<SourceSansBit type={SourceSansBit.TYPES.NEW_HEADER_2} style={Styles.title}>
						Apply filter to all photos
					</SourceSansBit>
					<GalleryBit unflex
						gutter={8}
						padding={15}
						data={ this.state.effectKeys }
						renderItem={ this.effectRenderer }
						style={Styles.effectGallery}
					/>
				</BoxBit>
			)
		}

		imageEditorRenderer() {
			return this.state.images.length ? this.state.images.length > 1 && this.multipleImageEditorRenderer() || (
				<BoxBit unflex style={Styles.container}>
					<BoxBit unflex centering>
						<ImageBit key={this.state.selectedEffectIndex} resize={ImageBit.TYPES.COVER}
							source={ CloudinaryHandler.effect(this.state.effectKeys[this.state.selectedEffectIndex], `${this.state.images[0].public_id}.jpg`, {
								width: size,
								height: size,
							}) }
							style={Styles.mainImage}
						/>
					</BoxBit>
					<BoxBit />
					<GalleryBit unflex
						gutter={8}
						padding={15}
						data={ this.state.effectKeys }
						renderItem={ this.effectRenderer }
						style={Styles.effectGallery}
					/>
				</BoxBit>
			) : this.loadingRenderer()
		}

		shareAsRenderer() {
			return this.state.activeIndex === 2 ? (
				<BoxBit unflex style={Styles.container}>
					<GalleryBit unflex
						gutter={8}
						padding={15}
						data={ this.state.images }
						renderItem={ this.thumbnailRenderer }
						style={Styles.imageGallery}
					/>
					<ListContainerLego component={ListLego} data={[{
						title: `${this.state.images.length} photos selected`,
					}]} />
					<ListContainerLego data={[{
						title: 'Share as Social Post',
						icon: 'next',
						iconSize: 16,
						onPress: this.onShareAs.bind(this, 'SOCIAL'),
					}, {
						title: 'Share as Market Place Post',
						icon: 'next',
						iconSize: 16,
						onPress: this.onShareAs.bind(this, 'MARKET'),
					}, {
						title: 'Share as Job Post',
						icon: 'next',
						iconSize: 16,
						onPress: this.onShareAs.bind(this, 'JOB'),
					}]} />
				</BoxBit>
			) : this.loadingRenderer()
		}

		finaliseRenderer() {
			return this.state.activeIndex === 3 ? (
				<ScrollViewBit style={Styles.container}>
					<GalleryBit unflex
						gutter={8}
						padding={15}
						data={ this.state.images }
						renderItem={ this.thumbnailRenderer }
						style={Styles.imageGallery}
					/>
					<ListContainerLego component={ListLego} data={[{
						title: `${this.state.images.length} photos selected`,
					}]} />
					<ListContainerLego key={`${this.state.peopleTags.length}${this.state.productTags.length}`} data={this.state.postType === 'SOCIAL' ? [{
						title: this.state.peopleTags.length ? `${this.state.peopleTags.length} people tagged` : 'Tag People',
						icon: 'next',
						iconSize: 16,
						onPress: this.onTagPeople,
					}] : this.state.postType === 'MARKET' && [{
						title: 'Tag Product',
						icon: this.state.productTags.length ? 'checkmark' : 'next',
						iconSize: 16,
						iconColor: this.state.productTags.length ? Colors.green.palette(3) : undefined,
						onPress: this.onTagProduct,
					}] || []} style={Styles.tag} />
					<FormPagelet
						config={ this.state.postType === 'SOCIAL' ? this.__socialForm : this.state.postType === 'MARKET' && this.__marketForm || this.__jobForm }
						onUpdate={ this.onUpdate }
						onSubmit={ this.onSubmit }
						style={ this.state.postType === 'JOB' && Styles.form || undefined }
					/>
				</ScrollViewBit>
			) : this.loadingRenderer()
		}

		render() {
			return super.render(
				<PageBit header={(
					<HeaderBit updater={`${this.state.activeIndex}${this.state.isDone}` } { ...this.header } { ...this.getHeader() } />
				)} scroll={false}>
					<ScrollSnapBit index={this.state.activeIndex} disableGesture>
						{ this.loaderRenderer() }
						{ this.imageEditorRenderer() }
						{ this.shareAsRenderer() }
						{ this.finaliseRenderer() }
						{ this.loadingRenderer(true) }
					</ScrollSnapBit>
				</PageBit>
			)
		}
	}
)
