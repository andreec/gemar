import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

const size = Sizes.app.width - (15 * 2)

export default StyleSheet.create({
	container: {
		width: Sizes.app.width,
	},
	mainImage: {
		width: size,
		height: size,
	},
	effect: {
		width: 80,
		opacity: .7,
	},
	effectSelected: {
		opacity: 1,
	},
	effectImage: {
		width: 80,
		height: 80,
	},
	effectText: {
		color: Colors.new.black.palette(5),
		marginTop: 5,
	},
	effectGallery: {
		marginBottom: 35,
	},
})
