import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

// import PageManager from 'app/managers/page';

import CloudinaryHandler from 'app/handlers/cloudinary';

// import Colors from 'coeur/constants/color';
import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
import GalleryBit from 'modules/bits/gallery';
import HeaderBit from 'modules/bits/header';
import ImageBit from 'modules/bits/image';
import PageBit from 'modules/bits/page';
import SourceSansBit from 'modules/bits/source.sans';
import TouchableBit from 'modules/bits/touchable';

// import CloudinaryUploader from 'react-native-cloudinary-unsigned';

import Styles from './style'

// const CLOUDINARY_CLOUD_NAME = 'hiandree';
// const CLOUDINARY_UPLOAD_PROFILE_NAME = 'h3wsaxnr';

// CloudinaryUploader.init(CLOUDINARY_CLOUD_NAME, CLOUDINARY_UPLOAD_PROFILE_NAME)

const size = Sizes.app.width - (15 * 2)


export default ConnectHelper(
	class EditorPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				image: PropTypes.object.isRequired,
				effects: PropTypes.object.isRequired,
				effectKeys: PropTypes.array.isRequired,
				selectedEffectIndex: PropTypes.number.isRequired,
				onDone: PropTypes.func.isRequired,
				onCancel: PropTypes.func.isRequired,
			}
		}

		constructor(p) {
			super(p, {
				effects: p.effects,
				selectedEffectIndex: p.selectedEffectIndex,
			}, [
				'onDone',
				'effectRenderer',
			]);

			this.header = {
				title: '',
				leftActions: [{
					type: HeaderBit.TYPES.BACK,
					onPress: p.onCancel,
				}],
				rightActions: [{
					type: HeaderBit.TYPES.TEXT,
					data: 'DONE',
					isActive: true,
					onPress: this.onDone,
				}],
			}
		}

		onDone() {
			this.props.onDone(this.state.selectedEffectIndex)
		}

		onChangeEffect(index) {
			this.setState({
				selectedEffectIndex: index,
			})
		}

		effectRenderer(key, i) {
			return (
				<TouchableBit unflex onPress={this.onChangeEffect.bind(this, i)} style={[Styles.effect, this.state.selectedEffectIndex === i && Styles.effectSelected]}>
					<ImageBit resize={ImageBit.TYPES.COVER} source={ this.props.effects[key] } style={Styles.effectImage} />
					<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_2} align="center" style={Styles.effectText}>{ key }</SourceSansBit>
				</TouchableBit>
			)
		}

		render() {
			return (
				<PageBit header={(
					<HeaderBit { ...this.header } />
				)} scroll={false}>
					<BoxBit unflex centering>
						<ImageBit key={this.state.selectedEffectIndex} resize={ImageBit.TYPES.COVER}
							source={ CloudinaryHandler.effect(this.props.effectKeys[this.state.selectedEffectIndex], `${this.props.image.public_id}.jpg`, {
								width: size,
								height: size,
							}) }
							style={Styles.mainImage}
						/>
					</BoxBit>
					<BoxBit />
					<GalleryBit
						gutter={8}
						padding={15}
						data={ this.props.effectKeys }
						renderItem={ this.effectRenderer }
						style={Styles.effectGallery}
					/>
				</PageBit>
			)
		}
	}
)
