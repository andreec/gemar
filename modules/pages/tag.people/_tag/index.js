import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';

import ProductManager from 'app/managers/product';
import UserManager from 'app/managers/user';

import CommonHelper from 'coeur/helpers/common';

import Animated from 'coeur/libs/animated';
// import TimeHelper from 'coeur/helpers/time';

import Colors from 'coeur/constants/color';
import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
import IconBit from 'modules/bits/icon';
import SourceSansBit from 'modules/bits/source.sans';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style'


export default ConnectHelper(
	class TagPart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				type: PropTypes.oneOf([
					'user',
					'product',
				]),
				disableMovement: PropTypes.bool,
				shouldShowCloseButton: PropTypes.bool,
				tag: PropTypes.shape({
					id: PropTypes.id,
					location: PropTypes.shape({
						x: PropTypes.number, // in percent
						y: PropTypes.number, // in percent
					}),
				}),
				onPress: PropTypes.func,
				onMove: PropTypes.func,
				onRemoveTag: PropTypes.func,

				width: PropTypes.number,
				height: PropTypes.number,
				tagHeight: PropTypes.number,
				tagWidth: PropTypes.number,
			}
		}

		static stateToProps(state, oP) {
			console.log(oP)
			const id = oP.tag.id
			let title = oP.tag.title || (oP.type === 'user' && 'Who\'s this?') || false

			if(id) {
				if(oP.type === 'product') {
					title = state.products.get(id) || ProductManager.get(id)
					title = title && title.title
				} else {
					title = state.users.get(id) || UserManager.get(id)
					title = title && `@${title.username}` || 'Who\'s this?'
				}
			}

			return {
				title,
			}
		}

		static defaultProps = {
			tag: {},
			width: Sizes.app.width - ( Sizes.margin.thick * 2 ),
			height: Sizes.app.width - ( Sizes.margin.thick * 2 ),
			tagHeight: 44,
			tagWidth: 70,
		}

		constructor(p) {
			super(p, {
				x: p.tag.location.x,
				y: p.tag.location.y,
				tagHeight: p.type === 'user' ? p.tagHeight : 64,
				tagWidth: p.type === 'user' ? p.tagWidth : 110,
				isDimensionSet: false,
			}, [
				'setOwnDimension',
			]);

			this._isCloseButtonShown = false
			this._arrowHeight = 6
			this._animationY = new Animated.Value()
			this._animationValue = new Animated.ValueXY({
				x: this.state.x,
				y: this.state.y,
			})

			const tagWidth = this.state.tagWidth
				, halfTagWidth = tagWidth / 2
				, halfTagWidthInPercent = halfTagWidth / p.width
				, onePixelInPercent = 1 / p.height
				, tagHeight = this.state.tagHeight - this._arrowHeight
				, tagHeightInPercent = tagHeight / p.height

			this._precisionInterpolation = {
				transform: [{
					translateX: this._animationValue.x.interpolate({
						inputRange: [0, halfTagWidthInPercent, 1 - halfTagWidthInPercent, 1],
						outputRange: [0, -halfTagWidth, -halfTagWidth, -tagWidth],
					}),
				}, {
					translateY: this._animationValue.y.interpolate({
						inputRange: [0, tagHeightInPercent, tagHeightInPercent + onePixelInPercent, 1],
						outputRange: [-this._arrowHeight, -this._arrowHeight, -tagHeight, -tagHeight],
					}),
				}],
			}

			this._containerInterpolation = {
				transform: [{
					translateX: this._animationValue.x.interpolate({
						inputRange: [0, 1],
						outputRange: [0, p.width],
					}),
				}, {
					translateY: this._animationValue.y.interpolate({
						inputRange: [0, 1],
						outputRange: [0, p.height],
					}),
				}],
			}

			this._arrowTopInterpolation = {
				opacity: this._animationValue.y.interpolate({
					inputRange: [0, tagHeightInPercent, tagHeightInPercent + onePixelInPercent, 1],
					outputRange: [1, 1, 0, 0],
				}),
				transform: [{
					translateX: this._animationValue.x.interpolate({
						inputRange: [0, halfTagWidthInPercent, 1 - halfTagWidthInPercent, 1],
						outputRange: [0, halfTagWidth, halfTagWidth, tagWidth],
					}),
				}],
			}

			this._arrowBottomInterpolation = {
				opacity: this._animationValue.y.interpolate({
					inputRange: [0, tagHeightInPercent, tagHeightInPercent + onePixelInPercent, 1],
					outputRange: [0, 0, 1, 1],
				}),
				transform: [{
					translateX: this._animationValue.x.interpolate({
						inputRange: [0, halfTagWidthInPercent, 1 - halfTagWidthInPercent, 1],
						outputRange: [0, halfTagWidth, halfTagWidth, tagWidth],
					}),
				}],
			}

			this._panResponder = this.props.disableMovement ? {} : {

				onMoveShouldSetPanResponder: (e, {dx, dy}) => {
					return dx > 5 || dx < -5 || dy > 5 || dy < -5
				},
				onPanResponderMove: (e, gh) => {
					this.props.onMove &&
					this.props.onMove()

					this._animationValue.setValue({
						x: CommonHelper.clamp(this.state.x + (gh.dx / p.width), 0, 1),
						y: CommonHelper.clamp(this.state.y + (gh.dy / p.height), 0, 1),
					})
				},
				onPanResponderRelease: (e, gH) => {
					this.setState({
						x: CommonHelper.clamp(this.state.x + (gH.dx / p.width), 0, 1),
						y: CommonHelper.clamp(this.state.y + (gH.dy / p.height), 0, 1),
					}, () => {
						this.props.tag.location.x = this.state.x
						this.props.tag.location.y = this.state.y
					})
				},
				// onPanResponderTerminate,
				// onPanResponderTerminationRequest,
			}
		}

		setOwnDimension(e) {
			if(this.state.tagWidth !== e.nativeEvent.layout.width || this.state.tagHeight !== e.nativeEvent.layout.height) {
				const tagWidth = e.nativeEvent.layout.width
					, halfTagWidth = tagWidth / 2
					, halfTagWidthInPercent = halfTagWidth / this.props.width
					, onePixelInPercent = 1 / this.props.height
					, tagHeight = e.nativeEvent.layout.height - this._arrowHeight
					, tagHeightInPercent = tagHeight / this.props.height

				this._precisionInterpolation = {
					transform: [{
						translateX: this._animationValue.x.interpolate({
							inputRange: [0, halfTagWidthInPercent, 1 - halfTagWidthInPercent, 1],
							outputRange: [0, -halfTagWidth, -halfTagWidth, -tagWidth],
						}),
					}, {
						translateY: this._animationValue.y.interpolate({
							inputRange: [0, tagHeightInPercent, tagHeightInPercent + onePixelInPercent, 1],
							outputRange: [-this._arrowHeight, -this._arrowHeight, -tagHeight, -tagHeight],
						}),
					}],
				}

				this._arrowTopInterpolation = {
					opacity: this._animationValue.y.interpolate({
						inputRange: [0, tagHeightInPercent, tagHeightInPercent + onePixelInPercent, 1],
						outputRange: [1, 1, 0, 0],
					}),
					transform: [{
						translateX: this._animationValue.x.interpolate({
							inputRange: [0, halfTagWidthInPercent, 1 - halfTagWidthInPercent, 1],
							outputRange: [0, halfTagWidth, halfTagWidth, tagWidth],
						}),
					}],
				}

				this._arrowBottomInterpolation = {
					opacity: this._animationValue.y.interpolate({
						inputRange: [0, tagHeightInPercent, tagHeightInPercent + onePixelInPercent, 1],
						outputRange: [0, 0, 1, 1],
					}),
					transform: [{
						translateX: this._animationValue.x.interpolate({
							inputRange: [0, halfTagWidthInPercent, 1 - halfTagWidthInPercent, 1],
							outputRange: [0, halfTagWidth, halfTagWidth, tagWidth],
						}),
					}],
				}

				this.setState({
					tagWidth: e.nativeEvent.layout.width,
					tagHeight: e.nativeEvent.layout.height,
					isDimensionSet: true,
				})
			} else {
				this.setState({
					isDimensionSet: true,
				})
			}
		}

		render() {
			this.log(this.props, this.state)
			return (
				<BoxBit unflex animated {...this._panResponder}
					style={[Styles.container, this._containerInterpolation]}
				>
					<BoxBit unflex animated style={[Styles.content, this.state.isDimensionSet && Styles.opaque, this._precisionInterpolation]} onLayout={this.setOwnDimension}>
						<TouchableBit unflex onPress={this.props.onPress}>
							<BoxBit unflex animated style={[Styles.arrowTop, this._arrowTopInterpolation]} />
							<BoxBit unflex style={[Styles.tag, this.props.type === 'product' && Styles.productTag]}>
								<SourceSansBit ellipsis={ this.props.type === 'product' } numberOfLines={ this.props.type === 'product' ? 2 : undefined } accessible={false} type={SourceSansBit.TYPES.NEW_NOTE_2}>
									{this.props.title}
								</SourceSansBit>
							</BoxBit>
							<BoxBit unflex animated style={[Styles.arrowBottom, this._arrowBottomInterpolation]} />
							{ this.props.shouldShowCloseButton && (
								<TouchableBit onPress={this.props.onRemoveTag} unflex centering style={Styles.circle}>
									<IconBit
										size={12}
										name="close"
										color={Colors.new.white.palette(1)}
									/>
								</TouchableBit>
							) }
						</TouchableBit>
					</BoxBit>
				</BoxBit>
			)
		}
	}
)
