import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

// import PageManager from 'app/managers/page';

import PageContext from 'coeur/contexts/page'

// import SearchHandler from 'app/handlers/search';

// import TimeHelper from 'coeur/helpers/time';

// import Colors from 'coeur/constants/color';
import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
// import ButtonBit from 'modules/bits/button';
// import HeaderBit from 'modules/bits/header';
// import IconBit from 'modules/bits/icon';
import ImageBit from 'modules/bits/image';
// import LoaderBit from 'modules/bits/loader';
// import PageBit from 'modules/bits/page';
// import ScrollViewBit from 'modules/bits/scroll.view';
// import SourceSansBit from 'modules/bits/source.sans';
import TouchableBit from 'modules/bits/touchable';
// import TextBit from 'modules/bits/text';
// import TextInputBit from 'modules/bits/text.input';

import TagPart from '../_tag'

import Styles from './style'


export default ConnectHelper(
	class ContainerPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				type: PropTypes.oneOf([
					'user',
					'product',
				]),
				shouldShowTags: PropTypes.bool,
				viewOnly: PropTypes.bool,
				tags: PropTypes.arrayOf(PropTypes.shape({
					id: PropTypes.id,
					location: PropTypes.shape({
						x: PropTypes.number, // in percent
						y: PropTypes.number, // in percent
					}),
				})),
				image: PropTypes.image,
				onAddTag: PropTypes.func,
				onRemoveTag: PropTypes.func,
				onCancelAddTag: PropTypes.func,

			}
		}

		static contexts = [
			PageContext,
		]

		static defaultProps = {
			type: 'user',
			viewOnly: false,
			tags: [],
		}

		constructor(p) {
			super(p, {
				shouldShowTags: p.viewOnly ? false : true,
				lastSelectedIndex: -1,
			}, [
				'onAddTag',
				'onRemoveTag',
				'onPress',
				'onMove',
				'tagRenderer',
			]);

			this._size = Sizes.app.width - (Sizes.margin.thick * 2)
		}

		componentDidMount() {
			// TODO
		}

		onAddTag(e) {
			if(this.props.viewOnly) {
				this.setState({
					shouldShowTags: !this.state.shouldShowTags,
				})
			} else {
				const newTag = {
					location: {
						x: e.nativeEvent.locationX / this._size,
						y: e.nativeEvent.locationY / this._size,
					},
				}

				this.props.onAddTag &&
				this.props.onAddTag(newTag)
			}
		}

		onRemoveTag(index) {
			// this.setState({
			// 	tags: this.state.tags.slice().splice(index, 1),
			// })

			this.props.onRemoveTag &&
			this.props.onRemoveTag(index)
		}

		onPress(tag, index) {
			if(this.props.viewOnly) {
				if(this.props.type === 'product') {
					this.props.page.navigator.navigate('post', {
						productId: tag.id,
					})
				} else {
					this.props.page.navigator.navigate('user.profile', {
						id: tag.id,
					})
				}
			} else {
				if (tag.id) {
					this.setState({
						lastSelectedIndex: index,
					})
				}

				const unknownTagIndex = this.props.tags.findIndex(t => !t.id)

				if (unknownTagIndex > -1) {
					this.props.onRemoveTag &&
					this.props.onRemoveTag(unknownTagIndex)
				}

				this.props.onCancelAddTag &&
				this.props.onCancelAddTag()
			}
		}

		onMove() {
			if (this.state.lastSelectedIndex !== -1) {
				this.setState({
					lastSelectedIndex: -1,
				})
			}
		}

		tagRenderer(tag, i) {
			return (
				<TagPart tag={tag} key={tag.location.x || i}
					type={this.props.type}
					disableMovement={ this.props.viewOnly }
					shouldShowCloseButton={ !this.props.viewOnly && this.state.lastSelectedIndex === i }
					onPress={ this.onPress.bind(this, tag, i) }
					onMove={ this.onMove }
					onRemoveTag={ this.onRemoveTag.bind(this, i) }
				/>
			)
		}

		render() {
			return (
				<BoxBit unflex accessible={ this.props.shouldShowTags === undefined ? undefined : false }>
					<TouchableBit activeOpacity={1} unflex onPress={this.onAddTag}>
						<ImageBit
							resizeMode={ImageBit.TYPES.COVER}
							source={this.props.image}
							style={Styles.mainImage}
						/>
					</TouchableBit>
					{ (this.props.shouldShowTags === undefined ? this.state.shouldShowTags : this.props.shouldShowTags) && this.props.tags.map(this.tagRenderer) }
				</BoxBit>
			)
		}
	}
)
