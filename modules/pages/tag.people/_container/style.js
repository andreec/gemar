import StyleSheet from 'coeur/libs/style.sheet'

// import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	mainImage: {
		width: Sizes.app.width - (Sizes.margin.thick * 2),
		height: Sizes.app.width - (Sizes.margin.thick * 2),
	},
})
