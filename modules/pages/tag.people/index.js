import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

// import PageManager from 'app/managers/page';

// import SearchHandler from 'app/handlers/search';
import TrackingHandler from 'app/handlers/tracking';

import ProfileService from 'app/services/profile';

// import TimeHelper from 'coeur/helpers/time';

import Colors from 'coeur/constants/color';
import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
// import ButtonBit from 'modules/bits/button';
import HeaderBit from 'modules/bits/header';
import IconBit from 'modules/bits/icon';
import ImageBit from 'modules/bits/image';
// import LoaderBit from 'modules/bits/loader';
import PageBit from 'modules/bits/page';
import ScrollSnapBit from 'modules/bits/scroll.snap';
import ScrollViewBit from 'modules/bits/scroll.view';
import SourceSansBit from 'modules/bits/source.sans';
import TouchableBit from 'modules/bits/touchable';
// import TextBit from 'modules/bits/text';
import TextInputBit from 'modules/bits/text.input';

import ContainerPart from './_container'

import Styles from './style'

import _ from 'lodash';

const ProfileImage = ImageBit.resolve('profile.png')


export default ConnectHelper(
	class TagPeoplePage extends PageModel {

		static routeName = 'tag.people'

		static propTypes(PropTypes) {
			return {
				tags: PropTypes.arrayOf(PropTypes.shape({
					location: PropTypes.shape({
						x: PropTypes.number, // in percent
						y: PropTypes.number, // in percent
					}),
					id: PropTypes.id,
				})),
				images: PropTypes.arrayOf(PropTypes.image),
				onDone: PropTypes.function,
			}
		}

		static defaultProps = {
			tags: [],
			images: [
				'https://res.cloudinary.com/hiandree/image/upload/v1539438197/dummies/dummy-user1-img3.jpg',
				'https://res.cloudinary.com/hiandree/image/upload/v1539438205/dummies/dummy-user3-img1.jpg',
			],
		}

		header = {
			title: 'Tag People',
			leftActions: [{
				type: HeaderBit.TYPES.BACK,
			}],
			rightActions: [{
				type: HeaderBit.TYPES.OK,
				data: {
					color: Colors.primary,
				},
				onPress: this.onDone,
				// data: 'DONE',
				// isActive: true,
			}],
		}

		constructor(p) {
			super(p, {
				currentIndex: 0,
				tags: p.tags,
				userIdOnTags: p.tags.map(tags => {
					return tags.map(tag => tag.id)
				}),
				selecting: false,
				users: [],
				search: '',
			}, [
				'getData',

				'onAddTag',
				'onCancelAddTag',
				'onRemoveTag',

				'onChangeIndex',
				'onSearch',
				'onSearchFinal',
				'onSelectUser',

				'onDone',

				'userRenderer',
				'containerRenderer',
			]);

			this._width = Sizes.app.width - (Sizes.margin.thick * 2)

			this._search = ''

			this.onSearch = _.debounce(this.onSearch, 100)
		}

		componentWillMount() {
			// TODO: Load Comment
		}

		componentDidMount() {
			TrackingHandler.trackPageView(this.routeName)
		}

		getData() {
			if(this._search !== '') {
				const search = this._search
				ProfileService.search({
					search,
					count: 20,
				}).then(users => {
					if(this._search === search) {
						this.setState({
							users,
						})
					}
				})
			}
		}

		onAddTag(newTag) {
			if(this.state.tags[this.state.currentIndex] === undefined) {
				this.state.tags[this.state.currentIndex] = []
			}

			if (this.state.tags[this.state.currentIndex].length && !this.state.tags[this.state.currentIndex][this.state.tags[this.state.currentIndex].length - 1].id) {
				// there is empty tags
				const newTags = this.state.tags.slice()
				newTags[this.state.currentIndex] = this.state.tags[this.state.currentIndex].slice().splice(0, this.state.tags[this.state.currentIndex].length - 1).concat(newTag)

				this.setState({
					tags: newTags,
					isAdding: true,
				})
			} else {
				const newTags = this.state.tags.slice()
				newTags[this.state.currentIndex] = this.state.tags[this.state.currentIndex].concat(newTag)

				this.setState({
					tags: newTags,
					isAdding: true,
				})
			}
		}

		onCancelAddTag() {
			this.setState({
				isAdding: false,
			})
		}

		onRemoveTag(index) {
			const newTags = this.state.tags.slice()
			const newCurrentTags = this.state.tags[this.state.currentIndex].slice()

			newCurrentTags.splice(index, 1)
			newTags[this.state.currentIndex] = newCurrentTags

			this.setState({
				tags: newTags,
			})
		}

		onChangeIndex(e, index) {
			this.setState({
				currentIndex: index,
			})
		}

		onSearch(e, val) {
			this._search = val

			if(val !== '') {
				this.setState({
					search: val,
				}, this.getData)
			} else {
				this.setState({
					search: val,
					users: [],
				})
			}
		}

		onSearchFinal(val) {
			this._search = val

			if(val !== '') {
				this.setState({
					search: '',
				}, this.getData)
			} else {
				this.setState({
					search: '',
					users: [],
				})
			}
		}

		onSelectUser(user) {
			const newTags = this.state.tags.slice()
				, newCurrentTags = this.state.tags[this.state.currentIndex].slice()
				, lastTag = newCurrentTags.pop()
				, tags = newCurrentTags.concat([{
					...lastTag,
					id: user.id,
				}])

			newTags[this.state.currentIndex] = tags

			this.setState({
				tags: newTags,
				userIdOnTags: newTags.map(ts => {
					return ts.map(tag => tag.id)
				}),
				users: [],
				search: '',
				isAdding: false,
			})
		}

		onDone() {
			this.navigator.back()

			this.props.onDone &&
			this.props.onDone(this.state.tags)
		}

		headerRenderer() {
			return this.state.isAdding ? (
				<BoxBit unflex row style={Styles.header}>
					<IconBit
						name="search"
						style={{
							marginRight: 8,
						}}
					/>
					<TextInputBit unflex={false} autofocus
						placeholder={'Type to search'}
						onChange={ this.onSearch }
						onSubmitEditing={ this.onSearchFinal }
						style={{
							borderBottomWidth: 0,
						}}
					/>
				</BoxBit>
			) : (
				<HeaderBit {...this.header} />
			)
		}

		userRenderer(user, i) {
			return this.state.userIdOnTags[this.state.currentIndex] && this.state.userIdOnTags[this.state.currentIndex].indexOf(user.id) > -1 ? false : (
				<TouchableBit unflex row key={i} onPress={ this.onSelectUser.bind(this, user) } style={Styles.user}>
					<ImageBit resizeMode={ImageBit.TYPES.COVER} source={ user.profile || ProfileImage } style={Styles.image} />
					<BoxBit unflex>
						<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_2}>{ user.name }</SourceSansBit>
						<SourceSansBit type={SourceSansBit.TYPES.NEW_NOTE} style={Styles.username}>@{ user.username }</SourceSansBit>
					</BoxBit>
				</TouchableBit>
			)
		}

		containerRenderer(image, i) {
			return (
				<ContainerPart
					key={ i }
					type={'user'}
					tags={this.state.tags[i]}
					image={image}
					onAddTag={this.onAddTag}
					onRemoveTag={this.onRemoveTag}
					onCancelAddTag={this.onCancelAddTag}
				/>
			)
		}

		render() {
			return super.render(
				<PageBit scroll={false}
					type={PageBit.TYPES.THICK}
					header={ this.headerRenderer() }
				>
					{ this.props.images.length > 1 ? (
						<ScrollSnapBit unflex width={this._width} snapToInterval={this._width} index={ this.state.currentIndex } onMomentumScrollEnd={ this.onChangeIndex }>
							{ this.props.images.map(this.containerRenderer) }
						</ScrollSnapBit>
					) : this.containerRenderer(this.props.images[0], 0)  }

					<BoxBit centering>
						<SourceSansBit align="center" type={SourceSansBit.TYPES.NEW_PARAGRAPH_2} style={Styles.note}>
							Tap photo to tag people
							{ '\n' }
							Drag to move, or tap to delete
							{ this.props.images.length > 1 ? '\nSwipe to tag other photo' : '' }
						</SourceSansBit>
					</BoxBit>

					{ !!this.state.users.length && (
						<ScrollViewBit type={ScrollViewBit.TYPES.THICK} style={Styles.users}>
							{ this.state.users.map(this.userRenderer) }
							{ !!this.state.search && (
								<TouchableBit row unflex onPress={ this.onSearchFinal.bind(this, this.state.search) } style={Styles.search}>
									<IconBit name="search" size={24} style={Styles.searchIcon} />
									<SourceSansBit type={SourceSansBit.TYPES.NEW_PARAGRAPH_2}>Search for "{ this.state.search }"</SourceSansBit>
								</TouchableBit>
							) }
						</ScrollViewBit>
					) }
				</PageBit>
			)
		}
	}
)
