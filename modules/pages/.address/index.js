import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

import AddressPagelet from 'modules/pagelets/address';


export default ConnectHelper(
	class AddressPage extends PageModel {

		static routeName = 'address'

		constructor(p) {
			super(p, {}, [
				'onNavigateToBack',
			])
		}

		onNavigateToBack() {
			this.navigator.back()
		}

		render() {
			return super.render(
				<AddressPagelet
					onRequestClose={ this.onNavigateToBack }
				/>
			)
		}
	}
)
