import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		backgroundColor: Colors.solid.grey.palette(1),
	},

	contentContainer: {
		flexGrow: 1,
	},

	content: {
		width: Sizes.screen.width,
	},

	header: {
		padding: 24,
	},

	empty: {
		color: Colors.black.palette(2),
	},

	button: {
		width: 80,
		marginLeft: 8,
	},

	address: {
		paddingTop: 16,
		paddingBottom: 16,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
		backgroundColor: Colors.white.primary,
	},

	mark: {
		marginTop: 16,
	},

	footnote: {
		color: Colors.green.palette(3),
		marginLeft: 8,
	},
})
