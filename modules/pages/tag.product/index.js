import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

// import PageManager from 'app/managers/page';

// import SearchHandler from 'app/handlers/search';
import TrackingHandler from 'app/handlers/tracking';

// import TimeHelper from 'coeur/helpers/time';

import Colors from 'coeur/constants/color';
import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
// import ButtonBit from 'modules/bits/button';
import HeaderBit from 'modules/bits/header';
// import ImageBit from 'modules/bits/image';
// import LoaderBit from 'modules/bits/loader';
import PageBit from 'modules/bits/page';
import ScrollSnapBit from 'modules/bits/scroll.snap';
import SourceSansBit from 'modules/bits/source.sans';
// import TouchableBit from 'modules/bits/touchable';
// import TextBit from 'modules/bits/text';

import ContainerPart from '../tag.people/_container'

import Styles from './style'


export default ConnectHelper(
	class TagProductPage extends PageModel {

		static routeName = 'tag.product'

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				title: PropTypes.string,
				tags: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.shape({
					id: PropTypes.id,
					location: PropTypes.shape({
						x: PropTypes.number, // in percent
						y: PropTypes.number, // in percent
					}),
				}))),
				images: PropTypes.arrayOf(PropTypes.image),
				onDone: PropTypes.func,
			}
		}

		static defaultProps = {
			// id: 2,
			title: 'Product name goes here',
			tags: [],
			images: [
				'https://res.cloudinary.com/hiandree/image/upload/v1539438197/dummies/dummy-user1-img3.jpg',
				'https://res.cloudinary.com/hiandree/image/upload/v1539438205/dummies/dummy-user3-img1.jpg',
			],
		}

		header = {
			title: 'Tag Product',
			leftActions: [{
				type: HeaderBit.TYPES.BACK,
			}],
			rightActions: [{
				type: HeaderBit.TYPES.OK,
				data: {
					color: Colors.primary,
				},
				onPress: this.onDone,
				// data: 'DONE',
				// isActive: true,
			}],
		}

		constructor(p) {
			super(p, {
				currentIndex: 0,
				tags: p.tags,
			}, [
				'onDone',
				'onAddTag',
				'onRemoveTag',
				'onChangeIndex',
				'containerRenderer',
			]);

			this._width = Sizes.app.width - (Sizes.margin.thick * 2)
		}

		componentWillMount() {
			// TODO: Load Comment
		}

		componentDidMount() {
			TrackingHandler.trackPageView(this.routeName)
		}

		onDone() {
			this.navigator.back()

			this.props.onDone &&
			this.props.onDone(this.state.tags)
		}

		onAddTag(newTag) {
			if (this.state.tags[this.state.currentIndex] === undefined) {
				this.state.tags[this.state.currentIndex] = []
			}

			const newTags = this.state.tags.slice()
			newTags[this.state.currentIndex] = [{
				id: this.props.id,
				title: this.props.title,
				...newTag,
			}]

			this.setState({
				tags: newTags,
			})
		}

		onRemoveTag(index) {
			const newTags = this.state.tags.slice()
			const newCurrentTags = this.state.tags[this.state.currentIndex].slice()

			newCurrentTags.splice(index, 1)
			newTags[this.state.currentIndex] = newCurrentTags

			this.setState({
				tags: newTags,
			})
		}

		onChangeIndex(e, index) {
			this.setState({
				currentIndex: index,
			})
		}

		containerRenderer(image, i) {
			return (
				<ContainerPart
					key={i}
					type={'product'}
					tags={this.state.tags[i]}
					image={image}
					onAddTag={this.onAddTag}
					onRemoveTag={this.onRemoveTag}
				/>
			)
		}

		render() {
			return super.render(
				<PageBit scroll={false}
					type={PageBit.TYPES.THICK}
					header={<HeaderBit {...this.header} />}
				>

					{this.props.images.length > 1 ? (
						<ScrollSnapBit unflex width={this._width} snapToInterval={this._width} index={this.state.currentIndex} onMomentumScrollEnd={this.onChangeIndex}>
							{this.props.images.map(this.containerRenderer)}
						</ScrollSnapBit>
					) : this.containerRenderer(this.props.images[0], 0)}

					<BoxBit centering>
						<SourceSansBit align="center" type={SourceSansBit.TYPES.NEW_PARAGRAPH_2} style={Styles.note}>
							Tap photo to add tag
							{ '\n' }
							Drag to move, or tap tag to delete
						</SourceSansBit>
					</BoxBit>
				</PageBit>
			)
		}
	}
)
