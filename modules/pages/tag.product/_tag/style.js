import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	container: {
		position: 'absolute',
		top: 0,
		left: 0,
	},
	content: {
		// overflow: 'hidden',
	},
	circle: {
		position: 'absolute',
		top: 4,
		right: -8,
		overflow: 'hidden',
		borderRadius: 8,
		width: 16,
		height: 16,
		backgroundColor: Colors.new.black.palette(1),
	},
	tag: {
		width: 110,
		height: 40,
		paddingTop: 6,
		paddingBottom: 6,
		paddingLeft: 7,
		paddingRight: 7,
		// borderRadius: 3,
		backgroundColor: Colors.new.white.palette(2),
	},
	arrowTop: {
		marginLeft: -6,
		width: 0,
		height: 0,
		backgroundColor: Colors.transparent,
		borderStyle: 'solid',
		borderLeftWidth: 6,
		borderRightWidth: 6,
		borderBottomWidth: 12,
		borderLeftColor: Colors.transparent,
		borderRightColor: Colors.transparent,
		borderBottomColor: Colors.new.white.palette(2),
	},
	arrowBottom: {
		marginLeft: -6,
		width: 0,
		height: 0,
		backgroundColor: Colors.transparent,
		borderStyle: 'solid',
		borderLeftWidth: 6,
		borderRightWidth: 6,
		borderTopWidth: 12,
		borderLeftColor: Colors.transparent,
		borderRightColor: Colors.transparent,
		borderTopColor: Colors.new.white.palette(2),
	},
})
