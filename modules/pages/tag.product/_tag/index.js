import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import CommonHelper from 'coeur/helpers/common';

import Animated from 'coeur/libs/animated';
// import TimeHelper from 'coeur/helpers/time';
import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
import SourceSansBit from 'modules/bits/source.sans';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style'


export default ConnectHelper(
	class TagPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				tag: PropTypes.shape({
					location: PropTypes.shape({
						x: PropTypes.number, // in percent
						y: PropTypes.number, // in percent
					}),
				}),
				title: PropTypes.string,
				onDelete: PropTypes.func,
				width: PropTypes.number,
				height: PropTypes.number,
				tagHeight: PropTypes.number,
			}
		}

		static defaultProps = {
			tag: {},
			width: Sizes.app.width - ( Sizes.margin.thick * 2 ),
			height: Sizes.app.width - ( Sizes.margin.thick * 2 ),
			tagHeight: 64,
			tagWidth: 110,
		}

		constructor(p) {
			super(p, {
				x: p.tag.location.x,
				y: p.tag.location.y,
				tagHeight: p.tagHeight,
				tagWidth: p.tagWidth,
			}, [
				'onTapTag',
				'setOwnDimension',
			]);

			this._arrowHeight = 6
			this._animationY = new Animated.Value()
			this._animationValue = new Animated.ValueXY({
				x: this.state.x,
				y: this.state.y,
			})

			const tagWidth = this.state.tagWidth
				, halfTagWidth = tagWidth / 2
				, halfTagWidthInPercent = halfTagWidth / p.width
				, onePixelInPercent = 1 / p.height
				, tagHeight = this.state.tagHeight - this._arrowHeight
				, tagHeightInPercent = tagHeight / p.height

			this._precisionInterpolation = {
				transform: [{
					translateX: this._animationValue.x.interpolate({
						inputRange: [0, halfTagWidthInPercent, 1 - halfTagWidthInPercent, 1],
						outputRange: [0, -halfTagWidth, -halfTagWidth, -tagWidth],
					}),
				}, {
					translateY: this._animationValue.y.interpolate({
						inputRange: [0, tagHeightInPercent, tagHeightInPercent + onePixelInPercent, 1],
						outputRange: [-this._arrowHeight, -this._arrowHeight, -tagHeight, -tagHeight],
					}),
				}],
			}

			this._containerInterpolation = {
				transform: [{
					translateX: this._animationValue.x.interpolate({
						inputRange: [0, 1],
						outputRange: [0, p.width],
					}),
				}, {
					translateY: this._animationValue.y.interpolate({
						inputRange: [0, 1],
						outputRange: [0, p.height],
					}),
				}],
			}

			this._arrowTopInterpolation = {
				opacity: this._animationValue.y.interpolate({
					inputRange: [0, tagHeightInPercent, tagHeightInPercent + onePixelInPercent, 1],
					outputRange: [1, 1, 0, 0],
				}),
				transform: [{
					translateX: this._animationValue.x.interpolate({
						inputRange: [0, halfTagWidthInPercent, 1 - halfTagWidthInPercent, 1],
						outputRange: [0, halfTagWidth, halfTagWidth, tagWidth],
					}),
				}],
			}

			this._arrowBottomInterpolation = {
				opacity: this._animationValue.y.interpolate({
					inputRange: [0, tagHeightInPercent, tagHeightInPercent + onePixelInPercent, 1],
					outputRange: [0, 0, 1, 1],
				}),
				transform: [{
					translateX: this._animationValue.x.interpolate({
						inputRange: [0, halfTagWidthInPercent, 1 - halfTagWidthInPercent, 1],
						outputRange: [0, halfTagWidth, halfTagWidth, tagWidth],
					}),
				}],
			}

			this._panResponder = {
				onMoveShouldSetPanResponder: (e, {dx, dy}) => {
					return dx > 5 || dx < -5 || dy > 5 || dy < -5
				},
				onPanResponderMove: (e, gh) => {
					this._animationValue.setValue({
						x: CommonHelper.clamp(this.state.x + (gh.dx / p.width), 0, 1),
						y: CommonHelper.clamp(this.state.y + (gh.dy / p.height), 0, 1),
					})
				},
				onPanResponderRelease: (e, gH) => {
					this.setState({
						x: CommonHelper.clamp(this.state.x + (gH.dx / p.width), 0, 1),
						y: CommonHelper.clamp(this.state.y + (gH.dy / p.height), 0, 1),
					})
				},
				// onPanResponderTerminate,
				// onPanResponderTerminationRequest,
			}
		}

		onTapTag() {
			this.props.onDelete &&
			this.props.onDelete()
		}

		setOwnDimension(e) {
			if(this.state.tagWidth !== e.nativeEvent.layout.width || this.state.tagHeight !== e.nativeEvent.layout.height) {
				const tagWidth = e.nativeEvent.layout.width
					, halfTagWidth = tagWidth / 2
					, halfTagWidthInPercent = halfTagWidth / this.props.width
					, onePixelInPercent = 1 / this.props.height
					, tagHeight = e.nativeEvent.layout.height - this._arrowHeight
					, tagHeightInPercent = tagHeight / this.props.height

				this._precisionInterpolation = {
					transform: [{
						translateX: this._animationValue.x.interpolate({
							inputRange: [0, halfTagWidthInPercent, 1 - halfTagWidthInPercent, 1],
							outputRange: [0, -halfTagWidth, -halfTagWidth, -tagWidth],
						}),
					}, {
						translateY: this._animationValue.y.interpolate({
							inputRange: [0, tagHeightInPercent, tagHeightInPercent + onePixelInPercent, 1],
							outputRange: [-this._arrowHeight, -this._arrowHeight, -tagHeight, -tagHeight],
						}),
					}],
				}

				this._arrowTopInterpolation = {
					opacity: this._animationValue.y.interpolate({
						inputRange: [0, tagHeightInPercent, tagHeightInPercent + onePixelInPercent, 1],
						outputRange: [1, 1, 0, 0],
					}),
					transform: [{
						translateX: this._animationValue.x.interpolate({
							inputRange: [0, halfTagWidthInPercent, 1 - halfTagWidthInPercent, 1],
							outputRange: [0, halfTagWidth, halfTagWidth, tagWidth],
						}),
					}],
				}

				this._arrowBottomInterpolation = {
					opacity: this._animationValue.y.interpolate({
						inputRange: [0, tagHeightInPercent, tagHeightInPercent + onePixelInPercent, 1],
						outputRange: [0, 0, 1, 1],
					}),
					transform: [{
						translateX: this._animationValue.x.interpolate({
							inputRange: [0, halfTagWidthInPercent, 1 - halfTagWidthInPercent, 1],
							outputRange: [0, halfTagWidth, halfTagWidth, tagWidth],
						}),
					}],
				}

				this.setState({
					tagWidth: e.nativeEvent.layout.width,
					tagHeight: e.nativeEvent.layout.height,
				})
			}
		}

		render() {
			return (
				<BoxBit unflex animated {...this._panResponder}
					style={[Styles.container, this._containerInterpolation]}
				>
					<BoxBit unflex animated style={[Styles.content, this._precisionInterpolation]} onLayout={this.setOwnDimension}>
						<TouchableBit unflex onPress={this.onTapTag}>
							<BoxBit unflex animated style={[Styles.arrowTop, this._arrowTopInterpolation]} />
							<BoxBit unflex style={Styles.tag}>
								<SourceSansBit ellipsis numberOfLines={2} accessible={false} type={SourceSansBit.TYPES.NEW_NOTE_2}>
									{this.props.title}
								</SourceSansBit>
							</BoxBit>
							<BoxBit unflex animated style={[Styles.arrowBottom, this._arrowBottomInterpolation]} />
						</TouchableBit>
					</BoxBit>
				</BoxBit>
			)
		}
	}
)
