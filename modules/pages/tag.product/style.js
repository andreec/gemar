import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	mainImage: {
		width: Sizes.app.width - ( Sizes.margin.thick * 2 ),
		height: Sizes.app.width - ( Sizes.margin.thick * 2 ),
	},
	note: {
		// marginTop: 5,
		color: Colors.new.black.palette(4),
	},
	users: {
		position: 'absolute',
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
		backgroundColor: Colors.new.white.palette(1, .9),
	},
	user: {
		paddingTop: 5,
		paddingBottom: 5,
		marginTop: 5,
		marginBottom: 5,
	},
	image: {
		width: 32,
		height: 32,
		borderRadius: 16,
		overflow: 'hidden',
		marginRight: 8,
	},
})
