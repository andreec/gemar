import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

export default ConnectHelper(
	class AuthPage extends PageModel {
		static stateToProps(state) {
			return {
				isAuthorized: !!state.me.loginToken,
			}
		}
		
		constructor(p) {
			super(p, {}, [])
			
			this.verifyAuth()
		}
		
		verifyAuth() {
			this.navigator.navigate(this.props.isAuthorized ? 'RootNavigator' : 'Auth')
		}
		
		render() {
			return false
		}
	}
)
