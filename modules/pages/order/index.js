import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';
import TimeHelper from 'coeur/helpers/time';

import OrderManager from 'app/managers/order';
import UserManager from 'app/managers/user';
import ProductManager from 'app/managers/product';

import TrackingHandler from 'app/handlers/tracking';

import BoxBit from 'modules/bits/box';
import ImageBit from 'modules/bits/image';
import HeaderBit from 'modules/bits/header';
import PageBit from 'modules/bits/page';

import ListHeaderLego from 'modules/legos/list.header';
import CardCartComponent from 'modules/components/card.cart';
import StatusHeaderLego from 'modules/legos/status.header';

import BodyPart from './_body';
import FooterPart from './_footer';

// const ProfileImage = ImageBit.resolve('profile.png')

import Styles from './style';

export default ConnectHelper(
	class OrderPage extends PageModel {

		static routeName = 'order'

		static stateToProps(state, oP) {
			const id = oP.id
				, order = state.orders.get(id) || OrderManager.get(id)
				, seller = order.seller_id && (state.users.get(order.seller_id) || UserManager.get(order.seller_id)) || new UserManager._record()
				, products = order.productIds && order.productIds.map(pId => state.products.get(pId) || ProductManager.get(pId) || new ProductManager._record()) || []

			return {
				id: oP.id,
				order,
				seller,
				products,
				me: state.me,
			}
		}

		header = {
			title: 'ORDER DETAIL',
			leftActions: [{
				type: HeaderBit.TYPES.BACK,
			}],
			rightActions: [{
				type: HeaderBit.TYPES.BLANK,
			}],
		}

		constructor(p) {
			super(p, {
			}, [
				'onNavigateToBack',
			]);
		}

		componentWillMount() {
		}

		componentDidMount() {
			TrackingHandler.trackPageView(this.routeName)
		}

		onNavigateToBack() {
			this.navigator.back()
		}

		onNavigateToSeller() {
			this.navigator.navigate('user.profile', {
				id: this.props.seller.id,
			})
		}

		cartCardRenderer(cartItem) {
			return (
				<CardCartComponent key={cartItem.id} id={cartItem.id} />
			)
		}

		onNavigateToTracking = () => {
			this.navigator.navigate('tracking', {
				id: this.props.id,
			})
		}

		render() {
			return super.render(
				<PageBit header={( <HeaderBit { ...this.header } /> )} contentContainerStyle={Styles.content}>
					<ListHeaderLego
						title={`ORDER ID: #${this.props.id}`}
						date={TimeHelper.moment()}
						style={Styles.orderId}
						inputStyle={Styles.darkGrey}
					/>

					<StatusHeaderLego
						status="WAITING PAYMENT"
						date={TimeHelper.moment()}
						onPress={ this.onNavigateToTracking }
					/>
					<BodyPart />
					<FooterPart />
				</PageBit>
			)
		}
	}
)
