import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	container: {
		paddingTop: 10 + Sizes.safe.top,
		paddingBottom: 11,
		alignItems: 'center',
	},

	image: {
		width: 30,
		height: 30,
		borderRadius: 14,
		marginRight: 7,
		overflow: 'hidden',
	},

	menu: {
		width: 24,
		height: 24,
		marginLeft: 7,
		marginRight: 7,
	},

	back: {
		marginRight: 10,
	},

	sub: {
		color: Colors.new.grey.palette(3),
	},

	content: {
		backgroundColor: Colors.new.grey.palette(1),
	},

	orderId: {
		paddingTop: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderTopWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.new.black.palette(3),
		backgroundColor: Colors.white.primary,
	},
	darkGrey: {
		color: Colors.new.black.palette(3),
	},

})
