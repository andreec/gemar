import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/connected.stateful';
// import PageManager from 'app/managers/page';

// import TrackingHandler from 'app/handlers/tracking';

// import Animated from 'coeur/libs/animated';

import BoxBit from 'modules/bits/box';
import FormHeaderLego  from 'modules/legos/form.header';
import AwbLego from 'modules/legos/awb';
import SourceSansBit from 'modules/bits/source.sans';

import CardCartComponent from 'modules/components/card.cart';

import LoaderBit from 'modules/bits/loader';
import ListHeaderLego from 'modules/legos/list.header';
import AddressPart from './_address';

import { isEmpty } from 'lodash';
import Styles from './style'

export default ConnectHelper(
	class HeaderPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				status: PropTypes.string,
				style: PropTypes.style,
			}
		}

		static stateToProps(state) {
			return {
				me: state.me,
				// cartItems: state.posts.keySeq().toJS().reverse(),
				cartItems: [{id: 1, quantity: 1}],
				total: state.me.cartItems.reduce((sum, cartItem) => {
					const post = state.posts.get(cartItem.id)
						, product = state.products.get(post.refId) || { price: 0 }
					return sum + (product.price * cartItem.quantity)
				}, 0),
			}
		}


		static defaultProps = {
			status: 'UNREAD',
		}

		constructor(p) {
			super(p, {
				data: [{
					title: 'Seller',
					answer: '@master.thinker',
				}, {
					title: 'Shipping Address',
					children: (
						<BoxBit unflex>
							<SourceSansBit type={SourceSansBit.TYPES.NEW_PARAGRAPH_3} style={Styles.header}>Victor Sembehda</SourceSansBit>
							<SourceSansBit type={SourceSansBit.TYPES.NEW_PARAGRAPH_3}>08923487234</SourceSansBit>
							<SourceSansBit type={SourceSansBit.TYPES.NEW_PARAGRAPH_3}>Jln. Prof Dr. Satrio</SourceSansBit>
							<SourceSansBit type={SourceSansBit.TYPES.NEW_PARAGRAPH_3}>Karet Semanggi</SourceSansBit>
							<SourceSansBit type={SourceSansBit.TYPES.NEW_PARAGRAPH_3}>Jakarta Selatan, 19320</SourceSansBit>
						</BoxBit>
					),
				}, {
					title: 'Order Date',
					answer: '-',
				}, {
					title: 'Courier Service',
					answer: 'JNE Reguler',
				}],
			});
		}


		cartCardRenderer(cartItem) {
			return (
				// SHOULD CHANGE STATIC CARTITEMS STATE TO PROPS
				<CardCartComponent
					key={cartItem.id}
					id={cartItem.id}
				/>
			)
		}
		

		render() {
			return (
				<BoxBit style={Styles.container}>
					<BoxBit unflex style={Styles.content}>
						<FormHeaderLego
							chunk={2}
							data={this.state.data}
						/>
						<AwbLego number="Test123" style={Styles.awb}/>

						<BoxBit unflex style={ Styles.summary }>
							{ this.props.cartItems.map(this.cartCardRenderer)}
						</BoxBit>
					</BoxBit>
				</BoxBit>
			)
		}
	}
)
