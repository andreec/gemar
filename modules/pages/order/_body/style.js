import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	container: {
		paddingTop: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
	},
	content: {
		paddingTop: Sizes.margin.default,
		paddingLeft: Sizes.margin.default,
		paddingRight: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
		backgroundColor: Colors.white.primary,
	},

	header: {
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderBottomColor: Colors.new.black.palette(3),
	},

	jobId: {
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderTopWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.new.black.palette(3),
	},

	awb: {
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.new.black.palette(3),
	},
})
