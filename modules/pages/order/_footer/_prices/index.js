import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import FormatHelper from 'coeur/helpers/format';

import BoxBit from 'modules/bits/box';
import BoxLoadingBit from 'modules/bits/box.loading';
import SourceSansBit from 'modules/bits/source.sans';
// import LoaderBit from 'modules/bits/loader';

import Styles from './style';


export default ConnectHelper(
	class PricesPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				price: PropTypes.number.isRequired,
				discount: PropTypes.number.isRequired,
				additionalDiscount: PropTypes.number,
				shipping: PropTypes.number.isRequired,
				total: PropTypes.number.isRequired,
				isLoading: PropTypes.bool,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			price: 0,
			discount: 0,
			shipping: 0,
			total: 0,
		}

		render() {
			return (
				<BoxBit unflex style={Styles.container}>
					<BoxBit unflex row style={[Styles.border]}>
						<BoxBit>
							<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_1_ALT_2} weight="medium" style={[Styles.prices, Styles.pale]}>Subtotal</SourceSansBit>
							<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_1_ALT_2} weight="medium" style={[Styles.prices, Styles.pale]}>Shipping</SourceSansBit>
							{ this.props.discount > 0 && (
								<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_1_ALT_2} weight="medium" style={[Styles.prices, Styles.pale]}>Discount</SourceSansBit>
							)}
							{ this.props.additionalDiscount > 0 && (
								<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_1_ALT_2} weight="medium" style={[Styles.prices, Styles.pale]}>Add. Discount</SourceSansBit>
							)}
						</BoxBit>
						<BoxBit style={Styles.price}>
							<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_2} weight="normal" style={[Styles.prices, Styles.bold]}>IDR { FormatHelper.currencyFormat(this.props.price) }</SourceSansBit>

							{ this.props.isLoading ? (
								<React.Fragment>
									<BoxLoadingBit unflex width={72} height={14} style={Styles.loader} />
									<BoxLoadingBit unflex width={72} height={14} style={Styles.loader} />
									<BoxLoadingBit unflex width={72} height={14} style={Styles.loader} />
								</React.Fragment>
							) : (
								<React.Fragment>
									<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_2} weight="normal" style={[Styles.prices, Styles.bold]}>IDR {FormatHelper.currencyFormat(this.props.shipping)}</SourceSansBit>
									{ this.props.discount > 0 && (
										<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_2} weight="normal" style={[Styles.prices, Styles.bold]}>-IDR {FormatHelper.currencyFormat(this.props.discount)}</SourceSansBit>
									)}
									{ this.props.additionalDiscount > 0 && (
										<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_2} weight="normal" style={[Styles.prices, Styles.bold]}>-IDR {FormatHelper.currencyFormat(this.props.additionalDiscount)}</SourceSansBit>
									)}
								</React.Fragment>
							) }
						</BoxBit>
					</BoxBit>
					<BoxBit unflex row style={Styles.padder}>
						<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_1_ALT_2} weight="medium" style={[Styles.prices]}>Total</SourceSansBit>
						<BoxBit />
						<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_2} weight="medium" style={[Styles.prices]}>IDR { FormatHelper.currencyFormat(this.props.total) }</SourceSansBit>
					</BoxBit>
				</BoxBit>
			)
		}
	}
)
