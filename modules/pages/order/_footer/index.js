import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';
import TimeHelper from 'coeur/helpers/time';
// import PageManager from 'app/managers/page';

// import TrackingHandler from 'app/handlers/tracking';

// import Animated from 'coeur/libs/animated';

import BoxBit from 'modules/bits/box';
// import ImageBit from 'modules/bits/image';
import LoaderBit from 'modules/bits/loader';
import SourceSansBit from 'modules/bits/source.sans';

import PricesPart from './_prices';
import Styles from './style'


export default ConnectHelper(
	class BodyPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				status: PropTypes.string,
			}
		}

		static defaultProps = {
			status: 'UNREAD',
		}

		constructor(p) {
			super(p, {
			}, [
			]);
		}

		componentDidMount() {
		}

		render() {
			return (
				<PricesPart
					price={ 20000 || this.state.totalAmount }
					discount={ 20000 || this.state.totalDiscount }
					shipping={ 20000 || this.state.totalShipping }
					total={ 20000 || this.state.total }
					isLoading={ this.state.isUpdating }
					style={ Styles.prices }
				/>
			)
		}
	}
)
