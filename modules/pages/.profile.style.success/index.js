import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

import MeManager from 'app/managers/me';

import TrackingHandler from 'app/handlers/tracking';

import EmptyPagelet from 'modules/pagelets/empty';


export default ConnectHelper(
	class StyleSuccessPage extends PageModel {

		static routeName = 'profile.style.success'

		constructor(p) {
			super(p, {}, [
				'updateData',
			])
		}

		componentDidMount() {
			// TODO: redirect to order detail after getting the new order
			TrackingHandler.trackPageView(this.routeName)
			this.updateData()
		}

		updateData() {
			MeManager.authenticate(MeManager.state.token).then(user => {
				if(user.isStyleProfileCompleted) {
					this.utilities.notification.show({
						type: this.utilities.notification.TYPES.SUCCESS,
						title: '🎉',
						message: 'Yeay, your style profile is successfully updated',
					})

					this.navigator.navigate('profile')
				} else {
					this.setTimeout(this.updateData, 3000)
				}
			}).catch(() => {
				this.utilities.notification.show({
					title: 'Oops…',
					message: 'Looks like you\'ve been logged out of your session. Try logging in.',
				})

				this.navigator.top()

				MeManager.logout()
			})
		}

		// onNavigateToHome() {
		// 	this.navigator.top()
		// }
		//
		// onNavigateToBack() {
		// 	this.navigator.back()
		// }

		render() {
			return super.render(
				<EmptyPagelet
					title="Getting your style profile…"
					description="Please wait."
				/>
			)
		}
	}
)
