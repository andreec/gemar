import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

// import PageManager from 'app/managers/page';
import MeManager from 'app/managers/me'

// import Colors from 'coeur/constants/color';
// import Sizes from 'coeur/constants/size';

import PageBit from 'modules/bits/page';

import CardPostComponent from 'modules/components/card.post'

import Styles from './style'


export default ConnectHelper(
	class HomePage extends PageModel {

		static routeName = 'landing'

		static stateToProps(state) {
			return {
				token: state.me.token,
				postIds: state.posts.keySeq().toJS().reverse(),
			}
		}

		static defaultProps = {
			postIds: []
		}

		constructor(p) {
			super(p, {
			}, [
			]);

			// if(!p.token) {
			// 	this.navigator.reset('signup')
			// }
		}

		componentWillMount() {
		}

		componentDidMount() {
			if(!MeManager.state.id) {
				MeManager.update({
					id: 999999,
				})
			}
		}

		postRenderer(postId) {
			return (
				<CardPostComponent
					id={postId}
					key={postId}
				/>
			)
		}

		render() {
			return super.render(
				<PageBit contentContainerStyle={Styles.container}>
					{ this.props.postIds.map(this.postRenderer) }
				</PageBit>
			)
		}
	}
)
