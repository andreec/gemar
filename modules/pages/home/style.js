import StyleSheet from 'coeur/libs/style.sheet'
// import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	container: {
		paddingTop: 20,
		paddingLeft: 15,
		paddingRight: 15,
		paddingBottom: 20,
	},
})
