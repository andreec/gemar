import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		width: Sizes.screen.width,
		height: '100vh',
		background: Colors.white.primary,
	},
	contentContainer: {
		flexGrow: 1,
	},
})
