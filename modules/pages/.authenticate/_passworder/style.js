import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	title: {
		color: Colors.black.palette(4),
	},
	form: {
		marginBottom: 24,
	},
})
