import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import UtilitiesContext from 'coeur/contexts/utilities';

import MeManager from 'app/managers/me';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import PageBit from 'modules/bits/page';

import FormPagelet from 'modules/pagelets/form';

import HeaderPart from '../_header'

import Styles from './style';


export default ConnectHelper(
	class PassworderPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string,
				token: PropTypes.string,
				email: PropTypes.string,
				utilities: PropTypes.object,
				onSuccess: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				isDone: false,
				isSending: false,
			}, [
				'checkPassword',
				'onUpdateData',
				'onSavePassword',
			])

			this.userData = {}
		}

		checkPassword(value) {
			const password = this.userData.password && this.userData.password.value
			return value === password
		}

		onUpdateData(isDone, data) {
			this.userData = {
				...this.userData,
				...data,
			}

			if(this.state.isDone !== isDone) {
				this.setState({
					isDone,
				})
			}
		}

		onSavePassword() {
			this.setState({
				isSending: true,
			}, () => {
				MeManager.changePasswordByToken({
					password1: this.userData.password.value,
					password2: this.userData.repeatPassword.value,
					token: this.props.token,
					email: this.props.email,
				}).then(() => {
					this.props.utilities.notification.show({
						type: this.props.utilities.notification.TYPES.SUCCESS,
						title: '🎉',
						message: 'Welcome to Yuna!',
					})

					this.props.onSuccess &&
					this.props.onSuccess()

				}).catch(err => {
					if(err && (err.code === '034' || err.code === '013')) {
						// passwords doesnt match
						this.props.utilities.notification.show({
							title: 'Oops…',
							message: 'Passwords doesn\'t match',
							timeout: 3000,
						})

						this.setState({
							isSending: false,
						})
					} else if(err && err.code === '031') {
						// token invalidated, maybe sudah terpakai?
						this.setState({
							needToSetPassword: false,
							isSending: false,
							type: err.detail === null ? null : this.state.type,
						})
					} else {
						this.props.utilities.alert.show({
							title: 'Oops…',
							message: 'Something wen\'t wrong, please try again later.',
							actions: [{
								title: 'OK',
								onPress: () => {
									this.navigator.top()
								},
							}],
						})
					}
				})
			})
		}


		titleRenderer(type) {
			switch(type) {
			case 'reset':
				return 'Time for a change'
			case 'verification':
			default:
				return 'One more step and\nyou are in.'
			}
		}

		errorRenderer(type) {
			switch(type) {
			case 'reset':
			case 'verification':
				return 'The token is expired. Your token link will only valid for 24 hours. (psst, it\'s for security measures 😉)'
			case null:
			default:
				return 'Either the link is invalid or your token has been used. If you are not sure what to do next, please contact our '
			}
		}

		render() {
			return (
				<BoxBit unflex type={PageBit.TYPES.THICK} style={this.props.style}>
					<HeaderPart
						title={ this.props.title }
						description={ 'Please choose a password for this account' }
					/>
					<FormPagelet
						config={[{
							id: 'password',
							isRequired: true,
							type: FormPagelet.TYPES.PASSWORD,
						}, {
							id: 'repeatPassword',
							isRequired: true,
							title: 'CONFIRM PASSWORD',
							type: FormPagelet.TYPES.PASSWORD,
							validator: this.checkPassword,
						}]}
						onUpdate={ this.onUpdateData }
						style={Styles.form}
					/>
					<ButtonBit
						type={ ButtonBit.TYPES.SHAPES.PROGRESS }
						size={ ButtonBit.TYPES.SIZES.COMPACT }
						theme={ ButtonBit.TYPES.THEMES.CONFIRMATION }
						title={ this.state.type === 'reset' ? 'RESET PASSWORD' : 'SIGN ME IN' }
						state={ this.state.isDone ? this.state.isSending && ButtonBit.TYPES.STATES.LOADING || ButtonBit.TYPES.STATES.NORMAL : ButtonBit.TYPES.STATES.DISABLED }
						onPress={ this.onSavePassword }
					/>
				</BoxBit>
			)
		}
	}
)
