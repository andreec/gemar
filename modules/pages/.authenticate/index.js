import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

import MeManager from 'app/managers/me';
import TrackingHandler from 'app/handlers/tracking';

import PageBit from 'modules/bits/page';
import ScrollSnapBit from 'modules/bits/scroll.snap';

import EmptyPagelet from 'modules/pagelets/empty';
import FourOFourPagelet from 'modules/pagelets/four.o.four';

import HeaderPart from './_header'
import PassworderPart from './_passworder'

import Styles from './style';

import QS from 'query-string';

export {
	HeaderPart,
	PassworderPart,
}


export default ConnectHelper(
	class AuthenticatePage extends PageModel {

		static routeName = 'authenticate'

		static propTypes(PropTypes) {
			return {
				token: PropTypes.string,
				email: PropTypes.string,
				redirect: PropTypes.string,
				me: PropTypes.object,
			}
		}

		static stateToProps(state, oP) {

			if(oP.location.search) {
				const query = QS.parse(oP.location.search)

				return {
					token: query.token,
					email: query.email,
					redirect: query.redirect,
					me: state.me,
				}
			}

			return {
				isCrashing: true,
			}
		}

		constructor(p) {
			super(p, {
				activeIndex: 0,
				type: undefined,
			}, [
				'onNavigateToHome',
				'onNavigateToBack',
			])
		}

		componentDidMount() {
			TrackingHandler.trackPageView(this.routeName)

			if(this.props.token && this.props.email) {
				MeManager.verifyToken({
					token: this.props.token,
					email: this.props.email,
				}).then(({
					needToSetPassword,
					type,
				}) => {
					if(needToSetPassword) {
						this.setState({
							activeIndex: 1,
							type,
						})
					} else {
						switch(type) {
						case 'verification':
						case 'styleprofile':
							MeManager.verifyUser({
								token: this.props.token,
								email: this.props.email,
							}).then(user => {
								this.utilities.notification.show({
									type: this.utilities.notification.TYPES.SUCCESS,
									title: '🎉',
									message: `Welcome back, ${user.firstName}!`,
								})

								this.setTimeout(this.onNavigateToHome, 100)
							})

							break;

						case 'reset':
							// NOTE: this should be impossible
							this.setState({
								activeIndex: 1,
								type,
							})

							break;
						}
					}
				}).catch(err => {
					if(err && err.code === '031') {
						if(err.detail) {
							// token exist but not valid anymore
							this.navigator.navigate('verify', {
								email: this.props.email,
								redirect: this.props.redirect,
								type: err.detail.type,
								title: 'Oops…',
								description: 'This link is already expired. Token links will only valid for 24 hours (psst, it\'s for your own security 😉). I can help you resending the email if you want.',
								button: 'RESEND EMAIL',
							})

						} else {
							// token not exist
							this.utilities.alert.show({
								title: 'Oops…',
								description: 'Either the link is invalid or your token has been used. If you are not sure what to do next, please contact our customer support',
								actions: [{
									title: 'OKAY',
									onPress: () => {
										this.navigator.top()
									},
								}],
							})
						}
					} else {
						this.onSomethingWentWrong()
					}
				})
			}
		}

		onNavigateToHome() {
			if(this.props.redirect) {
				this.navigator.navigate(this.props.redirect)
			} else {
				this.navigator.top()
			}
		}

		onNavigateToBack() {
			this.navigator.back()
		}

		onSomethingWentWrong() {
			this.utilities.alert.show({
				title: 'Oops…',
				message: 'Something wen\'t wrong, please try again later.',
				actions: [{
					title: 'OK',
					onPress: () => {
						this.navigator.top()
					},
				}],
			})
		}

		__errorRenderer() {
			return (
				<FourOFourPagelet />
			)
		}

		titleRenderer(type) {
			switch(type) {
			case 'reset':
				return 'Time for a change'
			case 'verification':
			default:
				return 'One more step and\nyou are in.'
			}
		}

		render() {
			return super.render(
				<PageBit scroll={false}>
					<ScrollSnapBit disableGesture index={ this.state.activeIndex } contentContainerStyle={Styles.contentContainer}>
						<EmptyPagelet
							source="//app/authorizing.jpg"
							title={'Authorizing you…'}
							description={'Please wait.'}
							style={Styles.container}
						/>
						<PassworderPart
							title={ this.titleRenderer(this.state.type) }
							token={ this.props.token }
							email={ this.props.email }
							onSuccess={ this.onNavigateToHome }
							style={ Styles.container }
						/>
					</ScrollSnapBit>
				</PageBit>
			)
		}
	}
)
