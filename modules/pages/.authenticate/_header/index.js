import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import BoxBit from 'modules/bits/box';
import SourceSansBit from 'modules/bits/source.sans';

import Styles from './style';


export default ConnectHelper(
	class HeaderPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string,
				description: PropTypes.string,
				// children: PropTypes.node,
				style: PropTypes.style,
			}
		}

		render() {
			return (
				<BoxBit unflex style={this.props.style}>
					<BoxBit unflex style={Styles.head} />
					<SourceSansBit type={SourceSansBit.TYPES.HEADER_4} style={Styles.title}>
						{ this.props.title }
					</SourceSansBit>
					<SourceSansBit type={SourceSansBit.TYPES.NOTE_1} style={Styles.description}>{ this.props.description }</SourceSansBit>
				</BoxBit>
			)
		}
	}
)
