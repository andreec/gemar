import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	head: {
		height: 64,
	},
	title: {
		marginTop: 16,
		marginBottom: 8,
		color: Colors.black.palette(5),
	},
	description: {
		color: Colors.black.palette(4),
	},
})
