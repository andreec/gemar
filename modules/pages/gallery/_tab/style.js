import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	top: {
		paddingTop: 14 + 20,
		paddingBottom: 7,
		paddingLeft: 15,
		paddingRight: 15,
		borderWidth: 0,
	},

	icon: {
		marginRight: 17,
	},

	tabs: {
		paddingLeft: 15,
		paddingRight: 15,
		borderBottomWidth: 1,
		borderBottomColor: Colors.new.grey.palette(1),
	},

	tab: {
		height: 47,
		flexShrink: 0,
	},

	tabActive: {
		height: 4,
		position: 'absolute',
		bottom: 0,
		left: 0,
		right: 0,
		backgroundColor: Colors.new.yellow.palette(3),
	},

	textActive: {
		color: Colors.new.yellow.palette(3),
	},

	textInactive: {
		color: Colors.new.black.palette(4),
	},

	input: {
		height: 24,
		borderBottomWidth: 0,
	},

	inputText: {
		fontSize: 16,
		lineHeight: 19,
	},
})
