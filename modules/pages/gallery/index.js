import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

// import PageManager from 'app/managers/page';

// import Animated from 'coeur/libs/animated';

import TrackingHandler from 'app/handlers/tracking';

// import Colors from 'coeur/constants/color';
// import Defaults from 'coeur/constants/default';
import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
// import BoxImageBit from 'modules/bits/box.image';
// import ButtonBit from 'modules/bits/button';
import ImageBit from 'modules/bits/image';
import SourceSansBit from 'modules/bits/source.sans';
import PageBit from 'modules/bits/page';
// import IconBit from 'modules/bits/icon';
// import GalleryBit from 'modules/bits/gallery';
import HeaderBit from 'modules/bits/header';
import RadioBit from 'modules/bits/radio';
// import ScrollViewBit from 'modules/bits/scroll.view';
import TouchableBit from 'modules/bits/touchable';
// import TextBit from 'modules/bits/text';

import TabPart from './_tab'

import Styles from './style'

import {
	// pinchGestureEnabled
	Animated,
	// ScrollView,
	CameraRoll,
	FlatList,
} from 'react-native';
import _ from 'lodash';
// eslint-disable-next-line import/default
import ImagePicker from 'react-native-image-picker';

function size(span = 1) {
	return ((Sizes.screen.width - ( 15 * 2 )) - ((3 - span) * 3)) * (span / 3)
}

const size1 = size(1)

// import {
// 	BoxesPart,
// 	CountdownPart,
// 	PromoPart,
// } from '../matchbox'
// import FooterPart from './_footer';
// import HowPart from './_how';
// import IntroductionPart from './_introduction';

// console.log(BoxImageBit.resolve('dummy-userbg.jpg'))


export default ConnectHelper(
	class GalleryPage extends PageModel {

		static routeName = 'gallery'

		header = {
			title: 'Gallery',
			leftActions: [{
				type: HeaderBit.TYPES.BLANK,
			}],
			rightActions: [{
				type: HeaderBit.TYPES.TEXT,
				data: 'NEXT',
				isActive: this.isActive,
				onPress: this.onNavigateToPosting,
			}],
		}

		constructor(p) {
			super(p, {
				next: undefined,
				isGetting: false,
				mainImage: undefined,
				selectedImageIndexes: [],
				images: [],
			}, [
				'isActive',
				'onNavigateToPosting',
				'getItemLayout',
				'onTabPress',
				'selectableImageRenderer',
			]);

			this.__batas = (Sizes.screen.width - (15 * 2)) * 2 / 3

			this.__headerAnimation = false
			this.__headerAnimationValue = new Animated.Value(0)
			this.__headerAnimationInterpolation = {
				transform: [{
					translateY: this.__headerAnimationValue.interpolate({
						inputRange: [0, this.__batas, this.__batas + 1, Infinity],
						outputRange: [0, -this.__batas, -this.__batas, -this.__batas],
					}),
				}],
			}
		}

		componentDidMount() {
			TrackingHandler.trackPageView(this.routeName)

			this.getData()
		}

		getData() {
			if(!this.state.isGetting && this.state.next !== false) {
				this.setState({
					isGetting: true,
				}, () => {
					CameraRoll.getPhotos({
						...(this.state.next ? {
							after: this.state.next,
						} : {
							first: 36,
						}),
						assetType: 'Photos',
					}).then(result => {
						if(result.page_info.has_next_page) {
							this.setState({
								next: result.page_info.end_cursor,
								images: result.edges.map(edge => {
									return edge.node.image.uri
								}),
							})
						} else {
							this.setState({
								next: false,
								images: result.edges.map(edge => {
									return edge.node.image.uri
								}),
							})
						}
					})
				})
			}
		}

		isActive() {
			return !!this.state.selectedImageIndexes.length
		}

		onNavigateToPosting(e, images) {
			if(images) {
				this.navigator.navigate('posting', {
					images,
				})
			} else if(this.state.selectedImageIndexes.length) {
				this.navigator.navigate('posting', {
					images: this.state.selectedImageIndexes.map(index => {
						return this.state.images[index]
					}),
				})
			}
		}

		getHeightAtIndex(i) {
			if(i % 3 === 0) {
				return size1 + 3
			} else {
				return 0
			}
		}

		getTotalHeightAtIndex(i, total = 0) {
			if(i === -1) {
				return total
			} else {
				return this.getTotalHeightAtIndex(i - 1, total + this.getHeightAtIndex(i))
			}
		}

		getItemLayout(data, i) {
			return {
				length: size1 + 3,
				offset: this.getTotalHeightAtIndex(i),
				index: i,
			}
		}

		onTabPress(index) {
			if(index === 1) {
				ImagePicker.launchCamera({
					title: 'Select Image',
					mediaType: 'photo',
					maxWidth: 1024,
					maxHeight: 1024,
					noData: true,
				}, response => {
					if(!response.didCancel && !response.error && !response.customButton) {
						// TODO: Navigate and add the image
						this.onNavigateToPosting(undefined, [response.uri])
					}
				})
			}
		}

		onToggleSelected(index) {
			if(this.state.selectedImageIndexes.indexOf(index) > -1) {
				this.setState({
					selectedImageIndexes: _.without(this.state.selectedImageIndexes, index),
				})
			} else {
				this.setState({
					selectedImageIndexes: [...this.state.selectedImageIndexes, index],
				})
			}
		}

		selectableImageRenderer({item, index}) {
			// this.log(item, index)
			return (
				<TouchableBit unflex onPress={this.onToggleSelected.bind(this, index)} style={Styles.image}>
					<ImageBit source={item} resizeMode={ImageBit.TYPES.COVER} style={{
						width: size1,
						height: size1,
					}} />
					<RadioBit isActive={ this.state.selectedImageIndexes.indexOf(index) > -1 } onPress={this.onToggleSelected.bind(this, index)} style={Styles.radio} />
				</TouchableBit>
			)
		}

		footerRenderer() {
			return (
				<TabPart
					index={0}
					tabs={['Gallery', 'Take Photo']}
					onChangeIndex={ this.onTabPress }
				/>
			)
		}

		render() {
			return super.render(
				<PageBit scroll={false}
					header={ <HeaderBit updater={ this.state.selectedImageIndexes.length } { ...this.header } /> }
					footer={ this.footerRenderer() }
				>
					<FlatList
						numColumns={3}
						scrollEventThrottle={ 1 }
						onScroll={ Animated.event([{
							nativeEvent: {
								contentOffset: {
									y: this.__headerAnimationValue,
								},
							},
						}]) }
						stickyHeaderIndices={[0]}
						ListHeaderComponent={(
							<BoxBit animated unflex style={[Styles.header, this.__headerAnimationInterpolation]}>
								<ImageBit source={ this.state.images[this.state.selectedImageIndexes[this.state.selectedImageIndexes.length - 1] || 0] } resizeMode={ImageBit.TYPES.COVER} style={Styles.headerImage} />
								{ !!this.state.selectedImageIndexes.length && (
									<BoxBit unflex centering style={Styles.selections}>
										<SourceSansBit type={SourceSansBit.TYPES.NOTE_2} style={Styles.text}>{ this.state.selectedImageIndexes.length } photo selected</SourceSansBit>
									</BoxBit>
								) }
							</BoxBit>
						)}
						contentContainerStyle={Styles.content}
						// keyExtractor={ this.props.keyExtractor }
						// extraData={ this.props.updater }
						// ref={ this.bindList }
						data={ this.state.images }
						getItemLayout={ this.getItemLayout }

						// onEndReached={ this.props.onEndReached }
						onEndReachedThreshold={ .5 }
						renderItem={ this.selectableImageRenderer }
					/>
				</PageBit>
			)
		}
	}
)
