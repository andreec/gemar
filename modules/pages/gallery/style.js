import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

const size = Sizes.screen.width - ( 15 * 2)

export default StyleSheet.create({
	content: {
		paddingLeft: 15,
		paddingRight: 15,
	},
	header: {
		width: size,
		height: size + 3,
		marginLeft: 1.5,
		marginRight: 1.5,
		borderBottomWidth: 3,
		borderBottomColor: Colors.new.white.palette(1),
	},
	headerImage: {
		width: size,
		height: size,
	},
	selections: {
		position: 'absolute',
		bottom: 8,
		left: 8,
		paddingTop: 4,
		paddingBottom: 4,
		paddingLeft: 8,
		paddingRight: 8,
		backgroundColor: 'black',
		borderRadius: 2,
	},
	text: {
		color: Colors.new.white.palette(1),
	},
	scroller: {
		paddingTop: size,
	},
	images: {
		flexDirection: 'row',
		flexWrap: 'wrap',
		justifyContent: 'space-between',
	},
	image: {
		marginBottom: 3,
		marginLeft: 1.5,
		marginRight: 1.5,
	},
	radio: {
		position: 'absolute',
		top: 4,
		right: 4,
	},
})
