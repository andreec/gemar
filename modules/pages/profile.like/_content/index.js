import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

// import PageManager from 'app/managers/page';

// import TrackingHandler from 'app/handlers/tracking';

// import Animated from 'coeur/libs/animated';

// import Colors from 'coeur/constants/color';
import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
// import ImageBit from 'modules/bits/image';
import LoaderBit from 'modules/bits/loader';
import SourceSansBit from 'modules/bits/source.sans';

import CardPostThumbnailComponent from 'modules/components/card.post.thumbnail';

import Styles from './style'

import {
	FlatList,
} from 'react-native';
import _ from 'lodash';

// import {
// 	BoxesPart,
// 	CountdownPart,
// 	PromoPart,
// } from '../matchbox'
// import FooterPart from './_footer';
// import HowPart from './_how';
// import IntroductionPart from './_introduction';

function size(span = 1) {
	return ((Sizes.screen.width - ( 15 * 2 )) - ((3 - span) * 3)) * (span / 3)
}

const size1 = size(1)
// , size2 = size(2)


export default ConnectHelper(
	class ContentPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				postIds: PropTypes.array,
				isLoading: PropTypes.bool,
			}
		}

		static defaultProps = {
			postIds: [],
		}

		constructor(p) {
			super(p, {
			}, [
				'bindList',
				'getItemLayout',
				'getHeightAtIndex',
				'getTotalHeightAtIndex',
				'rowRenderer',
			]);

			this._scroller = undefined
		}

		componentDidMount() {
			// super.componentDidMount()

			// let index = 1;
			// this.setInterval(() => {
			// 	this._scroller &&
			// 	this._scroller.scrollToIndex({index: index++})
			// }, 4000)
		}

		bindList(scroller) {
			this._scroller = scroller
		}

		keyExtractor(item, index) {
			return `${index}`
		}

		rowRenderer({ item: items, index: i}) {
			return (
				<BoxBit key={i} unflex row style={Styles.row}>
					{ items.map((item, index) => {
						return (
							<CardPostThumbnailComponent key={index} id={item} />
						)
					})}
					{ Array(3 - items.length).fill().map((empty, index) => {
						return (
							<BoxBit unflex key={index} style={{
								width: size1,
								height: size1,
							}} />
						)
					}) }
				</BoxBit>
			)
		}

		getHeightAtIndex() {
			return size1 + 3
		}

		getTotalHeightAtIndex(i, total = 0) {
			if(i === -1) {
				return total
			} else {
				return this.getTotalHeightAtIndex(i - 1, total + this.getHeightAtIndex(i))
			}
		}

		getItemLayout(data, i) {
			return {
				length: this.getHeightAtIndex(i),
				offset: this.getTotalHeightAtIndex(i),
				index: i,
			}
		}

		render() {
			return (
				<BoxBit>
					<FlatList
						ListHeaderComponent={ this.props.header }
						ListEmptyComponent={ !this.props.isLoading && (
							<BoxBit centering style={Styles.container}>
								<SourceSansBit type={SourceSansBit.TYPES.NEW_NOTE}>No post yet</SourceSansBit>
							</BoxBit>
						) || undefined }
						ListFooterComponent={ this.props.isLoading && (
							<BoxBit type={BoxBit.TYPES.ALL_THICK} centering style={Styles.container}>
								<LoaderBit />
							</BoxBit>
						) || undefined }
						keyExtractor={ this.props.keyExtractor || this.keyExtractor }
						extraData={ this.props.updater }
						ref={ this.bindList }
						data={ _.chunk(this.props.postIds, 3) }
						getItemLayout={ this.getItemLayout }
						onEndReached={ this.props.onEndReached }
						onEndReachedThreshold={ .5 }
						onScrollToIndexFailed={()=>{}}
						renderItem={ this.rowRenderer }
					/>
				</BoxBit>
			)
		}
	}
)
