import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	row: {
		paddingLeft: 15,
		paddingRight: 15,
		marginBottom: 3,
		justifyContent: 'space-between',
	},

	between: {
		justifyContent: 'space-between',
	},

	container: {
		minHeight: 133,
		// backgroundColor: Colors.new.grey.palette(1, .2),
	},
})
