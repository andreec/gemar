import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

import TrackingHandler from 'app/handlers/tracking';

import HeaderBit from 'modules/bits/header';
import PageBit from 'modules/bits/page';
import WebviewBit from 'modules/bits/webview';


export default ConnectHelper(
	class WebviewPage extends PageModel {

		static routeName = 'webview'

		static propTypes(PropTypes) {
			return {
				url: PropTypes.string,
				title: PropTypes.string,
				isBlocker: PropTypes.bool,
				onMount: PropTypes.func,
				onBackPress: PropTypes.func,
				onNavigationStateChange: PropTypes.func,
			}
		}

		header = {
			leftActions: [{
				type: HeaderBit.TYPES.BACK,
				onPress: this.onNavigateToBack,
			}],
			rightActions: [{
				type: HeaderBit.TYPES.RESET,
				onPress: this.onReload,
			}],
		}

		constructor(p) {
			super(p, {}, [
				'bindWebview',
				'onNavigateToBack',
				'onReload',
			]);

			if(!this.props.url) {
				this.navigator.back()
			}
		}

		componentDidMount() {
			this.props.onMount &&
			this.props.onMount()

			TrackingHandler.trackPageView(this.routeName, this.props.title)
		}

		bindWebview(wv) {
			this.webview = wv;
		}

		onNavigateToBack(e) {
			if(this.props.onBackPress) {
				this.props.onBackPress(e)
			} else {
				this.navigator.back()
			}
		}

		onReload() {
			this.webview &&
			this.webview.reload()
		}

		getHeader() {
			return !this.props.isBlocker && (
				<HeaderBit { ...this.header } title={ this.props.title } />
			) || undefined
		}

		render() {
			return (
				<PageBit header={ this.getHeader() }>
					<WebviewBit
						ref={ this.bindWebview }
						source={{uri: this.props.url}}
						onNavigationStateChange={this.props.onNavigationStateChange}
					/>
				</PageBit>
			);
		}
	}
)
