import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

import BatchManager from 'app/managers/batch';

import TrackingHandler from 'app/handlers/tracking';

import ButtonBit from 'modules/bits/button';

import EmptyPagelet from 'modules/pagelets/empty';

import QS from 'query-string';


export default ConnectHelper(
	class CheckoutSuccessPage extends PageModel {

		static routeName = 'checkout.success'

		constructor(p) {
			super(p, {
				query: QS.parse(p.location.search),
				title: 'Getting transaction detail…',
				description: 'Please wait.',
			}, [
				'onNavigateToMyOrders',
			])
		}

		onNavigateToMyOrders() {
			this.navigator.navigate('profile/order')
		}

		componentWillMount() {
			if(this.state.query.code) {
				switch(this.state.query.code) {
				case 'PENDING':
				default:
					this.setState({
						title: 'Order placed!',
						description: 'We will notify you via email once your payment is verified.',
					})
					break;
				case 'SUCCESS':
					this.setState({
						title: 'Thank you!',
						description: 'Your transaction is successful.',
					})
					break;
				case 'FAILED':
					this.setState({
						title: 'Sorry,',
						description: 'we are not able to process your order.\nPlease try to place the order again.',
					})
					break;
				}
			} else if(this.state.query.trx_id) {
				BatchManager.getBatchByFaspayNotification(this.state.query).then(() => {
					this.setState({
						title: 'Order placed!',
						description: 'We will notify you via email once your payment has been processed.',
					})
				}).catch(() => {
					this.setState({
						title: 'Sorry,',
						description: 'we are not able to process your order. Please try to place the order again.',
					})
				})
			}
		}

		componentDidMount() {
			TrackingHandler.trackPageView(this.routeName)
		}

		render() {
			return super.render(
				<EmptyPagelet
					title={ this.state.title }
					description={ this.state.description }
				>
					<ButtonBit
						title="BACK TO MY ORDERS"
						onPress={ this.onNavigateToMyOrders }
					/>
				</EmptyPagelet>
			)
		}
	}
)
