import React from 'react';
import PageModel from 'coeur/models/page';
import ConnectHelper from 'coeur/helpers/connect';

import MeManager from 'app/managers/me';
import AccountService from 'app/services/account';

import FacebookHandler from 'app/handlers/facebook';
import TrackingHandler from 'app/handlers/tracking';
import TwitterHandler from 'app/handlers/twitter';

// import Colors from 'coeur/constants/color';
import ErrorHelper from 'coeur/helpers/error';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import SourceSansBit from 'modules/bits/source.sans';
// import HeaderBit from 'modules/bits/header';
import InputValidatedBit from 'modules/bits/input.validated';
import IconBit from 'modules/bits/icon';
import ImageBit from 'modules/bits/image';
import LinkBit from 'modules/bits/link';
import PageBit from 'modules/bits/page';

import FormPagelet from 'modules/pagelets/form';
// import TextInputBit from 'modules/bits/text.input';

import Styles from './style';


export default ConnectHelper(
	class SignupPage extends PageModel {

		static routeName = 'signup'

		constructor(p) {
			super(p, {
				isValid: false,
				isEmailValid: false,
				isPasswordValid: false,
				isLoggingFacebook: false,
				isLoggingTwitter: false,
				isLoggingEmail: false,
			}, [
				// 'onFacebookLogin',
				'onTwitterLogin',
				'onEmailLogin',
				// 'onRegister',
			])

			// this._email = 'luisk123@yahoo.com'
			// this._password = '123456'

			this._email = ''
			this._password = ''

			this.formConfig = [{
				id: 'email',
				required: true,
				placeholder: 'Your Email',
				type: FormPagelet.TYPES.EMAIL,
				prefix: (
					<IconBit
						name="email"
						size={24}
						style={Styles.inputIcon}
					/>
				),
				style: [Styles.input],
			}, {
				id: 'password',
				required: true,
				placeholder: 'Your Password',
				type: FormPagelet.TYPES.PASSWORD,
				prefix: (
					<IconBit
						name="lock"
						size={24}
						style={Styles.inputIcon}
					/>
				),
				style: [Styles.input, Styles.padder],
			}]
		}

		componentWillMount() {
			FacebookHandler.init()
		}

		onFacebookLogin = reauth => {
			this.setState({
				isLoggingFacebook: true,
			}, () => {
				FacebookHandler.login(reauth).then(data => {
					// this.log('Facebook Login : ', data);
					return MeManager.fbLogin({
						type: 'facebook',
						...data,
					}).then(() => {
						
						this.setState({
							isLoggingFacebook: false,
						}, () => {
							this.navigator.navigate('RootNavigator')
						})
					}).catch(err => {
						this.warn('Me manager ', err)

						if(err && err.code) {
							if(err.code === '019') {
								// this.props.onNavigateToVerify(err.detail)
								// this.onClose()

								this.navigator.navigate('register', {
									email: err.email,
								})
								// Not yet registered

								this.setState({
									isLoggingFacebook: false,
								})
							} else if(err.code === '026') {
								this.utilities.alert.show({
									title: 'Cannot get email',
									message: 'To continue, please share your email address when connecting',
									actions: [{
										title: 'Continue',
										onPress: () => {
											this.onFacebookLogin(true)
										},
									}, {
										title: 'Cancel',
									}],
								})
							} else {
								this.utilities.notification.show({
									message: 'Oops… Something went wrong. Try again in a few moments',
								})
	
								this.setState({
									isLoggingFacebook: false,
								})
							}
						} else {
							this.utilities.notification.show({
								message: 'Oops… Something went wrong. Try again in a few moments',
							})
	
							this.setState({
								isLoggingFacebook: false,
							})
						}
					})
				}).catch(err => {
					this.warn(err)
					
					if(ErrorHelper.isOwn(err)) {
						this.props.utilities.notification.show({
							message: err.message,
						})
					} else {
						this.props.utilities.notification.show({
							message: 'Oops… Something went wrong. Try again in a few moments',
						})
					}

					this.setState({
						isLoggingFacebook: false,
					})
				})
			})
		}

		onTwitterLogin() {
			this.setState({
				isLoggingTwitter: true,
			}, () => {
				TwitterHandler.login().then(data => {
					this.log(data)
					return MeManager.twLogin({
						type: 'twitter',
						...data,
					}).then(res => {
						if(res.status === 2 && res.code === 1003) {
							this.navigator.navigate('register', {
								type: 'twitter',
								email: this._email,
								password: this._password,
								accessToken: data.accessToken,
								userId: data.id,
							})
						} else if(res.status === 1) {
							this.navigator.navigate('LandingNavigator')
						}
						this.setState({
							isLoggingTwitter: false,
						})
					}).catch(err => {
						this.warn(err)

						this.navigator.navigate('register', {
							email: err.email,
							type: 'twitter',
						})

						this.setState({
							isLoggingTwitter: false,
						})
					})
				}).catch(err => {
					this.warn(err)

					if(err.code === '1003') {
						this.utilities.notification.show({
							title: 'Oops…',
							message: 'Email & password combination is invalid.',
						})
					} else {
						this.navigator.navigate('register', {
							email: this._email,
							password: this._password,
						})
					}

					this.setState({
						isLoggingTwitter: false,
					})
				})
			})
		}

		onEmailLogin() {
			// TODO
			this.setState({
				isLoggingEmail: true,
			}, () => {
				MeManager.login({
					email: this._email,
					password: this._password,
				}).then(res => {
					this.setState({
						isLoggingEmail: false,
					}, () => {
						this.navigator.navigate('RootNavigator')
					})
				}).catch(err => {
					// Not yet registered
					this.warn(err)

					if(err.code === '1003') {
						this.utilities.notification.show({
							title: 'Oops…',
							message: 'Email & password combination is invalid.',
						})
					} else {
						this.navigator.navigate('register', {
							email: this._email,
							password: this._password,
							type: 'email',
						})
					}

					this.setState({
						isLoggingEmail: false,
					})
				})
			})
		}

		onChange = (isValid, {
			email,
			password,
		}) => {
			this._email = email.value
			this._password = password.value

			this.setState({
				isValid,
			})
		}

		isValid = () => {
			
		}
		// onRegister() {
		// 	// TODO
		// 	this.navigator.navigate('register', {
		// 		redirect: this.props.redirect,
		// 	})
		// }

		render() {
			return super.render(
				<PageBit type={PageBit.TYPES.THICK}>
					<ImageBit source={ ImageBit.resolve('logo-gemar.png') } style={Styles.image} />
					<SourceSansBit type={SourceSansBit.TYPES.NEW_HEADER_1} align="center" style={Styles.header}>
						Welcome!
					</SourceSansBit>
					<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_1} align="center" style={Styles.subheader}>
						It took only 1 minute to join Gemar.
					</SourceSansBit>

					<ButtonBit
						title="Use Facebook"
						type={ ButtonBit.TYPES.SHAPES.PROGRESS }
						theme={ButtonBit.TYPES.THEMES.FACEBOOK}
						state={ this.state.isLoggingFacebook ? ButtonBit.TYPES.STATES.LOADING : ButtonBit.TYPES.STATES.NORMAL }
						onPress={ this.onFacebookLogin }
						style={Styles.padder}
					/>
					<ButtonBit
						title="Use Twitter"
						type={ ButtonBit.TYPES.SHAPES.PROGRESS }
						theme={ButtonBit.TYPES.THEMES.TWITTER}
						state={ this.state.isLoggingTwitter ? ButtonBit.TYPES.STATES.LOADING : ButtonBit.TYPES.STATES.NORMAL }
						onPress={ this.onTwitterLogin }
					/>

					<BoxBit unflex centering style={Styles.or}>
						<SourceSansBit type={ SourceSansBit.TYPES.NEW_SUBHEADER_2 } style={Styles.orText}>Or</SourceSansBit>
					</BoxBit>

					<FormPagelet
						config={ this.formConfig }
						onUpdate={ this.onChange  }
					/>

					<ButtonBit
						title="I forgot my password"
						type={ ButtonBit.TYPES.SHAPES.GHOST }
						align={ ButtonBit.TYPES.ALIGNS.CENTER }
						style={ Styles.buttonForgot }
						onPress={ this.onForgot }
					/>

					<ButtonBit
						title="Sign Up / Login"
						type={ ButtonBit.TYPES.SHAPES.PROGRESS }
						theme={ ButtonBit.TYPES.THEMES.PRIMARY }
						state={ this.state.isValid
							? this.state.isLoggingEmail
								&& ButtonBit.TYPES.STATES.LOADING
								|| ButtonBit.TYPES.STATES.NORMAL
							: ButtonBit.TYPES.STATES.DISABLED
						}
						style={ Styles.buttonForgot }
						onPress={ this.onEmailLogin }
					/>

					<BoxBit />

					<SourceSansBit type={SourceSansBit.TYPES.NEW_NOTE} align="center" style={Styles.note}>
						By tapping on Use Facebook, Use Twitter or Signup / Login, I agree to Gemar <LinkBit underline title="Terms of Service" href="/term.html">Terms of Use</LinkBit> & <LinkBit underline title="Privacy Policy" href="/privacy.html">Privacy Policy</LinkBit>
					</SourceSansBit>
				</PageBit>
			)
		}
	}
)
