import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	image: {
		alignSelf: 'center',
		width: 80,
		height: 80,
		marginTop: 40 + 20,
		marginBottom: 33,
	},
	header: {
		marginBottom: 5,
	},
	subheader: {
		marginBottom: 23,
	},
	padder: {
		marginBottom: 6,
	},

	or: {
		alignSelf: 'center',
		width: 111,
		borderTopWidth: 1,
		borderStyle: 'solid',
		borderColor: Colors.new.grey.palette(1),
		marginTop: 32,
		marginBottom: -1,
	},

	orText: {
		top: -8,
		paddingLeft: 16,
		paddingRight: 16,
		backgroundColor: Colors.white.primary,
	},

	inputIcon: {
		marginRight: 9,
	},

	input: {
		marginBottom: -4,
	},

	buttonForgot: {
		marginBottom: 19,
	},

	note: {
		// color: Colors.new.black.palette(2),
		marginBottom: 18,
	},
})
