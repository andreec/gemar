import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

import PostManager from 'app/managers/post';

import Animated from 'coeur/libs/animated';

import CloudinaryHandler from 'app/handlers/cloudinary';
// import TrackingHandler from 'app/handlers/tracking';

import Colors from 'coeur/constants/color';
import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
import GalleryBit from 'modules/bits/gallery';
import HeaderBit from 'modules/bits/header';
import ImageBit from 'modules/bits/image';
import PageBit from 'modules/bits/page';
import LoaderBit from 'modules/bits/loader';
import ScrollSnapBit from 'modules/bits/scroll.snap';
import ScrollViewBit from 'modules/bits/scroll.view';
import SourceSansBit from 'modules/bits/source.sans';

import ListLego from 'modules/legos/list';
import ListContainerLego from 'modules/legos/list.container';
// import ListInputLego from 'modules/legos/list.input';

import FormPagelet from 'modules/pagelets/form';

// import CloudinaryUploader from 'react-native-cloudinary-unsigned';

// import EditorPart from './_editor'

import Styles from './style'

// const CLOUDINARY_CLOUD_NAME = 'hiandree';
// const CLOUDINARY_UPLOAD_PROFILE_NAME = 'h3wsaxnr';

// CloudinaryUploader.init(CLOUDINARY_CLOUD_NAME, CLOUDINARY_UPLOAD_PROFILE_NAME)

const size = Sizes.app.width - (15 * 2)


export default ConnectHelper(
	class PostEditPage extends PageModel {

		static routeName = 'post.edit'

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				images: PropTypes.array,
				post: PropTypes.object,
				job: PropTypes.object,
				product: PropTypes.object,
			}
		}

		static stateToProps(state, oP) {
			const postId = oP.id
			const post = state.posts.get(postId)
			const job = post.type === 'JOB' ? state.jobs.get(post.refId) : false
			const product = post.type === 'MARKET' ? state.products.get(post.refId) : false

			return {
				postId,
				post,
				job,
				product,
			}
		}

		constructor(p) {
			super(p, {
				isDone: true,
				activeIndex: 0,
				postType: p.post.type,
				peopleTags: p.post.type !== 'MARKET' ? p.post.tags : [],
				productTags: p.post.type === 'MARKET' ? p.post.tags : [],
			}, [
				'onNavigateToBack',
				'onNext',
				'onChangeIndividualEffect',
				'onModalRequestClose',
				'onTagPeople',
				'onTagProduct',
				'onUpdate',
				'onSubmit',
				'thumbnailRenderer',
			]);

			this.header = {
				title: 'EDIT POST',
				leftActions: [{
					type: HeaderBit.TYPES.BACK,
					onPress: this.onNavigateToBack,
				}],
			}

			this.__uploadWritten = []
			this.__downloadWritten = []
			this.__uploadTotal = []
			this.__downloadTotal = []
			this.__progress = new Animated.Value(0)

			this.__userData = {
				caption: p.post.caption,
				productName: p.product && p.product.title,
				productPrice: p.product && p.product.price,
				productWeight: p.product && p.product.weight,
				productWidth: p.product && p.product.width,
				productHeight: p.product && p.product.height,
				productLength: p.product && p.product.length,
				productAvailability: p.product && p.product.stock,
				productDescription: p.product && p.product.description,
				jobPosition: p.job && p.job.position,
				jobCompany: p.job && p.job.company,
				jobMinimumSallary: p.job && p.job.minSallary,
				jobMaximumSallary: p.job && p.job.maxSallary,
				jobAvailability: p.job && p.job.availability,
			}
			this.__socialForm = [{
				id: 'caption',
				placeholder: 'Write your caption...',
				required: true,
				type: FormPagelet.TYPES.TEXTAREA,
				style: Styles.input,
				inputStyle: Styles.textInput,
				inputContainerStyle: Styles.inputContainer,
				value: this.__userData.caption,
			}]

			this.__marketForm = [{
				id: 'productName',
				required: true,
				placeholder: 'Product Name',
				type: FormPagelet.TYPES.INPUT,
				style: Styles.input,
				inputStyle: Styles.textInput,
				inputContainerStyle: Styles.inputContainer,
				value: this.__userData.productName,
			}, {
				id: 'productPrice',
				placeholder: 'Product Price',
				required: true,
				type: FormPagelet.TYPES.CURRENCY,
				style: Styles.input,
				inputStyle: Styles.textInput,
				inputContainerStyle: Styles.inputContainer,
				value: this.__userData.productPrice,
			}, {
				id: 'productWeight',
				placeholder: 'Product Weight (in Kg)',
				required: true,
				type: FormPagelet.TYPES.CURRENCY,
				style: Styles.input,
				inputStyle: Styles.textInput,
				inputContainerStyle: Styles.inputContainer,
				value: this.__userData.productWeight,
			}, {
				id: 'productWidth',
				placeholder: 'Product Width (in Cm)',
				required: true,
				type: FormPagelet.TYPES.CURRENCY,
				style: Styles.input,
				inputStyle: Styles.textInput,
				inputContainerStyle: Styles.inputContainer,
				value: this.__userData.productWidth,
			}, {
				id: 'productHeight',
				placeholder: 'Product Height (in Cm)',
				required: true,
				type: FormPagelet.TYPES.CURRENCY,
				style: Styles.input,
				inputStyle: Styles.textInput,
				inputContainerStyle: Styles.inputContainer,
				value: this.__userData.productHeight,
			}, {
				id: 'productLength',
				placeholder: 'Product Length (in Cm)',
				required: true,
				type: FormPagelet.TYPES.CURRENCY,
				style: Styles.input,
				inputStyle: Styles.textInput,
				inputContainerStyle: Styles.inputContainer,
				value: this.__userData.productLength,
			}, {
				id: 'productAvailability',
				placeholder: 'Product Availability',
				required: true,
				type: FormPagelet.TYPES.CURRENCY,
				style: Styles.input,
				inputStyle: Styles.textInput,
				inputContainerStyle: Styles.inputContainer,
				value: this.__userData.productAvailability,
			}, {
				id: 'caption',
				placeholder: 'Write your caption...',
				required: true,
				type: FormPagelet.TYPES.TEXTAREA,
				style: Styles.input,
				inputStyle: Styles.textInput,
				inputContainerStyle: Styles.inputContainer,
				value: this.__userData.caption,
			}, {
				id: 'productDescription',
				placeholder: 'Describe your product...',
				required: true,
				type: FormPagelet.TYPES.TEXTAREA,
				style: Styles.input,
				inputStyle: Styles.textInput,
				inputContainerStyle: Styles.inputContainer,
				value: this.__userData.productDescription,
			}]

			this.__jobForm = [{
				id: 'jobPosition',
				placeholder: 'Job Position',
				required: true,
				type: FormPagelet.TYPES.INPUT,
				style: Styles.input,
				inputStyle: Styles.textInput,
				inputContainerStyle: Styles.inputContainer,
				value: this.__userData.jobPosition,
			}, {
				id: 'jobCompany',
				placeholder: 'Company Name',
				required: true,
				type: FormPagelet.TYPES.INPUT,
				style: Styles.input,
				inputStyle: Styles.textInput,
				inputContainerStyle: Styles.inputContainer,
				value: this.__userData.jobCompany,
			}, {
				id: 'jobMinimumSallary',
				placeholder: 'Minimum Sallary',
				required: true,
				type: FormPagelet.TYPES.CURRENCY,
				style: Styles.input,
				inputStyle: Styles.textInput,
				inputContainerStyle: Styles.inputContainer,
				value: this.__userData.jobMinimumSallary,
			}, {
				id: 'jobMaximumSallary',
				placeholder: 'Maximum Sallary',
				required: true,
				type: FormPagelet.TYPES.CURRENCY,
				style: Styles.input,
				inputStyle: Styles.textInput,
				inputContainerStyle: Styles.inputContainer,
				value: this.__userData.jobMaximumSallary,
			}, {
				id: 'jobAvailability',
				placeholder: 'Available Position',
				required: true,
				type: FormPagelet.TYPES.CURRENCY,
				style: Styles.input,
				inputStyle: Styles.textInput,
				inputContainerStyle: Styles.inputContainer,
				value: this.__userData.jobAvailability,
			}, {
				id: 'caption',
				placeholder: 'Write your caption...',
				required: true,
				type: FormPagelet.TYPES.TEXTAREA,
				style: Styles.input,
				inputStyle: Styles.textInput,
				inputContainerStyle: Styles.inputContainer,
				value: this.__userData.caption,
			}]

			// TODO: textarea mention and hashtag detection
		}

		componentDidMount() {
			// TrackingHandler.trackPageView(this.routeName)
		}

		onNavigateToBack() {
			if(this.state.activeIndex > 1) {
				this.setState({
					activeIndex: this.state.activeIndex - 1,
				})
			} else {
				this.navigator.back()
			}
		}

		onNext() {
			this.setState({
				activeIndex: this.state.activeIndex + 1,
			})
		}

		updateValue() {
			this.__progress.setValue(
				(
					this.__uploadWritten.reduce((sum, w) => {
						return sum + w
					}, 0)
					+
					this.__downloadWritten.reduce((sum, w) => {
						return sum + w
					}, 0)
				)
				/
				(
					this.__uploadTotal.reduce((sum, t) => {
						return sum + t
					}, 0)
					+
					this.__downloadTotal.reduce((sum, t) => {
						return sum + t
					}, 0)
				)
				*
				100
			)
		}
		onChangeEffect(index) {
			this.setState({
				selectedEffectIndex: index,
			})
		}

		onChangeIndividualEffect(index, effectIndex) {
			const newEffects = this.state.overrideEffectIndex.slice()

			newEffects[index] = effectIndex

			this.setState({
				overrideEffectIndex: newEffects,
			}, this.onModalRequestClose)
		}

		onModalRequestClose() {
			this.utilities.alert.hide()
		}

		onUpdate(isDone, {
			caption,
			productName,
			productPrice,
			productWeight,
			productWidth,
			productHeight,
			productLength,
			productAvailability,
			productDescription,
			jobPosition,
			jobCompany,
			jobMinimumSallary,
			jobMaximumSallary,
			jobAvailability,
		}) {

			this.__userData = {
				...this.__userData,
				caption: caption && caption.value,
				productName: productName && productName.value,
				productPrice: productPrice && productPrice.value,
				productWeight: productWeight && productWeight.value,
				productWidth: productWidth && productWidth.value,
				productHeight: productHeight && productHeight.value,
				productLength: productLength && productLength.value,
				productAvailability: productAvailability && productAvailability.value,
				productDescription: productDescription && productDescription.value,
				jobPosition: jobPosition && jobPosition.value,
				jobCompany: jobCompany && jobCompany.value,
				jobMinimumSallary: jobMinimumSallary && jobMinimumSallary.value,
				jobMaximumSallary: jobMaximumSallary && jobMaximumSallary.value,
				jobAvailability: jobAvailability && jobAvailability.value,
			}

			// TODO: mention detection

			this.setState({
				isDone,
			})
		}

		onTagPeople() {
			this.navigator.navigate('tag.people', {
				images: this.state.images.map((image, i) => {
					return CloudinaryHandler.effect(this.state.effectKeys[this.state.overrideEffectIndex[i] || this.state.selectedEffectIndex], `${image.public_id}.jpg`, {
						height: size,
						width: size,
					})
				}),
				tags: this.state.peopleTags,
				onDone: (tags) => {
					this.setState({
						peopleTags: tags,
					})
				},
			})
		}

		onTagProduct() {
			this.navigator.navigate('tag.product', {
				images: this.state.images.map((image, i) => {
					return CloudinaryHandler.effect(this.state.effectKeys[this.state.overrideEffectIndex[i] || this.state.selectedEffectIndex], `${image.public_id}.jpg`, {
						height: size,
						width: size,
					})
				}),
				tags: this.state.productTags,
				onDone: (tags) => {
					this.setState({
						productTags: tags,
					})
				},
				title: this.__userData.productName || 'Your Product Name',
			})
		}

		onSubmit() {
			// TODO
			if(this.state.activeIndex === 0) {
				if(this.state.isDone) {
					// final submit
					// Upload to server,
					this.setState({
						activeIndex: this.state.activeIndex + 1,
					})

					this.setState({
						activeIndex: this.state.activeIndex + 1,
					})

					PostManager.updateOne({
						id: this.props.id,
						tags: this.state.postType === 'MARKET' ? this.state.productTags : this.state.peopleTags,
						product: this.props.product ? {
							id: this.props.product.id,
							title: this.__userData.productName,
							description: this.__userData.productDescription,
							price: this.__userData.productPrice,
							weight: this.__userData.productWeight,
							width: this.__userData.productWidth,
							length: this.__userData.productLength,
							height: this.__userData.productHeight,
							stock: this.__userData.productAvailability,
						} : undefined,
						job: this.props.job ? {
							id: this.props.job.id,
							position: this.__userData.jobPosition,
							company: this.__userData.jobCompany,
							minSallary: this.__userData.jobMinimumSallary,
							maxSallary: this.__userData.jobMaximumSallary,
							availability: this.__userData.jobAvailability,
						} : undefined,
						caption: this.__userData.caption,
					})

					this.setTimeout(() => {
						this.navigator.reset('home')
					}, 2000)
				}
			} else {
				this.setState({
					activeIndex: this.state.activeIndex + 1,
				})
			}
		}

		loadingRenderer(isFinal) {
			return (
				<BoxBit unflex style={Styles.container}>
					<BoxBit centering>
						<LoaderBit />
						<BoxBit unflex style={Styles.effectGallery} />
						{ isFinal && (
							<SourceSansBit type={ SourceSansBit.TYPES.NEW_NOTE }>
								Please wait while we are uploading your post
							</SourceSansBit>
						) }
					</BoxBit>
				</BoxBit>
			)
		}

		thumbnailRenderer(image, i) {
			return (
				<ImageBit key={this.state.overrideEffectIndex[i] || this.state.selectedEffectIndex} resize={ImageBit.TYPES.COVER}
					source={ CloudinaryHandler.effect(this.state.effectKeys[this.state.overrideEffectIndex[i] || this.state.selectedEffectIndex], `${image.public_id}.jpg`) }
					style={Styles.effectImage}
				/>
			)
		}

		finaliseRenderer() {
			return this.state.activeIndex === 0 ? (
				<ScrollViewBit style={Styles.container}>
					<ListContainerLego key={`${this.state.peopleTags.length}${this.state.productTags.length}`} data={this.state.postType === 'SOCIAL' ? [{
						title: this.state.peopleTags.length ? `${this.state.peopleTags.length} people tagged` : 'Tag People',
						icon: 'next',
						iconSize: 16,
						onPress: this.onTagPeople,
					}] : this.state.postType === 'MARKET' && [{
						title: 'Tag Product',
						icon: this.state.productTags.length ? 'checkmark' : 'next',
						iconSize: 16,
						iconColor: this.state.productTags.length ? Colors.green.palette(3) : undefined,
						onPress: this.onTagProduct,
					}] || []} style={Styles.tag} />
					<FormPagelet
						config={ this.state.postType === 'SOCIAL' ? this.__socialForm : this.state.postType === 'MARKET' && this.__marketForm || this.__jobForm }
						onUpdate={ this.onUpdate }
						onSubmit={ this.onSubmit }
						style={ this.state.postType === 'JOB' && Styles.form || undefined }
					/>
				</ScrollViewBit>
			) : this.loadingRenderer()
		}

		render() {
			return super.render(
				<PageBit header={(
					<HeaderBit updater={`${this.state.activeIndex}${this.state.isDone}`} {...this.header} rightActions={[{
						type: HeaderBit.TYPES.TEXT,
						data: 'DONE',
						isActive: this.state.isDone,
						onPress: this.onSubmit,
					}]} />
				)} scroll={false}>
					<ScrollSnapBit index={this.state.activeIndex} disableGesture>
						{ this.finaliseRenderer() }
						{ this.loadingRenderer(true) }
					</ScrollSnapBit>
				</PageBit>
			)
		}
	}
)
