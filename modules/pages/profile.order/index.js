import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

// import PageManager from 'app/managers/page';
import OrderManager from 'app/managers/order';

// import FormatHelper from 'coeur/helpers/format';

// import Colors from 'coeur/constants/color';
// import Sizes from 'coeur/constants/size';

// import BoxBit from 'modules/bits/box';
// import BoxImageBit from 'modules/bits/box.image';
// import ButtonBit from 'modules/bits/button';
// import ImageBit from 'modules/bits/image';
// import SourceSansBit from 'modules/bits/source.sans';
import PageBit from 'modules/bits/page';
// import IconBit from 'modules/bits/icon';
// import GalleryBit from 'modules/bits/gallery';
import HeaderBit from 'modules/bits/header';
// import TouchableBit from 'modules/bits/touchable';
// import TextBit from 'modules/bits/text';

import HeaderLego from 'modules/legos/header';

import ContentPart from './_content'

import Styles from './style';

// import {
// 	BoxesPart,
// 	CountdownPart,
// 	PromoPart,
// } from '../matchbox'
// import FooterPart from './_footer';
// import HowPart from './_how';
// import IntroductionPart from './_introduction';


export default ConnectHelper(
	class ProfileOrderPage extends PageModel {

		static routeName = 'profile.order'

		static stateToProps(state) {
			return {
				me: state.me,
			}
		}

		constructor(p) {
			super(p, {
				isLoading: false,
				orderIds: [],
			}, [
				'getData',
			]);
		}

		componentDidMount() {
			this.getData()
		}

		getData() {
			if (!this.state.isLoading) {
				this.setState({
					isLoading: true,
				})

				OrderManager.getOrders({
					offset: this.state.orderIds.length,
				}).then(orders => {
					this.setState({
						isLoading: false,
						orderIds: [...this.state.orderIds, ...orders.map(order => order.id)],
					})
				})
			}
		}

		header = {
			// title: 'LIKES',
			leftActions: [{
				type: HeaderBit.TYPES.BACK,
			}],
			rightActions: [{
				type: HeaderBit.TYPES.BLANK,
			}],
		}

		render() {
			return super.render(
				<PageBit scroll={false} header={ ( <HeaderBit { ...this.header } /> ) } >
					<ContentPart
						header={<HeaderLego title="My Orders" description={'“By the time a bartender knows what drink a man will have before he orders, there is little else about him worth knowing.”\n― Don Marquis'} style={Styles.header} /> }
						isLoading={ this.state.isLoading }
						orderIds={ this.state.orderIds }
						onEndReached={ this.getData }
					/>
				</PageBit>
			)
		}
	}
)
