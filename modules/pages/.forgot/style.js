import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		width: Sizes.screen.width,
	},
	contentContainer: {
		flexGrow: 1,
	},
	content: {
		marginBottom: 24,
	},
	image: {
		width: 240,
		height: 240,
		borderRadius: 120,
		overflow: 'hidden',
	},
	desc: {
		textAlign: 'center',
		paddingTop: 16,
		paddingBottom: 24,
	},
	padder: {
		flexGrow: .2,
	},
	header: {
		marginTop: 16,
		marginBottom: 8,
	},
	head: {
		height: 64,
	},
	form: {
		marginBottom: 24,
	},
	colorBlack: {
		color: Colors.black.palette(5),
	},
	colorGrey: {
		color: Colors.black.palette(4),
	},
})
