import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

import MeManager from 'app/managers/me';

import TrackingHandler from 'app/handlers/tracking';

import TimeHelper from 'coeur/helpers/time';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import SourceSansBit from 'modules/bits/source.sans';
import HeaderBit from 'modules/bits/header';
import PageBit from 'modules/bits/page';
import ScrollSnapBit from 'modules/bits/scroll.snap';

import EmptyPagelet from 'modules/pagelets/empty';
import FormPagelet from 'modules/pagelets/form';

import Styles from './style';


export default ConnectHelper(
	class ForgotPage extends PageModel {

		static routeName = 'forgot.password'

		header = {
			leftActions: [{
				type: HeaderBit.TYPES.BACK,
			}],
		}

		constructor(p) {
			super(p, {
				activeIndex: 0,
				isDone: false,
				isLoading: false,
				takenEmail: [],
			}, [
				'onNavigateToHome',
				'takenEmailLength',
				'checkTakenEmail',
				'onUpdateData',
				'onConfirmReset',
			])

			this.userData = {}
			this.formConfig = [{
				id: 'email',
				updater: this.takenEmailLength,
				isRequired: true,
				value: p.email,
				type: FormPagelet.TYPES.EMAIL,
				validator: this.checkTakenEmail,
				testOnMount: true,
			}]
		}

		componentDidMount() {
			TrackingHandler.trackPageView(this.routeName)
		}

		onNavigateToHome() {
			this.navigator.top()
		}

		takenEmailLength() {
			return this.state.takenEmail.length
		}

		checkTakenEmail(value) {
			return !this.state.takenEmail.length || this.state.takenEmail.indexOf(value) === -1
		}

		onUpdateData(isDone, data) {
			this.userData = {
				...this.userData,
				...data,
			}

			if(this.state.isDone !== isDone) {
				this.setState({
					isDone,
				})
			}
		}

		onConfirmReset() {
			if(this.state.isDone) {
				this.setState({
					isLoading: true,
				}, () => {
					MeManager.resetPassword(this.userData.email.value).then(() => {
						this.setState({
							activeIndex: 1,
						})
					}).catch(err => {
						this.warn(err)

						if(err && err.code === '036') {
							this.utilities.alert.show({
								title: 'Oops…',
								message: `You have requested a password reset ${err.detail ? TimeHelper.moment(err.detail).fromNow() : 'earlier'}. Please check your email.`,
								actions: [{
									title: 'OK',
									onPress: () => {
										this.setState({
											isLoading: false,
											takenEmail: [
												...this.state.takenEmail,
												this.userData.email.value,
											],
										})
									},
								}],
							})
						} else if(err && err.code === '017') {
							this.utilities.notification.show({
								title: 'Oops…',
								message: 'We cannot find such an email registered.',
							})

							this.setState({
								isLoading: false,
								takenEmail: [
									...this.state.takenEmail,
									this.userData.email.value,
								],
							})
						} else {
							this.utilities.notification.show({
								title: 'Oops…',
								message: 'Something went wrong. Please try again later.',
							})

							this.setState({
								isLoading: false,
								takenEmail: [
									...this.state.takenEmail,
									this.userData.email.value,
								],
							})
						}
					})
				})
			}
		}

		render() {
			return super.render(
				<PageBit scroll={false} header={<HeaderBit { ...this.header } />} >
					<ScrollSnapBit disableGesture index={ this.state.activeIndex } contentContainerStyle={Styles.contentContainer}>
						<BoxBit unflex type={PageBit.TYPES.THICK} style={Styles.container}>
							<SourceSansBit type={SourceSansBit.TYPES.HEADER_4} style={[Styles.header, Styles.colorBlack]}>
								Forgot Password?
							</SourceSansBit>
							<SourceSansBit type={SourceSansBit.TYPES.NOTE_1} style={[Styles.colorGrey]}>Enter your email below and I will find your account.</SourceSansBit>
							<FormPagelet
								updater={ this.takenEmailLength() }
								config={ this.formConfig }
								onUpdate={ this.onUpdateData }
								onSubmit={ this.onConfirmReset }
								style={Styles.form}
							/>
							<ButtonBit
								title="SUBMIT"
								type={ ButtonBit.TYPES.SHAPES.PROGRESS }
								size={ ButtonBit.TYPES.SIZES.COMPACT }
								theme={ ButtonBit.TYPES.THEMES.CONFIRMATION }
								state={ this.state.isDone ? this.state.isLoading && ButtonBit.TYPES.STATES.LOADING || ButtonBit.TYPES.STATES.NORMAL : ButtonBit.TYPES.STATES.DISABLED }
								onPress={ this.onConfirmReset }
							/>
						</BoxBit>
						<EmptyPagelet
							source="//app/success.jpg"
							title={ 'High five…' }
							description={ 'Your reset password instruction\nhas been sent.' }
							style={Styles.container}
						>
							<ButtonBit
								title={'BACK TO HOME'}
								onPress={ this.onNavigateToHome }
							/>
						</EmptyPagelet>
					</ScrollSnapBit>
				</PageBit>
			)
		}
	}
)
