import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		paddingTop: 16,
		paddingBottom: 16,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
		borderWidth: 0,
		borderStyle: 'solid',
		borderTopWidth: StyleSheet.hairlineWidth,
		borderTopColor: Colors.coal.palette(2),
	},

	textareaContainer: {
		borderBottomWidth: 0,
	},

	textarea: {
		maxHeight: 220 - 48,
		height: 220 - 48,
		padding: 0,
		fontSize: 16,
		letterSpacing: .5,
	},

	footer: {
		justifyContent: 'space-between',
	},

	text: {
		color: Colors.black.palette(4),
		marginTop: 8,
		marginBottom: 8,
		marginLeft: 0,
		marginRight: 0,
	},

	redColor: {
		color: Colors.red.primary,
	},

	paleColor: {
		color: Colors.grey.palette(3),
	},
})
