import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import BoxBit from 'modules/bits/box';
import SourceSansBit from 'modules/bits/source.sans';
import TextAreaBit from 'modules/bits/text.area';

import Styles from './style';


export default ConnectHelper(
	class TextareaPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				placeholder: PropTypes.string,
				maxlength: PropTypes.number,
				minlength: PropTypes.number,
				onChange: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			maxlength: 500,
			minlength: 24,
		}

		constructor(p) {
			super(p, {
				len: 0,
			}, [
				'onChange',
			])

			this._value = undefined;
		}

		shouldComponentUpdate(nP, nS) {
			return nS.len !== this.state.len
		}

		onChange(e, val) {
			this.setState({
				len: val.length,
			})

			this._value = val

			this.props.onChange &&
			this.props.onChange(e, val)
		}

		render() {
			return (
				<BoxBit unflex style={[Styles.container, this.props.style]}>
					<TextAreaBit autofocus autogrow={false}
						placeholder={ this.props.placeholder }
						maxlength={ this.props.maxlength }
						onChange={ this.onChange }
						style={ Styles.textareaContainer }
						inputStyle={ Styles.textarea }
					/>
					<BoxBit unflex row style={Styles.footer}>
						{ this.state.len < this.props.minlength ? (
							<SourceSansBit type={SourceSansBit.TYPES.NOTE_1} style={[Styles.text, Styles.paleColor]}>minimum 24 characters</SourceSansBit>
						) : (
							<BoxBit />
						) }
						<SourceSansBit type={SourceSansBit.TYPES.CAPTION_1} style={[Styles.text, this.state.len < this.props.minlength && Styles.redColor]}>{this.state.len}/{this.props.maxlength}</SourceSansBit>
					</BoxBit>
				</BoxBit>
			)
		}
	}
)
