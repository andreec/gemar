import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

import MeManager from 'app/managers/me';
import VolatileManager from 'app/managers/volatile';

import TrackingHandler from 'app/handlers/tracking';

import Defaults from 'coeur/constants/default';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import CollapsibleBit from 'modules/bits/collapsible';
import SourceSansBit from 'modules/bits/source.sans';
import HeaderBit from 'modules/bits/header';
import InputFileBit from 'modules/bits/input.file';
import PageBit from 'modules/bits/page';

import FourOFourPagelet from 'modules/pagelets/four.o.four';
import TipsPagelet from 'modules/pagelets/tips';

import TextareaPart from './_textarea'

import Styles from './style'


export default ConnectHelper(
	class MatchboxOrderPage extends PageModel {

		static routeName = 'matchbox.order'

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				isUserVerified: PropTypes.bool,
				isUserLoggedIn: PropTypes.bool,
				email: PropTypes.string,
				firstName: PropTypes.string,
				token: PropTypes.string,
			}
		}

		static stateToProps(state, oP) {
			const mb = state.matchboxes.get(oP.id)
				, returned = {
					id: oP.id,
					isUserVerified: state.me.isVerified,
					isUserLoggedIn: !!state.me.token,
					email: state.me.email,
					firstName: state.me.firstName,
					token: state.me.token,
				}

			if(!mb) {
				returned.isCrashing = true
				returned.crashError = new Error(`No matchbox found with id "${oP.id}" supplied`)
			}

			return returned
		}

		static defaultProps = {
			matchbox: {},
		}

		header = {
			leftActions: [{
				type: HeaderBit.TYPES.BACK,
			}],
		}

		constructor(p) {
			super(p, {
				isSending: false,
				note: '',
				attachments: Array(Defaults.MATCHBOX_MAX_ATTACHMENT).fill(),
			}, [
				'canSend',
				'onNavigateToTips',
				'onNoteChange',
				'onFileChange',
				'onOrderMatchbox',
				'onModalRequestClose',
			])

			if(!p.id) {
				this.navigator.back()
			} else if(!p.isUserLoggedIn) {
				this.navigator.navigate('signup', {
					redirect: this.props.location.pathname.replace(/\//, ''),
				})
			} else if(!p.isUserVerified) {
				this.navigator.navigate('verify', {
					title: `${this.props.firstName},`,
					description: 'We noticed that you haven\'t verify your email yet. In order to continue, please verify your email first.',
					email: this.props.email,
					redirect: this.props.location.pathname.replace(/\//, ''),
				})
			}
		}

		componentWillMount() {
			if(VolatileManager.isExpired('me') && this.props.isUserLoggedIn) {
				MeManager.authenticate(this.props.token).then(() => {
					VolatileManager.mark('me')
				}).catch(() => {
					this.utilities.notification.show({
						title: 'Oops…',
						message: 'Looks like you\'ve been logged out of your session. Try logging in.',
					})

					this.navigator.top()

					MeManager.logout()
				})
			}
		}

		componentDidMount() {
			TrackingHandler.trackPageView(this.routeName)
		}

		canSend() {
			return this.state.note.length >= 24 && !this.state.isSending
		}

		onNavigateToTips() {
			this.utilities.alert.modal({
				component: (
					<TipsPagelet
						title={'MATCHBOX NOTE TIPS'}
						data={[{
							description: 'If you have specific requests. Matchbox note is the best place to include them. You can include requests like:',
						}, {
							title: 'Special Occasions',
							description: 'Let us know in the comments if you have any special occasions coming up that requires specific attire. Going to a wedding? Have a job interview? Or an important first date? Tell your Stylist what’s on your calendar.',
						}, {
							title: 'Trips',
							description: 'Dressing for an out-of-town adventure, like a vacation? Let us know where you’re headed so we can include appropriate attire (e.g. island getaway to Bali).',
						}, {
							title: 'Trends',
							description: 'Are there any current trends you’ve been wanting to try out? Tell your Stylist what’s inspired you lately.',
						}, {
							title: 'Seasonal Request',
							description: 'Is your closet missing some key pieces for the change in weather? Tell us what items you’d like to refresh for the current or upcoming season.',
						}, {
							title: 'One last thing…',
							description: 'If you have a crush on OOTD looks you see on the web, include the photo as an attachment in your Matchbox note. Your Stylist will do her best to select similar pieces.',
						}]}
						onRequestClose={ this.onModalRequestClose }
					/>
				),
			})
		}

		onNoteChange(e, val) {
			this.setState({
				note: val,
			})
		}

		onFileChange(id, file) {
			const update = this.state.attachments.slice()
			update[id] = file

			this.setState({
				attachments: update,
			})
		}

		onOrderMatchbox() {
			TrackingHandler.trackEvent('matchbox-order', this.props.id)

			this.setState({
				isSending: true,
			}, () => {
				MeManager.addToCart('matchbox', this.props.id, {
					note: this.state.note,
					attachments: this.state.attachments,
				}).then(() => {
					// AUTOCHECKOUT
					this.navigator.navigate('checkout')
				}).catch(err => {
					this.warn(err)

					this.utilities.notification.show({
						title: 'Oops…',
						message: 'Looks like something is error, please try again later',
					})

					this.setState({
						isSending: false,
					})
				})
			})
		}

		onModalRequestClose() {
			this.utilities.alert.hide()
		}

		__errorRenderer() {
			return (
				<FourOFourPagelet />
			)
		}

		render() {
			return super.render(
				<PageBit header={ <HeaderBit { ...this.header } /> }>
					<BoxBit unflex style={Styles.section}>
						<SourceSansBit type={SourceSansBit.TYPES.HEADER_4}>Matchbox note</SourceSansBit>
						<SourceSansBit type={SourceSansBit.TYPES.PARAGRAPH_2} style={Styles.description}>
							Leave a note for your stylist
						</SourceSansBit>
					</BoxBit>
					<TextareaPart
						placeholder="Write your note here in the language you’re most comfortable with—English or Bahasa Indonesia."
						onChange={ this.onNoteChange }
					/>
					<CollapsibleBit title={`ATTACHMENT (${this.state.attachments.filter(a => !!a).length})`}>
						<BoxBit unflex style={Styles.content}>
							<BoxBit unflex row style={Styles.inputs}>
								{ this.state.attachments.map((u, i) => {
									return (
										<InputFileBit
											key={i}
											id={`${i}`}
											onDataLoaded={ this.onFileChange }
										/>
									)
								}) }
							</BoxBit>
							<SourceSansBit type={SourceSansBit.TYPES.CAPTION_1} style={Styles.footnote}>You may add up to {Defaults.MATCHBOX_MAX_ATTACHMENT} attachments</SourceSansBit>
						</BoxBit>
					</CollapsibleBit>
					<BoxBit unflex style={Styles.footer}>
						<ButtonBit
							title="ORDER"
							type={ ButtonBit.TYPES.SHAPES.PROGRESS }
							state={ !this.canSend() ? this.state.isSending && ButtonBit.TYPES.STATES.LOADING || ButtonBit.TYPES.STATES.DISABLED : ButtonBit.TYPES.STATES.NORMAL }
							onPress={ this.onOrderMatchbox }
							style={Styles.button}
						/>
						<ButtonBit
							type={ButtonBit.TYPES.SHAPES.GHOST}
							theme={ButtonBit.TYPES.THEMES.BLACK}
							onPress={ this.onNavigateToTips }
							title="Matchbox Note Tips"
						/>
					</BoxBit>
				</PageBit>
			)
		}
	}
)
