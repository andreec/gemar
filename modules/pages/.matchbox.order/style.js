import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({

	section: {
		paddingTop: 16,
		paddingBottom: 16,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
	},

	description: {
		marginTop: 8,
		color: Colors.black.palette(4),
	},

	content: {
		paddingBottom: 16,
	},

	inputs: {
		justifyContent: 'space-between',
		paddingBottom: 16,
	},

	footnote: {
		marginTop: 8,
		marginBottom: 8,
		marginLeft: 0,
		marginRight: 0,
		color: Colors.black.palette(2),
	},

	footer: {
		paddingTop: 24,
		paddingBottom: 64,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
	},

	button: {
		marginBottom: 16,
	},
})
