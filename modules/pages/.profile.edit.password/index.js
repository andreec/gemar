import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

import MeManager from 'app/managers/me';

import TrackingHandler from 'app/handlers/tracking';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import HeaderBit from 'modules/bits/header';
import PageBit from 'modules/bits/page';

import NotificationLego from 'modules/legos/notification';

import FormPagelet from 'modules/pagelets/form';

import Styles from './style';


export default ConnectHelper(
	class ProfileEditPasswordPage extends PageModel {

		static routeName = 'profile.edit.password'

		header = {
			title: 'CHANGE PASSWORD',
			leftActions: [{
				type: HeaderBit.TYPES.BACK,
			}],
			rightActions: [{
				type: HeaderBit.TYPES.TEXT,
				data: 'SAVE',
				isActive: this.canSave,
				onPress: this.onSavePassword,
			}],
		}

		constructor(p) {
			super(p, {
				isSaving: false,
				canSave: false,
			}, [
				'canSave',
				'checkPassword',
				'checkRepeatPassword',
				'onUpdate',
				'onSavePassword',
			]);

			this._formData = {
				currentPassword: undefined,
				newPassword: undefined,
				repeatPassword: undefined,
			}

			this._formConfig = [{
				id: 'currentPassword',
				title: 'CURRENT PASSWORD',
				isRequired: true,
				type: FormPagelet.TYPES.PASSWORD,
			}, {
				id: 'newPassword',
				title: 'NEW PASSWORD',
				isRequired: true,
				validator: this.checkPassword,
				type: FormPagelet.TYPES.PASSWORD,
			}, {
				id: 'repeatPassword',
				title: 'CONFIRM NEW PASSWORD',
				isRequired: true,
				validator: this.checkRepeatPassword,
				type: FormPagelet.TYPES.PASSWORD,
			}]
		}

		componentDidMount() {
			TrackingHandler.trackPageView(this.routeName)
		}

		canSave() {
			return this.state.canSave && !this.state.isSaving
		}

		checkPassword(value) {
			const password = this._formData.currentPassword && this._formData.currentPassword.value
			return value !== password
		}

		checkRepeatPassword(value) {
			const password = this._formData.newPassword && this._formData.newPassword.value
			return value === password
		}

		onUpdate(isDone, data) {
			if(isDone !== this.state.canSave) {
				this.setState({
					canSave: isDone,
				})
			}

			this._formData = {
				...this._formData,
				...data,
			}
		}

		onSavePassword() {
			if(this.state.canSave) {
				this.setState({
					isSaving: true,
				}, () => {
					MeManager.changePassword({
						currentPassword: this._formData.currentPassword.value,
						password1: this._formData.newPassword.value,
						password2: this._formData.repeatPassword.value,
					}).then(() => {
						this.utilities.notification.show({
							title: '🎉',
							message: 'Password updated successfuly',
							type: this.utilities.notification.TYPES.SUCCESS,
						})

						this.navigator.back()
					}).catch(err => {
						if(err.code) {
							this.utilities.notification.show({
								title: 'Oops…',
								message: 'Your password is not correct',
							})
						} else {
							this.utilities.notification.show({
								title: 'Oops…',
								message: 'Sorry, something wen\'t wrong, please try again later.',
							})
						}

						this.setState({
							isSaving: false,
						})
					})
				})
			}
		}

		render() {
			return super.render(
				<PageBit header={ <HeaderBit { ...this.header } updater={this.canSave()} /> }
					contentContainerStyle={Styles.container}
				>
					<NotificationLego
						message="Your password must be 6 or more characters long and include at least one number or symbol."
					/>
					<FormPagelet
						config={this._formConfig}
						onUpdate={ this.onUpdate }
						onSubmit={ this.onSavePassword }
						style={Styles.form}
					/>
					<BoxBit unflex style={Styles.footer}>
						<ButtonBit
							title="SAVE NEW PASSWORD"
							state={ this.state.isSaving ? ButtonBit.TYPES.STATES.LOADING : this.state.canSave && ButtonBit.TYPES.STATES.NORMAL || ButtonBit.TYPES.STATES.DISABLED }
							onPress={ this.onSavePassword }
						/>
					</BoxBit>
				</PageBit>
			)
		}
	}
)
