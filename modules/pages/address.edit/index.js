import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

// import PageManager from 'app/managers/page';
import AddressManager from 'app/managers/address';

import Colors from 'coeur/constants/color';
// import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
// import BoxImageBit from 'modules/bits/box.image';
import ButtonBit from 'modules/bits/button';
import PageBit from 'modules/bits/page';
import HeaderBit from 'modules/bits/header';
import ShadowBit from 'modules/bits/shadow';

import HeaderLego from 'modules/legos/header';

import FormPagelet from 'modules/pagelets/form';

import Styles from './style';

// import {
// 	BoxesPart,
// 	CountdownPart,
// 	PromoPart,
// } from '../matchbox'
// import FooterPart from './_footer';
// import HowPart from './_how';
// import IntroductionPart from './_introduction';


export default ConnectHelper(
	class ContactPage extends PageModel {

		static routeName = 'address.edit'

		static stateToProps(state, oP) {
			return {
				me: state.me,
				id: oP.id,
				address: state.address.get(oP.id) || {},
			}
		}

		constructor(p) {
			super(p, {
				isDone: false,
				isSaving: false,
				isChanged: false,
			}, [
				'onUpdate',
				'onSendEmail',
			]);

			this.__userData = {}
		}

		header = {
			// title: 'CHANGE PASSWORD',
			leftActions: [{
				type: HeaderBit.TYPES.BACK,
			}],
			rightActions: [{
				type: HeaderBit.TYPES.BLANK,
			}],
		}

		onUpdate(isDone, data) {
			console.log('heyyyyyy', isDone, data)
			if (isDone !== this.state.isDone) {
				this.setState({
					isDone: isDone,
					isChanged: true,
				})
			} else {
				this.setState({
					isChanged: true,
				})
			}

			this.__userData = {
				...this.__userData,
				...data,
			}
		}

		onSendEmail() {
			this.setState({
				isSaving: true,
			}, () => {
				if(this.props.id) {
					AddressManager.updateAddress(this.props.id, {
						title: this.__userData.title.value,
						pic: this.__userData.pic.value,
						phone: this.__userData.phone.value,
						address: this.__userData.address.value,
						district: this.__userData.district.value,
						postal: this.__userData.postal.value,
					}).then(() => {
						this.utilities.notification.show({
							message: 'Address updated successfuly',
							type: this.utilities.notification.TYPES.SUCCESS,
						})

						this.navigator.back()
					}).catch(err => {
						this.warn(err)

						this.utilities.notification.show({
							message: 'Oops… Sorry, something wen\'t wrong, please try again later.',
						})

						this.setState({
							isSaving: false,
						})
					})
				} else {
					AddressManager.addAddress({
						title: this.__userData.title.value,
						pic: this.__userData.pic.value,
						phone: this.__userData.phone.value,
						address: this.__userData.address.value,
						district: this.__userData.district.value,
						postal: this.__userData.postal.value,
					}).then(() => {
						this.utilities.notification.show({
							message: 'Address updated successfuly',
							type: this.utilities.notification.TYPES.SUCCESS,
						})

						this.navigator.back()
					}).catch(err => {
						this.warn(err)

						this.utilities.notification.show({
							message: 'Oops… Sorry, something wen\'t wrong, please try again later.',
						})

						this.setState({
							isSaving: false,
						})
					})
				}
			})
		}

		render() {
			return super.render(
				<PageBit type={PageBit.TYPES.THICK} header={(
					<HeaderBit { ...this.header } />
				)} footer={(
					<ShadowBit
						y={-1}
						blur={8}
						color={Colors.new.grey.palette(2)}
						style={ Styles.footer }
					>
						<BoxBit unflex style={Styles.button}>
							<ButtonBit
								type={ButtonBit.TYPES.SHAPES.PROGRESS}
								state={ this.state.isChanged && this.state.isDone ? this.state.isSaving && ButtonBit.TYPES.STATES.LOADING || ButtonBit.TYPES.STATES.NORMAL : ButtonBit.TYPES.STATES.DISABLED }
								title={ 'SAVE' }
								onPress={ this.onSendEmail }
							/>
						</BoxBit>
					</ShadowBit>
				)} contentContainerStyle={Styles.page} >
					<HeaderLego
						title={ this.props.id ? 'Updating address' : 'Add address' }
						description={'“I believe we all have one address and that is Earth.”\n― Avijeet Das'}
					/>
					<FormPagelet
						config={[{
							id: 'title',
							title: 'TITLE',
							type: FormPagelet.TYPES.INPUT,
							value: this.props.address.title,
						}, {
							id: 'pic',
							title: 'RECEIVER NAME',
							type: FormPagelet.TYPES.NAME,
							value: this.props.address.pic,
						}, {
							id: 'phone',
							title: 'PHONE NUMBER',
							required: true,
							type: FormPagelet.TYPES.PHONE,
							value: this.props.address.phone,
						}, {
							id: 'address',
							title: 'ADDRESS',
							required: true,
							type: FormPagelet.TYPES.TEXTAREA,
							value: this.props.address.address,
						}, {
							id: 'district',
							title: 'DISTRICT (KECAMATAN / KELURAHAN)',
							required: true,
							type: FormPagelet.TYPES.INPUT,
							value: this.props.address.district,
						}, {
							id: 'postal',
							title: 'POSTAL CODE',
							required: true,
							type: FormPagelet.TYPES.POSTAL,
							value: this.props.address.postal,
						}]}
						onUpdate={ this.onUpdate }
					/>
				</PageBit>
			)
		}
	}
)
