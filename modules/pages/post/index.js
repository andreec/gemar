import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

import FormatHelper from 'utils/helpers/format';
import PostManager from 'app/managers/post';
import UserManager from 'app/managers/user';

import ProfileService from 'app/services/profile';

import TrackingHandler from 'app/handlers/tracking';

// import Colors from 'coeur/constants/color';
// import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
import ImageBit from 'modules/bits/image';
import SourceSansBit from 'modules/bits/source.sans';
import PageBit from 'modules/bits/page';
import IconBit from 'modules/bits/icon';
// import GalleryBit from 'modules/bits/gallery';
import TouchableBit from 'modules/bits/touchable';
// import TextBit from 'modules/bits/text';

import CardPostComponent from 'modules/components/card.post.new'

// import {
// 	BoxesPart,
// 	CountdownPart,
// 	PromoPart,
// } from '../matchbox'
// import FooterPart from './_footer';
// import HowPart from './_how';
// import IntroductionPart from './_introduction';
import { isEmpty } from 'lodash';
import Styles from './style';

const ProfileImage = ImageBit.resolve('profile.png')

export default ConnectHelper(
	class PostPage extends PageModel {

		static routeName = 'post'

		static stateToProps(state, oP) {
			const id = oP.id
				, post = state.posts.get(id) || PostManager.get(id)
				, user = post.userId && (state.users.get(post.userId) || UserManager.get(post.userId))
			
			return {
				id: oP.id,
				post,
				user,
				me: state.me,
			}
		}

		static defaultProps = {
			user: {},
		}

		constructor(p) {
			super(p, {
				isLoading: false,
				post: {},
			}, [
				'onNavigateToBack',
				'onMenuPress',
				'onEditPost',
				'onReportPost',
				'onDeletePost',
				'onUnfollow',
				'onConfirmReport',
				'onConfirmUnfollow',
				'onConfirmDelete',
				'onProfilePress',
			]);
		}

		componentWillMount() {
			this.getData()
		}

		getData() {
			this.setState({
				isLoading: true,
			}, () => {
				ProfileService.getPostDetail(
					this.props.id
					// TODO CHANGE STATIC ID
					// 3
				).then(res => {
					if(res.status === 1) {
						this.setState({
							isLoading: false,
							post: FormatHelper.snakeToCamelCase(res.data.post),
						})
					}
				}).catch(err => {
					this.warn(err)

					this.setState({
						isLoading: false,
					}, () => {
						this.utilities.notification.show({
							title: 'Oops',
							message: 'Something went wrong',
						})
					})
				})
			})
		}

		getType() {
			if(this.state.post.postType === 2) {
				return 'MARKET'
			} else if(this.state.post.postType === 3) {
				return 'JOB'
			} else {
				return 'SOCIAL'
			}
		}

		onNavigateToBack() {
			this.navigator.back()
		}

		onMenuPress() {
			this.props.utilities.menu.show({
				actions: this.props.user.id === this.props.me.id ? [{
					title: 'Edit Post',
					onPress: this.onEditPost,
				}, {
					title: 'Delete Post',
					onPress: this.onDeletePost,
				}] : [{
					title: 'Report Post',
					onPress: this.onReportPost,
				}, {
					title: `Unfollow @${this.props.user.username}`,
					onPress: this.onUnfollow,
				}],
				closeOnPress: true,
			})
		}

		onReportPost() {
			this.props.utilities.alert.show({
				title: 'Report',
				message: 'Are you sure want to report this post?',
				actions: [{
					title: 'Yes',
					type: 'OK',
					onPress: this.onConfirmReport,
				}, {
					title: 'Cancel',
					type: 'CANCEL',
				}],
			})
		}

		onConfirmReport() {
			// TODO
			this.props.utilities.notification.show({
				message: 'Done reporting this post.',
			})
		}

		onUnfollow() {
			this.props.utilities.alert.show({
				title: 'Unfollow',
				message: `If you change your mind, you\'ll have to request to follow @${this.props.user.username} again.`,
				actions: [{
					title: 'Unfollow',
					type: 'OK',
					onPress: this.onConfirmUnfollow,
				}, {
					title: 'Cancel',
					type: 'CANCEL',
				}],
			})
		}

		onConfirmUnfollow() {
			// TODO
			this.props.utilities.notification.show({
				message: 'Done unfollowing.',
			})
		}

		onEditPost() {
			// TODO
		}

		onDeletePost() {
			this.props.utilities.alert.show({
				title: 'Warning',
				message: 'Are you sure want to delete this post? This action cannot be undone.',
				actions: [{
					title: 'Yes, I\'m sure',
					type: 'OK',
					onPress: this.onConfirmDelete,
				}, {
					title: 'Cancel',
					type: 'CANCEL',
				}],
			})
		}

		onConfirmDelete() {
			// TODO
		}

		onProfilePress() {
			this.navigator.navigate('user.profile', {
				id: this.props.user.id,
			})
		}

		headerRenderer() {
			return (
				<BoxBit type={BoxBit.TYPES.THICK} row style={Styles.container}>
					<TouchableBit onPress={this.onNavigateToBack} unflex enlargeHitSlop centering style={[Styles.menu, Styles.back]}>
						<IconBit
							name="back"
							size={19}
						/>
					</TouchableBit>
					<TouchableBit row onPress={this.onProfilePress}>
						{/* <ImageBit source={this.props.user.profile || ProfileImage} resizeMode={ImageBit.TYPES.COVER} style={Styles.image} /> */}
						<ImageBit source={ProfileImage} resizeMode={ImageBit.TYPES.COVER} style={Styles.image} />
						<BoxBit>
							<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_1_ALT_2}>{'Name not provided'}</SourceSansBit>
							<SourceSansBit type={SourceSansBit.TYPES.NEW_NOTE} style={Styles.sub}>@{this.props.me.username}</SourceSansBit>
						</BoxBit>
					</TouchableBit>
					<TouchableBit onPress={this.onMenuPress} unflex enlargeHitSlop centering style={Styles.menu}>
						<IconBit
							name="menu"
							size={24}
						/>
					</TouchableBit>
				</BoxBit>
			)
		}

		render() {
			this.log(this.state, this.props)
			return super.render(
				<PageBit type={PageBit.TYPES.THICK} headerHeight={81} header={this.headerRenderer()}>
					{ isEmpty(this.state.post) ? this.__errorRenderer() : (
						<CardPostComponent noHeader
							id={this.props.id}
							userId={this.props.userId}
							createdAt={this.state.post.createdAt}
							images={this.state.post.imageList}
							image={this.state.post.mainImage}
							caption={this.state.post.caption}
							likeCount={this.state.post.likeCount}

							
							type={this.getType()}
						/>
					)}
				</PageBit>
			)
		}
	}
)
