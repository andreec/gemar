import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	container: {
		position: 'absolute',
		zIndex: 1,
		left: 0,
		top: Sizes.screen.height,
		width: Sizes.screen.width,
		backgroundColor: Colors.white.primary,
	},

	tabs: {
		justifyContent: 'space-around',
	},

	results: {
		marginLeft: 15,
		marginRight: 15,
		marginTop: 8,
		marginBottom: 8,
	},

	result: {
		marginTop: 8,
		marginBottom: 8,
	},
})
