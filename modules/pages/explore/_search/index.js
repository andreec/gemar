import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

// import PageManager from 'app/managers/page';

// import PostManager from 'app/managers/post';

import SearchHandler from 'app/handlers/search';

import Animated from 'coeur/libs/animated';

import FormatHelper from 'coeur/helpers/format';

// import Colors from 'coeur/constants/color';
import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
// import ImageBit from 'modules/bits/image';
import SourceSansBit from 'modules/bits/source.sans';
// import PageBit from 'modules/bits/page';
// import IconBit from 'modules/bits/icon';
// import GalleryBit from 'modules/bits/gallery';
import ScrollViewBit from 'modules/bits/scroll.view';
// import TouchableBit from 'modules/bits/touchable';
// import TextBit from 'modules/bits/text';
// import TextInputBit from 'modules/bits/text.input';

import SearchResultComponent from 'modules/components/search.result';

import TabPart from '../_tab'

import Styles from './style'

import _ from 'lodash';

// import {
// 	BoxesPart,
// 	CountdownPart,
// 	PromoPart,
// } from '../matchbox'
// import FooterPart from './_footer';
// import HowPart from './_how';
// import IntroductionPart from './_introduction';


export default ConnectHelper(
	class SearchPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				isActive: PropTypes.bool,
				headerHeight: PropTypes.number,
				search: PropTypes.string,
				style: PropTypes.style,
			}
		}

		constructor(p) {
			super(p, {
				index: 0,
				tabs: [
					'Top',
					'People',
					'Tags',
				],
				results: [],
				filter: '',
				isLoading: false,
			}, [
				'getData',
				'onChangeIndex',
			]);

			this.getData = _.debounce(this.getData, 100)

			this._animation = false
			this._animationValue = new Animated.Value(0)
			this._animationInterpolation = {
				transform: [{
					translateY: this._animationValue.interpolate({
						inputRange: [0, 1],
						outputRange: [0, -Sizes.screen.height],
					}),
				}],
			}
		}

		componentWillMount() {
		}

		componentDidMount() {
		}

		componentDidUpdate(pP) {
			if(pP.isActive !== this.props.isActive) {
				this.onToggleSearchPage(!!this.props.isActive)
			}

			if(pP.search !== this.props.search) {
				this.getData()
			}
		}

		getData() {
			this.setState({
				isLoading: true,
			})

			const index = this.state.index
				, search = this.props.search

			if(search) {
				SearchHandler.explore({
					search,
					filter: this.state.filter,
					offset: this.state.results.length,
				}).then(newResults => {
					if(index === this.state.index && this.props.search === search) {
						this.setState({
							isLoading: false,
							results: newResults,
						})
					}
				})
			}
		}

		onToggleSearchPage(isActive) {
			this._animation &&
			this._animation.stop()

			this._animation = Animated.timing(this._animationValue, {
				toValue: isActive ? 1 : 0,
				duration: 300,
			}).start()
		}

		onChangeIndex(index) {
			this.setState({
				index,
				filter: index ? this.state.tabs[index] : '',
				results: [],
			}, this.getData)
		}

		resultRenderer(result) {
			return (
				<SearchResultComponent
					key={`${result.__type}${result.id}`}
					id={ result.id }
					type={ result.__type }
					title={ result.hashtag || result.name }
					description={ FormatHelper.currencyFormat(result.count) || result.username }
					image={ result.profile }
					style={ Styles.result }
				/>
			)
		}

		render() {
			return (
				<BoxBit animated unflex style={[Styles.container, {
					height: Sizes.screen.height - this.props.headerHeight,
					marginTop: this.props.headerHeight,
				}, this._animationInterpolation]}>
					<TabPart
						index={ this.state.index }
						tabs={ this.state.tabs }
						onChangeIndex={ this.onChangeIndex }
						style={ Styles.tabs }
					/>
					{ this.state.results.length ? (
						<ScrollViewBit unflex contentContainerStyle={Styles.results}>
							{ this.state.results.map(this.resultRenderer) }
						</ScrollViewBit>
					) : (
						<BoxBit centering>
							<SourceSansBit type={SourceSansBit.TYPES.NOTE_2}>Not found</SourceSansBit>
						</BoxBit>
					) }
				</BoxBit>
			)
		}
	}
)
