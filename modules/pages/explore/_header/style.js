import StyleSheet from 'coeur/libs/style.sheet'
// import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	top: {
		paddingTop: 14 + 20,
		paddingBottom: 7,
		paddingLeft: 15,
		paddingRight: 15,
		borderWidth: 0,
		width: Sizes.app.width,
	},

	icon: {
		marginRight: 17,
	},

	input: {
		height: 24,
		borderBottomWidth: 0,
	},

	inputText: {
		fontSize: 16,
		lineHeight: 19,
	},
})
