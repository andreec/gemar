import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

// import PageManager from 'app/managers/page';

// import TrackingHandler from 'app/handlers/tracking';

// import Animated from 'coeur/libs/animated';

// import Colors from 'coeur/constants/color';
// import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
import IconBit from 'modules/bits/icon';
// import ImageBit from 'modules/bits/image';
// import GalleryBit from 'modules/bits/gallery';
// import PageBit from 'modules/bits/page';
// import SourceSansBit from 'modules/bits/source.sans';
// import TextBit from 'modules/bits/text';
import TextInputBit from 'modules/bits/text.input';
// import TouchableBit from 'modules/bits/touchable';

import TabPart from '../_tab';

import Styles from './style'


export default ConnectHelper(
	class HeaderPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				index: PropTypes.number,
				headers: PropTypes.array,
				onChangeIndex: PropTypes.func,
				onSearch: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			index: 0,
			headers: [
				'All',
				'Social',
				'Market Place',
				'Job',
			],
		}

		render() {
			return (
				<React.Fragment>
					<BoxBit unflex style={Styles.top}>
						<TextInputBit
							key={ this.state.resetter }
							prefix={(
								<IconBit
									name="search"
									style={Styles.icon}
								/>
							)}
							placeholder="Search"
							onChange={ this.props.onSearch }
							style={Styles.input}
							inputStyle={Styles.inputText}
						/>
					</BoxBit>
					<TabPart
						index={ this.props.index }
						onChangeIndex={ this.props.onChangeIndex }
						tabs={this.props.headers}
					/>
				</React.Fragment>
			)
		}
	}
)
