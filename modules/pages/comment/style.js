import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	container: {
		paddingTop: 32,
		paddingBottom: 32,
	},
	top: {
		paddingBottom: 15,
		borderBottomWidth: .5,
		borderBottomColor: Colors.new.grey.palette(1),
	},
	image: {
		width: 32,
		height: 32,
		borderRadius: 16,
		overflow: 'hidden',
		marginRight: 10,
	},
	time: {
		// marginTop: 5,
		color: Colors.new.black.palette(4),
	},
	buttonBox: {
		marginTop: 20,
		marginBottom: 10,
	},
	button: {
		marginLeft: 42,
	},
	comment: {
		paddingTop: 10,
		// paddingBottom: 5,
	},
	commenter: {
		padding: 10,
		width: Sizes.app.width,
		backgroundColor: Colors.new.white.primary,
	},
	input: {
		height: 32,
		borderBottomWidth: 0,
	},
	commenting: {
		opacity: .5,
	},
})
