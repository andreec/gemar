import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

// import PageManager from 'app/managers/page';

import TrackingHandler from 'app/handlers/tracking';

import TimeHelper from 'coeur/helpers/time';

import Colors from 'coeur/constants/color';
import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import HeaderBit from 'modules/bits/header';
import ImageBit from 'modules/bits/image';
import LoaderBit from 'modules/bits/loader';
import SourceSansBit from 'modules/bits/source.sans';
import PageBit from 'modules/bits/page';
import IconBit from 'modules/bits/icon';
import GalleryBit from 'modules/bits/gallery';
import ShadowBit from 'modules/bits/shadow';
import TouchableBit from 'modules/bits/touchable';
import TextBit from 'modules/bits/text';
import TextInputBit from 'modules/bits/text.input';

// import {
// 	BoxesPart,
// 	CountdownPart,
// 	PromoPart,
// } from '../matchbox'
// import FooterPart from './_footer';
// import HowPart from './_how';
// import IntroductionPart from './_introduction';

import Styles from './style'

const ProfileImage = ImageBit.resolve('profile.png');


export default ConnectHelper(
	class CommentPage extends PageModel {

		static routeName = 'comment'

		static propTypes(PropTypes) {
			return {
				me: PropTypes.object,
				comments: PropTypes.arrayOf(PropTypes.shape({
					user: PropTypes.string,
					profile: PropTypes.string,
					comment: PropTypes.string,
					time: PropTypes.date,
				})),
				caption: PropTypes.shape({
					user: PropTypes.string,
					profile: PropTypes.string,
					comment: PropTypes.string,
					time: PropTypes.string,
				}),
				total: PropTypes.number,
			}
		}

		static stateToProps(state) {
			return {
				me: state.me,
			}
		}

		static defaultProps = {
			caption: {
				user: 'amanda.tasik',
				profile: null,
				comment: 'Inilah caption saya sahek',
				time: Date.now(),
			},
			comments: [],
			total: 0,
		}

		header = {
			title: 'COMMENTS',
			leftActions: [{
				type: HeaderBit.TYPES.BACK,
			}],
			rightActions: [{
				type: HeaderBit.TYPES.BLANK,
			}],
		}

		constructor(p) {
			super(p, {
				comment: '',
				isLoading: false,
				isCommenting: false,
				commentRerenderCount: 0,
				comments: p.comments,
			}, [
				'onLoadMoreComment',
				'onAddComment',
			]);
		}

		componentWillMount() {
			// TODO: Load Comment
		}

		componentDidMount() {
			TrackingHandler.trackPageView(this.routeName)
		}

		onAddComment(value) {
			const nonce = Date.now()

			this.setState({
				isCommenting: value,
				comments: this.state.comments.concat({
					user: this.props.me.username,
					comment: value,
					nonce,
					profile: this.props.me.profile,
					time: nonce,
					isCommenting: true,
				}),
				commentRerenderCount: this.state.commentRerenderCount + 1,
			}, () => {
				// TODO
				this.setTimeout(() => {
					const newComment = this.state.comments.slice()
						, updatedComment = newComment.findIndex(c => c.nonce === nonce)

					newComment.splice(updatedComment, 1, {
						...newComment[updatedComment],
						nonce: undefined,
						isCommenting: false,
					 })

					this.setState({
						comments: newComment,
					})
				}, 1000)
			})
		}

		onLoadMoreComment() {}

		footerRenderer() {
			return (
				<ShadowBit
					y={-1}
					blur={8}
					color={Colors.new.grey.palette(2)}
				>
					<BoxBit row unflex style={Styles.commenter}>
						<ImageBit source={ this.props.me.profile || ProfileImage } style={Styles.image} />
						<TextInputBit unflex={false}
							key={ this.state.commentRerenderCount }
							placeholder="Add a comment..."
							onSubmitEditing={ this.onAddComment }
							style={Styles.input}
						/>
					</BoxBit>
				</ShadowBit>
			)
		}

		commentRenderer(comment, i) {
			return (
				<BoxBit key={i} type={BoxBit.TYPES.THICK} unflex row style={[Styles.comment, comment.isCommenting && Styles.commenting]}>
					<ImageBit
						source={ comment.profile || ProfileImage }
						style={Styles.image}
					/>
					<BoxBit>
						<SourceSansBit type={SourceSansBit.TYPES.NEW_PARAGRAPH_2}>
							<TextBit weight="bold">{ comment.user }</TextBit> { comment.comment }
						</SourceSansBit>
						<SourceSansBit type={SourceSansBit.TYPES.NEW_NOTE_2} style={Styles.time}>{ TimeHelper.ago(comment.time) }</SourceSansBit>
					</BoxBit>
				</BoxBit>
			)
		}

		render() {
			return this.state.isLoading ? super.render(
				<PageBit header={(
					<HeaderBit {...this.header} />
				)} footer={this.footerRenderer()} contentContainerStyle={Styles.container}>
					<BoxBit centering>
						<LoaderBit />
					</BoxBit>
				</PageBit>
			) : super.render(
				<PageBit header={(
					<HeaderBit { ...this.header } />
				)} footer={ this.footerRenderer() } contentContainerStyle={Styles.container}>

					<BoxBit type={BoxBit.TYPES.THICK} unflex row style={Styles.top}>
						<ImageBit
							source={ this.props.caption.profile || ProfileImage }
							style={Styles.image}
						/>
						<BoxBit>
							<SourceSansBit type={SourceSansBit.TYPES.NEW_PARAGRAPH_2}>
								<TextBit weight="bold">{ this.props.caption.user }</TextBit> { this.props.caption.comment }
							</SourceSansBit>
							<SourceSansBit type={SourceSansBit.TYPES.NEW_NOTE_2} style={Styles.time}>{ TimeHelper.ago(this.props.caption.time) }</SourceSansBit>
						</BoxBit>
					</BoxBit>

					{ this.props.total > this.props.comments.length ? (
						<BoxBit type={BoxBit.TYPES.THICK} unflex style={Styles.buttonBox}>
							<ButtonBit
								title="+ Load more comments"
								type={ButtonBit.TYPES.SHAPES.BADGE}
								// theme={ButtonBit.TYPES.THEMES.ACTIVATED}
								width={ButtonBit.TYPES.WIDTHS.FIT}
								onPress={this.onLoadMoreComment}
								style={Styles.button}
							/>
						</BoxBit>
					) : false }

					{ this.state.comments.map(this.commentRenderer) }
				</PageBit>
			)
		}
	}
)
