import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

import MeManager from 'app/managers/me';

import TrackingHandler from 'app/handlers/tracking';

import TimeHelper from 'coeur/helpers/time';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import ScrollSnapBit from 'modules/bits/scroll.snap';

import EmptyPagelet from 'modules/pagelets/empty';
import FourOFourPagelet from 'modules/pagelets/four.o.four';

import {
	HeaderPart,
} from '../authenticate'

import Styles from './style';


export default ConnectHelper(
	class VerifyPage extends PageModel {

		static routeName = 'verify'

		static propTypes(PropTypes) {
			return {
				email: PropTypes.string,
				redirect: PropTypes.string,
				type: PropTypes.oneOf([
					'verification',
					'reset',
					'styleprofile',
				]),
				title: PropTypes.string,
				description: PropTypes.string,
				token: PropTypes.string,
				button: PropTypes.string,
			}
		}

		static stateToProps(state, oP) {
			if(
				oP.email
			) {
				return {
					token: state.me.token,
				}
			} else {
				return {
					isCrashing: true,
				}
			}
		}

		static defaultProps = {
			type: 'verification',
		}

		constructor(p) {
			super(p, {
				activeIndex: 0,
				isSending: false,
				title: '',
				description: '',
			}, [
				'onNavigateToHome',
				'onRedirect',
				'onSendEmail',
				'onSuccess',
				'onFailed',
			])
		}

		componentDidMount() {
			TrackingHandler.trackPageView(this.routeName)
		}

		onNavigateToHome() {
			this.navigator.top()
		}

		onRedirect() {
			if(this.props.redirect) {
				this.navigator.navigate(this.props.redirect)
			} else {
				this.onNavigateToHome()
			}
		}

		onSendEmail() {
			this.setState({
				isSending: true,
			}, () => {
				if(this.props.type === 'reset') {

					MeManager
						.resetPassword(this.props.email, this.props.redirect)
						.then(this.onSuccess)
						.catch(this.onFailed)

				} else if(this.props.type === 'verification') {

					MeManager
						.verify(this.props.email, this.props.redirect)
						.then(this.onSuccess)
						.catch(this.onFailed)

				}
			})
		}

		onSuccess() {
			let description = ''
			switch(this.props.type) {
			case 'reset':
				description = 'Your reset password instruction\nhas been sent.'
				break;
			case 'verification':
				description = 'The verification link has been sent to your email.'
				break;
			default:
				description = 'Your email is on it\'s way.'
				break;
			}

			this.setState({
				activeIndex: 1,
				isSending: false,
				title: 'High five…',
				description,
			})
		}

		onFailed(err) {
			this.warn(err)

			if(err && err.code) {
				if(
					err.code === '030'		// FAILED TO SEND EMAIL
				) {
					this.utilities.notification.show({
						title: 'Oops…',
						message: 'We\'re experiencing problems when sending your email. Please try again later.',
					})

					this.setState({
						isSending: false,
					})

					return;

				} else if(
					err.code === '028'		// EMAIL ALREADY VERIFIED
				) {
					if(this.props.token) {
						MeManager.authenticate(this.props.token).then(user => {
							this.utilities.notification.show({
								type: this.utilities.notification.TYPES.SUCCESS,
								title: '🎉',
								message: `Welcome back, ${user.firstName}!`,
							})

							this.onRedirect()
						})
					} else {
						this.utilities.alert.show({
							type: this.utilities.notification.TYPES.SUCCESS,
							title: 'Horaaay 🎉',
							message: 'Verification success, try logging in.',
							actions: [{
								title: 'OK',
								onPress: this.onNavigateToHome,
							}],
						})

						this.onRedirect()
					}
				} else if(
					err.code === '036'		// EMAIL ALREADY SENT
				) {
					this.setState({
						activeIndex: 1,
						isSending: false,
						title: 'Oops…',
						description: `Email already sent ${err.detail && TimeHelper.moment(err.detail).fromNow() || 'earlier'}. Please check your email inbox or spam folder.`,
					})

					return;

				}
			}

			this.utilities.notification.show({
				title: 'Oops…',
				message: 'Something went wrong. Please try again later.',
			})

			this.setState({
				isSending: false,
			})
		}

		__errorRenderer() {
			return (
				<FourOFourPagelet />
			)
		}

		render() {
			return super.render(
				<BoxBit unflex style={this.props.style}>
					<ScrollSnapBit disableGesture index={ this.state.activeIndex } contentContainerStyle={Styles.contentContainer}>
						<BoxBit unflex type={BoxBit.TYPES.THICK} style={Styles.container}>
							<HeaderPart
								title={ this.props.title || 'Hello,' }
								description={ this.props.description }
								style={ Styles.content }
							/>
							<ButtonBit
								title={ this.props.button || 'SEND EMAIL' }
								type={ ButtonBit.TYPES.SHAPES.PROGRESS }
								size={ ButtonBit.TYPES.SIZES.TINY }
								width={ ButtonBit.TYPES.WIDTHS.FIT }
								state={ this.state.isSending ? ButtonBit.TYPES.STATES.LOADING : ButtonBit.TYPES.STATES.NORMAL }
								onPress={ this.onSendEmail }
							/>
						</BoxBit>
						<EmptyPagelet
							source="//app/success.jpg"
							title={ this.state.title }
							description={ this.state.description }
							style={Styles.container}
						>
							<ButtonBit
								title={'BACK TO HOME'}
								onPress={ this.onNavigateToHome }
							/>
						</EmptyPagelet>
					</ScrollSnapBit>
				</BoxBit>
			)
		}
	}
)
