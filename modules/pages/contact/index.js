import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

// import PageManager from 'app/managers/page';
import MeManager from 'app/managers/me';

import Colors from 'coeur/constants/color';
// import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
// import BoxImageBit from 'modules/bits/box.image';
import ButtonBit from 'modules/bits/button';
import PageBit from 'modules/bits/page';
import HeaderBit from 'modules/bits/header';
import ShadowBit from 'modules/bits/shadow';

import HeaderLego from 'modules/legos/header';

import FormPagelet from 'modules/pagelets/form';

import Styles from './style';

// import {
// 	BoxesPart,
// 	CountdownPart,
// 	PromoPart,
// } from '../matchbox'
// import FooterPart from './_footer';
// import HowPart from './_how';
// import IntroductionPart from './_introduction';


export default ConnectHelper(
	class ContactPage extends PageModel {

		static routeName = 'contact'

		static stateToProps(state) {
			return {
				me: state.me,
			}
		}

		constructor(p) {
			super(p, {
				isDone: false,
				isSending: false,
			}, [
				'onUpdate',
				'onSendEmail',
			]);

			this.__userData = {}
		}

		header = {
			// title: 'CHANGE PASSWORD',
			leftActions: [{
				type: HeaderBit.TYPES.BACK,
			}],
			rightActions: [{
				type: HeaderBit.TYPES.BLANK,
			}],
		}

		onUpdate(isDone, data) {
			if (isDone !== this.state.isDone) {
				this.setState({
					isDone: isDone,
				})
			}

			this.__userData = {
				...this.__userData,
				...data,
			}
		}

		onSendEmail() {
			this.setState({
				isSending: true,
			}, () => {
				MeManager.contact(this.__userData.title.value, this.__userData.message.value).then(() => {
					this.utilities.notification.show({
						message: 'Message sent successfuly',
						type: this.utilities.notification.TYPES.SUCCESS,
					})

					this.navigator.back()
				}).catch(err => {
					this.warn(err)

					this.utilities.notification.show({
						message: 'Oops… Sorry, something wen\'t wrong, please try again later.',
					})

					this.setState({
						isSending: false,
					})
				})
			})
		}

		render() {
			return super.render(
				<PageBit type={PageBit.TYPES.THICK} header={(
					<HeaderBit { ...this.header } />
				)} footer={(
					<ShadowBit
						y={-1}
						blur={8}
						color={Colors.new.grey.palette(2)}
						style={ Styles.footer }
					>
						<BoxBit unflex style={Styles.button}>
							<ButtonBit
								type={ButtonBit.TYPES.SHAPES.PROGRESS}
								state={ this.state.isDone ? this.state.isSending && ButtonBit.TYPES.STATES.LOADING || ButtonBit.TYPES.STATES.NORMAL : ButtonBit.TYPES.STATES.DISABLED }
								title="SEND EMAIL"
								onPress={ this.onSendEmail }
							/>
						</BoxBit>
					</ShadowBit>
				)} contentContainerStyle={Styles.page} >
					<HeaderLego
						title="Contact us"
						description="Anytime, anywhere you could reach us by simply sending an email to care@gemar.com. Our customer care representative will reply as soon as possible. Or you could fill out the form below."
					/>
					<FormPagelet
						config={[{
							id: 'title',
							title: 'TITLE',
							required: true,
							type: FormPagelet.TYPES.INPUT,
						}, {
							id: 'message',
							title: 'MESSAGE',
							required: true,
							type: FormPagelet.TYPES.TEXTAREA,
						}]}
						onUpdate={ this.onUpdate }
					/>
				</PageBit>
			)
		}
	}
)
