import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

// import PageManager from 'app/managers/page';
// import UserManager from 'app/managers/user';
import MeManager from 'app/managers/me';

// import FormatHelper from 'coeur/helpers/format';

// import Colors from 'coeur/constants/color';
// import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
// import BoxImageBit from 'modules/bits/box.image';
// import ButtonBit from 'modules/bits/button';
// import ImageBit from 'modules/bits/image';
// import SourceSansBit from 'modules/bits/source.sans';
import PageBit from 'modules/bits/page';
import LoaderBit from 'modules/bits/loader';
// import IconBit from 'modules/bits/icon';
// import GalleryBit from 'modules/bits/gallery';
import HeaderBit from 'modules/bits/header';
import SwitchBit from 'modules/bits/switch';
// import TouchableBit from 'modules/bits/touchable';
// import TextBit from 'modules/bits/text';

import HeaderLego from 'modules/legos/header'
import ListLego from 'modules/legos/list'
import ListContainerLego from 'modules/legos/list.container'

import Styles from './style';

// import {
// 	BoxesPart,
// 	CountdownPart,
// 	PromoPart,
// } from '../matchbox'
// import FooterPart from './_footer';
// import HowPart from './_how';
// import IntroductionPart from './_introduction';


export default ConnectHelper(
	class SettingPage extends PageModel {

		static routeName = 'setting'

		static stateToProps(state) {
			return {
				me: state.me,
			}
		}

		constructor(p) {
			super(p, {
				isSettingPrivate: false,
			}, [
				'onNavigateToProfileLike',
				'onNavigateToProfileOrder',
				'onNavigateToEditProfile',
				'onNavigateToEditPassword',
				'onNavigateToJobApplication',
				'onNavigateToAddress',
				'onNavigateToContact',
				'onTogglePrivate',
				'onLogout',
			]);
		}

		header = {
			leftActions: [{
				type: HeaderBit.TYPES.BACK,
			}],
			rightActions: [{
				type: HeaderBit.TYPES.BLANK,
			}],
			// title: 'ACCOUNT & SETTINGS'
		}

		onNavigateToProfileLike() {
			this.navigator.navigate('profile.like')
		}

		onNavigateToProfileOrder() {
			this.navigator.navigate('profile.order')
		}

		onNavigateToEditProfile() {
			this.navigator.navigate('profile.edit')
		}

		onNavigateToEditPassword() {
			this.navigator.navigate('profile.edit.password')
		}

		onNavigateToAddress() {
			this.navigator.navigate('address')
		}

		onNavigateToContact() {
			this.navigator.navigate('contact')
		}

		onNavigateToJobApplication() {
			this.navigator.navigate('job.application')
		}

		onTogglePrivate(val) {
			if (!this.state.isSettingPrivate) {
				this.setState({
					isSettingPrivate: true,
				}, () => {
					let _val

					if (val === true || val === false) {
						_val = val
					} else {
						_val = !this.props.me.isPrivate
					}
					MeManager.updateProfile({
						isPrivate: _val,
					}).then(() => {
						this.setState({
							isSettingPrivate: false,
						})
					})
				})
			}
		}

		onLogout() {
			MeManager.logout()

			this.navigator.navigate('Auth')
		}

		render() {
			return super.render(
				<PageBit header={ ( <HeaderBit { ...this.header } /> ) } contentContainerStyle={Styles.page} >
					<HeaderLego title="Account" description="Everything related to your profile and account details." style={Styles.header} />
					<ListContainerLego key={`${this.props.me.isPrivate}${this.state.isSettingPrivate}`} component={ListLego} data={[{
						title: 'Likes',
						style: Styles.list,
						onPress: this.onNavigateToProfileLike,
					}, {
						title: 'My Orders',
						style: Styles.list,
						onPress: this.onNavigateToProfileOrder,
					}, {
						title: 'My Job Applications',
						style: Styles.list,
						onPress: this.onNavigateToJobApplication,
					}, {
						title: 'Edit Profile',
						style: Styles.list,
						onPress: this.onNavigateToEditProfile,
					}, {
						title: 'Addresses',
						style: Styles.list,
						onPress: this.onNavigateToAddress,
					}, {
						title: 'Change Password',
						style: Styles.list,
						onPress: this.onNavigateToEditPassword,
					}, {
						title: 'Set as Private Account',
						style: Styles.list,
						children: this.state.isSettingPrivate ? (
							<BoxBit unflex centering style={Styles.loader}>
								<LoaderBit simple />
							</BoxBit>
						) : (
							<SwitchBit
								passive
								value={ this.props.me.isPrivate }
								onChange={ this.onTogglePrivate }
							/>
						),
						onPress: this.onTogglePrivate,
					}, {
						title: 'Logout',
						style: Styles.list,
						onPress: this.onLogout,
					}]} style={Styles.container} />
					<HeaderLego title="Social connect" description="Connect to your social accounts make it easy to share." style={Styles.header} />
					<ListContainerLego component={ListLego} data={[{
						title: 'Facebook',
						style: Styles.list,
						// onPress: undefined,
					}, {
						title: 'Twitter',
						style: Styles.list,
						// onPress: undefined,
					}]} style={Styles.container} />
					<HeaderLego title="Help & Support" description={'“There are two things a person should never be angry at, what they can help, and what they cannot.”\n― Plato'} style={Styles.header} />
					<ListContainerLego component={ListLego} data={[{
						title: 'Privacy Policy',
						style: Styles.list,
						// onPress: undefined,
					}, {
						title: 'Terms of Service',
						style: Styles.list,
						// onPress: undefined,
					}, {
						title: 'Help Center',
						style: Styles.list,
						// onPress: undefined,
					}, {
						title: 'Contact Us',
						style: Styles.list,
						onPress: this.onNavigateToContact,
					}]} style={Styles.container} />
				</PageBit>
			)
		}
	}
)
