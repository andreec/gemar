import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

// import PageManager from 'app/managers/page';

// import TrackingHandler from 'app/handlers/tracking';

// import Animated from 'coeur/libs/animated';

// import Colors from 'coeur/constants/color';
// import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
// import IconBit from 'modules/bits/icon';
// import ImageBit from 'modules/bits/image';
// import LoaderBit from 'modules/bits/loader';
import SourceSansBit from 'modules/bits/source.sans';
import TextBit from 'modules/bits/text';
// import TouchableBit from 'modules/bits/touchable';

import Styles from './style'


export default ConnectHelper(
	class BioPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				job: PropTypes.string,
				bio: PropTypes.string,
				trimLength: PropTypes.number,
			}
		}

		static defaultProps = {
			job: '-',
			bio: 'Your bio is currently empty',
			trimLength: 200,
		}

		constructor(p) {
			super(p, {
				isTruncated: p.bio.length > p.trimLength ? true : false,
			}, [
				'onMorePress',
			]);
		}

		onMorePress() {
			this.setState({
				isTruncated: false,
			})
		}

		render() {
			return (
				<BoxBit unflex style={Styles.container}>
					<SourceSansBit type={ SourceSansBit.TYPES.NEW_SUBHEADER_1_ALT_2 } weight="medium" style={Styles.job}>{ this.props.job }</SourceSansBit>
					<SourceSansBit type={ SourceSansBit.TYPES.NEW_PARAGRAPH_1 } style={Styles.bio}>{ this.state.isTruncated ? this.props.bio.substring(0, this.props.trimLength) : this.props.bio } { this.state.isTruncated ? (
						<TextBit style={Styles.more} onPress={ this.onMorePress }>…more</TextBit>
					) : false }</SourceSansBit>
				</BoxBit>
			)
		}
	}
)
