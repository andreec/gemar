import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

// import PageManager from 'app/managers/page';
import MeManager from 'app/managers/me';

import PageContext from 'coeur/contexts/page'

// import TrackingHandler from 'app/handlers/tracking';

// import Animated from 'coeur/libs/animated';

// import Colors from 'coeur/constants/color';
// import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
// import IconBit from 'modules/bits/icon';
import ImageBit from 'modules/bits/image';
// import LoaderBit from 'modules/bits/loader';
import SourceSansBit from 'modules/bits/source.sans';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style'

const ProfileImage = ImageBit.resolve('profile.png');


export default ConnectHelper(
	class BioPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				image: PropTypes.string,
				name: PropTypes.string,
				hobbies: PropTypes.string,
				isFollowing: PropTypes.bool,
				isMe: PropTypes.bool,
				onToggleFollowing: PropTypes.func,
			}
		}

		static contexts = [
			PageContext,
		]

		static defaultProps = {
			image: undefined,
			name: '',
			hobbies: '',
			isFollowing: false,
			isMe: true,
		}

		constructor(p) {
			super(p, {
			}, [
				'onLogout',
			]);
		}

		onLogout() {
			MeManager.logout()

			this.props.page.navigator.reset('signup')
		}

		render() {
			return (
				<BoxBit unflex row>
					<ImageBit
						source={this.props.image || ProfileImage }
						resizeMode={ImageBit.TYPES.COVER}
						style={Styles.image}
					/>
					<BoxBit>
						<TouchableBit unflex onPress={ this.onLogout }>
							<SourceSansBit type={ SourceSansBit.TYPES.NEW_HEADER_2 } weight="semibold" style={Styles.name}>{ this.props.name }</SourceSansBit>
						</TouchableBit>
						<SourceSansBit type={ SourceSansBit.TYPES.NEW_SUBHEADER_1 } style={Styles.hobby}>
							{ this.props.hobbies }
						</SourceSansBit>
						{ this.props.isMe ? false : (
							<ButtonBit
								title={ this.props.isFollowing ? 'Following' : 'Follow'}
								type={ ButtonBit.TYPES.SHAPES.BADGE }
								theme={ this.props.isFollowing ? ButtonBit.TYPES.THEMES.ACTIVATED : ButtonBit.TYPES.THEMES.PRIMARY }
								onPress={ this.props.onToggleFollowing }
								width={ ButtonBit.TYPES.WIDTHS.FIT }
							/>
						) }
					</BoxBit>
				</BoxBit>
			)
		}
	}
)
