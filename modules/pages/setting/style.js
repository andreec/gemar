import StyleSheet from 'coeur/libs/style.sheet'
// import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	page: {
	 	paddingBottom: 75,
	},
	header: {
		// paddingTop: 25,
		paddingLeft: 17,
		paddingRight: 17,
		// borderBottomWidth: 1.5,
		// borderBottomColor: Colors.new.grey.palette(1),
		// backgroundColor: Colors.white.primary,
	},
	container: {
		paddingTop: 0,
		paddingBottom: 0,
	},
	list: {
		marginLeft: 30,
		paddingTop: 8,
		paddingBottom: 8,
		paddingLeft: 0,
		borderTopWidth: 0,
		alignItems: 'center',
	},
	loader: {
		height: 31,
	},
	counts: {
		marginTop: 24,
		marginBottom: 40,
	},
	bell: {
		marginRight: 10,
	},
})
