import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	line: {
		borderTopWidth: 1,
		borderTopColor: Colors.new.grey.palette(1),
		position: 'absolute',
		top: '50%',
		left: 0,
		right: 0,
	},
	container: {
		paddingTop: 4,
		paddingBottom: 4,
		paddingRight: 9,
		paddingLeft: 9,
		borderWidth: 1,
		borderColor: Colors.new.black.palette(4),
		borderRadius: 12,
		backgroundColor: Colors.new.white.palette(1),
	},
	text: {
		color: Colors.new.black.palette(4),
	},
})
