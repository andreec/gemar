import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

// import PageManager from 'app/managers/page';

// import TrackingHandler from 'app/handlers/tracking';

// import Animated from 'coeur/libs/animated';

import Colors from 'coeur/constants/color';
// import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
import IconBit from 'modules/bits/icon';
// import ImageBit from 'modules/bits/image';
// import LoaderBit from 'modules/bits/loader';
// import SourceSansBit from 'modules/bits/source.sans';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style'


export default ConnectHelper(
	class TabPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				tabs: PropTypes.array,
				activeIndex: PropTypes.number,
				onPress: PropTypes.func.isRequired,
			}
		}

		static defaultProps = {
			tabs: [
				'all-icon',
				'social',
				'market-outline',
				'job-outline',
			],
			activeIndex: 0,
		}

		constructor(p) {
			super(p, {
			}, [
				'tabRenderer',
			]);
		}

		tabRenderer(icon, i) {
			return (
				<TouchableBit key={i} centering onPress={ this.props.onPress.bind(this, i) }>
					<IconBit
						name={ icon }
						size={ 24 }
						color={ this.props.activeIndex === i ? Colors.new.black.palette(1) : Colors.new.black.palette(5) }
					/>
				</TouchableBit>
			)
		}

		render() {
			return (
				<BoxBit unflex row style={Styles.container}>
					{ this.props.tabs.map(this.tabRenderer) }
				</BoxBit>
			)
		}
	}
)
