import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

// import PageManager from 'app/managers/page';
import MeManager from 'app/managers/me';

// import FormatHelper from 'coeur/helpers/format';

import CommonHelper from 'coeur/helpers/common'

import CloudinaryHandler from 'app/handlers/cloudinary';

import Colors from 'coeur/constants/color';
// import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
// import BoxImageBit from 'modules/bits/box.image';
import ButtonBit from 'modules/bits/button';
import ImageBit from 'modules/bits/image';
import InputFileBit from 'modules/bits/input.file';
import PageBit from 'modules/bits/page';
import LoaderBit from 'modules/bits/loader';
// import IconBit from 'modules/bits/icon';
// import GalleryBit from 'modules/bits/gallery';
import HeaderBit from 'modules/bits/header';
import ShadowBit from 'modules/bits/shadow';
// import TouchableBit from 'modules/bits/touchable';
// import TextBit from 'modules/bits/text';

import HeaderLego from 'modules/legos/header';

import FormPagelet from 'modules/pagelets/form';

import Styles from './style';

const ProfileImage = ImageBit.resolve('profile.png');

// import {
// 	BoxesPart,
// 	CountdownPart,
// 	PromoPart,
// } from '../matchbox'
// import FooterPart from './_footer';
// import HowPart from './_how';
// import IntroductionPart from './_introduction';


export default ConnectHelper(
	class ProfileEditPage extends PageModel {

		static routeName = 'profile.edit'

		static stateToProps(state) {
			return {
				me: state.me,
				image: state.me.profile,
				// hobbies: state.me.hobbyIds.map(hobbyId => {
				// 	console.log(hobbyId);
				// 	return state.hobbies.get(hobbyId)
				// }).map(hobby => {
				// 	return hobby.title
				// }).join(' & '),
				hobbies: [],
			}
		}

		constructor(p) {
			super(p, {
				currentPhoto: p.image || ProfileImage,
				isSavingPhoto: false,
				isDone: false,
				isSaving: false,
				isChanged: false,
				isPublicValid: true,
				isPrivateValid: true,
			}, [
				'onUpdatePublic',
				'onUpdatePrivate',
				'onUpdateProfile',
				'updateProfilePicture',
			]);

			this.__userData = {}
		}

		header = {
			// title: 'EDIT PROFILE',
			leftActions: [{
				type: HeaderBit.TYPES.BACK,
			}],
			rightActions: [{
				type: HeaderBit.TYPES.BLANK,
			}],
		}

		profileImageRenderer() {
			return (
				<BoxBit unflex centering style={Styles.profile}>
					{ this.state.isSavingPhoto ? (
						<BoxBit centering unflex style={Styles.image}>
							<LoaderBit />
						</BoxBit>
					) : (
						<InputFileBit
							key={ this.state.currentPhoto }
							value={ this.state.currentPhoto }
							style={Styles.image}
							onChange={ this.updateProfilePicture }
						/>
					) }
				</BoxBit>
			)
		}

		updateProfilePicture(id, file) {
			// this.log(file)
			if(file && file.uri) {
				this.setState({
					isSavingPhoto: true,
				}, () => {
					CloudinaryHandler.upload({
						type: 'image/jpeg',
						uri: file.uri.replace('file://', ''),
						name: `user_${this.props.me.id}`,
					}).then(res => res.json()).then(result => {
						this.utilities.notification.show({
							message: 'Profile picture changed',
							type: this.utilities.notification.TYPES.SUCCESS,
						})
						this.setState({
							isSavingPhoto: false,
							currentPhoto: result.secure_url,
						})

						MeManager.updateProfile({
							profile: result.secure_url,
						})
					}).catch(err => {
						this.warn(err)

						this.utilities.notification.show({
							title: 'Oops…',
							message: 'Something went wrong. Please try again later.',
						})

						this.setState({
							isSavingPhoto: false,
							currentPhoto: this.props.image || ProfileImage,
						})
					})
				})
			}
		}

		onUpdateProfile() {
			this.setState({
				isSaving: true,
			}, () => {
				MeManager.updateProfile(CommonHelper.stripUndefined(this.__userData)).then(() => {
					this.setState({
						isSaving: false,
						isChanged: false,
					})

					this.utilities.notification.show({
						message: 'Your data is updated successfuly',
						type: this.utilities.notification.TYPES.SUCCESS,
					})

					this.navigator.back()
				}).catch(() => {
					this.setState({
						isSaving: false,
					})

					this.utilities.notification.show({
						message: 'Oops… Something wen\'t wrong. Please try again later.',
					})
				})
			})
		}

		onUpdatePublic(isValid, {
			name,
			status,
			bio,
			job,
		}) {
			this.setState({
				isChanged: true,
				isPublicValid: isValid,
			})

			this.__userData.name = name.value
			this.__userData.status = status.value
			this.__userData.bio = bio.value
			this.__userData.job = job.value
		}

		onUpdatePrivate(isValid, {
			phone,
			birthdate,
			gender,
		}) {
			this.setState({
				isChanged: true,
				isPrivateValid: isValid,
			})

			this.__userData.phone = phone.value
			this.__userData.birthdate = birthdate.value
			this.__userData.gender = gender.value
		}

		render() {
			return super.render(
				<PageBit type={PageBit.TYPES.THICK} header={(
					<HeaderBit { ...this.header } style={Styles.header} />
				)} footer={(
					<ShadowBit
						y={-1}
						blur={8}
						color={Colors.new.grey.palette(2)}
						style={ Styles.footer }
					>
						<BoxBit unflex style={Styles.button}>
							<ButtonBit
								type={ButtonBit.TYPES.SHAPES.PROGRESS}
								state={ this.state.isChanged && this.state.isPrivateValid && this.state.isPublicValid ? this.state.isSaving && ButtonBit.TYPES.STATES.LOADING || ButtonBit.TYPES.STATES.NORMAL : ButtonBit.TYPES.STATES.DISABLED }
								title="SAVE CHANGE"
								onPress={ this.onUpdateProfile }
							/>
						</BoxBit>
					</ShadowBit>
				)} style={Styles.container} contentContainerStyle={Styles.page} >
					{ this.profileImageRenderer() }
					<HeaderLego
						title="Public profile"
						description="You will share these information publicly."
					/>
					<FormPagelet
						config={[{
							id: 'name',
							title: 'NAME',
							type: FormPagelet.TYPES.NAME,
							value: this.props.me.name,
							style: Styles.firstInput,
						}, {
							id: 'username',
							title: 'USERNAME',
							type: FormPagelet.TYPES.INPUT,
							value: `@${this.props.me.username}`,
							disabled: true,
						}, {
							id: 'hobby',
							title: 'HOBBIES',
							type: FormPagelet.TYPES.INPUT,
							value: this.props.hobbies,
							disabled: true,
						}, {
							id: 'status',
							title: 'STATUS',
							type: FormPagelet.TYPES.SELECTION,
							options: [{
								title: 'I\'m ready for adventure',
								selected: this.props.me.status === 'I\'m ready for adventure',
							}, {
								title: 'Sorry, I\'m not available',
								selected: this.props.me.status === 'Sorry, I\'m not available',
							}],
							// value: 'I\'m ready for adventure',
						}, {
							id: 'bio',
							title: 'BIO',
							type: FormPagelet.TYPES.TEXTAREA,
							value: this.props.me.bio,
						}, {
							id: 'job',
							title: 'JOB',
							type: FormPagelet.TYPES.INPUT,
							value: this.props.me.job,
						}]}
						style={Styles.form}
						onUpdate={ this.onUpdatePublic }
					/>
					<BoxBit unflex style={Styles.divider} />
					<HeaderLego
						title="Private information"
						description="We won’t share this information to others."
					/>
					<FormPagelet
						config={[{
							id: 'email',
							title: 'EMAIL',
							type: FormPagelet.TYPES.Email,
							value: this.props.me.email,
							disabled: true,
							style: Styles.firstInput,
						}, {
							id: 'phone',
							title: 'PHONE NUMBER',
							type: FormPagelet.TYPES.PHONE,
							value: this.props.me.phone,
						}, {
							id: 'birthdate',
							title: 'BIRTHDATE',
							type: FormPagelet.TYPES.BIRTHDATE,
							value: this.props.me.birthdate,
						}, {
							id: 'gender',
							title: 'GENDER',
							type: FormPagelet.TYPES.SELECTION,
							options: [{
								title: 'Male',
								selected: this.props.me.gender === 'Male',
							}, {
								title: 'Female',
								selected: this.props.me.gender === 'Female',
							}],
						}]}
						style={Styles.form}
						onUpdate={ this.onUpdatePrivate }
					/>
					<BoxBit unflex style={Styles.divider} />
					<BoxBit unflex style={Styles.divider} />
				</PageBit>
			)
		}
	}
)
