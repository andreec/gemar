import StyleSheet from 'coeur/libs/style.sheet'

import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	header: {
		backgroundColor: Colors.solid.grey.palette(1),
	},
	footer: {
		width: Sizes.app.width,
		backgroundColor: Colors.new.white.primary,
	},
	container: {
		paddingBottom: 17,
		backgroundColor: Colors.solid.grey.palette(1),
	},
	button: {
		paddingTop: 11,
		paddingBottom: 11,
		paddingLeft: 17,
		paddingRight: 17,
	},
	form: {
		marginLeft: -Sizes.margin.thick,
		marginRight: -Sizes.margin.thick,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
		// backgroundColor: Colors.new.grey.palette(7),
		paddingTop: 11,
		paddingBottom: 17,
		// marginLeft: 2,
		// paddingLeft: 15,
		// borderLeftWidth: 3,
		// borderLeftColor: Colors.new.grey.palette(1),
	},
	divider: {
		height: 30,
		marginLeft: -Sizes.margin.thick,
		marginRight: -Sizes.margin.thick,
		backgroundColor: Colors.solid.grey.palette(1),
	},
	firstInput: {
		marginTop: 0,
	},
	profile: {
		paddingTop: 20,
		paddingBottom: 50,
		marginLeft: -Sizes.margin.thick,
		marginRight: -Sizes.margin.thick,
		backgroundColor: Colors.solid.grey.palette(1),
	},
	image: {
		height: 146,
		width: 146,
		borderRadius: 83,
		overflow: 'hidden',
		backgroundColor: Colors.new.grey.palette(1),
	},
})
