import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

// import PageManager from 'app/managers/page';

// import TrackingHandler from 'app/handlers/tracking';

// import Animated from 'coeur/libs/animated';

import BoxBit from 'modules/bits/box';
// import ImageBit from 'modules/bits/image';
import LoaderBit from 'modules/bits/loader';
import SourceSansBit from 'modules/bits/source.sans';

import CardUserFollowComponent from 'modules/components/card.user.follow';

import Styles from './style'

import {
	FlatList,
} from 'react-native';

// import {
// 	BoxesPart,
// 	CountdownPart,
// 	PromoPart,
// } from '../matchbox'
// import FooterPart from './_footer';
// import HowPart from './_how';
// import IntroductionPart from './_introduction';

export default ConnectHelper(
	class ContentPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				userIds: PropTypes.array,
				isLoading: PropTypes.bool,
			}
		}

		static defaultProps = {
			userIds: [],
		}

		constructor(p) {
			super(p, {
			}, [
				'bindList',
				'rowRenderer',
			]);

			this._scroller = undefined
		}

		componentDidMount() {
			// super.componentDidMount()

			// let index = 1;
			// this.setInterval(() => {
			// 	this._scroller &&
			// 	this._scroller.scrollToIndex({index: index++})
			// }, 4000)
		}

		bindList(scroller) {
			this._scroller = scroller
		}

		keyExtractor(item, index) {
			return `${index}`
		}

		rowRenderer({ item, index}) {
			return (
				<CardUserFollowComponent key={index} id={item} isActive style={Styles.card} />
			)
		}

		render() {
			return (
				<BoxBit>
					<FlatList
						ListHeaderComponent={ this.props.header }
						ListEmptyComponent={ !this.props.isLoading && (
							<BoxBit centering style={Styles.container}>
								<SourceSansBit type={SourceSansBit.TYPES.NEW_NOTE}>No post yet</SourceSansBit>
							</BoxBit>
						) || undefined }
						ListFooterComponent={ this.props.isLoading && (
							<BoxBit type={BoxBit.TYPES.ALL_THICK} centering style={Styles.container}>
								<LoaderBit />
							</BoxBit>
						) || undefined }
						keyExtractor={ this.props.keyExtractor || this.keyExtractor }
						extraData={ this.props.updater }
						ref={ this.bindList }
						data={ this.props.userIds }
						onEndReached={ this.props.onEndReached }
						onEndReachedThreshold={ .5 }
						onScrollToIndexFailed={()=>{}}
						renderItem={ this.rowRenderer }
					/>
				</BoxBit>
			)
		}
	}
)
