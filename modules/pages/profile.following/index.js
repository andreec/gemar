import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

// import PageManager from 'app/managers/page';
import UserManager from 'app/managers/user';

// import FormatHelper from 'coeur/helpers/format';

// import Colors from 'coeur/constants/color';
// import Sizes from 'coeur/constants/size';

// import BoxBit from 'modules/bits/box';
// import BoxImageBit from 'modules/bits/box.image';
// import ButtonBit from 'modules/bits/button';
// import ImageBit from 'modules/bits/image';
// import SourceSansBit from 'modules/bits/source.sans';
import PageBit from 'modules/bits/page';
// import IconBit from 'modules/bits/icon';
// import GalleryBit from 'modules/bits/gallery';
import HeaderBit from 'modules/bits/header';
// import TouchableBit from 'modules/bits/touchable';
// import TextBit from 'modules/bits/text';

import HeaderLego from 'modules/legos/header';

import ContentPart from './_content'

import Styles from './style';

// import {
// 	BoxesPart,
// 	CountdownPart,
// 	PromoPart,
// } from '../matchbox'
// import FooterPart from './_footer';
// import HowPart from './_how';
// import IntroductionPart from './_introduction';


export default ConnectHelper(
	class ProfileFollowingPage extends PageModel {

		static routeName = 'profile.following'

		static stateToProps(state) {
			return {
				me: state.me,
			}
		}

		constructor(p) {
			super(p, {
				isLoading: false,
				userIds: [],
			}, [
				'getData',
			]);
		}

		componentDidMount() {
			this.getData()
		}

		getData() {
			if (!this.state.isLoading) {
				this.setState({
					isLoading: true,
				})

				UserManager.getFollowers({
					filter: '',
					offset: this.state.userIds.length,
				}).then(users => {
					this.setState({
						isLoading: false,
						userIds: [...this.state.userIds, ...users.map(user => user.id)],
					})
				})
			}
		}

		header = {
			// title: 'LIKES',
			leftActions: [{
				type: HeaderBit.TYPES.BACK,
			}],
			rightActions: [{
				type: HeaderBit.TYPES.BLANK,
			}],
		}

		render() {
			return super.render(
				<PageBit scroll={false} header={ ( <HeaderBit { ...this.header } /> ) } >
					<ContentPart
						header={<HeaderLego title="Following" description={'“I don\'t like following in people\'s footsteps; I like making my own trail.”\n― The Miz'} style={Styles.header} /> }
						isLoading={ this.state.isLoading }
						userIds={ this.state.userIds }
						onEndReached={ this.getData }
					/>
				</PageBit>
			)
		}
	}
)
