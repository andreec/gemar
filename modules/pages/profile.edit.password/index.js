import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

// import PageManager from 'app/managers/page';
import MeManager from 'app/managers/me';

import Colors from 'coeur/constants/color';
// import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
// import BoxImageBit from 'modules/bits/box.image';
import ButtonBit from 'modules/bits/button';
import PageBit from 'modules/bits/page';
import HeaderBit from 'modules/bits/header';
import ShadowBit from 'modules/bits/shadow';

import HeaderLego from 'modules/legos/header';

import FormPagelet from 'modules/pagelets/form';

import Styles from './style';

// import {
// 	BoxesPart,
// 	CountdownPart,
// 	PromoPart,
// } from '../matchbox'
// import FooterPart from './_footer';
// import HowPart from './_how';
// import IntroductionPart from './_introduction';


export default ConnectHelper(
	class ProfileEditPasswordPage extends PageModel {

		static routeName = 'profile.edit.password'

		static stateToProps(state) {
			return {
				me: state.me,
			}
		}

		constructor(p) {
			super(p, {
				isDone: false,
				isSaving: false,
			}, [
				'checkPassword',
				'checkRepeatPassword',
				'onUpdate',
				'onSavePassword',
			]);

			this.__userData = {}
		}

		header = {
			// title: 'CHANGE PASSWORD',
			leftActions: [{
				type: HeaderBit.TYPES.BACK,
			}],
			rightActions: [{
				type: HeaderBit.TYPES.BLANK,
			}],
		}

		checkPassword(value) {
			const password = this.__userData.currentPassword && this.__userData.currentPassword.value
			return value !== password
		}

		checkRepeatPassword(value) {
			const password = this.__userData.newPassword && this.__userData.newPassword.value
			return value === password
		}

		onUpdate(isDone, data) {
			if (isDone !== this.state.isDone) {
				this.setState({
					isDone: isDone,
				})
			}

			this.__userData = {
				...this.__userData,
				...data,
			}
		}

		onSavePassword() {
			this.setState({
				isSaving: true,
			}, () => {
				MeManager.changePassword({
					currentPassword: this.__userData.currentPassword.value,
					password1: this.__userData.newPassword.value,
					password2: this.__userData.repeatPassword.value,
				}).then(() => {
					this.utilities.notification.show({
						message: '🎉 Password updated successfuly',
						type: this.utilities.notification.TYPES.SUCCESS,
					})

					this.navigator.back()
				}).catch(err => {
					if (err && err.code === 'ERR_105') {
						this.utilities.notification.show({
							message: 'Oops… current password is not correct',
						})
					} else {
						this.utilities.notification.show({
							message: 'Oops… Sorry, something wen\'t wrong, please try again later.',
						})
					}

					this.setState({
						isSaving: false,
					})
				})
			})
		}

		render() {
			return super.render(
				<PageBit type={PageBit.TYPES.THICK} header={(
					<HeaderBit { ...this.header } />
				)} footer={(
					<ShadowBit
						y={-1}
						blur={8}
						color={Colors.new.grey.palette(2)}
						style={ Styles.footer }
					>
						<BoxBit unflex style={Styles.button}>
							<ButtonBit
								type={ButtonBit.TYPES.SHAPES.PROGRESS}
								state={ this.state.isDone ? this.state.isSaving && ButtonBit.TYPES.STATES.LOADING || ButtonBit.TYPES.STATES.NORMAL : ButtonBit.TYPES.STATES.DISABLED }
								title="SAVE NEW PASSWORD"
								onPress={ this.onSavePassword }
							/>
						</BoxBit>
					</ShadowBit>
				)} contentContainerStyle={Styles.page} >
					<HeaderLego
						title="Changing password"
						description="Your password must be 6 or more characters long and include at least one number or symbol."
					/>
					<FormPagelet
						config={[{
							id: 'currentPassword',
							title: 'CURRENT PASSWORD',
							// required: true,
							type: FormPagelet.TYPES.PASSWORD,
						}, {
							id: 'newPassword',
							title: 'NEW PASSWORD',
							required: true,
							validator: this.checkPassword,
							type: FormPagelet.TYPES.PASSWORD,
						}, {
							id: 'repeatPassword',
							title: 'CONFIRM NEW PASSWORD',
							required: true,
							validator: this.checkRepeatPassword,
							type: FormPagelet.TYPES.PASSWORD,
						}]}
						onUpdate={ this.onUpdate }
					/>
				</PageBit>
			)
		}
	}
)
