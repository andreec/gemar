import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

const cardWidth = Math.floor((Sizes.screen.width - 2 - (2 * Sizes.margin.thick)) / 3)


export default StyleSheet.create({
	header: {
		marginTop: 58,
		marginBottom: 36,
		color: Colors.new.black.palette(2),
	},
	padder: {
		paddingBottom: 12,
	},
	center: {
		justifyContent: 'center',
	},
	input: {
		textAlign: 'center',
	},
	card: {
		width: cardWidth,
		height: cardWidth,
		marginBottom: 1.5,
	},
	name: {
		color: Colors.primary,
	}
})
