import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';

// import Colors from 'coeur/constants/color';
// import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
import GridBit from 'modules/bits/grid';
import SourceSansBit from 'modules/bits/source.sans';
import TextBit from 'modules/bits/text';

import CardHobbyComponent from 'modules/components/card.hobby';

import Styles from './style'

import _ from 'lodash'


export default ConnectHelper(
	class HobbyPart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				name: PropTypes.string,
				onUpdate: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static stateToProps(state) {
			return {
				hobbyIds: state.hobbies.keySeq().toJS(),
			}
		}

		constructor(p) {
			super(p, {
				selectedIds: [],
			}, [
				'keyExtractor',
				'onToggleSelect',
				'cardRenderer',
			])
		}

		keyExtractor(ids) {
			return ids.map(id => {
				return `${id}_${this.state.selectedIds.indexOf(id) > -1}`
			}).join(',')
		}

		onToggleSelect(id) {
			if(this.state.selectedIds.indexOf(id) > -1) {
				// remove
				this.setState({
					selectedIds: _.difference(this.state.selectedIds, [id]),
				}, this.onUpdate)
			} else {
				// add
				this.setState({
					selectedIds: [id, ...this.state.selectedIds].slice(0, 2),
				}, this.onUpdate)
			}
		}

		onUpdate() {
			this.props.onUpdate &&
			this.props.onUpdate(this.state.selectedIds, this.state.selectedIds.length === 2)
		}

		cardRenderer(id) {
			return (
				<CardHobbyComponent selectable isActive={ this.state.selectedIds.indexOf(id) > -1 } id={id} key={id} onPress={ this.onToggleSelect } style={Styles.card} />
			)
		}

		render() {
			this.log(this.props, this.state)
			return (
				<BoxBit unflex type={BoxBit.TYPES.THICK} style={[Styles.padder, this.props.style]}>
					<SourceSansBit type={SourceSansBit.TYPES.NEW_PARAGRAPH_1} align="center" weight="normal" style={Styles.header}>
						Great! Now let us know you better.
						{'\n'}
						Please <TextBit weight="bold">select two</TextBit> of your most favorite ke-"Gemar"-an, <TextBit weight="bold" style={Styles.name}>@{this.props.name}</TextBit>!
					</SourceSansBit>
					<GridBit
						updater={ this.state.selectedIds.join(',') }
						keyExtractor={ this.keyExtractor }
						column={3}
						data={this.props.hobbyIds}
						renderItem={this.cardRenderer}
					/>
				</BoxBit>
			)
		}
	}
)
