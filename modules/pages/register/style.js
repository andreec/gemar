import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	content: {
		width: Sizes.screen.width,
	},
	header: {
		marginTop: 16,
		marginBottom: 8,
	},
	center: {
		justifyContent: 'center',
	},
	form: {
		marginBottom: 24,
	},
	colorBlack: {
		color: Colors.black.palette(5),
	},
	colorGrey: {
		color: Colors.black.palette(4),
	},
	button: {
		paddingTop: 11,
		paddingBottom: 11,
		paddingLeft: 17,
		paddingRight: 17,
		backgroundColor: Colors.white.primary,
	},
})
