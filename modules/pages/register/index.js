import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

import MeManager from 'app/managers/me';
import AccountService from 'app/services/account';
// import UserManager from 'app/managers/user';

import TrackingHandler from 'app/handlers/tracking';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import HeaderBit from 'modules/bits/header';
import PageBit from 'modules/bits/page';
// import ProgressBit from 'modules/bits/progress';
import ScrollSnapBit from 'modules/bits/scroll.snap';
import ShadowBit from 'modules/bits/shadow';


import FollowPart from './_follow';
import HobbyPart from './_hobby';
import TermPart from './_tnc';
import UsernamePart from './_username';

import {
	Keyboard,
} from 'react-native';

import Styles from './style';


export default ConnectHelper(
	class RegisterPage extends PageModel {

		static routeName = 'register'

		static propTypes(PropTypes) {
			return {
				email: PropTypes.string,
				password: PropTypes.string,
				type: PropTypes.string,
				accessToken: PropTypes.string,
				userID: PropTypes.string,
			}
		}

		header = {
			title: 'Create Account',
			leftActions: [{
				type: HeaderBit.TYPES.BACK,
				onPress: this.onNavigateToBack,
			}],
			rightActions: [{
				type: HeaderBit.TYPES.BLANK,
			}],
		}

		constructor(p) {
			super(p, {
				isLoading: false,
				activeIndex: 0,
				completeds: {
					0: false,
					1: false,
					2: false,
					3: false,
				},
			}, [
				'emailUpdater',
				'isEligibleToRegister',
				'checkPassword',
				'checkTakenEmail',
				'onNavigateToBack',
				'onUpdateUsername',
				'onUpdateHobby',
				'onUpdateFollower',
				'onUpdateTerm',
				'onUpdateData',
				'onNextPress',
				'onSubmitEditing',
				'onSuccess',
				// 'onError',
			])

			this.userData = {}
		}

		componentDidMount() {
			TrackingHandler.trackPageView(this.routeName)
		}

		emailUpdater() {
			return this.state.takenEmail && this.state.takenEmail.length
		}

		isEligibleToRegister() {
			return Object.values(this.state.completeds).findIndex(status => !status) === -1
		}

		checkPassword(value) {
			const password = this.userData.password && this.userData.password.value
			return value === password
		}

		checkTakenEmail(value) {
			return !this.state.takenEmail || !this.state.takenEmail.length || this.state.takenEmail.indexOf(value) === -1
		}

		onNavigateToBack() {
			if(this.state.activeIndex > 0) {
				this.setState({
					activeIndex: this.state.activeIndex - 1,
				})
			} else {
				this.navigator.back();
			}
		}

		onUpdateUsername(value, isValid) {
			this.userData.username = {
				value,
				isValid,
			}

			this.updateCompleted(isValid)
		}

		onUpdateHobby(value, isValid) {
			this.log(value)
			this.userData.hobbies = {
				value,
				isValid,
			}


			this.warn(this.userData);
			this.updateCompleted(isValid)
		}

		onUpdateFollower(value, isValid) {
			this.log(value)
			this.userData.followers = {
				value,
				isValid,
			}

			this.updateCompleted(isValid)
		}

		onUpdateTerm(value, isValid) {
			this.userData.terms = {
				value,
				isValid,
			}

			this.updateCompleted(isValid)
		}

		updateCompleted(isValid) {
			if(isValid && this.state.completeds[this.state.activeIndex] !== isValid) {
				this.setState({
					completeds: {
						...this.state.completeds,
						[this.state.activeIndex]: isValid,
					},
				})
			}
		}

		onUpdateData(isDone, data) {
			if(isDone !== this.state.completeds[this.state.activeIndex]) {
				this.setState({
					completeds: {
						...this.state.completeds,
						[this.state.activeIndex] : isDone,
					},
				})
			}

			this.userData = {
				...this.userData,
				...data,
			}
		}

		onNextPress() {
			if (this.state.completeds[this.state.activeIndex]) {
				if (!this.isEligibleToRegister()) {
					this.setState({
						activeIndex: this.state.activeIndex + 1,
					})
				} else {
					this.setState({
						isLoading: true,
					})

					if(this.props.type === 'email') {
						MeManager.register({
							email: this.props.email,
							username: this.userData.username.value,
							hobbyIds: this.userData.hobbies.value,
							// NOT PROVIDED BY API
							// followingUserIds: this.userData.followers.value,
							// status: this.userData.terms.value.ready ? 'I\'m ready for adventure' : 'Not available',
							password: this.props.password,
						}).then(this.onSuccess).catch(this.onError)
					} else {
						this.log(this.userData)

						MeManager.sosmedRegister({
							id: this.props.userID,
							type: this.props.type,
							email: this.props.email,
							username: this.userData.username.value,
							hobbyIds: this.userData.hobbies.value,
							
							// NOT PROVIDED BY API
							// followingUserIds: this.userData.followers.value,
							// status: this.userData.terms.value.ready ? 'I\'m ready for adventure' : 'Not available',
							accessToken: this.props.accessToken,
						}).then(this.onSuccess).catch(this.onError)

					}
				}
			}
		}

		onSubmitEditing() {
			Keyboard.dismiss()

			this.setTimeout(this.onNextPress, 300)
		}

		onSuccess(res) {
			console.warn('rest ', res);
			this.utilities.notification.show({
				type: this.utilities.notification.TYPES.SUCCESS,
				title: '🎉',
				message: `Welcome to Gemar, @${this.userData.username.value}!`,
			})

			if(this.props.redirect) {
				this.navigator.navigate(this.props.redirect)
			} else {
				this.navigator.navigate('RootNavigator')
			}
		}
		//
		// onError(err) {
		// 	this.warn(err)
		//
		// 	if(err.code === '021') {
		// 		this.setState({
		// 			isLoading: false,
		// 			activeIndex: 1,
		// 			takenEmail: this.state.takenEmail ? this.state.takenEmail.concat(this.userData.email.value) : [this.userData.email.value],
		// 		})
		//
		// 		this.utilities.notification.show({
		// 			title: 'Uh oh.',
		// 			message: 'Your email is already taken. Please try a different email.',
		// 			key: Date.now(),
		// 		})
		// 	} else if(err.code === '022') {
		// 		this.setState({
		// 			isLoading: false,
		// 		})
		//
		// 		this.utilities.notification.show({
		// 			title: 'Uh oh.',
		// 			message: 'Your email is registered. Please try logging in 😉',
		// 			key: Date.now(),
		// 		})
		// 	} else if(err.code === '019') {
		// 		this.navigator.navigate('verify', {
		// 			email: err.detail,
		// 			description: `We noticed that you have completed your Style Profile (${err.detail}) before, please verify your email to update it here.`,
		// 		})
		// 	} else {
		// 		this.setState({
		// 			isLoading: false,
		// 		})
		//
		// 		this.utilities.notification.show({
		// 			title: 'Uh oh.',
		// 			message: 'Something just went wrong 😱. While I\'m checking who\'s causing this mess, please try again in a few moments.',
		// 			key: Date.now(),
		// 		})
		// 	}
		// }

		render() {
			return super.render(
				<PageBit scroll={false}
					header={<HeaderBit { ...this.header } />}
				>
					<ScrollSnapBit disableGesture index={ this.state.activeIndex }>
						<UsernamePart onUpdate={this.onUpdateUsername} onSubmitEditing={ this.onSubmitEditing } style={Styles.content} />
						<HobbyPart name={ this.userData.username && this.userData.username.value } onUpdate={ this.onUpdateHobby } style={Styles.content} />
						<FollowPart onUpdate={ this.onUpdateFollower } style={Styles.content} />
						<TermPart hobbyIds={ this.userData.hobbies && this.userData.hobbies.value } onUpdate={ this.onUpdateTerm } style={Styles.content} />
					</ScrollSnapBit>
					<ShadowBit
						y={-1}
						blur={8}
						color={ Colors.new.grey.palette(2) }
					>
						<BoxBit unflex style={Styles.button}>
							<ButtonBit
								title={ this.isEligibleToRegister() ? 'Create My Account' : 'Next' }
								type={ ButtonBit.TYPES.SHAPES.PROGRESS }
								// size={ ButtonBit.TYPES.SIZES.COMPACT }
								// theme={ ButtonBit.TYPES.THEMES.CONFIRMATION }
								state={ this.state.completeds[this.state.activeIndex]
									? this.state.isLoading
										&& ButtonBit.TYPES.STATES.LOADING
										|| ButtonBit.TYPES.STATES.NORMAL
									: ButtonBit.TYPES.STATES.DISABLED
								}
								onPress={ this.onNextPress }
							/>
						</BoxBit>
					</ShadowBit>
				</PageBit>
			)
		}
	}
)
