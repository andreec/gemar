import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';

import HobbyManager from 'app/managers/hobby';

// import Colors from 'coeur/constants/color';
// import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import CheckboxBit from 'modules/bits/checkbox';
import SourceSansBit from 'modules/bits/source.sans';
import TextBit from 'modules/bits/text';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style'


export default ConnectHelper(
	class TermPart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				hobbyIds: PropTypes.array,
				onUpdate: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static stateToProps(state, oP) {
			const hobbyIds = oP.hobbyIds || [1, 2]

			return {
				hobbies: hobbyIds.map(hobbyId => {
					return state.hobbies.get(hobbyId) || HobbyManager.get(hobbyId)
				}),
			}
		}

		static defaultProps = {
			hobbyIds: [],
		}

		constructor(p) {
			super(p, {
				selected: -1,
				email: true,
			}, [
				'onSelect',
				'onToggleSelectEmail',
				'onUpdate',
			])
		}

		onSelect(index) {
			this.setState({
				selected: index,
			}, this.onUpdate)
		}

		onToggleSelectEmail() {
			this.setState({
				email: !this.state.email,
			}, this.onUpdate)
		}

		onUpdate() {
			this.props.onUpdate &&
			this.props.onUpdate({
				promotional: this.state.email,
				ready: this.state.selected === 0,
			}, this.state.selected !== -1)
		}

		render() {
			this.log(this.props, this.state)
			return (
				<BoxBit unflex type={BoxBit.TYPES.THICK} style={this.props.style}>
					<SourceSansBit type={SourceSansBit.TYPES.NEW_PARAGRAPH_1} align="center" style={Styles.header}>
						Last question!
						{'\n'}
						Now if others want to invite you to an activity related with <TextBit weigth="semibold" style={Styles.hobby}>{ this.props.hobbies[0].title }</TextBit> or <TextBit weigth="semibold" style={Styles.hobby}>{ this.props.hobbies[1].title }</TextBit>, what you would say?
					</SourceSansBit>
					<BoxBit style={Styles.center}>
						<ButtonBit
							title={ 'I\'m ready for adventure' }
							type={ this.state.selected === 0 ? ButtonBit.TYPES.SHAPES.PROGRESS : ButtonBit.TYPES.SHAPES.RECTANGLE }
							onPress={ this.onSelect.bind(this, 0) }
							style={[Styles.button, Styles.padder]}
						/>

						<ButtonBit
							title={ 'Sorry, I\'m not available' }
							type={ this.state.selected === 1 ? ButtonBit.TYPES.SHAPES.PROGRESS : ButtonBit.TYPES.SHAPES.RECTANGLE }
							onPress={ this.onSelect.bind(this, 1) }
							style={[Styles.button, Styles.thickPadder]}
						/>

						<TouchableBit unflex row onPress={ this.onToggleSelectEmail } style={Styles.tnc}>
							<CheckboxBit
								isActive={ this.state.email }
								onPress={ this.onToggleSelectEmail }
								style={Styles.checkbox}
							/>
							<SourceSansBit type={SourceSansBit.TYPES.NEW_NOTE_2}>I'd like to receive promotional mails and newsletters from Gemar and its affiliates</SourceSansBit>
						</TouchableBit>
					</BoxBit>
				</BoxBit>
			)
		}
	}
)
