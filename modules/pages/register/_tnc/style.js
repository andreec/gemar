import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	header: {
		marginTop: 58,
		marginBottom: 16,
		color: Colors.new.black.palette(2),
	},
	button: {
		marginLeft: 40,
		marginRight: 40,
	},
	padder: {
		marginBottom: 12,
	},
	thickPadder: {
		marginBottom: 24,
	},
	center: {
		justifyContent: 'center',
	},
	checkbox: {
		marginRight: 12,
	},
	tnc: {
		marginRight: 60,
		marginLeft: 60,
	},
	hobby: {
		color: Colors.primary,
	},
})
