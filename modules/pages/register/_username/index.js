import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

// import Colors from 'coeur/constants/color';
// import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
import IconBit from 'modules/bits/icon';
import InputValidatedBit from 'modules/bits/input.validated';
import SourceSansBit from 'modules/bits/source.sans';

import Styles from './style'


export default ConnectHelper(
	class UsernamePart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				onUpdate: PropTypes.func,
				onSubmitEditing: PropTypes.func,
				style: PropTypes.style,
			}
		}

		constructor(p) {
			super(p, {}, [
				'validator',
			])
		}

		validator(input) {
			return input.length > 5 && input.length < 30 && /^[A-Za-z0-9_']+$/.test(input)
		}

		render() {
			this.log(this.props, this.state)
			return (
				<BoxBit unflex type={BoxBit.TYPES.THICK} style={this.props.style}>
					<SourceSansBit type={SourceSansBit.TYPES.NEW_PARAGRAPH_1} align="center" style={Styles.header}>
						We are more than thrilled to have you here!
						{ '\n' }
						Please complete the creation of your account
						{ '\n' }
						first by choosing your username
					</SourceSansBit>
					<BoxBit style={Styles.center}>
						<InputValidatedBit
							type={InputValidatedBit.TYPES.USERNAME}
							title=" "
							placeholder="Username"
							validator={ this.validator }
							onChange={ this.props.onUpdate }
							onSubmitEditing={ this.props.onSubmitEditing }
							prefix={(
								<IconBit
									name="user"
									size={24}
									style={Styles.inputIcon}
								/>
							)}
							inputStyle={Styles.input}
						/>
						<SourceSansBit type={SourceSansBit.TYPES.NEW_PARAGRAPH_1} align="center" style={Styles.description}>Your username should only use letters, numbers, underscores and periods.</SourceSansBit>
					</BoxBit>
					<BoxBit />
				</BoxBit>
			)
		}
	}
)
