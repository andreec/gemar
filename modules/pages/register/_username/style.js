import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	header: {
		marginTop: 58,
		// marginBottom: 32,
		// color: Colors._black.palette(4),
	},
	padder: {
		paddingBottom: 12,
	},
	center: {
		justifyContent: 'center',
	},
	input: {
		// textAlign: 'center',
	},
	inputIcon: {
		marginRight: 8,
	},
	description: {
		marginTop: 9,
		paddingLeft: 42,
		paddingRight: 42,
		color: Colors.new.black.palette(4),
	},
})
