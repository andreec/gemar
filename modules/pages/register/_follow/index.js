import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';

// import Colors from 'coeur/constants/color';
// import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
import FlatlistBit from 'modules/bits/flatlist';
import SourceSansBit from 'modules/bits/source.sans';
// import TextBit from 'modules/bits/text';

import CardUserFollowComponent from 'modules/components/card.user.follow';

import Styles from './style'

import _ from 'lodash'


export default ConnectHelper(
	class FollowPart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				style: PropTypes.style,
			}
		}

		static stateToProps(state) {
			return {
				userIds: state.users.sortBy(user => -user.followerCount).keySeq().toJS(),
			}
		}

		constructor(p) {
			super(p, {
				selectedIds: [],
			}, [
				'onToggleSelect',
				'cardRenderer',
			])
		}

		onToggleSelect(id) {
			if(this.state.selectedIds.indexOf(id) > -1) {
				// remove
				this.setState({
					selectedIds: _.difference(this.state.selectedIds, [id]),
				}, this.onUpdate)
			} else {
				// add
				this.setState({
					selectedIds: [...this.state.selectedIds, id],
				}, this.onUpdate)
			}
		}

		onUpdate() {
			this.props.onUpdate &&
			this.props.onUpdate(this.state.selectedIds, this.state.selectedIds.length >= 2)
		}

		cardRenderer({item: id}) {
			return (
				<CardUserFollowComponent id={id} key={id} isActive={ this.state.selectedIds.indexOf(id) > -1 } onPress={ this.onToggleSelect } style={Styles.card} />
			)
		}

		render() {
			this.log(this.props, this.state)
			return (
				<BoxBit unflex type={BoxBit.TYPES.THICK} style={this.props.style}>
					<SourceSansBit type={SourceSansBit.TYPES.NEW_PARAGRAPH_1} align="center" weight="normal" style={Styles.header}>
						Introducing our top people to get along with. Maybe someday you could have a drink together ;)
					</SourceSansBit>
					<SourceSansBit align="center" italic type={SourceSansBit.TYPES.NEW_SUBHEADER_2} style={Styles.subheader}>Please select at least 2 persons to follow</SourceSansBit>
					<FlatlistBit
						updater={ this.state.selectedIds.join(',') }
						data={ this.props.userIds }
						renderItem={this.cardRenderer}
					/>
				</BoxBit>
			)
		}
	}
)
