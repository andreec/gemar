import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	header: {
		marginTop: 58,
		marginBottom: 8,
		color: Colors.new.black.palette(2),
	},
	subheader: {
		marginBottom: 36,
	},
	padder: {
		paddingBottom: 12,
	},
	center: {
		justifyContent: 'center',
	},
	card: {
		marginBottom: 12,
	},
})
