import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	marginHeader: {
		marginTop: 16,
		marginBottom: 8,
	},
	marginButton: {
		marginBottom: 24,
	},
	colorBlack: {
		color: Colors.black.palette(5),
	},
	colorGrey: {
		color: Colors.black.palette(4),
	},
})
