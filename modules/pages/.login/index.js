import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

import MeManager from 'app/managers/me';

import TrackingHandler from 'app/handlers/tracking';

import ButtonBit from 'modules/bits/button';
import SourceSansBit from 'modules/bits/source.sans';
import HeaderBit from 'modules/bits/header';
import PageBit from 'modules/bits/page';

import FormPagelet from 'modules/pagelets/form';

import Styles from './style';


export default ConnectHelper(
	class LoginPage extends PageModel {

		static routeName = 'login'

		header = {
			leftActions: [{
				type: HeaderBit.TYPES.BACK,
			}],
		}

		constructor(p) {
			super(p, {
				isError: false,
				isLoading: false,
				isAllValid: false,
			}, [
				'onNavigateToForgotPassword',
				'onUpdate',
				'onLogin',
				'onError',
			])

			this.userData = {}
			this.formConfig = [{
				id: 'email',
				isRequired: true,
				type: FormPagelet.TYPES.EMAIL,
				// placeholder: 'email@example.com',
			}, {
				id: 'password',
				isRequired: true,
				type: FormPagelet.TYPES.PASSWORD,
				// placeholder: 'Your secret',
			}]
		}

		componentDidMount() {
			TrackingHandler.trackPageView(this.routeName)
		}

		onNavigateToForgotPassword() {
			this.navigator.navigate('forgot', {
				email: this.userData.email && this.userData.email.value || undefined,
			})
		}

		onUpdate(isAllValid, data) {
			if(isAllValid !== this.state.isAllValid) {
				this.setState({
					isAllValid,
					isError: false,
				})
			} else if(this.state.isError === true) {
				this.setState({
					isError: false,
				})
			}

			this.userData = data
		}

		onError(err) {
			this.setState({
				isLoading: false,
				isError: true,
			})

			if(err && err.code === '018') {
				this.utilities.notification.show({
					title: 'Oops!',
					message: 'Either your email or password is invalid.',
					key: Date.now(),
				})
			} else if(err && err.code === '019') {
				this.navigator.navigate('verify', {
					email: err.detail,
					description: `We noticed that you have completed your Style Profile (${err.detail}) before, please verify your email to update it here.`,
				})
			} else {
				this.utilities.notification.show({
					title: 'Oops!',
					message: 'Something is wrong. Please try again later.',
					key: Date.now(),
				})
			}
		}

		onLogin() {
			// TODO
			this.setState({
				isLoading: true,
			}, () => {
				MeManager
					.login({
						email: this.userData.email.value,
						password: this.userData.password.value,
					})
					.then(() => {
						if(this.props.redirect) {
							this.navigator.navigate(this.props.redirect)
						} else {
							this.navigator.top()
						}
					})
					.catch(this.onError)
			})
		}

		render() {
			return super.render(
				<PageBit type={PageBit.TYPES.THICK}
					header={<HeaderBit { ...this.header } />}
				>
					<SourceSansBit type={SourceSansBit.TYPES.HEADER_4} style={[Styles.marginHeader, Styles.colorBlack]}>Welcome back!</SourceSansBit>
					<SourceSansBit type={SourceSansBit.TYPES.PARAGRAPH_2} style={[Styles.colorGrey]}>Please fill out your login detail below.</SourceSansBit>

					<FormPagelet
						config={this.formConfig}
						onUpdate={ this.onUpdate }
						onSubmit={ this.onLogin }
					/>

					<ButtonBit
						theme={ButtonBit.TYPES.THEMES.SECONDARY}
						type={ButtonBit.TYPES.SHAPES.GHOST}
						width={ButtonBit.TYPES.WIDTHS.FIT}
						title={'Forgot password?'}
						style={Styles.marginButton}
						onPress={ this.onNavigateToForgotPassword }
					/>

					<ButtonBit
						title="LOGIN"
						type={ ButtonBit.TYPES.SHAPES.PROGRESS }
						theme={ ButtonBit.TYPES.THEMES.CONFIRMATION }
						state={ !this.state.isAllValid ? ButtonBit.TYPES.STATES.DISABLED : this.state.isLoading && ButtonBit.TYPES.STATES.LOADING || ButtonBit.TYPES.STATES.NORMAL }
						onPress={ this.onLogin }
					/>
				</PageBit>
			)
		}
	}
)
