import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';
import FormatHelper from 'coeur/helpers/format';

import ShippingService from 'app/services/shipping';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import SourceSansBit from 'modules/bits/source.sans';
import ShadowBit from 'modules/bits/shadow';
import HeaderBit from 'modules/bits/header';
import LoaderBit from 'modules/bits/loader';
import PageBit from 'modules/bits/page';

import CardCartComponent from 'modules/components/card.cart';

import ListHeaderLego from 'modules/legos/list.header';

import AddressPagelet from 'modules/pagelets/address';

// import CardAddressComponent from 'modules/components/card.address';
// import {
// 	OrderPart,
// 	PricesPart,
// } from '../order.detail';
// import InputPromoPart from './_input.promo';
import AddressPart from './_address';
import PricesPart from './_prices';

import Styles from './style';


export default ConnectHelper(
	class CheckoutPage extends PageModel {

		static routeName = 'checkout'

		static propTypes(PropTypes) {
			return {
				me: PropTypes.object,
				cartItems: PropTypes.array,
			}
		}

		static stateToProps(state) {
			return {
				me: state.me,
				cartItems: state.me.cartItems,
				total: state.me.cartItems.reduce((sum, cartItem) => {
					const post = state.posts.get(cartItem.id)
						, product = state.products.get(post.refId) || { price: 0 }
					return sum + (product.price * cartItem.quantity)
				}, 0),
			}
		}

		header = {
			title: 'CHECKOUT',
			leftActions: [{
				type: HeaderBit.TYPES.CLOSE,
			}],
			rightActions: [{
				type: HeaderBit.TYPES.BLANK,
			}],
		}

		constructor(p) {
			super(p, {
				selectedAddressId: p.me.defaultAddressId,
				promoCode: undefined,
				isLoading: false,
				// isEmpty: !p.cart.length,
				isSending: false,
				isUpdating: false,
				isCouponValid: false,
				isClearing: false,
			}, [
				'canSend',
				'onSelectAddress',
				'onUpdateSelectedAddress',
				'onModalRequestClose',
				// 'onValidateCoupon',
				// 'onProceedToPayment',
				// 'onRedirectToPayment',
				// 'orderRenderer',
			])
		}

		componentWillMount() {
			// if(VolatileManager.isExpired('me')) {
			// 	MeManager.authenticate(this.props.me.token).then(() => {
			// 		VolatileManager.mark('me')
			// 	}).catch(() => {
			// 		this.utilities.notification.show({
			// 			title: 'Oops…',
			// 			message: 'Looks like you\'ve been logged out of your session. Try logging in.',
			// 		})
					
			// 		this.navigator.top()
					
			// 		MeManager.logout()
			// 	})
			// }
			ShippingService.getProvince().then(res => this.log(res))
		}

		shouldComponentUpdate(nP, nS) {
			if(nS.isClearing) return false

			return super.shouldComponentUpdate(nP, nS)
		}

		canSend() {
			return !this.state.isSending && this.state.selectedAddressId && this.state.selectedAddressId !== -1
		}

		onSelectAddress() {
			// TODO: update order shipping fee
			this.utilities.alert.modal({
				component: (
					<AddressPagelet
						selectionMode
						selectedAddressId={ this.state.selectedAddressId }
						onRequestClose={ this.onModalRequestClose }
						onSubmit={ this.onUpdateSelectedAddress }
					/>
				),
			})
		}

		onModalRequestClose() {
			this.utilities.alert.hide()
		}

		onUpdateSelectedAddress(aId) {
			this.setState({
				selectedAddressId: aId,
			}, () => {
				this.onModalRequestClose()
				this.getOrderDetail()
			})
		}

		// onValidateCoupon(couponCode) {
		// 	this.setState({
		// 		promoCode: couponCode,
		// 	}, () => {
		// 		this.getOrderDetail(true)
		// 	})
		// }

		// onProceedToPayment() {
		// 	TrackingHandler.trackEvent('matchbox-checkout', this.state.mockBatch.total)

		// 	this.utilities.alert.show({
		// 		title: 'Proceed to Payment',
		// 		message: 'You will be redirected to third-party secure payment page.',
		// 		actions: [{
		// 			title: 'OK',
		// 			type: 'OK',
		// 			onPress: this.onRedirectToPayment,
		// 		}, {
		// 			title: 'Don\'t Allow',
		// 			type: 'CANCEL',
		// 		}],
		// 	})
		// }

		// onRedirectToPayment() {
		// 	this.setState({
		// 		isSending: true,
		// 	}, () => {
		// 		BatchManager.create({
		// 			addressId: this.state.selectedAddressId,
		// 			products: this.props.cart,
		// 			couponCode: this.state.promoCode,
		// 		}).then(batch => {
		// 			// TODO: REDIRECT

		// 			BatchManager
		// 				.getCheckoutLink(batch.id)
		// 				.then(({
		// 					link,
		// 					data,
		// 				}) => {
		// 					CheckoutHelper.generateFormAutoSender(link, data)
		// 				})

		// 			// clear cart
		// 			MeManager.clearCart()
		// 		}).catch(err => {
		// 			this.warn(err)

		// 			this.utilities.notification.show({
		// 				title: 'Oops…',
		// 				message: 'There\'s something wrong. Please try again later.',
		// 			})

		// 			this.setState({
		// 				isSending: false,
		// 			})
		// 		})
		// 	})
		// }

		loadingRenderer() {
			return this.state.isLoading && !this.state.isEmpty && (
				<BoxBit centering>
					<LoaderBit color={Colors.coal.palette(3)} />
					<SourceSansBit type={SourceSansBit.TYPES.CAPTION_2} style={Styles.loader}>
						Fetching your order detail...
					</SourceSansBit>
				</BoxBit>
			)
		}

		emptyRenderer() {
			return this.state.isEmpty && (
				<BoxBit style={Styles.empty}>
					<SourceSansBit type={SourceSansBit.TYPES.HEADER_4} style={Styles.padder}>
						You currently have no items in your cart.
					</SourceSansBit>
					<SourceSansBit type={SourceSansBit.TYPES.PARAGRAPH_2}>
						Try to order some more items
					</SourceSansBit>
				</BoxBit>
			)
		}
		
		footerRenderer() {
			return (
				<ShadowBit x={0} y={-1} blur={8} color={Colors.new.grey.palette(2)}>
					<BoxBit unflex row style={Styles.footer}>
						<BoxBit style={Styles.footerText}>
							<SourceSansBit
								type={SourceSansBit.TYPES.NEW_SUBHEADER_1_ALT_2}
								weight="normal"
							>
								Cart Amount
							</SourceSansBit>
							<SourceSansBit
								type={SourceSansBit.TYPES.NEW_HEADER_2}
								weight="semibold"
								style={Styles.price}
							>
								Rp { FormatHelper.currencyFormat(this.props.total) }
							</SourceSansBit>
						</BoxBit>
						<ButtonBit
							title="Continue"
							type={ButtonBit.TYPES.SHAPES.PROGRESS}
							width={ ButtonBit.TYPES.WIDTHS.FORCE_BLOCK }
							onPress={this.onNavigateToCheckout}
						/>
					</BoxBit>
				</ShadowBit>
			)
		}
		
		cartCardRenderer(cartItem) {
			return (
				<CardCartComponent key={cartItem.id} id={cartItem.id} />
			)
		}
		
		render() {
			return super.render(
				<PageBit
					header={<HeaderBit { ...this.header } />}
					footer={ this.footerRenderer() }
					contentContainerStyle={ Styles.container }
				>
					{ this.loadingRenderer() }
					{ this.emptyRenderer() }
					{ !this.state.isLoading && !this.state.isEmpty && (
						<BoxBit unflex accessible={ !this.state.isSending }>
							<ListHeaderLego title="SHIPPING ADDRESS" />
							{/* <CardAddressComponent id={1}/> */}
							<AddressPart
								id={ 1 }
								onPress={ this.onSelectAddress }
							/>
							{/* <ListHeaderLego title="OPTIONS" />
							<InputPromoPart
								value={ this.state.promoCode }
								onSubmit={ this.onValidateCoupon }
								isLoading={ this.state.isUpdating }
								isValid={ this.state.isCouponValid }
							/> */}
							<ListHeaderLego title="ORDER SUMMARY" />
							<BoxBit unflex style={ Styles.summary }>
								{ this.props.cartItems.map(this.cartCardRenderer)}
							</BoxBit>
							
							<ListHeaderLego title="PAYMENT DETAIL" />
							<BoxBit style={Styles.padderThick}>
								<PricesPart
									price={ 20000 || this.state.totalAmount }
									discount={ 20000 || this.state.totalDiscount }
									shipping={ 20000 || this.state.totalShipping }
									total={ 20000 || this.state.total }
									isLoading={ this.state.isUpdating }
									style={ Styles.prices }
								/>
							</BoxBit>
						</BoxBit>
					) }
				</PageBit>
			)
		}
	}
)
