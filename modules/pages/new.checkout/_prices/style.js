import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({

	price: {
		alignItems: 'flex-end',
	},

	prices: {
		lineHeight: 24,
	},

	pale: {
		color: Colors.new.grey.palette(3),
	},

	bold: {
		color: Colors.new.grey.palette(3),
	},

	loader: {
		marginTop: 5,
		marginBottom: 5,
	},

	padder: {
		paddingTop: 16,
	},

	border: {
		borderTopWidth: StyleSheet.hairlineWidth,
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.new.grey.palette(3),
		paddingBottom: 8,
		paddingTop: 8,
	},
})
