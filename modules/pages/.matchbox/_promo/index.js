import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import BoxBit from 'modules/bits/box';
import SourceSansBit from 'modules/bits/source.sans';

import Styles from './style';


export default ConnectHelper(
	class PromoPart extends StatefulModel {

		shouldComponentUpdate() {
			return false
		}

		render() {
			return (
				<BoxBit style={Styles.container}>
					<SourceSansBit type={SourceSansBit.TYPES.SUBHEADER_2} style={Styles.text}>FREE SHIPPING JABODETABEK</SourceSansBit>
				</BoxBit>
			)
		}
	}
)
