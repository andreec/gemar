import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		paddingTop: Sizes.margin.default,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
		paddingBottom: Sizes.margin.default,
		backgroundColor: Colors.green.palette(3),
	},
	text: {
		color: Colors.white.primary,
		textAlign: 'center',
		fontWeight: '500',
	},
})
