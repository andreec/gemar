import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	header: {
		backgroundColor: Colors.solid.grey.palette(1),
	},
	button: {
		marginTop: -24,
		marginBottom: 72,
	},
})
