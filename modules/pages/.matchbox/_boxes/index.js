import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';

import BoxBit from 'modules/bits/box';
import SourceSansBit from 'modules/bits/source.sans';
import LoaderBit from 'modules/bits/loader';

import CardMatchboxComponent from 'modules/components/card.matchbox';

import Styles from './style';


export default ConnectHelper(
	class BoxesPart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				onPress: PropTypes.func,
				matchboxes: PropTypes.arrayOf(PropTypes.string),
			}
		}

		static stateToProps(state) {
			return {
				matchboxes: state.matchboxes.filter(matchbox => !matchbox._isInvalid).keySeq().toJS(),
			}
		}

		static defaultProps = {
			matchboxes: [],
		}

		constructor(p) {
			super(p, {}, [
				'boxRenderer',
			])
		}

		shouldComponentUpdate(nP) {
			return this.props.matchboxes.length !== nP.matchboxes.length
		}

		boxRenderer(id) {
			return (
				<CardMatchboxComponent key={id} id={id} onPress={this.props.onPress} />
			)
		}

		render() {
			return this.props.matchboxes.length ? (
				<BoxBit style={Styles.container}>
					<SourceSansBit type={SourceSansBit.TYPES.SUBHEADER_1} weight="medium" style={Styles.title}>PICK A MATCHBOX</SourceSansBit>
					<SourceSansBit type={SourceSansBit.TYPES.HEADER_3} style={Styles.header}>Look fab, no fuss.</SourceSansBit>
					<SourceSansBit type={SourceSansBit.TYPES.PARAGRAPH_2} style={Styles.description}>
						Let one of our stylists help you find clothes you’ll love. All online and completely free.
					</SourceSansBit>
					{ this.props.matchboxes.map(this.boxRenderer) }
				</BoxBit>
			) : (
				<BoxBit centering type={BoxBit.TYPES.ALL_THICK} style={Styles.container}>
					<LoaderBit />
				</BoxBit>
			)
		}
	}
)
