import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		paddingTop: 56,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
		paddingBottom: 40,
		backgroundColor: Colors.solid.grey.palette(1),
	},
	title: {
		textAlign: 'center',
		color: Colors.black.palette(5),
	},
	header: {
		textAlign: 'center',
		marginTop: 16,
		marginBottom: 24,
	},
	description: {
		textAlign: 'center',
		color: Colors.black.palette(4),
		marginBottom: 16,
	},
})
