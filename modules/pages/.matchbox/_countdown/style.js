import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		padding: 24,
		backgroundColor: Colors.black.primary,
	},

	content: {
		justifyContent: 'center',
		alignItems: 'flex-end',
	},

	header: {
		marginBottom: 7,
		textAlign: 'center',
	},

	textColor: {
		color: Colors.white.primary,
	},

	footer: {
		marginTop: 10,
		color: Colors.white.palette(5),
		textAlign: 'center',
	},
})
