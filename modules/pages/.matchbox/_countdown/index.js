import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import MatchboxManager from 'app/managers/matchbox';
// import VolatileManager from 'app/managers/volatile';

import BoxBit from 'modules/bits/box';
import SourceSansBit from 'modules/bits/source.sans';
import LoaderBit from 'modules/bits/loader';

import CountboxPart from '../__countbox';

import Styles from './style';


export default ConnectHelper(
	class CountdownPart extends StatefulModel {

		constructor(p) {
			super(p, {
				isLoading: true,
				lastOrderDate: false,
				day: 0,
				hour: 0,
				minute: 0,
				second: 0,
			}, [
				'updateTime',
			])
		}

		componentWillMount() {
			MatchboxManager.getTimer().then(timer => {
				this.setState({
					lastOrderDate: timer.lastOrderDate,
					isLoading: false,
				}, this.updateTime)
			})
		}

		componentDidMount() {
			this.__timer = this.setInterval(this.updateTime, 1000)
		}

		updateTime() {
			if(this.state.lastOrderDate) {
				let delta = (this.state.lastOrderDate - Date.now()) / 1000

				if(delta > 0) {
					const day = Math.floor( delta / 86400 )
						, hour = (delta -= day * 86400, Math.floor(delta / 3600) % 24)
						, minute = (delta -= hour * 3600, Math.floor(delta / 60) % 60)
						, second = (delta -= minute * 60, Math.floor(delta % 60))

					this.setState({
						day,
						hour,
						minute,
						second,
					})
				} else {
					MatchboxManager.getTimer().then(timer => {
						this.setState({
							lastOrderDate: timer.lastOrderDate,
						})
					})
				}
			}
		}

		render() {
			return this.state.isLoading ? (
				<BoxBit centering type={BoxBit.TYPES.ALL_THICK}>
					<LoaderBit />
				</BoxBit>
			) : (
				<BoxBit unflex style={Styles.container}>
					<SourceSansBit type={SourceSansBit.TYPES.SUBHEADER_3} weight="medium" style={[Styles.header, Styles.textColor]}>NEXT SHIPMENT ORDER CLOSE IN</SourceSansBit>
					<BoxBit row unflex style={Styles.content}>
						<CountboxPart value={ this.state.day } postfix={'D'} />
						<CountboxPart value={ this.state.hour } postfix={'H'} />
						<CountboxPart value={ this.state.minute } postfix={'M'} />
						<CountboxPart value={ this.state.second } postfix={'S'} />
					</BoxBit>
					<SourceSansBit type={SourceSansBit.TYPES.PARAGRAPH_3} style={Styles.footer}>Don’t miss out on your Matchbox</SourceSansBit>
				</BoxBit>
			)
		}
	}
)
