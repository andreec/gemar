import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		paddingTop: 0,
		paddingBottom: 0,
		paddingRight: 4,
		paddingLeft: 4,
		alignItems: 'flex-end',
	},

	bigBox: {
		width: 36,
		height: 26,
		textAlign: 'center',
		lineHeight: 26,
	},

	smallBox: {
		width: 16,
		height: 13,
		marginBottom: 5,
	},

	textColor: {
		color: Colors.white.primary,
	},
})
