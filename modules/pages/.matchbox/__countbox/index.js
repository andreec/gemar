import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import BoxBit from 'modules/bits/box';
import SourceSansBit from 'modules/bits/source.sans';

import Styles from './style';

import _ from 'lodash';


export default ConnectHelper(
	class CountboxPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				value: PropTypes.number.isRequired,
				postfix: PropTypes.string,
				padLength: PropTypes.number,
				padCharacter: PropTypes.string,
			}
		}

		static defaultProps = {
			padLength: 2,
			padCharacter: '0',
		}

		render() {
			return (
				<BoxBit row unflex style={Styles.container}>
					<SourceSansBit type={SourceSansBit.TYPES.HEADER_5} style={[Styles.bigBox, Styles.textColor]}>{ _.padStart(this.props.value, this.props.padLength, this.props.padCharacter) }</SourceSansBit>
					<SourceSansBit type={SourceSansBit.TYPES.SUBHEADER_3} weight="medium" style={[Styles.smallBox, Styles.textColor]}>{ this.props.postfix }</SourceSansBit>
				</BoxBit>
			)
		}
	}
)
