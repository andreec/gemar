import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import PageModel from 'coeur/models/page';

import PageManager from 'app/managers/page';

import TrackingHandler from 'app/handlers/tracking';

import ButtonBit from 'modules/bits/button';
import PageBit from 'modules/bits/page';

import HeaderNavigationLego from 'modules/legos/header.navigation';

import BoxesPart from './_boxes';
import CountdownPart from './_countdown';
import PromoPart from './_promo';

import Styles from './style'

export {
	BoxesPart,
	CountdownPart,
	PromoPart,
};


export default ConnectHelper(
	class MatchboxPage extends PageModel {

		static routeName = 'matchbox'

		constructor(p) {
			super(p, {}, [
				'onNavigateToMatchboxDetail',
				'onNavigateToMatchboxFAQ',
			]);
		}

		componentWillMount() {
			PageManager.get('matchbox')
		}

		componentDidMount() {
			TrackingHandler.trackPageView(this.routeName)
		}

		onNavigateToMatchboxDetail(id) {
			this.navigator.navigate(`matchbox/${id}`)
		}

		onNavigateToMatchboxFAQ() {
			this.navigator.navigate('webview', {
				title: 'FAQ',
				url: '/faq.html',
			})
		}

		render() {
			return super.render(
				<PageBit header={ <HeaderNavigationLego /> } style={Styles.header} >
					<CountdownPart />
					<PromoPart />
					<BoxesPart onPress={ this.onNavigateToMatchboxDetail } />
					<ButtonBit
						title="More info about matchbox"
						onPress={ this.onNavigateToMatchboxFAQ }
						type={ButtonBit.TYPES.SHAPES.GHOST}
						theme={ButtonBit.TYPES.THEMES.BLACK}
						style={Styles.button}
					/>
				</PageBit>
			)
		}
	}
)
