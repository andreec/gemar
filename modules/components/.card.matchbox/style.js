import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		borderRadius: 4,
		marginTop: 16,
		marginBottom: 16,
		marginRight: 0,
		marginLeft: 0,
		overflow: 'hidden',
	},

	badge: {
		width: 44,
		height: 24,
		position: 'absolute',
		top: 8,
		left: 8,
		backgroundColor: Colors.pink.palette(2),
		borderRadius: 2,
	},

	badgeColor: {
		color: Colors.black.palette(5),
	},

	image: {
		height: 184 * (Sizes.screen.width - (Sizes.margin.thick * 2)) / 328,
	},

	content: {
		backgroundColor: Colors.white.primary,
		padding: 16,
	},
})
