import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import IconBit from 'modules/bits/icon';

import Styles from './style';


export default ConnectHelper(
	class CheckmarkPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				isActive: PropTypes.bool,
			}
		}

		static defaultProps = {
			isActive: false,
		}

		render() {
			return (
				<BoxBit unflex row style={Styles.container}>
					<BoxBit unflex style={Styles.box} />
					{ this.props.isActive && (
						<IconBit
							name="checkmark"
							color={ Colors.red.primary }
							size={ 24 }
						/>
					) }
				</BoxBit>
			)
		}
	}
)
