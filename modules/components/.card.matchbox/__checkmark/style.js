import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		width: 24,
		height: 24,
	},

	box: {
		position: 'absolute',
		top: 3,
		left: 2,
		width: 18,
		height: 18,
		borderRadius: 1,
		borderWidth: 1,
		borderStyle: 'solid',
		borderColor: Colors.coal.primary,
	},
})
