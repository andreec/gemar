import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import BoxBit from 'modules/bits/box';
import SourceSansBit from 'modules/bits/source.sans';

import CheckmarkPart from '../__checkmark'
import ColumnPart from '../_column'

import Styles from './style';

import _ from 'lodash';


export default ConnectHelper(
	class ColumnCheckmarkPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string.isRequired,
				data: PropTypes.arrayOf(PropTypes.shape({
					title: PropTypes.string,
					isActive: PropTypes.bool,
				})).isRequired,
				column: PropTypes.number,
				note: PropTypes.string,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			column: 1,
			data: [],
		}

		constructor(p) {
			super(p, {}, [
				'contentRenderer',
				'columnRenderer',
				'rowRenderer',
			])
		}

		contentRenderer() {
			return (
				<BoxBit unflex row style={Styles.container}>
					{ this.props.column > 1 ? _.chunk(this.props.data, Math.ceil(this.props.data.length / this.props.column)).map(this.columnRenderer) : this.columnRenderer(this.props.data) }
				</BoxBit>
			)
		}

		columnRenderer(datas, i) {
			return (
				<BoxBit key={i} style={Styles.column}>
					{ datas.map(this.rowRenderer) }
				</BoxBit>
			)
		}

		rowRenderer(data, i) {
			return (
				<BoxBit unflex row key={i} style={Styles.row}>
					<CheckmarkPart isActive={ data.isActive }/>
					<SourceSansBit type={SourceSansBit.TYPES.SUBHEADER_2} style={Styles.title}>{ data.title }</SourceSansBit>
				</BoxBit>
			)
		}

		render() {
			return (
				<BoxBit unflex style={this.props.style}>
					<ColumnPart
						data={[{
							title: this.props.title,
							children: this.contentRenderer(),
						}]}
					/>
					{ this.props.note && (
						<SourceSansBit type={SourceSansBit.TYPES.CAPTION_1} style={Styles.footnote}>{ this.props.note }</SourceSansBit>
					) }
				</BoxBit>
			)
		}
	}
)
