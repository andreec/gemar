import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import CardComponentModel from 'coeur/models/components/card';

import MatchboxManager from 'app/managers/matchbox';

import FormatHelper from 'coeur/helpers/format';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import SourceSansBit from 'modules/bits/source.sans';
import ImageBit from 'modules/bits/image';
import ShadowBit from 'modules/bits/shadow';
import TouchableBit from 'modules/bits/touchable';

import ColumnPart from './_column';
import ColumnCheckmarkPart from './_column.checkmark';
import TitlePart from './_title';

import Styles from './style';

export {
	ColumnPart,
	ColumnCheckmarkPart,
	TitlePart,
};


export default ConnectHelper(
	class CardMatchboxComponent extends CardComponentModel(MatchboxManager) {
		render() {
			return (
				<ShadowBit x={0} y={0} blur={1} color={Colors.grey.palette(3)} style={Styles.container}>
					<ShadowBit x={0} y={2} blur={2} color={Colors.coal.palette(2)}>
						<TouchableBit unflex onPress={ this.onPress }>
							<ImageBit source={this.props.data.image} resizeMode={ImageBit.TYPES.COVER} style={Styles.image} />
							<BoxBit unflex type={BoxBit.TYPES.THIN} style={Styles.content}>
								<TitlePart
									type={ this.props.data.type }
									title={ this.props.data.title }
									isDiscounted={ this.props.data.isDiscounted }
									basePrice={ this.props.data.basePrice }
									price={ this.props.data.price }
								/>
								<ColumnPart
									data={[{
										title: 'CONTENT',
										content: `${ this.props.data.count } pcs`,
									}, {
										title: 'APPROX. VALUE',
										content: `IDR ${ FormatHelper.currencyFormat(this.props.data.value) }`,
									}]}
								/>
							</BoxBit>
							{ this.props.data.isNew && (
								<BoxBit unflex centering style={Styles.badge}>
									<SourceSansBit type={SourceSansBit.TYPES.SUBHEADER_3} style={Styles.badgeColor}>NEW</SourceSansBit>
								</BoxBit>
							) }
						</TouchableBit>
					</ShadowBit>
				</ShadowBit>
			)
		}
	}
)
