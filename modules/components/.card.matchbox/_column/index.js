import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import BoxBit from 'modules/bits/box';
import SourceSansBit from 'modules/bits/source.sans';

import Styles from './style';


export default ConnectHelper(
	class ColumnPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				data: PropTypes.arrayOf(PropTypes.shape({
					title: PropTypes.string,
					content: PropTypes.string,
					children: PropTypes.node,
				})).isRequired,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			data: [],
		}

		render() {
			return (
				<BoxBit unflex row style={[Styles.container, this.props.style]}>
					{ this.props.data.map((d, i) => {
						return (
							<BoxBit key={i} style={Styles.column}>
								<SourceSansBit type={SourceSansBit.TYPES.SUBHEADER_3} weight="medium" style={Styles.point}>{ d.title }</SourceSansBit>
								{ d.children ? d.children : (
									<SourceSansBit type={SourceSansBit.TYPES.SUBHEADER_2}>{ d.content }</SourceSansBit>
								) }
							</BoxBit>
						)
					}) }
				</BoxBit>
			)
		}
	}
)
