import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	type: {
		height: 14,
	},

	// subheader: {
	// 	color: Colors.black.palette(5),
	// },
	//
	// subheaderMargin: {
	// 	marginBottom: 8,
	// },

	basePrice: {
		marginTop: 8,
		color: Colors.black.palette(4),
	},

	price: {
		paddingTop: 29,
		marginBottom: 24,
	},

	noPriceMargin: {
		paddingTop: 0,
	},
})
