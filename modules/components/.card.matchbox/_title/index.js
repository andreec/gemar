import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import FormatHelper from 'coeur/helpers/format';

import BoxBit from 'modules/bits/box';
import SourceSansBit from 'modules/bits/source.sans';

import Styles from './style';


export default ConnectHelper(
	class TitlePart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				type: PropTypes.string,
				title: PropTypes.string,
				isDiscounted: PropTypes.bool,
				basePrice: PropTypes.number,
				price: PropTypes.number,
			}
		}

		static defaultProps = {
			type: '',
			title: '',
			isDiscounted: false,
			basePrice: 0,
			price: 0,
		}

		static getDerivedStateFromProps(nP) {
			return {
				useSmallTitle: nP.title.length > 22,
			}
		}

		constructor(p) {
			super(p, {
				useSmallTitle: false,
			})
		}

		render() {
			return (
				<BoxBit unflex>
					<BoxBit unflex style={Styles.type} />
					{/* <SourceSansBit type={SourceSansBit.TYPES.SUBHEADER_2} style={[Styles.subheader, this.state.useSmallTitle && Styles.subheaderMargin]} weight="medium">{ this.props.type.toUpperCase() }</SourceSansBit> */}
					<SourceSansBit type={this.state.useSmallTitle ? SourceSansBit.TYPES.HEADER_3 : SourceSansBit.TYPES.HEADER_1} weight="bold">{ this.props.title }</SourceSansBit>
					{ this.props.isDiscounted && (
						<SourceSansBit type={SourceSansBit.TYPES.PARAGRAPH_3} style={Styles.basePrice}>IDR { FormatHelper.currencyFormat(this.props.basePrice) }</SourceSansBit>
					) }
					<SourceSansBit type={SourceSansBit.TYPES.HEADER_4} style={[Styles.price, this.props.isDiscounted && Styles.noPriceMargin]}>IDR { FormatHelper.currencyFormat(this.props.price) }</SourceSansBit>
				</BoxBit>
			)
		}
	}
)
