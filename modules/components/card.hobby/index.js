import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import CardComponentModel from 'coeur/models/components/card';

import HobbyManager from 'app/managers/hobby';

// import Colors from 'coeur/constants/color';
// import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
import BoxImageBit from 'modules/bits/box.image';
// import CheckboxBit from 'modules/bits/checkbox';
import SourceSansBit from 'modules/bits/source.sans';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style'


export default ConnectHelper(
	class CardHobbyComponent extends CardComponentModel(HobbyManager) {

		static propTypes(PropTypes) {
			return {
				...super.propTypes(PropTypes),
				selectable: PropTypes.bool,
				isActvie: PropTypes.bool,
				style: PropTypes.style,
			}
		}

		render() {
			// this.log(this.props, this.state)
			return (
				<TouchableBit activeOpacity={ this.props.selectable ? undefined : 1 } onPress={ this.onPress }>
					<BoxImageBit broken centering unflex source={ this.props.data.image } style={[Styles.container, this.props.style]}>
						<BoxBit centering style={[Styles.content, this.props.isActive && Styles.selected]}>
							<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_1_ALT} style={[Styles.text, this.props.isActive && Styles.textActive]}>{ this.props.data.title ? this.props.data.title : '' }</SourceSansBit>
						</BoxBit>
					</BoxImageBit>
					{/* { this.props.selectable && (
						<CheckboxBit
							isActive={ this.props.isActive }
							onPress={ this.onPress }
							style={Styles.checkbox}
						/>
					) } */}
				</TouchableBit>
			)
		}
	}
)
