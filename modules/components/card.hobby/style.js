import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	container: {
		width: 50,
		height: 50,
	},

	checkbox: {
		position: 'absolute',
		top: 4,
		right: 4,
	},

	content: {
		alignSelf: 'stretch',
		backgroundColor: Colors.new.black.palette(4),
		paddingLeft: 4,
		paddingRight: 4,
	},

	selected: {
		backgroundColor: Colors.new.yellow.palette(4),
	},

	text: {
		color: Colors.new.white.palette(1),
	},

	textActive: {
		color: Colors.new.black.palette(1),
	},
})
