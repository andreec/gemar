import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import CardComponentModel from 'coeur/models/components/card';

import JobManager from 'app/managers/job';
import UserManager from 'app/managers/user';

import PageContext from 'coeur/contexts/page'

// import FormatHelper from 'coeur/helpers/format';
import TimeHelper from 'coeur/helpers/time';
import Sizes from 'coeur/constants/size';

// import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import ImageBit from 'modules/bits/image';
// import ButtonBit from 'modules/bits/button';
// import ImageBit from 'modules/bits/image';
import SourceSansBit from 'modules/bits/source.sans';
// import TextBit from 'modules/bits/text';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style'


function size(span = 1) {
	return ((Sizes.screen.width - ( 15 * 2 )) - ((3 - span) * 4)) * (span / 4)
}


export default ConnectHelper(
	class CardJobApplicationComponent extends CardComponentModel(JobManager) {

		static propTypes(PropTypes) {
			return {
				...super.propTypes(PropTypes),
				style: PropTypes.style,
			}
		}

		static stateToProps(state, oP) {
			const {
				id,
				data,
			} = super.stateToProps(state, oP)

			return {
				id,
				data,
				job: JobManager.get(id),
			}
		}

		static contexts = [
			PageContext,
		]

		getStatusColor(status) {
			switch(status) {
			case 'WAITING PAYMENT':
			default:
				return Styles.red;
			case 'ON GOING':
				return Styles.green;
			case 'CANCELLED':
				return Styles.grey;
			case 'COMPLETED':
				return Styles.blue;
			}
		}

		onNavigateToJobDetail = () => {
			this.log(this.props)
			this.props.page.navigator.navigate('job.detail', {
				id: this.props.id,
			})
		}

		render() {
			const _size = this.props.big ? size(2) : size()

			return (
				<TouchableBit unflex style={[Styles.container, this.props.style]} onPress={ this.onNavigateToJobDetail }>
					<BoxBit row>
						<ImageBit broken={!this.props.data.image} source={this.props.data.image || undefined} style={[Styles.image, {height: _size, width: _size}]}/>
						<BoxBit style={Styles.description}>
							<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_1} style={Styles.darkGrey}>#JOB-{ this.props.data.id }</SourceSansBit>
							<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_1} weight="semibold" style={Styles.darkGrey60}>{ this.props.data.position } at {this.props.data.company}</SourceSansBit>
							<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_1} style={Styles.darkGrey60}>{TimeHelper.moment(this.props.data.updatedAt).format('MMMM DD, YYYY') }</SourceSansBit>
						</BoxBit>
						<BoxBit unflex style={[Styles.status]}>
							<SourceSansBit type={SourceSansBit.TYPES.NEW_HEADER_3} weight="semibold" style={[Styles.status, Styles.black]}>{ this.props.data.status || 'UNREAD'}</SourceSansBit>
						</BoxBit>
					</BoxBit>
					{/* <SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_1} style={Styles.transaction}>Transaction with: @{ this.props.seller.username }</SourceSansBit> */}
				</TouchableBit>
			)
		}
	}
)
