import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	container: {
		paddingTop: 10,
		paddingBottom: 10,
		paddingLeft: 10,
		paddingRight: 10,
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.new.grey.palette(2),
		borderRadius: 2,
	},

	number: {
		color: Colors.black.primary,
		marginBottom: 8,
	},

	sub: {
		color: Colors.new.black.palette(1, .4),
	},

	transaction: {
		color: Colors.new.black.palette(1, .8),
	},

	darkGrey: {
		color: Colors.black.palette(3),
	},

	darkGrey60: {
		color: Colors.black.palette(3),
	},

	image: {
		marginRight: Sizes.margin.default,
	},
	description: {
		justifyContent: 'space-between',
	},
	status: {
		paddingRight: 4,
		paddingTop: 4,
		paddingLeft: 4,
		paddingBottom: 4,
		borderRadius: 2,
		alignSelf: 'flex-start',
	},

	red: {
		backgroundColor: Colors.new.red.primary,
	},

	green: {
		backgroundColor: Colors.new.green.primary,
	},

	blue: {
		backgroundColor: Colors.new.blue.primary,
	},

	grey: {
		backgroundColor: Colors.new.grey.palette(3),
	},
	black: {
		color: Colors.new.black.palette(1),
	},

	text: {
		color: Colors.new.white.primary,
	},

	// image: {
	// 	height: 51,
	// 	width: 51,
	// 	borderRadius: 26,
	// 	overflow: 'hidden',
	// },

	// text: {
	// 	marginLeft: 10.5,
	// 	marginRight: 13,
	// },

	// button: {
	// 	alignSelf: 'center',
	// },

	// sub: {
	// 	paddingTop: 10,
	// 	paddingBottom: 5,
	// 	color: Colors.new.black.palette(3),
	// },

})
