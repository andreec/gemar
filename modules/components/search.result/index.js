import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import PageContext from 'coeur/contexts/page';

// import HobbyManager from 'app/managers/hobby';
// import UserManager from 'app/managers/user';

// import FormatHelper from 'coeur/helpers/format';

// import Colors from 'coeur/constants/color';
// import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
// import ButtonBit from 'modules/bits/button';
import ImageBit from 'modules/bits/image';
import SourceSansBit from 'modules/bits/source.sans';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style'


export default ConnectHelper(
	class SearchResultComponent extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id.isRequired,
				type: PropTypes.string,
				title: PropTypes.string,
				description: PropTypes.string,
				image: PropTypes.string,
				style: PropTypes.style,
			}
		}

		static contexts = [
			PageContext,
		]

		constructor(p) {
			super(p, {}, [
				'onPress',
			])
		}

		onPress() {
			if(this.props.type === 'hashtag') {
				this.props.page.navigator.navigate('posts', {
					hashtag: this.props.title,
				})
			} else {
				this.props.page.navigator.navigate('user.profile', {
					id: this.props.id,
				})
			}
		}

		render() {
			return (
				<TouchableBit onPress={ this.onPress } unflex row style={[Styles.container, this.props.style]}>
					{ this.props.type === 'hashtag' ? (
						<BoxBit centering unflex style={[Styles.image, Styles.hashtag]}>
							<SourceSansBit type={SourceSansBit.TYPES.NEW_HEADER_1} weight="semibold" style={Styles.tag}>#</SourceSansBit>
						</BoxBit>
					) : (
						<ImageBit
							resizeMode={ ImageBit.TYPES.COVER }
							source={ this.props.image }
							style={Styles.image}
						/>
					) }
					<BoxBit style={Styles.text}>
						<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_1_ALT_2} weight="semibold">{ this.props.title }</SourceSansBit>
						<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_2} style={Styles.sub}>{ this.props.description }</SourceSansBit>
					</BoxBit>
				</TouchableBit>
			)
		}
	}
)
