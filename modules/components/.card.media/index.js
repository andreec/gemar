import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import CardComponentModel from 'coeur/models/components/card';

import MediaManager from 'app/managers/media';

import ImageBit from 'modules/bits/image';


export default ConnectHelper(
	class CardMediaComponent extends CardComponentModel(MediaManager) {

		static propTypes(PropTypes) {
			return {
				...super.propTypes(PropTypes),

				unflex: PropTypes.bool,
				accessible: PropTypes.bool,

				source: PropTypes.image,
				transform: PropTypes.object,
				broken: PropTypes.bool,
				resizeMode: PropTypes.oneOf(ImageBit.TYPES),
				loader: PropTypes.bool,
				overlay: PropTypes.bool,
				alt: PropTypes.string,
				onLayout: PropTypes.func,
				style: PropTypes.style,
			}
		}

		render() {
			return (
				<ImageBit
					source={this.props.data._isGetting ? undefined : this.props.data}
					unflex={this.props.unflex}
					accessible={this.props.accessible}
					transform={this.props.transform}
					broken={this.props.broken}
					resizeMode={this.props.resizeMode}
					loader={this.props.loader}
					overlay={this.props.overlay}
					alt={this.props.alt}
					onLayout={this.props.onLayout}
					style={this.props.style}
				/>
			)
		}
	}
)
