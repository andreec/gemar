import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import CardComponentModel from 'coeur/models/components/card';

import Colors from 'coeur/constants/color';

import AddressManager from 'app/managers/address';

import BoxBit from 'modules/bits/box';
import SourceSansBit from 'modules/bits/source.sans';
import IconBit from 'modules/bits/icon';
import RadioBit from 'modules/bits/radio';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style';


export default ConnectHelper(
	class CardAddressComponent extends CardComponentModel(AddressManager) {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				title: PropTypes.string,
				pic: PropTypes.string,
				phone: PropTypes.string,
				address: PropTypes.string,
				district: PropTypes.string,
				postal: PropTypes.string,

				divider: PropTypes.bool,
				selectionMode: PropTypes.bool,
				isActive: PropTypes.bool,
				isDefaultAddress: PropTypes.bool,

				onPress: PropTypes.func,
			}
		}

		constructor(p) {
			super(p, {}, [
				'onPress',
			])
		}

		onPress() {
			this.props.onPress &&
			this.props.onPress(this.props.id)
		}

		render() {
			console.log('Card address : ', this.props)
			return (
				<TouchableBit unflex onPress={ this.onPress } style={[Styles.container, this.props.divider && Styles.divider]}>
					<BoxBit unflex row>
						<BoxBit>
							<SourceSansBit
								type={SourceSansBit.TYPES.NEW_PARAGRAPH_1} weight="bold">{ this.props.data.title || this.props.data.pic }
							</SourceSansBit>
							{ this.props.data.phone && (
								<SourceSansBit
									type={SourceSansBit.TYPES.NEW_PARAGRAPH_2} weight="medium">{ this.props.data.phone }
								</SourceSansBit>
							) }
							<SourceSansBit
								type={SourceSansBit.TYPES.NEW_PARAGRAPH_2}>{ this.props.data.address }
							</SourceSansBit>
							<SourceSansBit
								type={SourceSansBit.TYPES.NEW_PARAGRAPH_2}>{ this.props.data.district }
							</SourceSansBit>
							<SourceSansBit
								type={SourceSansBit.TYPES.NEW_PARAGRAPH_2}>{ this.props.data.postal }
							</SourceSansBit>
							{this.props.data.isDefaultAddress && (
								<BoxBit unflex row style={Styles.mark}>
									<SourceSansBit
										weight="bold"
										type={SourceSansBit.TYPES.NEW_NOTE_3}
										style={Styles.footnote}
									>
										PREFERRED
									</SourceSansBit>
								</BoxBit>
							)}
						</BoxBit>
						{ this.props.selectionMode ? (
							<RadioBit isActive={this.props.isActive} onPress={ this.onPress } />
						) : (
							<BoxBit unflex>
								<BoxBit />
								<IconBit
									size={20.5}
									name="edit"
									color={Colors.coal.palette(4)}
								/>
							</BoxBit>
						) }
					</BoxBit>
				</TouchableBit>
			)
		}
	}
)
