import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({

	container: {
		// marginLeft: -Sizes.margin.thick,
		marginRight: -Sizes.margin.thick,
		marginLeft: 13,
		paddingTop: 16,
		paddingBottom: 16,
		paddingRight: 13,
		backgroundColor: Colors.white.primary,

		borderBottomWidth: 1,
		borderBottomColor: Colors.coal.palette(2),
	},

	divider: {
		borderWidth: 0,
		borderStyle: 'solid',
		borderTopWidth: StyleSheet.hairlineWidth,
		borderTopColor: Colors.coal.palette(2),
	},

	button: {
		width: 80,
		marginLeft: 8,
	},

	mark: {
		alignSelf: 'flex-start',
		marginTop: 16,
		borderRadius: 4,
		backgroundColor: Colors.new.yellow.palette(3),
	},

	footnote: {
		color: Colors.new.white.primary,
		paddingLeft: 8,
		paddingRight: 8,
	},
})
