import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	container: {
		paddingTop: 4.5,
		paddingBottom: 4.5,
		paddingLeft: 4.5,
		alignItems: 'center',
	},

	image: {
		height: 51,
		width: 51,
		borderRadius: 26,
		overflow: 'hidden',
	},

	text: {
		marginLeft: 10.5,
		marginRight: 13,
	},

	button: {
		alignSelf: 'center',
	},

	sub: {
		paddingTop: 4.5,
		paddingBottom: 5,
		color: Colors.new.black.palette(3),
	},

})
