import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import CardComponentModel from 'coeur/models/components/card';

import HobbyManager from 'app/managers/hobby';
import UserManager from 'app/managers/user';

import FormatHelper from 'coeur/helpers/format';

// import Colors from 'coeur/constants/color';
// import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import ImageBit from 'modules/bits/image';
import SourceSansBit from 'modules/bits/source.sans';
import TextBit from 'modules/bits/text';

import Styles from './style'


export default ConnectHelper(
	class CardUserFollowComponent extends CardComponentModel(UserManager) {

		static propTypes(PropTypes) {
			return {
				...super.propTypes(PropTypes),
				isActive: PropTypes.bool,
				style: PropTypes.style,
			}
		}

		static stateToProps(state, oP) {
			const {
				id,
				data,
			} = super.stateToProps(state, oP)

			const hobbyId = data.hobbyIds && data.hobbyIds[0]
				, hobby = hobbyId && (state.hobbies.get(hobbyId) || HobbyManager.get(hobbyId)) || new HobbyManager.record()

			return {
				id,
				data,
				hobby,
			}
		}

		render() {
			return (
				<BoxBit row style={[Styles.container, this.props.style]}>
					<ImageBit
						resizeMode={ ImageBit.TYPES.COVER }
						source={ this.props.data.profile }
						style={Styles.image}
					/>
					<BoxBit style={Styles.text}>
						<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_1} weight="semibold">{ this.props.data.name }</SourceSansBit>
						<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_2} style={Styles.sub}>{ this.props.hobby.title }</SourceSansBit>
						{/* <SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_2}><TextBit weight="semibold">{ FormatHelper.condenseNumber(this.props.data.followerCount) }</TextBit> followers</SourceSansBit> */}
					</BoxBit>
					<ButtonBit
						title={ this.props.isActive ? 'Following' : 'Follow' }
						type={ ButtonBit.TYPES.SHAPES.BADGE }
						theme={ this.props.isActive ? ButtonBit.TYPES.THEMES.ACTIVATED : ButtonBit.TYPES.THEMES.PRIMARY }
						onPress={ this.onPress }
						style={Styles.button}
					/>
				</BoxBit>
			)
		}
	}
)
