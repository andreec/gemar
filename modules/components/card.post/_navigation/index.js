import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import BoxBit from 'modules/bits/box';

import Styles from './style';


export default ConnectHelper(
	class NavigationPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				index: PropTypes.number,
				total: PropTypes.number,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			index: 0,
			total: 0,
		}

		constructor(p) {
			super(p, {}, [
				'dotRenderer',
			])
		}

		dotRenderer(data, index) {
			return (
				<BoxBit key={index} unflex style={this.props.index === index ? Styles.dotActive : Styles.dot} />
			)
		}

		render() {
			return this.props.total > 1 ? (
				<BoxBit unflex row centering style={this.props.style}>
					{ Array(this.props.total).fill().map(this.dotRenderer) }
				</BoxBit>
			) : false
		}
	}
)
