import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	container: {
	},

	image: {
		width: Sizes.screen.width - (15 * 2),
		height: Sizes.screen.width - (15 * 2),
		backgroundColor: Colors._black.palette(3),
	},

	toggler: {
		position: 'absolute',
		width: Sizes.screen.width - (15 * 2),
		height: Sizes.screen.width - (15 * 2),
	},

	navi: {
		marginTop: 22,
		marginBottom: -32,
	},

	comment: {
		width: 28,
		height: 28,
		marginRight: 8,
		borderRadius: 14,
		overflow: 'hidden',
	},

})
