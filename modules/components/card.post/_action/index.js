import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import FormatHelper from 'coeur/helpers/format';
// import TimeHelper from 'coeur/helpers/time';

// import Colors from 'coeur/constants/color';
// import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
import IconBit from 'modules/bits/icon';
// import ImageBit from 'modules/bits/image';
import SourceSansBit from 'modules/bits/source.sans';
import SpriteBit from 'modules/bits/sprite';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style'


export default ConnectHelper(
	class ActionPart extends StatefulModel {

		static TYPES = {
			SOCIAL: 'SOCIAL',
			JOB: 'JOB',
			MARKET: 'MARKET',
		}

		static propTypes(PropTypes) {
			return {
				isLiked: PropTypes.bool,
				isCommented: PropTypes.bool,
				isApplied: PropTypes.bool,
				isOnCart: PropTypes.bool,
				onPress: PropTypes.func,
				likeCount: PropTypes.number,
				type: PropTypes.oneOf(this.TYPES),
			}
		}

		constructor(p) {
			super(p, {
			}, [
				'onPress',
			]);
		}

		onPress(name) {
			this.props.onPress &&
			this.props.onPress(name)
		}

		iconRenderer(name) {
			return (
				<TouchableBit unflex centering onPress={ this.onPress.bind(this, name) } style={Styles.icon}>
					<IconBit
						name={ name }
						size={ 26 }
					/>
				</TouchableBit>
			)
		}

		spriteRenderer(name) {
			return (
				<TouchableBit unflex centering onPress={ this.onPress.bind(this, name) } style={Styles.icon}>
					<SpriteBit
						name={ name }
						size={ 28 }
					/>
				</TouchableBit>
			)
		}

		render() {
			return (
				<BoxBit unflex row style={Styles.container}>
					{ this.props.isLiked ? this.spriteRenderer('love-active') : this.iconRenderer('love-outline') }
					{ this.props.isCommented ? this.spriteRenderer('comment-active') : this.iconRenderer('comment-outline') }
					{ this.props.type === this.TYPES.JOB ? this.props.isApplied && this.spriteRenderer('job-active') || this.iconRenderer('job-outline') : false }
					{ this.props.type === this.TYPES.MARKET ? this.props.isOnCart && this.spriteRenderer('cart-active') || this.iconRenderer('cart-outline') : false }
					<BoxBit />
					<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_1_ALT_2}>{ FormatHelper.currencyFormat(this.props.likeCount) } likes</SourceSansBit>
				</BoxBit>
			)
		}
	}
)
