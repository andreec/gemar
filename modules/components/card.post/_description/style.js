import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	container: {
		// paddingTop: 12,
		paddingBottom: 12,
	},

	padder: {
		marginTop: 12,
		marginBottom: 8,
	},

	more: {
		color: Colors.new.yellow.palette(3),
	},

	comment: {
		color: Colors.new.yellow.palette(3),
	},

	note: {
		color: Colors.new.black.palette(4),
	},

	topMargin: {
		marginTop: 8,
	},

})
