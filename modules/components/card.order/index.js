import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import CardComponentModel from 'coeur/models/components/card';

import OrderManager from 'app/managers/order';
import UserManager from 'app/managers/user';

import PageContext from 'coeur/contexts/page'

// import FormatHelper from 'coeur/helpers/format';
import TimeHelper from 'coeur/helpers/time';

// import Colors from 'coeur/constants/color';
// import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
// import ButtonBit from 'modules/bits/button';
// import ImageBit from 'modules/bits/image';
import SourceSansBit from 'modules/bits/source.sans';
// import TextBit from 'modules/bits/text';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style'


export default ConnectHelper(
	class CardOrderComponent extends CardComponentModel(OrderManager) {

		static propTypes(PropTypes) {
			return {
				...super.propTypes(PropTypes),
				style: PropTypes.style,
			}
		}

		static stateToProps(state, oP) {
			const {
				id,
				data,
			} = super.stateToProps(state, oP)

			return {
				id,
				data,
				seller: UserManager.get(data.seller_id),
			}
		}

		static contexts = [
			PageContext,
		]

		getStatusColor(status) {
			switch(status) {
			case 'WAITING PAYMENT':
			default:
				return Styles.red;
			case 'ON GOING':
				return Styles.green;
			case 'CANCELLED':
				return Styles.grey;
			case 'COMPLETED':
				return Styles.blue;
			}
		}

		onNavigateToOrderDetail = () => {
			this.props.page.navigator.navigate('order', {
				id: this.props.id,
			})
		}

		render() {
			return (
				<TouchableBit unflex style={[Styles.container, this.props.style]} onPress={ this.onNavigateToOrderDetail }>
					<BoxBit row unflex>
						<SourceSansBit type={SourceSansBit.TYPES.NEW_HEADER_2} weight="semibold" style={Styles.number}>#{ this.props.data.number }</SourceSansBit>
						<BoxBit />
						<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_1} style={Styles.sub}>{ TimeHelper.moment(this.props.data.updatedAt).format('MMMM DD, YYYY') }</SourceSansBit>
					</BoxBit>
					<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_1} style={Styles.transaction}>Transaction with: @{ this.props.seller.username }</SourceSansBit>
					<BoxBit unflex style={[Styles.status, this.getStatusColor(this.props.data.status)]}>
						<SourceSansBit type={SourceSansBit.TYPES.NEW_HEADER_2} weight="semibold" style={Styles.text}>{ this.props.data.status }</SourceSansBit>
					</BoxBit>
				</TouchableBit>
			)
		}
	}
)
