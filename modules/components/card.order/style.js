import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	container: {
		paddingTop: 10,
		paddingBottom: 10,
		paddingLeft: 10,
		paddingRight: 10,
		marginLeft: 15,
		marginRight: 15,
		marginTop: 7,
		marginBottom: 8,
		// borderWidth: StyleSheet.hairlineWidth,
		// borderColor: Colors.new.grey.palette(2),
		borderRadius: 2,
		backgroundColor: Colors.new.grey.palette(1, .3),
	},

	number: {
		color: Colors.black.primary,
		marginBottom: 8,
	},

	sub: {
		color: Colors.new.black.palette(1, .4),
	},

	transaction: {
		color: Colors.new.black.palette(1, .8),
	},

	status: {
		marginTop: 20,
		padding: 4,
		borderRadius: 2,
		alignSelf: 'flex-start',
	},

	red: {
		backgroundColor: Colors.new.red.primary,
	},

	green: {
		backgroundColor: Colors.new.green.primary,
	},

	blue: {
		backgroundColor: Colors.new.blue.primary,
	},

	grey: {
		backgroundColor: Colors.new.grey.palette(3),
	},

	text: {
		color: Colors.new.white.primary,
	},

	// image: {
	// 	height: 51,
	// 	width: 51,
	// 	borderRadius: 26,
	// 	overflow: 'hidden',
	// },

	// text: {
	// 	marginLeft: 10.5,
	// 	marginRight: 13,
	// },

	// button: {
	// 	alignSelf: 'center',
	// },

	// sub: {
	// 	paddingTop: 10,
	// 	paddingBottom: 5,
	// 	color: Colors.new.black.palette(3),
	// },

})
