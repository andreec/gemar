import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

// import PageManager from 'app/managers/page';

// import TrackingHandler from 'app/handlers/tracking';

import FormatHelper from 'coeur/helpers/format';
import TimeHelper from 'coeur/helpers/time';

// import Colors from 'coeur/constants/color';
// import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
import ImageBit from 'modules/bits/image';
import SourceSansBit from 'modules/bits/source.sans';
import TextInputBit from 'modules/bits/text.input';
import TouchableBit from 'modules/bits/touchable';
import TextBit from 'modules/bits/text';

import Styles from './style';


export default ConnectHelper(
	class DescriptionPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				commentCount: PropTypes.number,
				username: PropTypes.string.isRequired,
				myProfile: PropTypes.image,
				myComment: PropTypes.string,
				caption: PropTypes.string,
				onCommentPress: PropTypes.func,
				onMorePress: PropTypes.func,
				onSubmitComment: PropTypes.func,
				createdAt: PropTypes.date,
				style: PropTypes.style,
			}
		}

		constructor(p) {
			super(p, {
				isTruncated: true,
			}, [
				'onMorePress',
			])

			this._sliceStringLength = 220
		}

		componentWillMount() {
		}

		onMorePress() {
			this.setState({
				isTruncated: false,
			})
		}

		render() {
			return (
				<BoxBit unflex style={[Styles.container, this.props.style]}>
					<SourceSansBit type={SourceSansBit.TYPES.NEW_PARAGRAPH_1}>
						<TextBit weight="medium">{ this.props.username }</TextBit> { this.state.isTruncated ? this.props.caption.substring(0, this._sliceStringLength) : this.props.caption } { this.state.isTruncated && this.props.caption.length > this._sliceStringLength ? (
							<TextBit style={Styles.more} onPress={ this.onMorePress }>…more</TextBit>
						) : false }
					</SourceSansBit>
					{ this.props.commentCount > 0 ? (
						<TouchableBit onPress={this.props.onCommentPress} style={Styles.padder}>
							<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_1_ALT_2} style={Styles.comment}>See all {FormatHelper.currencyFormat(this.props.commentCount)} comments</SourceSansBit>
						</TouchableBit>
					) : false }
					{/* <TextInputBit
						prefix={(
							<ImageBit
								source={ this.props.myProfile }
								style={ Styles.profile }
							/>
						)}
						defaultValue={ this.props.myComment }
						onSubmitEditting={ this.props.onSubmitComment }
					/> */}
					<SourceSansBit type={SourceSansBit.TYPES.NEW_NOTE} style={[Styles.note, this.props.commentCount === 0 && Styles.topMargin]}>{ TimeHelper.moment(this.props.createdAt).fromNow().toUpperCase() }</SourceSansBit>
				</BoxBit>
			)
		}
	}
)
