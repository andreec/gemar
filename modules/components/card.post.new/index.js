import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';
import CardComponentModel from 'coeur/models/components/card';

// import PageManager from 'app/managers/page';
import MeManager from 'app/managers/me';
import PostManager from 'app/managers/post';
import UserManager from 'app/managers/user';

import UtilitiesContext from 'coeur/contexts/utilities';
import PageContext from 'coeur/contexts/page';

import TrackingHandler from 'app/handlers/tracking';

import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
import ImageBit from 'modules/bits/image';
import GalleryBit from 'modules/bits/gallery';
// import TextInputBit from 'modules/bits/text.input';
import TouchableBit from 'modules/bits/touchable';

import TogglerAnimationLego from 'modules/legos/toggler.animation';

import ActionPart from './_action'
import DescriptionPart from './_description'
import HeaderPart from './_header'
import NavigationPart from './_navigation'

import ApplyPagelet from 'modules/pagelets/apply'

import ContainerPart from 'modules/pages/tag.people/_container'

import Styles from './style';

const IMAGE = [
	'https://res.cloudinary.com/hiandree/image/upload/v1539438198/dummies/dummy-user1.jpg',
	'https://res.cloudinary.com/hiandree/image/upload/v1539438201/dummies/dummy-user2.jpg',
	'https://res.cloudinary.com/hiandree/image/upload/v1539438201/dummies/dummy-user3.jpg',
]

export default ConnectHelper(
	class CardPostComponent extends StatefulModel {

		static TYPES = ActionPart.TYPES

		static propTypes(PropTypes) {
			return {
				...super.propTypes(PropTypes),
				noHeader: PropTypes.bool,
			}
		}

		static stateToProps(state, oP) {
		// 	const {
		// 			data,
		// 		} = super.stateToProps(state, oP)
			const user = state.users.get(oP.userId) || (state.me.id === oP.userId ? state.me : UserManager.get(oP.userId))

			return {
				// data,
				user,
				isLiked: state.me.likedPostIds.indexOf(this.props.id) > -1,
				isOnCart: state.me.cartItems.map(cI => cI.id).indexOf(this.props.id) > -1,
				isApplied: state.me.appliedPostIds.indexOf(this.props.id) > -1,
				me: state.me,
			}
		}

		static contexts = [
			UtilitiesContext,
			PageContext,
		]

		constructor(p) {
			super(p, {
				index: p.index || 0,
				isSendingComment: false,
				shouldShowTags: false,
			}, [
				'onMenuPress',
				'onProfilePress',
				'onUpdateIndex',
				'onReportPost',
				'onConfirmReport',
				'onUnfollow',
				'onConfirmUnfollow',
				'onEditPost',
				'onDeletePost',
				'onConfirmDelete',
				'onNavigateToPostDetail',
				'onNavigateToComment',
				'onToggleAction',
				'onToggleLike',
				'onToggleShowTags',
				'imageRenderer',
				// 'onChangeComment',
				// 'onSubmitComment',
			]);

			this.tags = this.props.images.map(() => {
				return Array(Math.floor(Math.random() * 2) + 1).fill().map(() => {
					return {
						id: Math.floor(Math.random() * 2) + 1,
						location: {
							x: Math.random(),
							y: Math.random(),
						},
					}
				})
			})
		}

		componentWillMount() {
		}

		onUpdateIndex(e, index) {
			this.setState({
				index,
			})
		}

		onMenuPress() {
			this.props.utilities.menu.show({
				actions: this.props.userId === this.props.me.id ? [{
					title: 'Edit Post',
					onPress: this.onEditPost,
				}, {
					title: 'Delete Post',
					onPress: this.onDeletePost,
				}] : [{
					title: 'Report Post',
					onPress: this.onReportPost,
				}, {
					title: `Unfollow @${this.props.user.username}`,
					onPress: this.onUnfollow,
				}],
				closeOnPress: true,
			})
		}

		onReportPost() {
			this.props.utilities.alert.show({
				title: 'Report',
				message: 'Are you sure want to report this post?',
				actions: [{
					title: 'Yes',
					type: 'OK',
					onPress: this.onConfirmReport,
				}, {
					title: 'Cancel',
					type: 'CANCEL',
				}],
			})
		}

		onConfirmReport() {
			// TODO
			this.props.utilities.notification.show({
				message: 'Done reporting this post.',
			})
		}

		onUnfollow() {
			this.props.utilities.alert.show({
				title: 'Unfollow',
				message: `If you change your mind, you\'ll have to request to follow @${ this.props.user.username } again.`,
				actions: [{
					title: 'Unfollow',
					type: 'OK',
					onPress: this.onConfirmUnfollow,
				}, {
					title: 'Cancel',
					type: 'CANCEL',
				}],
			})
		}

		onConfirmUnfollow() {
			// TODO
			this.props.utilities.notification.show({
				message: 'Done unfollowing.',
			})
		}

		onEditPost() {
			// TODO
			this.props.page.navigator.navigate('post.edit', {
				id: this.props.id,
			})
		}

		onDeletePost() {
			this.props.utilities.alert.show({
				title: 'Warning',
				message: 'Are you sure want to delete this post? This action cannot be undone.',
				actions: [{
					title: 'Yes, I\'m sure',
					type: 'OK',
					onPress: this.onConfirmDelete,
				}, {
					title: 'Cancel',
					type: 'CANCEL',
				}],
			})
		}

		onConfirmDelete() {
			// TODO
			PostManager.clear(this.props.id)

			this.props.utilities.notification.show({
				message: 'Post deleted',
				type: this.props.utilities.notification.TYPES.SUCCESS,
			})
		}

		onNavigateToPostDetail() {
			// TODO
		}

		onNavigateToComment() {
			// TODO
			this.props.page.navigator.navigate('comment', {
				// id: this.props.id,
			})
		}

		onProfilePress() {
			if (this.props.user.id === this.props.me.id) {
				this.props.page.navigator.navigate('profile')
			} else {
				this.props.page.navigator.navigate('user.profile', {
					id: this.props.user.id,
				})
			}
		}

		onToggleAction(name) {
			switch(name) {
			case 'love-active':
			// UNLIKE
				MeManager.toggleLike(this.props.id, false)
				break;
			case 'comment-active':
			case 'comment-outline':
			// NAVIGATE TO POST DETAIL
				this.props.page.navigator.navigate('comment', {
					// id: this.props.id,
				})
				break;
			case 'cart-active':
			// NAVIGATE TO CART
				this.props.page.navigator.navigate('cart')
				break;
			case 'job-active':
			// NAVIGATE TO JOB LISTING
				break;
			case 'love-outline':
			// LIKE
				MeManager.toggleLike(this.props.id, true)
				break;
			case 'cart-outline':
			// ADD TO CART
				MeManager.addToCart(this.props.id)
				break;
			case 'job-outline':
			// APPLY JOB
				this.props.utilities.alert.modal({
					component: (
						<ApplyPagelet
							position="Senior Interior Designer"
							company="PT Superior Interior"
							minSallary={8000000}
							maxSallary={10000000}
							availability={10}
							applied={2}
							onRequestClose={() => {
								this.props.utilities.alert.hide()
							}}
						/>
					),
				})
				break;
			}
		}

		// onChangeComment(e, val) {
		// 	console.log(e, val)
		// }

		// onSubmitComment(val) {
		// 	console.log(val)
		// }

		onToggleLike() {
			if(this.props.isLiked) {
				MeManager.toggleLike(this.props.id, false)
			} else {
				MeManager.toggleLike(this.props.id, true)
			}
		}

		onToggleShowTags() {
			this.setState({
				shouldShowTags: !this.state.shouldShowTags,
			})
		}

		imageRenderer(image, index) {
			return (
				<TouchableBit activeOpacity={1} unflex key={index} onPress={this.onToggleShowTags} onDoublePress={ this.onToggleLike }>
					<ContainerPart viewOnly key={index}
						shouldShowTags={ this.state.shouldShowTags }
						type={ this.props.type === 'MARKET' ? 'product' : 'user' }
						tags={this.props.tags && this.props.tags[index] }
						// image={ image.image_url }
						image={ image.image_url }
					/>
					{/* <ImageBit resizeMode={ImageBit.TYPES.COVER} source={Image} key={index} style={Styles.image} /> */}
					<BoxBit accessible={false} centering style={Styles.toggler}>
						<TogglerAnimationLego resetOnComplete sprite={'love-active-big'} isActive={this.props.isLiked} size={160} />
					</BoxBit>
				</TouchableBit>
			)
		}

		render() {
			return (
				<BoxBit unflex style={Styles.container}>
					{ !this.props.noHeader && (
						<HeaderPart
							user={ this.props.user.name }
							username={ this.props.user.username }
							profile={ this.props.user.profile }
							onMenuPress={ this.onMenuPress }
							onProfilePress={ this.onProfilePress }
						/>
					) }
					<GalleryBit
						width={ Sizes.screen.width - (15 * 2) }
						itemWidth={ Sizes.screen.width - (15 * 2) }
						gutter={0}
						padding={0}
						data={ this.props.images }
						renderItem={ this.imageRenderer }
						onMomentumScrollEnd={ this.onUpdateIndex }
					/>
					<NavigationPart
						index={ this.state.index }
						total={ this.props.images.length }
						style={ Styles.navi }
					/>
					<ActionPart
						isLiked={ this.props.isLiked }
						isCommented={ false }
						isApplied={ this.props.isApplied }
						// isOnCart={ this.props.isOnCart }
						// type={ this.props.type }
						type={'MARKET'}
						likeCount={ parseInt(this.props.likeCount, 10) }
						onPress={ this.onToggleAction }
					/>
					<DescriptionPart
						commentCount={ this.props.commentCount }
						createdAt={ this.props.createdAt }
						// TODO
						// username={ this.props.name }
						username="Bambang"
						caption={ this.props.caption }

						// UNUSED
						// myProfile={ this.props.me.profile }
						// myComment={ this.props.mycomment }
						// onSubmitComment={ this.onSubmitComment }

						onCommentPress={ this.onNavigateToComment }
						onMorePress={ this.onNavigateToPostDetail }
					/>
					{/* MAYBE TODO? */}
					{/* <BoxBit unflex row>
						<ImageBit
							source={ this.props.me.profile || ProfileImage }
							style={ Styles.comment }
						/>
						<TextInputBit unflex={false}
							placeholder="Add a comment..."
							onChange={ this.onChangeComment }
							onSubmitEditting={ this.onSubmitComment }
							style={{
								height: 28,
								borderBottomWidth: 0,
							}}
						/>
					</BoxBit> */}
				</BoxBit>
			)
		}
	}
)
