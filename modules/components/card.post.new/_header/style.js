import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	container: {
		paddingTop: 20,
		paddingBottom: 11,
		alignItems: 'center',
	},

	image: {
		width: 30,
		height: 30,
		borderRadius: 14,
		marginRight: 7,
		overflow: 'hidden',
	},

	menu: {
		width: 28,
		height: 28,
	},

	sub: {
		color: Colors.new.grey.palette(3),
	},

})
