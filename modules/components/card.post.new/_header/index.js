import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

// import PageManager from 'app/managers/page';

import BoxBit from 'modules/bits/box';
import ImageBit from 'modules/bits/image';
import SourceSansBit from 'modules/bits/source.sans';
import IconBit from 'modules/bits/icon';
// import GalleryBit from 'modules/bits/gallery';
import TouchableBit from 'modules/bits/touchable';
// import TextBit from 'modules/bits/text';

import Styles from './style'

const ProfileImage = ImageBit.resolve('profile.png');


export default ConnectHelper(
	class HeaderPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				user: PropTypes.string,
				username: PropTypes.string,
				profile: PropTypes.image,
				onMenuPress: PropTypes.func,
				style: PropTypes.style,
			}
		}

		static defaultProprs = {
			user: '',
			username: '',
			profile: undefined,
		}

		render() {
			return (
				<BoxBit row style={[Styles.container, this.props.style]}>
					<TouchableBit row onPress={ this.props.onProfilePress }>
						<ImageBit source={ this.props.profile || ProfileImage } resizeMode={ImageBit.TYPES.COVER} style={Styles.image} />
						<BoxBit>
							<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_1_ALT_2}>{ this.props.user || `@${this.props.username}` }</SourceSansBit>
							<SourceSansBit type={SourceSansBit.TYPES.NEW_NOTE} style={Styles.sub}>{this.props.user ? `@${this.props.username}` : '' }</SourceSansBit>
						</BoxBit>
					</TouchableBit>
					<TouchableBit onPress={ this.props.onMenuPress } unflex enlargeHitSlop centering style={Styles.menu}>
						<IconBit
							name="menu"
							size={24}
						/>
					</TouchableBit>
				</BoxBit>
			)
		}
	}
)
