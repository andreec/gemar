import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	container: {
		paddingTop: 12,
		paddingBottom: 12,
		alignItems: 'center',
	},

	icon: {
		marginRight: 8,
		width: 28,
		height: 28,
		// borderRadius: 14,
	},

})
