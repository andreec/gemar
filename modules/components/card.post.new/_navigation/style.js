import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	dot: {
		backgroundColor: Colors.new.grey.palette(4),
		width: 6,
		height: 6,
		borderRadius: 3,
		margin: 2,
	},

	dotActive: {
		backgroundColor: Colors.new.yellow.palette(3),
		width: 6,
		height: 6,
		borderRadius: 3,
		margin: 2,
	},

})
