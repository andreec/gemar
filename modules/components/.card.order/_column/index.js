import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import BoxBit from 'modules/bits/box';
import BoxLoadingBit from 'modules/bits/box.loading';
import SourceSansBit from 'modules/bits/source.sans';

import Styles from './style';


export default ConnectHelper(
	class ColumnPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				data: PropTypes.arrayOf(PropTypes.shape({
					title: PropTypes.string,
					content: PropTypes.string,
				})).isRequired,
				isLoading: PropTypes.bool,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			data: [],
		}

		constructor(p) {
			super(p, {}, [
				'columnRenderer',
			])
		}

		columnRenderer(data, i) {
			return this.props.isLoading ? (
				<BoxBit key={i} style={Styles.column}>
					<BoxLoadingBit unflex height={13} width={120} style={Styles.title} />
					<BoxLoadingBit unflex height={18} width={100} />
				</BoxBit>
			) : (
				<BoxBit key={i} style={Styles.column}>
					<SourceSansBit type={SourceSansBit.TYPES.SUBHEADER_3} weight="medium" style={Styles.title}>{ data.title }</SourceSansBit>
					<SourceSansBit type={SourceSansBit.TYPES.SUBHEADER_1} weight="medium" style={Styles.content}>{ data.content }</SourceSansBit>
				</BoxBit>
			)
		}

		render() {
			return (
				<BoxBit unflex row style={[Styles.container, this.props.style]}>
					{ this.props.data.map(this.columnRenderer) }
				</BoxBit>
			)
		}
	}
)
