import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({

	container: {
		marginTop: 0,
		marginBottom: 0,
		marginRight: -6,
		marginLeft: -6,
	},

	// border: {
	// 	borderBottomColor: Colors.coal.palette(2),
	// 	borderBottomWidth: 1,
	// 	borderBottomStyle: 'solid',
	// },

	column: {
		marginTop: 0,
		marginBottom: 0,
		marginRight: 6,
		marginLeft: 6,
	},

	title: {
		color: Colors.coal.palette(4),
		marginBottom: 4,
	},

	content: {
		color: Colors.black.palette(5),
	},

})
