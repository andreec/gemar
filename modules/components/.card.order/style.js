import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({

	container: {
		paddingTop: 16,
		paddingBottom: 16,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
		backgroundColor: Colors.white.primary,
		borderColor: Colors.coal.palette(2),
		borderWidth: 0,
		borderStyle: 'solid',
		borderTopWidth: StyleSheet.hairlineWidth,
		borderBottomWidth: StyleSheet.hairlineWidth,
	},

	header: {
		justifyContent: 'space-between',
		alignItems: 'center',
	},

	status: {
		marginBottom: 8,
	},

	bottom4: {
		marginBottom: 4,
	},

	row: {
		marginBottom: 16,
	},

	colorBlack: {
		color: Colors.black.palette(5),
	},

	colorGrey: {
		color: Colors.coal.palette(5),
	},

	colorPale: {
		color: Colors.coal.palette(4),
	},
})
