import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import CardComponentModel from 'coeur/models/components/card';

// import PageManager from 'app/managers/page';
import PostManager from 'app/managers/post';
import UserManager from 'app/managers/user';

import UtilitiesContext from 'coeur/contexts/utilities';
import PageContext from 'coeur/contexts/page';

// import TrackingHandler from 'app/handlers/tracking';

import Colors from 'coeur/constants/color';
import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
import BoxImageBit from 'modules/bits/box.image';
import IconBit from 'modules/bits/icon';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style';

function size(span = 1) {
	return ((Sizes.screen.width - ( 15 * 2 )) - ((3 - span) * 3)) * (span / 3)
}


export default ConnectHelper(
	class CardPostThumbnailComponent extends StatefulModel {
		static TYPES = {
			SOCIAL: 'SOCIAL',
			JOB: 'JOB',
			MARKET: 'MARKET',
		}

		static propTypes(PropTypes) {
			return {
				...super.propTypes(PropTypes),
				// TODO FIX THIS TYPES
				// type: PropTypes.oneOf(this.TYPES),
				big: PropTypes.bool,
			}
		}

		static contexts = [
			UtilitiesContext,
			PageContext,
		]

		getIcon(type) {
			switch(type) {
			case this.TYPES.SOCIAL:
			default:
				return false;
			case this.TYPES.JOB:
				return 'job-outline';
			case this.TYPES.MARKET:
				return 'market-outline';
			}
		}

		onPress = () => {
			if(this.props.onPress) {
				this.props.onPress(this.props.id)
			} else {
				this.props.page.navigator.navigate('post', {
					id: this.props.id,
					userId: this.props.userId,
				})
			}
		}

		render() {
			const _size = this.props.big ? size(2) : size()

			return (
				<TouchableBit unflex onPress={this.onPress}>
					<BoxImageBit unflex
						source={this.props.image}
						style={{
							width: _size,
							height: _size,
						}}
					>
						{/* {this.props.images.length > 1 && (
							<IconBit
								size={this.props.big ? 25 : 18}
								name="docs"
								color={Colors.new.white.primary}
								style={this.props.big ? Styles.multipleBig : Styles.multiple}
							/>
						)} */}
						{/* {this.props.data.type !== this.TYPES.SOCIAL && (
							<BoxBit centering style={this.props.big ? Styles.iconBig : Styles.icon}>
								<IconBit
									size={this.props.big ? 23 : 13}
									name={this.getIcon(this.props.data.type)}
								/>
							</BoxBit>
						)} */}
					</BoxImageBit>
				</TouchableBit>
			)
		}
	}
)
