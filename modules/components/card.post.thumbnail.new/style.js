import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	icon: {
		position: 'absolute',
		backgroundColor: Colors.new.white.palette(2),
		borderWidth: 1,
		borderColor: Colors.new.grey.palette(5),

		bottom: 4,
		left: 4,
		width: 24,
		height: 24,
		borderRadius: 12,
	},

	iconBig: {
		position: 'absolute',
		backgroundColor: Colors.new.white.palette(2),
		borderWidth: 1,
		borderColor: Colors.new.grey.palette(5),

		bottom: 8,
		left: 8,
		width: 32,
		height: 32,
		borderRadius: 16,
	},

	multiple: {
		position: 'absolute',

		top: 4,
		right: 4,
	},

	multipleBig: {
		position: 'absolute',

		top: 6,
		right: 6,
	},

})
