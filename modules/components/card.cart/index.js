import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import CardComponentModel from 'coeur/models/components/card';


import HobbyManager from 'app/managers/hobby';
import MeManager from 'app/managers/me';
import PostManager from 'app/managers/post';
import ProductManager from 'app/managers/product';
import UserManager from 'app/managers/user';

import UtilitiesContext from 'coeur/contexts/utilities';

import FormatHelper from 'coeur/helpers/format';

import Colors from 'coeur/constants/color';
// import Sizes from 'coeur/constants/size';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import IconBit from 'modules/bits/icon';
import InputValidatedBit from 'modules/bits/input.validated';
import ImageBit from 'modules/bits/image';
import SourceSansBit from 'modules/bits/source.sans';
import TextBit from 'modules/bits/text';
import TextInputBit from 'modules/bits/text.input';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style'


export default ConnectHelper(
	class CardCartComponent extends CardComponentModel(PostManager) {

		static stateToProps(state, oP) {
			const {
				id,
				data,
			} = super.stateToProps(state, oP)
			const cartIndex = state.me.cartItems.findIndex(cI => cI.id === id)

			return {
				id,
				data,
				user: UserManager.get(data.userId || 2),
				product: ProductManager.get(data.refId),
				cartIndex,
				cartItem: { id: 1, quantity: 1},
				// cartItem: state.me.cartItems[0],
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				isEditing: false,
			}, [
				'onChangeEditing',
				'onSubmitNote',
				'onAdd',
				'onSubtract',
			])
		}

		onChangeEditing() {
			this.setState({
				isEditing: !this.state.isEditing,
			})
		}

		onAdd() {
			MeManager.updateCartItems(this.props.id, {
				quantity: this.props.cartItem.quantity + 1,
			})
		}

		onSubtract() {
			if(this.props.quantity === 1) {
				this.props.utilities.alert.show({
					title: 'Remove Item',
					message: 'Do you want to remove this item from your cart?',
					actions: [{
						type: 'OK',
						title: 'Yes, please',
						onPress: () => {
							MeManager.removeFromCart(this.props.id)
						},
					}, {
						type: 'CANCEL',
						title: 'Cancel',
					}],
				})
			} else {
				MeManager.updateCartItems(this.props.id, {
					quantity: this.props.cartItem.quantity - 1,
				})
			}
		}

		onSubmitNote(val) {
			MeManager.updateCartItems(this.props.id, {
				note: val,
			})

			this.setState({
				isEditing: false,
			})
		}

		quantityRenderer() {
			return (
				<BoxBit row unflex style={Styles.quantity}>
					<TouchableBit centering unflex onPress={ this.onAdd } style={Styles.quantifier}>
						<IconBit
							name="plus"
							size={16}
						/>
					</TouchableBit>
					<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_1} style={Styles.input}>{ this.props.cartItem.quantity }</SourceSansBit>
					<TouchableBit centering unflex onPress={ this.onSubtract } style={Styles.quantifier}>
						<IconBit
							name="minus"
							size={16}
						/>
					</TouchableBit>
				</BoxBit>
			)
		}

		render() {
			return (
				<BoxBit unflex style={[Styles.container, this.props.style]}>
					<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_2} style={Styles.seller}>Seller : <TextBit weight="medium">@{ this.props.user.username }</TextBit></SourceSansBit>
					<BoxBit row>
						<ImageBit
							resizeMode={ ImageBit.TYPES.COVER }
							source={ this.props.data.image }
							style={Styles.image}
						/>
						<BoxBit style={Styles.text}>
							<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_1_ALT_2} weight="semibold" style={Styles.title}>{ this.props.product.title }</SourceSansBit>
							<SourceSansBit type={SourceSansBit.TYPES.NEW_HEADER_2} weight="semibold" style={Styles.price}>IDR { FormatHelper.currencyFormat(this.props.product.price) }</SourceSansBit>
							{ this.quantityRenderer() }
						</BoxBit>
					</BoxBit>
					{ this.state.isEditing ? (
						<BoxBit unflex>
							<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_2} weight="medium" style={Styles.editTextEditting}>Note for the seller (optional)</SourceSansBit>
							<TextInputBit autofocus
								style={Styles.textarea}
								inputStyle={{
									paddingTop: 0,
									paddingBottom: 0,
									height: 32,
								}}
								defaultValue={ this.props.cartItem.note }
								onSubmitEditing={ this.onSubmitNote }
							/>
						</BoxBit>
					) : (
						<TouchableBit row style={Styles.edit} onPress={ this.onChangeEditing }>
							<IconBit name="pencil" size={12} color={Colors.new.yellow.palette(3)} style={Styles.icon} />
							<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_2} weight="medium" style={this.props.cartItem.note ? undefined : Styles.editText}>{ this.props.cartItem.note || 'Write a note for the seller (optional)' }</SourceSansBit>
						</TouchableBit>
					) }
				</BoxBit>
			)
		}
	}
)
