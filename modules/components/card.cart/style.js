import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'

export default StyleSheet.create({

	container: {
		paddingBottom: 18,
		borderBottomWidth: 1,
		borderBottomColor: Colors.new.grey.palette(1),
	},

	seller: {
		marginBottom: 12,
	},

	image: {
		height: 86,
		width: 86,
		borderWidth: 1,
		borderColor: Colors.new.grey.palette(1),
		marginBottom: 11,
	},

	text: {
		marginLeft: 16,
	},

	input: {
		textAlign: 'center',
		width: 20,
	},

	title: {
		paddingBottom: 6,
	},

	price: {
		color: Colors.new.yellow.palette(3),
		paddingBottom: 17,
	},

	button: {
		alignSelf: 'center',
	},

	sub: {
		paddingTop: 4.5,
		paddingBottom: 5,
		color: Colors.new.black.palette(3),
	},

	quantity: {
		alignSelf: 'flex-end',
		alignItems: 'center',
	},

	quantifier: {
		width: 18,
		height: 18,
		backgroundColor: Colors.new.grey.palette(1),
		borderRadius: 9,
		borderColor: Colors.new.grey.palette(2),
		borderWidth: 1,
		marginLeft: 4,
		marginRight: 4,
	},

	edit: {
		alignItems: 'center',
	},

	icon: {
		margin: 2,
		marginRight: 8,
	},

	editText: {
		color: Colors.new.yellow.palette(3),
	},

	editTextEditting: {
		color: Colors.new.black.palette(4),
		marginBottom: 9,
	},

	textarea: {
		borderBottomWidth: 0,
	},

})
