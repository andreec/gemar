import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';
// import PageManager from 'app/managers/page';

// import TrackingHandler from 'app/handlers/tracking';

// import Animated from 'coeur/libs/animated';

import BoxBit from 'modules/bits/box';
import SourceSansBit from 'modules/bits/source.sans';

import ListHeaderLego from 'modules/legos/list.header';

import Styles from './style'
import { capitalize, chunk, isEmpty } from 'lodash';

export default ConnectHelper(
	class FormHeaderLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string,
				data: PropTypes.array,
				chunk: PropTypes.number,
				
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			data: [],
			chunk: 3,
		}

		static getDerivedStateFromProps(nP) {
			return {
				datas: chunk(nP.data, nP.chunk),
			}
		}


		constructor(p) {
			super(p, {
				datas: [],
			});
		}

		contentRenderer = ({
			title,
			answer,
			children,
		}, i) => {
			return (
				<BoxBit unflex key={i} style={Styles.padder}>
					<SourceSansBit type={SourceSansBit.TYPES.NEW_PARAGRAPH_3} style={Styles.darkGrey}>{ capitalize(title) }:</SourceSansBit>
					{ children ? children : (
						<SourceSansBit type={SourceSansBit.TYPES.NEW_PARAGRAPH_3} weight="semibold">{ answer || '-'}</SourceSansBit>
					)}
				</BoxBit>
			)
		}

		chunkedRenderer(data) {
			return (
				<BoxBit>
					{ data.map(this.contentRenderer) }
				</BoxBit>
			)
		}

		render() {
			return !isEmpty(this.state.datas) && (
				<BoxBit style={[Styles.container, this.props.style]}>
					{ this.props.title && (
						<ListHeaderLego title={this.props.title}/>
					)}
					<BoxBit row>
						{ this.state.datas.map(data => this.chunkedRenderer(data)) }
					</BoxBit>
				</BoxBit>
			)
		}
	}
)
