import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	container: {
		paddingBottom: Sizes.margin.thick * 2,
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderBottomColor: Colors.new.black.palette(3),
	},
	padder: {
		paddingBottom: Sizes.margin.default,
	},
	header: {
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderBottomColor: Colors.new.black.palette(3),
	},

	jobId: {
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderTopWidth: StyleSheet.hairlineWidth,
		borderColor: Colors.new.black.palette(3),
	},

	darkGrey: {
		color: Colors.new.black.palette(3),
	},

	detail: {
		alignItems: 'center',
	},
})
