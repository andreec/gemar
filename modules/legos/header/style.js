import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		paddingTop: 25,
		paddingBottom: 15,
	},

	title: {
		marginBottom: 5,
	},

	description: {
		color: Colors.new.black.palette(3),
	},
})
