import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import BoxBit from 'modules/bits/box';
import SourceSansBit from 'modules/bits/source.sans';

import Styles from './style';


export default ConnectHelper(
	class HeaderLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string,
				description: PropTypes.string,
				style: PropTypes.style,
			}
		}

		render() {
			return (
				<BoxBit unflex style={[Styles.container, this.props.style]}>
					<SourceSansBit weight="bold" type={SourceSansBit.TYPES.NEW_HEADER_1} children={this.props.title} style={Styles.title} />
					<SourceSansBit italic weight="light" type={SourceSansBit.TYPES.NEW_SUBHEADER_2} children={this.props.description} style={Styles.description} />
				</BoxBit>
			);
		}
	}
)
