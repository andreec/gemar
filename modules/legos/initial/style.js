import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'


export default StyleSheet.create({
	container: {
		width: 52,
		height: 52,
		borderRadius: 26,
		overflow: 'hidden',
	},
	title: {
		color: Colors.white.primary,
	},
})
