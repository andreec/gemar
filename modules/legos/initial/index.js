import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import SourceSansBit from 'modules/bits/source.sans';

import Styles from './style';

const backgrounds = [
		Colors.purple.primary,
		Colors.pink.primary,
		Colors.blue.primary,
		Colors.green.primary,
		Colors.yellow.primary,
		Colors.red.primary,
	], backgroundLen = backgrounds.length


export default ConnectHelper(
	class InitialLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				name: PropTypes.string.isRequired,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			name: '',
		}

		static getDerivedStateFromProps(nP) {
			return {
				background: backgrounds[Math.floor(Math.random() * backgroundLen)],
				name: nP.name.split(' ').map(name => {
					return name[0]
				}).join('').substring(0, 2).toUpperCase(),
			}
		}

		render() {
			return (
				<BoxBit unflex centering style={[Styles.container, {
					backgroundColor: this.state.background,
				}, this.props.style]}>
					<SourceSansBit type={SourceSansBit.TYPES.HEADER_5} style={Styles.title}>{ this.state.name }</SourceSansBit>
				</BoxBit>
			)
		}
	}
)
