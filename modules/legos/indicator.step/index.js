import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import BoxBit from 'modules/bits/box';

import Styles from './style';


export default ConnectHelper(
	class IndicatorStepLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				total: PropTypes.number,
				index: PropTypes.number,
				style: PropTypes.style,
				// textIndicator: PropTypes.bool,
			}
		}

		render() {
			return (
				<BoxBit unflex centering row style={[Styles.container, this.props.style]}>
					{ Array(this.props.total).fill().map((u, i) => {
						return (
							<BoxBit unflex key={i} style={[Styles.paging, this.props.index === i ? Styles.pagingActive : Styles.pagingInactive]} />
						)
					}) }
				</BoxBit>
			)
		}
	}
)
