import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'


export default StyleSheet.create({
	container: {
		paddingTop: 4,
		paddingBottom: 4,
	},
	paging: {
		width: 8,
		height: 8,
		borderRadius: 4,
		marginLeft: 4,
		marginRight: 4,
	},

	pagingActive: {
		borderWidth: 1,
		borderStyle: 'solid',
		borderColor: Colors.primary,
		backgroundColor: Colors.primary,
	},

	pagingInactive: {
		borderWidth: 1,
		borderStyle: 'solid',
		borderColor: Colors.white.primary,
		backgroundColor: Colors.grey.palette(4),
	},
})
