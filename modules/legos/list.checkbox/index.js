import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import CheckboxBit from 'modules/bits/checkbox';

import ListLego from '../list';


export default ConnectHelper(
	class ListCheckboxLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				isActive: PropTypes.bool,
				...ListLego.propTypes,
			}
		}

		static defaultProps = {
			isActive: false,
		}

		render() {
			return (
				<ListLego
					title={ this.props.title }
					isFirst={ this.props.isFirst }
					onPress={ this.props.onPress }
					titleStyle={ this.props.titleStyle }
				>
					<CheckboxBit
						isActive={ this.props.isActive }
						onPress={ this.props.onPress }
					/>
				</ListLego>
			)
		}
	}
)
