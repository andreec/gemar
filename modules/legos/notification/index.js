import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import SourceSansBit from 'modules/bits/source.sans';
import IconBit from 'modules/bits/icon';

import Styles from './style';


export default ConnectHelper(
	class IndicatorStepLego extends StatefulModel {

		static TYPES = {
			DEFAULT: 'DEFAULT',
			WARNING: 'WARNING',
		}

		static propTypes(PropTypes) {
			return {
				message: PropTypes.string.isRequired,
				type: PropTypes.oneOf(this.TYPES),
				icon: PropTypes.oneOf(IconBit.TYPES),
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			type: 'DEFAULT',
			icon: 'circle-alert',
		}

		getBackgroundColor() {
			switch(this.props.type) {
			case this.TYPES.WARNING:
				return Colors.red.palette(4)
			case this.TYPES.DEFAULT:
			default:
				return Colors.coal.palette(5)
			}
		}

		render() {
			return (
				<BoxBit unflex row centering style={[Styles.container, {
					backgroundColor: this.getBackgroundColor(),
				}, this.props.style]}>
					<BoxBit style={Styles.titleContainer}>
						<SourceSansBit type={SourceSansBit.TYPES.CAPTION_1} style={Styles.title}>{ this.props.message }</SourceSansBit>
					</BoxBit>
					<IconBit
						name={this.props.icon}
						color={ Colors.white.primary }
					/>
				</BoxBit>
			)
		}
	}
)
