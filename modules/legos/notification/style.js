import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'


export default StyleSheet.create({
	container: {
		paddingTop: 16,
		paddingBottom: 16,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
	},
	titleContainer: {
		marginRight: 16,
	},
	title: {
		color: Colors.white.primary,
	},
})
