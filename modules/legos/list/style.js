import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'


export default StyleSheet.create({
	container: {
		paddingTop: 16,
		paddingBottom: 16,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
		backgroundColor: Colors.white.primary,
		borderStyle: 'solid',
		borderColor: Colors.coal.palette(2),
		borderWidth: 0,
		borderBottomWidth: StyleSheet.hairlineWidth,
		justifyContent: 'space-between',
		alignItems: 'flex-start',
	},

	head: {
		borderTopWidth: StyleSheet.hairlineWidth,
	},

	text: {
		color: Colors.black.palette(4),
		marginRight: 8,
	},
})
