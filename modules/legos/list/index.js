import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import SourceSansBit from 'modules/bits/source.sans';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style';


export default ConnectHelper(
	class ListLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string.isRequired,
				isFirst: PropTypes.bool,
				children: PropTypes.node,
				onPress: PropTypes.func,
				titleStyle: PropTypes.style,
				style: PropTypes.style,
			}
		}

		static defaultProps = {
			isFirst: false,
		}

		render() {
			return (
				<TouchableBit unflex row onPress={this.props.onPress} style={[Styles.container, this.props.isFirst && Styles.head, this.props.style]}>
					{ this.props.title && (
						<SourceSansBit type={SourceSansBit.TYPES.NEW_LIST} weight="medium" style={[Styles.text, this.props.titleStyle]}>{ this.props.title }</SourceSansBit>
					) }
					{ this.props.children }
				</TouchableBit>
			)
		}
	}
)
