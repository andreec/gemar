import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import ToggleableComponentModel from 'coeur/models/components/toggleable';

import BoxBit from 'modules/bits/box';
import IconBit from 'modules/bits/icon';
import SpriteBit from 'modules/bits/sprite';

import Animated from 'coeur/libs/animated';


export default ConnectHelper(
	class TogglerAnimationLego extends ToggleableComponentModel {

		static TYPES = {
			ICONS: IconBit.TYPES,
			SPRITES: SpriteBit.TYPES,
		}

		static propTypes(PropTypes) {
			return {
				...super.propTypes(PropTypes),
				icon: PropTypes.oneOf(this.TYPES.ICONS),
				sprite: PropTypes.oneOf(this.TYPES.SPRITES),
				size: PropTypes.number,
				color: PropTypes.string,
				animationInterpolator: PropTypes.func,
				resetOnComplete: PropTypes.bool,
			}
		}

		static defaultProps = {
			isActive: false,
			size: 24,
		}

		constructor(p) {
			super(p, {
			}, [
				'startOutAnimation',
			])

			this.__timeoutAnimation = false;
			this.__animationTiming = undefined;
			this.__animationValue = new Animated.Value(p.isActive ? p.resetOnComplete && 2 || 1 : p.resetOnComplete && 2 || 0)
			this.__animationInterpolation = this.getAnimationInterpolation(this.__animationValue)
		}

		getAnimationInterpolation(animationValue) {
			if (this.props.animationInterpolator) {
				return this.props.animationInterpolator(animationValue)
			} else {
				return {
					opacity: animationValue.interpolate({
						inputRange: [0, 1, 2],
						outputRange: [0, 1, 0],
					}),
					transform: [{
						scale: animationValue.interpolate({
							inputRange: [0, 1, 2],
							outputRange: [.5, 1, .5],
						}),
					}, {
						translateY: animationValue.interpolate({
							inputRange: [0, 1, 2],
							outputRange: [100, 0, -100],
						}),
					}],
				}
			}
		}

		shouldComponentUpdate(nP, nS) {
			return this.state.isActive !== nS.isActive
		}

		// toggle(isActive, cb) {
		// 	if (!this.props.resetOnComplete || (this.props.resetOnComplete && isActive)) {
		// 		this.setState({
		// 			isActive,
		// 		}, cb)
		// 	} else if (this.props.resetOnComplete && !isActive) {
		// 		this.setState({
		// 			isActive: false,
		// 		}, cb)
		// 	}
		// }

		componentDidUpdate() {
			if (!this.props.resetOnComplete || (this.props.resetOnComplete && this.state.isActive)) {
				this.__animationTiming &&
				this.__animationTiming.stop();

				if (this.state.isActive) {
					this.__animationValue.setValue(0)
				}

				this.__animationTiming = Animated.spring(this.__animationValue, {
					toValue: this.state.isActive ? 1 : 0,
					useNativeDriver: true,
				}).start(({ finished }) => {
					if (this.state.isActive && finished) {
						if (this.__timeoutAnimation) {
							clearTimeout(this.__timeoutAnimation);
						}

						this.__timeoutAnimation = this.setTimeout(this.startOutAnimation, 300)
					}
				})
			} else if (this.props.resetOnComplete && !this.state.isActive) {
				this.__animationTiming &&
				this.__animationTiming.stop();

				this.__animationValue.setValue(0);
			}
		}


		startOutAnimation() {
			if (!this.props.resetOnComplete || (this.props.resetOnComplete && this.state.isActive)) {
				this.__animationTiming = Animated.spring(this.__animationValue, {
					toValue: 2,
					useNativeDriver: true,
				}).start(() => {
					if (this.props.resetOnComplete) {
						this.setState({
							isActive: false,
						})
						// }, () => {
						// 	this.__animationValue.setValue(0)
						// })
					}
				})
			}
		}

		render() {
			return (
				<BoxBit animated unflex accessible={false} style={[this.__animationInterpolation, this.props.style]}>
					{this.props.icon ? (
						<IconBit
							name={this.props.icon}
							size={this.props.size}
							color={this.props.color}
						/>
					) : (
						<SpriteBit
							name={this.props.sprite}
							size={this.props.size}
						/>
					)}
				</BoxBit>
			);
		}
	}
)
