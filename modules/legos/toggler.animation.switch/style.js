import {
	StyleSheet,
	// Platform,
} from 'react-native';
// import {
// 	Colors,
// 	// Sizes,
// 	// Fonts,
// } from '@yuna/app/constants';

export default StyleSheet.create({
	container: {
		minWidth: 100,
		minHeight: 100,
	},
	content: {
		position: 'absolute',
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
	},
})
