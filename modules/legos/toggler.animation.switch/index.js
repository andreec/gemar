import React from 'react';
import Styles from './style';
import {
	StatefulModel,
} from '@yuna/app/models';
import {
	ConnectDecorator,
} from '@yuna/utilities/decorators';
// import {
// 	CommonHelper,
// } from '@yuna/utilities/helpers';
import {
	BoxBit,
	SpriteBit,
	IconBit,
} from '@yuna/modules/bits';

import TogglerAnimationLego from '../toggler.animation'

@ConnectDecorator
export default class TogglerAnimationSwitchLego extends StatefulModel {

	static TYPES = {
		ICONS: IconBit.TYPES,
		SPRITES: SpriteBit.TYPES,
	}

	static propTypes(PropTypes) {
		const animationConfig = {
			icon: PropTypes.oneOf(this.TYPES.ICONS),
			sprite: PropTypes.oneOf(this.TYPES.SPRITES),
			size: PropTypes.number,
			color: PropTypes.string,
			animationInterpolator: PropTypes.func,
		}

		return {
			isActive: PropTypes.bool,
			active: PropTypes.shape(animationConfig).isRequired,
			inactive: PropTypes.shape(animationConfig).isRequired,
			style: PropTypes.style,
		}
	}

	static defaultProps = {
		isActive: false,
	}

	constructor(p, c) {
		super(p, c, {
			isActive: p.isActive,
		})
	}

	shouldComponentUpdate(nP, nS) {
		return (nS.isActive !== this.state.isActive) || (nP.isActive !== this.state.isActive)
	}

	componentWillReceiveProps(nP) {
		if(this.state.isActive !== nP.isActive) {
			this.setState({
				isActive: nP.isActive,
			})
		}
	}

	animationInterpolator(animationValue) {
		return {
			opacity: animationValue.interpolate({
				inputRange: [0, 1, 2],
				outputRange: [0, 1, 0],
			}),
			transform: [{
				scale: animationValue.interpolate({
					inputRange: [0, 1, 2],
					outputRange: [.5, 1, 2],
				}),
			}],
		}
	}

	animationRenderer(config, isActive) {
		return (
			<TogglerAnimationLego resetOnComplete
				isActive={ isActive }
				animationInterpolator={ this.animationInterpolator }
				{ ...config }
			/>
		)
	}

	render() {
		return (
			<BoxBit unflex style={[Styles.container, this.props.style]}>
				<BoxBit unflex centering style={Styles.content}>
					{ this.animationRenderer(this.props.active, this.state.isActive) }
				</BoxBit>
				<BoxBit unflex centering style={Styles.content}>
					{ this.animationRenderer(this.props.inactive, !this.state.isActive) }
				</BoxBit>
			</BoxBit>
		);
	}
}
