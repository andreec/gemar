import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import BoxBit from 'modules/bits/box';
import SourceSansBit from 'modules/bits/source.sans';


import Styles from './style'

export default ConnectHelper(
	class AwbLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string,
				number: PropTypes.string,

				style: PropTypes.style,
			}
		}

		static defaultProps = {
			title: 'AWB Number:',
		}

		constructor(p) {
			super(p, {
			})
		}

		render() {
			return (
				<BoxBit unflex row style={[Styles.container, this.props.style]}>
					<BoxBit style={{flexGrow: 2}}>
						<SourceSansBit type={SourceSansBit.TYPES.NEW_PARAGRAPH_3} style={Styles.darkGrey}>{this.props.title}</SourceSansBit>
						<SourceSansBit type={SourceSansBit.TYPES.NEW_PARAGRAPH_3}>{this.props.number}</SourceSansBit>
					</BoxBit>
					<BoxBit unflex style={Styles.copy}>
						<SourceSansBit type={SourceSansBit.TYPES.NEW_PARAGRAPH_3} style={Styles.copyText}>Copy</SourceSansBit>
					</BoxBit>
				</BoxBit>
			)
		}
	}
)
