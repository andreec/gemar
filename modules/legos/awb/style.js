import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	container: {
		paddingTop: Sizes.margin.default,
		paddingBottom: Sizes.margin.default,
	},
	darkGrey: {
		color: Colors.new.black.palette(3),
	},
	copy: {
		alignSelf: 'center',
	},
	copyText: {
		textAlign: 'center',
	},
})
