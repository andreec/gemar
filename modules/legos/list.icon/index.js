import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import IconBit from 'modules/bits/icon';

import ListLego from '../list';


export default ConnectHelper(
	class ListIconLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				icon: PropTypes.string,
				iconSize: PropTypes.number,
				iconColor: PropTypes.string,
				...ListLego.propTypes,
			}
		}

		static defaultProps = {
			icon: 'arrow-right',
		}

		shouldComponentUpdate() {
			return false
		}

		render() {
			return (
				<ListLego
					title={ this.props.title }
					isFirst={ this.props.isFirst }
					onPress={ this.props.onPress }
					titleStyle={ this.props.titleStyle }
				>
					<BoxBit unflex centering style={{
						alignSelf: 'stretch',
					}}>
						<IconBit
							name={this.props.icon}
							size={this.props.iconSize}
							color={this.props.iconColor || Colors.coal.palette(4)}
						/>
					</BoxBit>
				</ListLego>
			)
		}
	}
)
