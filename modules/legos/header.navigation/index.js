import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import PageContext from 'coeur/contexts/page';
import UtilitiesContext from 'coeur/contexts/utilities';

import BoxBit from 'modules/bits/box';

import HeaderBit, {
	ButtonPart,
} from 'modules/bits/header';
import ModalPart from './_modal'

import Styles from './style';


export default ConnectHelper(
	class HeaderNavigationLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				style: PropTypes.style,
			}
		}

		static contexts = [
			PageContext,
			UtilitiesContext,
		]

		static defaultProps = {
		}

		header = {
			leftActions: [{
				type: HeaderBit.TYPES.BLANK,
			}],
			rightActions: [{
				type: HeaderBit.TYPES.MENU,
				onPress: this.onShowMenu,
			}],
		}

		constructor(p) {
			super(p, {
			}, [
				'onBackToTop',
				'onShowMenu',
			])
		}

		onBackToTop() {
			this.props.utilities.alert.hide()
			this.props.page.navigator.top()
		}

		onShowMenu() {
			this.props.utilities.alert.modal({
				component: this.menuRenderer(),
				animationType: 'FLY_TO_BOTTOM',
				// overlayStyle: Styles.overlay,
			})
		}

		menuRenderer() {
			return (
				<ModalPart navigator={this.props.page.navigator} />
			)
		}

		render() {
			return (
				<HeaderBit style={Styles.container} { ...this.header }>
					<BoxBit>
						<ButtonPart
							type={ButtonPart.TYPES.LOGO}
							onPress={this.onBackToTop}
						/>
					</BoxBit>
				</HeaderBit>
			);
		}
	}
)
