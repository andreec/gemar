import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		zIndex: 2,
		minHeight: 64,
		paddingTop: 16,
		paddingBottom: 16,
		paddingLeft: 24,
		paddingRight: 24,
	},

	menuContainer: {
		height: Sizes.screen.height,
		backgroundColor: Colors.coal.primary,
	},

	menuContent: {
		paddingLeft: 24,
		paddingRight: 24,
		paddingTop: 16,
		paddingBottom: 16,
	},

	menuList: {
		paddingTop: 16,
		paddingBottom: 16,
	},

	// alert: {
	// 	padding: 0,
	// },

	overlay: {
		backgroundColor: 'transparent',
	},
})
