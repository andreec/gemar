import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		backgroundColor: Colors.coal.primary,
	},

	header: {
		paddingTop: 16,
		paddingBottom: 16,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
	},

	content: {
		padding: 24,
	},

	menu: {
		paddingTop: 8,
		paddingBottom: 8,
		paddingLeft: 0,
		paddingRight: 0,
	},

	centering: {
		alignItems: 'center',
	},

	text: {
		color: Colors.white.primary,
	},

	subheader: {
		color: Colors.grey.palette(6),
	},

	subheader2: {
		marginTop: 24,
	},

	icon: {
		marginLeft: 16,
	},

	footer: {
		color: Colors.white.palette(4),
	},
})
