import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';

import UtilitiesContext from 'coeur/contexts/utilities';

import Linking from 'coeur/libs/linking';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import SourceSansBit from 'modules/bits/source.sans';
import HeaderBit from 'modules/bits/header';
import IconBit from 'modules/bits/icon';
import PageBit from 'modules/bits/page';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style';


export default ConnectHelper(
	class ModalPart extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				navigator: PropTypes.object,
				me: PropTypes.object,
			}
		}

		static stateToProps(state) {
			return {
				me: state.me,
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		header = {
			leftActions: [{
				type: HeaderBit.TYPES.LOGO_YUNA,
				onPress: this.onBackToTop,
				data: {
					color: Colors.white.primary,
				},
			}],
			rightActions: [{
				type: HeaderBit.TYPES.CLOSE,
				onPress: this.onToggleMenu,
				data: {
					color: Colors.white.primary,
				},
			}],
			background: Colors.coal.primary,
			style: Styles.header,
		}

		constructor(p) {
			super(p, {
			}, [
				'onBackToTop',
				'onToggleMenu',
				'onNavigateToMatchbox',
				'onNavigateToJournal',
				'onNavigateToProfile',
				'onNavigateToFAQ',
				'onNavigateToPrivacy',
				'onNavigateToTerms',
				'onNavigateToInstagram',
				'onNavigateToFacebook',
				'onAskUs',
				'onEmailUs',
			])
		}

		shouldComponentUpdate() {
			return false
		}

		onBackToTop() {
			this.props.utilities.alert.hide()
			this.props.navigator.top()
		}

		onToggleMenu() {
			this.props.utilities.alert.hide()
		}

		onNavigateToMatchbox() {
			this.props.utilities.alert.hide()
			this.props.navigator.navigate('matchbox')
		}

		onNavigateToJournal() {
			this.props.utilities.alert.hide()
			this.props.navigator.navigate('webview', {
				title: 'JOURNAL',
				url: 'https://journal.helloyuna.io/',
			})
		}

		onNavigateToProfile() {
			this.props.utilities.alert.hide()
			if(this.props.me.id) {
				this.props.navigator.navigate('profile')
			} else {
				this.props.navigator.navigate('signup')
			}
		}

		onNavigateToFAQ() {
			this.props.utilities.alert.hide()
			this.props.navigator.navigate('webview', {
				title: 'FAQ',
				url: '/faq.html',
			})
		}

		onNavigateToPrivacy() {
			this.props.utilities.alert.hide()
			this.props.navigator.navigate('webview', {
				title: 'PRIVACY POLICY',
				url: '/privacy.html',
			})
		}

		onNavigateToTerms() {
			this.props.utilities.alert.hide()
			this.props.navigator.navigate('webview', {
				title: 'TERMS OF SERVICE',
				url: '/term.html',
			})
		}

		onNavigateToInstagram() {
			this.props.utilities.alert.hide()
			Linking.open('https://www.instagram.com/yunaandco/')
		}

		onNavigateToFacebook() {
			this.props.utilities.alert.hide()
			Linking.open('https://www.facebook.com/yunaandco/')
		}

		onAskUs() {
			this.props.utilities.alert.hide()
			Linking.open('whatsapp://send?phone=628176333222&text=Hi%2C%20I\'d%20like%20to%20know%20more%20about%20Matchbox.')
		}

		onEmailUs() {
			this.props.utilities.alert.hide()
			Linking.open('mailto:care@helloyuna.io')
		}

		render() {
			return (
				<PageBit header={ <HeaderBit { ...this.header } /> } style={Styles.container}>
					<BoxBit unflex style={Styles.content}>
						<TouchableBit unflex onPress={ this.onNavigateToProfile } style={Styles.menu}>
							<SourceSansBit type={SourceSansBit.TYPES.HEADER_4} style={[Styles.text]}>{ this.props.me.id ? this.props.me.firstName : 'Login/Register'}</SourceSansBit>
						</TouchableBit>
						<TouchableBit unflex onPress={ this.onNavigateToMatchbox } style={Styles.menu}>
							<SourceSansBit type={SourceSansBit.TYPES.HEADER_4} style={[Styles.text]}>Matchbox</SourceSansBit>
						</TouchableBit>
						<TouchableBit unflex onPress={ this.onNavigateToJournal } style={Styles.menu}>
							<SourceSansBit type={SourceSansBit.TYPES.HEADER_4} style={[Styles.text]}>Journal</SourceSansBit>
						</TouchableBit>
					</BoxBit>
					<BoxBit unflex style={Styles.content}>
						<SourceSansBit type={SourceSansBit.TYPES.SUBHEADER_3} weight="medium" style={[Styles.menu, Styles.subheader]}>GET STYLE INSPIRATION</SourceSansBit>
						<TouchableBit unflex row style={[Styles.centering, Styles.menu]} onPress={this.onNavigateToInstagram}>
							<IconBit
								name="instagram"
								color={Colors.white.primary}
							/>
							<SourceSansBit type={SourceSansBit.TYPES.SECONDARY_3} weight="medium" style={[Styles.text, Styles.icon]}>Instagram</SourceSansBit>
						</TouchableBit>
						<TouchableBit unflex row style={[Styles.centering, Styles.menu]} onPress={this.onNavigateToFacebook}>
							<IconBit
								name="facebook"
								color={Colors.white.primary}
							/>
							<SourceSansBit type={SourceSansBit.TYPES.SECONDARY_3} weight="medium" style={[Styles.text, Styles.icon]}>Facebook</SourceSansBit>
						</TouchableBit>
						<SourceSansBit type={SourceSansBit.TYPES.SUBHEADER_3} weight="medium" style={[Styles.menu, Styles.subheader, Styles.subheader2]}>QUESTIONS?</SourceSansBit>
						<TouchableBit unflex row style={[Styles.centering, Styles.menu]} onPress={this.onAskUs}>
							<IconBit
								name="whatsapp"
								color={Colors.white.primary}
							/>
							<SourceSansBit type={SourceSansBit.TYPES.SECONDARY_3} weight="medium" style={[Styles.text, Styles.icon]}>Ask us</SourceSansBit>
						</TouchableBit>
						<TouchableBit unflex row style={[Styles.centering, Styles.menu]} onPress={this.onEmailUs}>
							<IconBit
								name="mail"
								color={Colors.white.primary}
							/>
							<SourceSansBit type={SourceSansBit.TYPES.SECONDARY_3} weight="medium" style={[Styles.text, Styles.icon]}>Email us</SourceSansBit>
						</TouchableBit>
					</BoxBit>
					<BoxBit unflex style={Styles.content}>
						<TouchableBit unflex onPress={ this.onNavigateToFAQ } style={Styles.menu}>
							<SourceSansBit type={SourceSansBit.TYPES.SECONDARY_3} weight="medium" style={[Styles.text]}>FAQ</SourceSansBit>
						</TouchableBit>
						<TouchableBit unflex onPress={ this.onNavigateToPrivacy } style={Styles.menu}>
							<SourceSansBit type={SourceSansBit.TYPES.SECONDARY_3} weight="medium" style={[Styles.text]}>Privacy Policy</SourceSansBit>
						</TouchableBit>
						<TouchableBit unflex onPress={ this.onNavigateToTerms } style={Styles.menu}>
							<SourceSansBit type={SourceSansBit.TYPES.SECONDARY_3} weight="medium" style={[Styles.text]}>Terms of Service</SourceSansBit>
						</TouchableBit>
					</BoxBit>
					<BoxBit unflex style={Styles.content}>
						<SourceSansBit type={SourceSansBit.TYPES.CAPTION_1} style={Styles.footer}>© Yuna+Co.</SourceSansBit>
					</BoxBit>
				</PageBit>
			)
		}
	}
)
