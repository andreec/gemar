import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

// import Colors from 'coeur/constants/color';

import InputValidatedBit from 'modules/bits/input.validated';

import ListLego from '../list';


export default ConnectHelper(
	class ListInputLego extends StatefulModel {

		static propTypes() {
			return {
				...ListLego.propTypes,
				...InputValidatedBit.propTypes,
			}
		}

		static defaultProps = {
			icon: 'arrow-right',
		}

		shouldComponentUpdate() {
			return false
		}

		render() {
			const {
				isFirst,
				...props
			} = this.props
			return (
				<ListLego
					isFirst={ isFirst }
				>
					<InputValidatedBit { ...props } />
				</ListLego>
			)
		}
	}
)
