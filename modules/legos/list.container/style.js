import StyleSheet from 'coeur/libs/style.sheet'
// import Colors from 'coeur/constants/color'
// import Sizes from 'coeur/constants/size'


export default StyleSheet.create({
	container: {
		paddingTop: 8,
		paddingBottom: 8,
	},
})
