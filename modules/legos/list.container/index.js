import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import BoxBit from 'modules/bits/box';

import ListIconLego from '../list.icon'

import Styles from './style';


export default ConnectHelper(
	class ListContainerLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				component: PropTypes.func,
				data: PropTypes.arrayOf(PropTypes.shape(ListIconLego.propTypes)).isRequired,
			}
		}

		static defaultProps = {
			data: [],
		}

		constructor(p) {
			super(p, {}, [
				'listRenderer',
			])
		}

		shouldComponentUpdate() {
			return false
		}

		listRenderer(data, i) {
			return this.props.component ? (
				<this.props.component
					key={ i }
					isFirst={ i === 0 }
					{ ...data }
				/>
			) : (
				<ListIconLego
					key={ i }
					isFirst={ i === 0 }
					{ ...data }
				/>
			)
		}

		render() {
			return (
				<BoxBit unflex style={[Styles.container, this.props.style]}>
					{ this.props.data.map(this.listRenderer) }
				</BoxBit>
			)
		}
	}
)
