import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'


export default StyleSheet.create({
	container: {
		paddingTop: 28,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
		paddingBottom: 12,
	},
	title: {
		color: Colors.black.palette(5),
	},
})
