import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import BoxBit from 'modules/bits/box';
import SourceSansBit from 'modules/bits/source.sans';

import Styles from './style';


export default ConnectHelper(
	class ListHeaderLego extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string.isRequired,
				style: PropTypes.style,
				inputStyle: PropTypes.style,
			}
		}

		render() {
			return (
				<BoxBit unflex style={[Styles.container, this.props.style]}>
					<SourceSansBit type={SourceSansBit.TYPES.SUBHEADER_1} style={[Styles.title, this.props.inputStyle]}>
						{ this.props.title }
					</SourceSansBit>
				</BoxBit>
			)
		}
	}
)
