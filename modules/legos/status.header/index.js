import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';
import TimeHelper from 'coeur/helpers/time';
// import PageManager from 'app/managers/page';

// import TrackingHandler from 'app/handlers/tracking';

// import Animated from 'coeur/libs/animated';

import BoxBit from 'modules/bits/box';
import IconBit from 'modules/bits/icon';
import TouchableBit from 'modules/bits/touchable';
// import ImageBit from 'modules/bits/image';
import LoaderBit from 'modules/bits/loader';
import SourceSansBit from 'modules/bits/source.sans';

import Styles from './style'
import { capitalize } from 'lodash';

export default ConnectHelper(
	class HeaderPart extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string,
				status: PropTypes.string,
				date: PropTypes.string,

				onPress: PropTypes.func,
			}
		}

		static defaultProps = {
			title: 'status',
			status: 'UNREAD',
		}

		constructor(p) {
			super(p, {
			});
		}

		render() {
			return (
				<BoxBit row unflex style={Styles.container}>
					<BoxBit>
						<SourceSansBit type={SourceSansBit.TYPES.NEW_PARAGRAPH_3} style={[Styles.darkGrey, Styles.text]}>{ capitalize(this.props.title) }</SourceSansBit>
						<SourceSansBit type={SourceSansBit.TYPES.NEW_HEADER_2} weight="semibold" style={[Styles.darkGrey60, Styles.text]}>{ this.props.status.toUpperCase() }</SourceSansBit>
						{ !!this.props.date && (
							<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_1} style={[Styles.darkGrey, Styles.text]}>
								{ TimeHelper.moment(this.props.date).format('MMMM DD, YYYY') }
							</SourceSansBit>
						)}
					</BoxBit>

					{ this.props.onPress && (
						<TouchableBit row unflex centering onPress={this.props.onPress} style={Styles.detail}>
							<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_1} style={Styles.darkGrey}>See detail</SourceSansBit>
							<IconBit
								name="arrow-right"
								size={20}
							/>
						</TouchableBit>
					)}
				</BoxBit>
			)
		}
	}
)
