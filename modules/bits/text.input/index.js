import ConnectHelper from 'coeur/helpers/connect';
import CoreTextInputBit from 'coeur/modules/bits/text.input';

import Colors from 'coeur/constants/color';

import BoxBit from '../box';

import Styles from './style';


export default ConnectHelper(
	class TextInputBit extends CoreTextInputBit({
		Styles,
		BoxBit,
		placeholderColor: Colors.new.black.palette(3),
	}) {}
)
