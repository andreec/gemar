import StyleSheet from 'coeur/libs/style.sheet';
import Colors from 'coeur/constants/color';
import Fonts from 'coeur/constants/font';

export default StyleSheet.create({
	container: {
		height: 41,
		borderWidth: 0,
		borderStyle: 'solid',
		borderBottomWidth: 1,
		borderBottomColor: Colors.new.grey.palette(1),
	},
	focused: {
		borderBottomColor: Colors.primary,
	},
	input: {
		flex: 1,
		borderWidth: 0,
		padding: 0,
		fontFamily: Fonts.src,
		fontSize: 14,
		fontWeight: '400',
		lineHeight: 16,
		letterSpacing: 0,
		color: Colors.new.black.palette(3),
	},
})
