import ConnectHelper from 'coeur/helpers/connect';
import CoreSpriteBit from 'coeur/modules/bits/sprite';

import ImageBit from '../image';

import SpriteCartActiveImage from 'assets/img/sprite/cart-active.png';
import SpriteCommentActiveImage from 'assets/img/sprite/comment-active.png';
import SpriteJobActiveImage from 'assets/img/sprite/job-active.png';
import SpriteLoveActiveImage from 'assets/img/sprite/love-active.png';
import SpriteLoveActiveBigImage from 'assets/img/sprite/love-active-big.png';


export default ConnectHelper(
	class SpriteBit extends CoreSpriteBit({
		ImageBit,
		Sprites: {
			'cart-active': SpriteCartActiveImage,
			'comment-active': SpriteCommentActiveImage,
			'job-active': SpriteJobActiveImage,
			'love-active': SpriteLoveActiveImage,
			'love-active-big': SpriteLoveActiveBigImage,
		},
	}) {}
)
