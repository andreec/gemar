import ConnectHelper from 'coeur/helpers/connect';
import CoreSelectionBit from 'coeur/modules/bits/selection';

import Colors from 'coeur/constants/color';

import IconBit from '../icon';
import TextInputBit from '../text.input';


export default ConnectHelper(
	class SelectionBit extends CoreSelectionBit({
		IconBit,
		TextInputBit,
		iconDisabledColor: Colors.black.palette(1),
	}) {}
)
