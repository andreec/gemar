import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from '../../../constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		borderWidth: 0,
		borderColor: Colors.coal.palette(2),
		borderStyle: 'solid',
		borderTopWidth: StyleSheet.hairlineWidth,
		borderBottomWidth: StyleSheet.hairlineWidth,
	},
})
