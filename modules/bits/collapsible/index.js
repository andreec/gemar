import ConnectHelper from 'coeur/helpers/connect';
import CoreCollapsibleBit from 'coeur/modules/bits/collapsible';

import BoxBit from '../box';
import TouchableBit from '../touchable';

import Styles from './style'

export default ConnectHelper(
	class BoxLoadingBit extends CoreCollapsibleBit({
		Styles,
		BoxBit,
		TouchableBit,
	}) {}
)
