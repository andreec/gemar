import ConnectHelper from 'coeur/helpers/connect';
import CoreImageBit from 'coeur/modules/bits/image';

import Colors from 'coeur/constants/color';

import BoxBit from '../box';
import IconBit from '../icon';
import LoaderBit from '../loader';

import LogoGemarImage from 'assets/img/logo-gemar.png';
import ProfileImage from 'assets/img/profile.png';

const IMAGES = {
	'logo-gemar.png': LogoGemarImage,
	'profile.png': ProfileImage,
}


export default ConnectHelper(
	class ImageBit extends CoreImageBit({
		BoxBit,
		IconBit,
		LoaderBit,
		imageResolver: function(path) {
			return Promise.resolve(IMAGES[path] || undefined)
		},
		backgroundColor: Colors.grey.palette(1),
		indicatorColor: Colors.grey.primary,
		// indicatorColor = Colors.default.palette(5),
	}) {}
)
