import ConnectHelper from 'coeur/helpers/connect';
import CoreTouchableBit from 'coeur/modules/bits/touchable';

import BoxBit from '../box';


export default ConnectHelper(
	class TouchableBit extends CoreTouchableBit({
		BoxBit,
	}) {}
)
