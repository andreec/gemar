import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import CoreHeaderBit from 'coeur/modules/bits/header';

import Colors from 'coeur/constants/color';

import BoxBit from '../box';
import SourceSansBit from '../source.sans';

import ButtonPart from './_button'

import Styles from './style';

export {
	ButtonPart,
}


export default ConnectHelper(
	class HeaderBit extends CoreHeaderBit({
		Styles,
		BoxBit,
		TextBit: function(props) {
			return (
				<SourceSansBit type={ SourceSansBit.TYPES.NEW_HEADER_2 } { ...props } weight="medium" />
			)
		},
		ButtonPart,
		backgroundColor: Colors.white.primary,
	}) {}
)
