import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import {
	ButtonPart as CoreButtonPart,
} from 'coeur/modules/bits/header';

import SourceSansBit from '../../source.sans'
import IconBit from '../../icon'
import TouchableBit from '../../touchable'

import Styles from './style'

export default ConnectHelper(
	class ButtonPart extends CoreButtonPart({
		IconBit,
		TouchableBit,
	}) {
		static TYPES = {
			BACK: 'BACK_BUTTON',
			BLANK: 'BLANK',
			CLOSE: 'CLOSE_BUTTON',

			BELL: 'BELL_BUTTON',
			MENU: 'MENU_BUTTON',
			RESET: 'RESET_BUTTON',
			TEXT: 'TEXT',
			OK: 'OK',
		}

		getIconName(type) {
			switch(type) {
			case this.TYPES.BACK:
				return 'back';
			case this.TYPES.MENU:
				return 'menu';
			case this.TYPES.BELL:
				return 'notification';
			case this.TYPES.RESET:
				return 'refresh';
			case this.TYPES.CLOSE:
				return 'close';
			case this.TYPES.OK:
				return 'ok';
			default:
				return super.getIconName(type)
				// return 'tail-left';
			}
		}

		getSize(type) {
			switch(type) {
			case this.TYPES.MENU:
				return 24;
			case this.TYPES.BELL:
				return 24;
			case this.TYPES.OK:
				return 16;
			default:
				return 19;
			}
		}

		buttonRenderer() {
			switch(this.props.type) {

			case this.TYPES.TEXT:
				return (
					<SourceSansBit type={SourceSansBit.TYPES.PRIMARY_2} weight={'semibold'} style={ this.state.isLoading || !this.state.isActive ? Styles.textInactive : Styles.textActive }>
						{ this.state.data }
					</SourceSansBit>
				)
			case this.TYPES.BLANK:
				return false
			default:
				return (
					<IconBit
						name={ this.getIconName(this.props.type) }
						color={ this.props.data && this.props.data.color }
						size={ this.getSize(this.props.type) }
					/>
				)
			}
		}

	}
)
