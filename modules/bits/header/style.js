import StyleSheet from 'coeur/libs/style.sheet'
import Defaults from 'coeur/constants/default';
import Sizes from 'coeur/constants/size'

export default StyleSheet.create({
	header: {
		height: 65,
		paddingTop: 14 + (Defaults.PLATFORM === 'ios' ? 20 : 0), // the IOS bumper
		paddingBottom: 12,
		paddingLeft: 24,
		paddingRight: 24,
		width: Sizes.app.width,
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
	title: {
		marginTop: 0,
		marginBottom: 0,
	},
})
