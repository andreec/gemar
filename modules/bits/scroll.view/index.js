import ConnectHelper from 'coeur/helpers/connect';
import CoreScrollViewBit from 'coeur/modules/bits/scroll.view';

import BoxBit from '../box';


export default ConnectHelper(
	class ScrollSnapBit extends CoreScrollViewBit({
		BoxBit,
	}) {}
)
