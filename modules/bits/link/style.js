import StyleSheet from 'coeur/libs/style.sheet'
import Colors from 'coeur/constants/color'
// import Sizes from '../../../constants/size'

export default StyleSheet.create({
	container: {
		textDecorationLine: 'none',
	},
	underline: {
		color: Colors.new.yellow.palette(3),
		textDecorationLine: 'underline',
	},
})
