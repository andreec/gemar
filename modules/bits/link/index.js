import ConnectHelper from 'coeur/helpers/connect';
import CoreLinkBit from 'coeur/modules/bits/link';

import Styles from './style'


export default ConnectHelper(
	class LinkBit extends CoreLinkBit({
		Styles,
	}) {}
)
