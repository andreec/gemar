import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'
// import Fonts from 'coeur/constants/font'

export default StyleSheet.create({
	container: {
		height: 100,
		width: 100,
		backgroundColor: Colors.grey.palette(1),
	},
	border: {
		borderWidth: 1,
		borderColor: Colors.white.palette(1),
		borderStyle: 'solid',
	},
	close: {
		position: 'absolute',
		top: 0,
		right: 0,
		width: 24,
		height: 24,
		backgroundColor: Colors.white.primary,
	},
})
