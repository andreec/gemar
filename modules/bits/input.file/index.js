import ConnectHelper from 'coeur/helpers/connect';
import CoreInputFileBit from 'coeur/modules/bits/input.file';

import Colors from 'coeur/constants/color';

import BoxBit from '../box';
import IconBit from '../icon';
import ImageBit from '../image';
import LoaderBit from '../loader';
import TouchableBit from '../touchable';

import Styles from './style';

export default ConnectHelper(
	class InputFileBit extends CoreInputFileBit({
		Styles,
		BoxBit,
		IconBit,
		ImageBit,
		LoaderBit,
		TouchableBit,
		closeColor: Colors.red.primary,
		loaderColor: Colors.coal.palette(3),
		iconColor: Colors.coal.palette(3),
	}) {}
)
