import StyleSheet from 'coeur/libs/style.sheet';
import Colors from 'coeur/constants/color';
import Fonts from 'coeur/constants/font';

export default StyleSheet.create({
	container: {
		borderWidth: 0,
		borderStyle: 'solid',
		borderBottomWidth: 1,
		borderBottomColor: Colors.new.grey.palette(1),
	},
	focused: {
		borderBottomColor: Colors.primary,
	},
	input: {
		flex: 1,
		height: 75,
		minHeight: 75,
		maxHeight: 212,
		borderWidth: 0,
		paddingTop: 14,
		paddingLeft: 0,
		paddingRight: 0,
		paddingBottom: 13,
		fontFamily: Fonts.src,
		fontSize: 14,
		fontWeight: '400',
		lineHeight: 16,
		letterSpacing: 0,
		color: Colors.new.black.palette(3),
	},
})
