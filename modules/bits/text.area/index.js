import ConnectHelper from 'coeur/helpers/connect';
import CoreTextAreaBit from 'coeur/modules/bits/text.area';

import Colors from 'coeur/constants/color';

import BoxBit from '../box';

import Styles from './style';


export default ConnectHelper(
	class TextAreaBit extends CoreTextAreaBit({
		Styles,
		BoxBit,
		placeholderColor: Colors.new.black.palette(3),
	}) {}
)
