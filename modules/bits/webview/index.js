import ConnectHelper from 'coeur/helpers/connect';
import CoreWebviewBit from 'coeur/modules/bits/webview';

import Colors from 'coeur/constants/color';

import BoxBit from '../box'
import LoaderBit from '../loader'

import Styles from './style'


export default ConnectHelper(
	class WebviewBit extends CoreWebviewBit({
		Styles,
		BoxBit,
		LoaderBit,
		iconColor: Colors.coal.palette(2),
	}) {}
)
