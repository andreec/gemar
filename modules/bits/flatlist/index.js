import ConnectHelper from 'coeur/helpers/connect';
import CoreFlatlistBit from 'coeur/modules/bits/flatlist';

import BoxBit from '../box';

export default ConnectHelper(
	class BoxLoadingBit extends CoreFlatlistBit({
		BoxBit,
	}) {}
)
