import ConnectHelper from 'coeur/helpers/connect';
import CoreSwitchBit from 'coeur/modules/bits/switch';

import Colors from 'coeur/constants/color';


export default ConnectHelper(
	class SwitchBit extends CoreSwitchBit({
		trackColor: Colors.primary,
	}) {}
)
