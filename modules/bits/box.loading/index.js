import ConnectHelper from 'coeur/helpers/connect';
import CoreBoxLoadingBit from 'coeur/modules/bits/box.loading';

import BoxBit from '../box'

import Styles from './style'

export default ConnectHelper(
	class BoxLoadingBit extends CoreBoxLoadingBit({
		Styles,
		BoxBit,
	}) {}
)
