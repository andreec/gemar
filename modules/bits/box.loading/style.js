import StyleSheet from 'coeur/libs/style.sheet';
import Colors from 'coeur/constants/color';

export default StyleSheet.create({
	container: {
		backgroundColor: Colors.grey.palette(2),
		borderRadius: 4,
		overflow: 'hidden',
	},
})
