import ConnectHelper from 'coeur/helpers/connect';
import CoreBoxImageBit from 'coeur/modules/bits/box.image';

import BoxBit from '../box';
import ImageBit from '../image';

export default ConnectHelper(
	class BoxImageBit extends CoreBoxImageBit({
		BoxBit,
		ImageBit,
	}) {}
)
