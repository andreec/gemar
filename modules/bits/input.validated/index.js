import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import CoreInputValidatedBit from 'coeur/modules/bits/input.validated';

import Colors from 'coeur/constants/color';

import BoxBit from '../box';
import SourceSansBit from '../source.sans';
import IconBit from '../icon';
import SelectionBit from '../selection';
import TextAreaBit from '../text.area';
import TextInputBit from '../text.input';
import TouchableBit from '../touchable';

import Styles from './style';


export default ConnectHelper(
	class InputValidatedBit extends CoreInputValidatedBit({
		Styles,
		BoxBit,
		IconBit,
		SelectionBit,
		TextBit: SourceSansBit,
		TextAreaBit,
		TextInputBit,
		TouchableBit,
		checkmarkColor: Colors.green.palette(3),
	}) {
		titleRenderer(title, style) {
			return (
				<SourceSansBit type={SourceSansBit.TYPES.SUBHEADER_2} style={style}>{ title }</SourceSansBit>
			)
		}

		counterRenderer(counter, style) {
			return (
				<SourceSansBit type={SourceSansBit.TYPES.SUBHEADER_2} style={style}>
					{ counter }
				</SourceSansBit>
			)
		}

		descriptionRenderer(style) {
			return (
				<SourceSansBit type={SourceSansBit.TYPES.CAPTION_1} style={style}>{ this.props.description }</SourceSansBit>
			)
		}

		prefixTextRenderer(prefix, style) {
			return (
				<SourceSansBit type={SourceSansBit.TYPES.PARAGRAPH_1} style={style}>{ prefix }</SourceSansBit>
			)
		}

		postfixRenderer() {
			return super.postfixRenderer(16)
		}
	}
)
