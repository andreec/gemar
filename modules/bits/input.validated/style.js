import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	title: {
		color: Colors.black.palette(5),
	},
	colorBlack: {
		color: Colors.black.palette(4),
	},
	colorGrey: {
		color: Colors.black.palette(2),
	},
	colorPale: {
		color: Colors.grey.palette(2),
	},
	colorRed: {
		color: Colors.red.primary,
	},

	fit: {
		alignSelf: 'flex-start',
	},

	titleContainer: {
		justifyContent: 'space-between',
	},

	prefix: {
		marginRight: 8,
		color: Colors.default.palette(3),
	},

	note: {
		marginTop: 8,
		marginBottom: 8,
		marginLeft: 0,
		marginRight: 0,
		color: Colors.default.palette(5),
	},

	titleDisabled: {
		color: Colors.default.palette(5),
	},

	titleReadonly: {
	},

	counter: {
		color: Colors.default.palette(5),
	},

	inputInvalid: {
		color: Colors.default.palette(1),
	},

	inputValid: {
		color: Colors.default.palette(3),
	},

	postfix: {},
})
