import ConnectHelper from 'coeur/helpers/connect';
import CoreTextTypeBit from 'coeur/modules/bits/text.type';

import Colors from 'coeur/constants/color'
import Fonts from 'coeur/constants/font'

import TextBit from '../text'


export default ConnectHelper(
	class SourceSansBit extends CoreTextTypeBit({
		TextBit,
		fontFamily: Fonts.src,
		weights: [
			'extralight',
			'light',
			'normal',
			'medium',
			'semibold',
			'bold',
			'black',
		],
		TYPES: {

			PARAGRAPH_1: {
				fontSize: 12,
				lineHeight: 18,
				fontWeight: '500',
				letterSpacing: 0,
			},

			PARAGRAPH_2: {
				fontSize: 14,
				lineHeight: 18,
				letterSpacing: 0,
			},

			HEADER_2: {
				fontSize: 20,
				lineHeight: 25,
				fontWeight: '700',
				letterSpacing: 0,
			},

			HEADER_4: {
				fontSize: 16,
				lineHeight: 20,
				fontWeight: '600',
				letterSpacing: 0,
				color: Colors.black.primary,
			},

			SUBHEADER_1: {
				fontSize: 15,
				lineHeight: 18,
				fontWeight: '600',
				letterSpacing: .8,
				color: Colors.black.primary,
			},

			SUBHEADER_2: {
				fontSize: 12,
				lineHeight: 14,
				fontWeight: '600',
				letterSpacing: 1.2,
				color: Colors.black.primary,
			},

			SUBHEADER_3: {
				fontSize: 14,
				lineHeight: 18,
				fontWeight: '600',
				letterSpacing: .4,
				color: Colors.black.primary,
			},

			SUBHEADER_4: {
				fontSize: 12,
				lineHeight: 18,
				fontWeight: '600',
				letterSpacing: .4,
				color: Colors.black.primary,
			},

			SUBHEADER_5: {
				fontSize: 10,
				lineHeight: 13,
				fontWeight: '600',
				color: Colors.black.primary,
			},

			NOTE_1: {
				fontSize: 12,
				lineHeight: 16,
				letterSpacing: .4,
			},

			NOTE_2: {
				fontSize: 11,
				lineHeight: 14,
				letterSpacing: 0,
			},

			PRIMARY_1: {
				fontSize: 13,
				lineHeight: 18,
				fontWeight: '700',
				letterSpacing: 1.4,
				color: Colors.black.primary,
			},

			// NOT YET USED

			PARAGRAPH_3: {
				fontSize: 14,
				lineHeight: 21,
				letterSpacing: .3,
			},

			CAPTION_1: {
				fontSize: 12,
				lineHeight: 16,
				fontWeight: '500',
				letterSpacing: .4,
			},

			CAPTION_2: {
				fontSize: 11,
				lineHeight: 14,
				fontWeight: '500',
				letterSpacing: .4,
			},

			PRIMARY_2: {
				fontSize: 13,
				lineHeight: 16,
				fontWeight: '700',
				letterSpacing: 1.2,
				color: Colors.black.primary,
			},

			PRIMARY_3: {
				fontSize: 11,
				lineHeight: 13,
				fontWeight: '700',
				letterSpacing: 1,
				color: Colors.black.primary,
			},

			SECONDARY_1: {
				fontSize: 18,
				lineHeight: 23,
				fontWeight: '600',
				letterSpacing: .4,
				color: Colors.black.primary,
			},

			SECONDARY_2: {
				fontSize: 16,
				lineHeight: 19,
				fontWeight: '600',
				letterSpacing: .4,
				color: Colors.black.primary,
			},

			SECONDARY_3: {
				fontSize: 14,
				lineHeight: 20,
				fontWeight: '600',
				letterSpacing: .4,
				color: Colors.black.primary,
			},

			// NEWWWWWWWWWWWWW

			NEW_HEADER_1: {
				fontSize: 22,
				lineHeight: 26,
				letterSpacing: 0,
				fontWeight: '400',
				color: Colors.new.black.primary,
			},

			NEW_HEADER_2: {
				fontSize: 16,
				lineHeight: 19,
				letterSpacing: 0,
				fontWeight: '500',
				color: Colors.new.black.primary,
			},

			NEW_SUBHEADER_1: {
				fontSize: 14,
				lineHeight: 16,
				letterSpacing: 0,
				fontWeight: '400',
				color: Colors.new.black.palette(2),
			},

			NEW_SUBHEADER_1_ALT: {
				fontSize: 14,
				lineHeight: 16,
				letterSpacing: 0.4,
				fontWeight: '600',
				color: Colors.white.primary,
			},

			NEW_SUBHEADER_1_ALT_2: {
				fontSize: 14,
				lineHeight: 16,
				letterSpacing: 0,
				fontWeight: '500',
				color: Colors.new.black.primary,
			},

			NEW_SUBHEADER_2: {
				fontSize: 12,
				lineHeight: 14,
				letterSpacing: 0,
				fontWeight: '400',
				color: Colors.new.black.primary,
			},

			NEW_NOTE: {
				fontSize: 10,
				lineHeight: 12,
				letterSpacing: 0,
				fontWeight: '400',
				color: Colors.new.black.palette(2),
			},

			NEW_NOTE_2: {
				fontSize: 10,
				lineHeight: 14,
				letterSpacing: 0,
				fontWeight: '400',
				color: Colors.new.black.palette(3),
			},

			NEW_NOTE_3: {
				fontSize: 9,
				lineHeight: 20,
				letterSpacing: 0,
				fontWeight: '400',
				color: Colors.new.black.palette(3),
			},

			NEW_PARAGRAPH_1: {
				fontSize: 14,
				lineHeight: 18,
				fontWeight: '400',
				color: Colors.new.black.palette(2),
			},

			NEW_PARAGRAPH_2: {
				fontSize: 12,
				lineHeight: 18,
				letterSpacing: 0,
				fontWeight: '400',
				color: Colors.new.black.primary,
			},

			NEW_LIST: {
				fontSize: 14,
				lineHeight: 24,
				fontWeight: '500',
				color: Colors.new.black.primary,
			},
		},
	}) {}
)
