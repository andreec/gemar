import ConnectHelper from 'coeur/helpers/connect';
import CoreTextBit from 'coeur/modules/bits/text';

import Styles from './style'


export default ConnectHelper(
	class TextBit extends CoreTextBit({
		Styles,
	}) {}
)
