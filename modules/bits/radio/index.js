import ConnectHelper from 'coeur/helpers/connect';
import CoreRadioBit from 'coeur/modules/bits/radio';

import Colors from 'coeur/constants/color';

import IconBit from '../icon';
import TouchableBit from '../touchable';

import Styles from './style';


export default ConnectHelper(
	class RadioBit extends CoreRadioBit({
		Styles,
		IconBit,
		TouchableBit,
		iconColor: Colors.white.primary,
		iconSize: 16,
	}) {}
)
