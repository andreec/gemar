import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	active: {
		backgroundColor: Colors.primary,
	},
	inactive: {
		borderWidth: 1,
		borderColor: Colors.grey.palette(4),
		borderStyle: 'solid',
	},
	icon: {},
})
