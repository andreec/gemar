import StyleSheet from 'coeur/libs/style.sheet';
import Colors from 'coeur/constants/color';

export default StyleSheet.create({
	shadow: {
		backgroundColor: Colors.transparent,
	},
})
