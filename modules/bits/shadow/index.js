import ConnectHelper from 'coeur/helpers/connect';
import CoreShadowBit from 'coeur/modules/bits/shadow';

import Colors from 'coeur/constants/color';

import BoxBit from '../box'

import Styles from './style'


export default ConnectHelper(
	class ShadowBit extends CoreShadowBit({
		Styles,
		BoxBit,
		shadowColor: Colors.grey.palette(3),
	}) {}
)
