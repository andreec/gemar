import ConnectHelper from 'coeur/helpers/connect';
import CoreProgressBit from 'coeur/modules/bits/progress';

import Colors from 'coeur/constants/color';

import BoxBit from '../box';


export default ConnectHelper(
	class ProgressBit extends CoreProgressBit({
		BoxBit,
		backgroundColor: Colors.theme.palette(3),
		barColor: Colors.theme.palette(4),
	}) {}
)
