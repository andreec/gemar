import StyleSheet from 'coeur/libs/style.sheet';
import Colors from 'coeur/constants/color';

export default StyleSheet.create({
	container: {
		borderWidth: 0,
		borderStyle: 'solid',
		borderBottomWidth: 1,
		borderBottomColor: Colors.grey.palette(2),
	},
})
