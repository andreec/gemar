import ConnectHelper from 'coeur/helpers/connect';
import CoreLineBit from 'coeur/modules/bits/line';

import BoxBit from '../box'

import Styles from './style'

export default ConnectHelper(
	class LineBit extends CoreLineBit({
		Styles,
		BoxBit,
	}) {}
)
