import ConnectHelper from 'coeur/helpers/connect';
import CoreGridBit from 'coeur/modules/bits/grid';

import BoxBit from '../box'
import FlatListBit from '../flatlist'

// import Styles from './style'

export default ConnectHelper(
	class GridBit extends CoreGridBit({
		// Styles,
		BoxBit,
		FlatListBit,
	}) {}
)
