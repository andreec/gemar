import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from '../../../constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	active: {
		backgroundColor: Colors.new.yellow.palette(1),
	},
	inactive: {
		borderWidth: 1,
		borderColor: Colors.new.black.palette(1),
		borderStyle: 'solid',
	},
})
