import React from 'react'
import ConnectHelper from 'coeur/helpers/connect'
import CoreButtonProgressBit from 'coeur/modules/bits/button.progress'

import Colors from 'coeur/constants/color'

import BoxBit from '../../box'
import IconBit from '../../icon'
import SourceSansBit from '../../source.sans'
import LoaderBit from '../../loader'
import TouchableBit from '../../touchable'

import Styles from './style';


export default ConnectHelper(
	class ButtonBadge extends CoreButtonProgressBit({
		BoxBit,
		LoaderBit,
		TextBit: SourceSansBit,
		TouchableBit,
		THEMES: {
			PRIMARY: {
				NORMAL: [Colors.white.primary, Colors.new.yellow.palette(1), Colors.new.yellow.palette(1)],
				ACTIVE: [Colors.white.primary, Colors.new.yellow.palette(3), Colors.new.yellow.palette(3)],
				DISABLED: [Colors.new.grey.palette(3), Colors.new.grey.palette(1), Colors.new.grey.palette(1)],
				LOADING: [Colors.white.primary, Colors.new.yellow.palette(1), Colors.new.yellow.palette(1), Colors.new.yellow.palette(4)],
			},
			ACTIVATED: {
				NORMAL: [Colors.new.black.palette(1), Colors.transparent, Colors.new.yellow.palette(5)],
				ACTIVE: [Colors.new.black.palette(2), Colors.transparent, Colors.new.yellow.palette(3)],
				DISABLED: [Colors.new.grey.palette(3), Colors.transparent, Colors.new.grey.palette(1)],
				LOADING: [Colors.new.black.palette(1), Colors.transparent, Colors.new.yellow.palette(5), Colors.new.yellow.palette(4)],
			},
		},
		SIZES: {
			NORMAL: Styles.normal,
			COMPACT: Styles.compact,
		},
		containerStyle: Styles.container,
	}) {

		static propTypes(PropTypes) {
			return {
				...super.propTypes(PropTypes),
				icon: PropTypes.oneOf(IconBit.TYPES),
			}
		}

		titleRenderer(textStyle) {
			let textType = undefined

			switch(this.props.size) {
			case this.TYPES.SIZES.COMPACT:
				textType = SourceSansBit.TYPES.NEW_SUBHEADER_2
				break;
			case this.TYPES.SIZES.NORMAL:
			default:
				textType = SourceSansBit.TYPES.NEW_SUBHEADER_1_ALT_2
				break;
			}

			return (
				<SourceSansBit weight={this.props.weight} type={textType} style={[textStyle, { color: this.state.color }]}>{ this.props.title }</SourceSansBit>
			)
		}

		iconRenderer(iconStyle) {
			return this.props.icon && (
				<IconBit
					name={this.props.icon}
					size={24}
					color={this.state.color}
					style={iconStyle}
				/>
			)
		}

	}
)
