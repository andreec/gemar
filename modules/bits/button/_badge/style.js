import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
// import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		borderRadius: 3,
		borderWidth: 1,
		borderStyle: 'solid',
	},

	normal: {
		height: 28,
		paddingLeft: 24,
		paddingRight: 24,
	},

	compact: {
		height: 24,
		paddingLeft: 5,
		paddingRight: 5,
	},
})
