import React from 'react'
import ConnectHelper from 'coeur/helpers/connect'
import CoreButtonProgressBit from 'coeur/modules/bits/button.progress'

import Colors from 'coeur/constants/color'

import BoxBit from '../../box'
import IconBit from '../../icon'
import SourceSansBit from '../../source.sans'
import LoaderBit from '../../loader'
import TouchableBit from '../../touchable'

import Styles from './style';


export default ConnectHelper(
	class ButtonProgressRectangle extends CoreButtonProgressBit({
		BoxBit,
		LoaderBit,
		TextBit: SourceSansBit,
		TouchableBit,
		THEMES: {
			PRIMARY: {
				NORMAL: [Colors.white.primary, Colors.new.yellow.palette(1)],
				ACTIVE: [Colors.white.primary, Colors.new.yellow.palette(4)],
				DISABLED: [Colors.new.grey.palette(3), Colors.new.grey.palette(1)],
				LOADING: [Colors.white.primary, Colors.new.yellow.palette(1), undefined, Colors.new.yellow.palette(2)],
			},
			FACEBOOK: {
				NORMAL: [Colors.white.primary, Colors.new.blue.palette(1)],
				ACTIVE: [Colors.white.primary, Colors.new.blue.palette(3)],
				DISABLED: [Colors.white.primary, Colors.new.grey.palette(1)],
				LOADING: [Colors.white.primary, Colors.new.blue.palette(1), undefined, Colors.new.blue.palette(3)],
			},
			TWITTER: {
				NORMAL: [Colors.white.primary, Colors.new.blue.palette(2)],
				ACTIVE: [Colors.white.primary, Colors.new.blue.palette(4)],
				DISABLED: [Colors.white.primary, Colors.new.grey.palette(1)],
				LOADING: [Colors.white.primary, Colors.new.blue.palette(2), undefined, Colors.new.blue.palette(4)],
			},
		},
		SIZES: {
			NORMAL: Styles.normal,
		},
		containerStyle: Styles.container,
	}) {

		static propTypes(PropTypes) {
			return {
				...super.propTypes(PropTypes),
				icon: PropTypes.oneOf(IconBit.TYPES),
			}
		}

		titleRenderer(textStyle) {
			let textType = undefined

			switch(this.props.size) {
			case this.TYPES.SIZES.NORMAL:
			default:
				textType = SourceSansBit.TYPES.NEW_SUBHEADER_1
				break;
			}

			return (
				<SourceSansBit weight={this.props.weight || 'semibold'} type={textType} style={[textStyle, { color: this.state.color }]}>{ this.props.title }</SourceSansBit>
			)
		}

		iconRenderer(iconStyle) {
			return this.props.icon && (
				<IconBit
					name={this.props.icon}
					size={24}
					color={this.state.color}
					style={iconStyle}
				/>
			)
		}

	}
)
