import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
// import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		borderWidth: 0,
		borderStyle: 'solid',
	},

	normal: {
		height: 46,
		paddingLeft: 16,
		paddingRight: 16,
	},
})
