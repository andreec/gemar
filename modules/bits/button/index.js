import React from 'react';
import StatefulModel from 'coeur/models/stateful';
import ConnectHelper from 'coeur/helpers/connect';

import ButtonBadge from './_badge';
import ButtonGhost from './_ghost';
import ButtonProgress from './_progress';
import ButtonProgressRectangle from './_progress.rectangle';
import ButtonRectangle from './_rectangle';


const SHAPES = {
	BADGE: 'BADGE',
	GHOST: 'GHOST',
	PROGRESS: 'PROGRESS',
	PROGRESS_RECTANGLE: 'PROGRESS_RECTANGLE',
	RECTANGLE: 'RECTANGLE',
}


export default ConnectHelper(
	class ButtonBit extends StatefulModel {

		static TYPES = {
			...ButtonRectangle.TYPES,
			THEMES: {
				...ButtonBadge.TYPES.THEMES,
				...ButtonGhost.TYPES.THEMES,
				...ButtonProgress.TYPES.THEMES,
				...ButtonProgressRectangle.TYPES.THEMES,
				...ButtonRectangle.TYPES.THEMES,
			},
			SIZES: {
				...ButtonBadge.TYPES.SIZES,
				...ButtonGhost.TYPES.SIZES,
				...ButtonProgress.TYPES.SIZES,
				...ButtonProgressRectangle.TYPES.SIZES,
				...ButtonRectangle.TYPES.SIZES,
			},
			SHAPES,
		}

		static propTypes(PropTypes) {
			return {
				...ButtonRectangle.propTypes,
				theme: PropTypes.oneOf(this.TYPES.THEMES),
				size: PropTypes.oneOf(this.TYPES.SIZES),
				type: PropTypes.oneOf(this.TYPES.SHAPES),
			}
		}

		static defaultProps = {
			...ButtonRectangle.defaultProps,
			type: SHAPES.RECTANGLE,
		}

		render() {
			const {
				type,
				...props
			} = this.props

			switch(type) {
			case this.TYPES.SHAPES.BADGE:
				return (
					<ButtonBadge { ...props } />
				)
			case this.TYPES.SHAPES.GHOST:
				return (
					<ButtonGhost { ...props } />
				)
			case this.TYPES.SHAPES.PROGRESS:
				return (
					<ButtonProgress { ...props } />
				)
			case this.TYPES.SHAPES.PROGRESS_RECTANGLE:
				return (
					<ButtonProgressRectangle {...props} />
				)
			case this.TYPES.SHAPES.RECTANGLE:
			default:
				return (
					<ButtonRectangle { ...props } />
				)
			}
		}
	}
)
