import React from 'react'
import ConnectHelper from 'coeur/helpers/connect'
import CoreButtonBit from 'coeur/modules/bits/button'

import Colors from 'coeur/constants/color'

import BoxBit from '../../box'
import IconBit from '../../icon'
import SourceSansBit from '../../source.sans'
import LoaderBit from '../../loader'
import TouchableBit from '../../touchable'

import Styles from './style';


export default ConnectHelper(
	class ButtonGhost extends CoreButtonBit({
		BoxBit,
		LoaderBit,
		TextBit: SourceSansBit,
		TouchableBit,
		THEMES: {
			PRIMARY: {
				NORMAL: [Colors.new.black.palette(1), Colors.white.primary],
				ACTIVE: [Colors.new.black.palette(1), Colors.white.primary],
				DISABLED: [Colors.new.black.palette(1), Colors.white.primary],
				LOADING: [Colors.new.black.palette(1), Colors.white.primary],
			},
		},
		SIZES: {
			NORMAL: Styles.content,
		},
	}) {

		static propTypes(PropTypes) {
			return {
				...super.propTypes(PropTypes),
				icon: PropTypes.oneOf(IconBit.TYPES),
			}
		}

		titleRenderer(textStyle) {
			return (
				<SourceSansBit weight={this.props.weight} type={SourceSansBit.TYPES.NEW_SUBHEADER_1} style={[textStyle, { color: this.state.color }]}>{ this.props.title }</SourceSansBit>
			)
		}

		iconRenderer(iconStyle) {
			return this.props.icon && (
				<IconBit
					name={this.props.icon}
					size={24}
					color={this.state.color}
					style={iconStyle}
				/>
			)
		}
	}
)
