import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
// import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	content: {
		height: 38,
		paddingLeft: 0,
		paddingRight: 0,
	},
})
