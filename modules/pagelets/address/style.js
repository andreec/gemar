import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	page: {
		height: Sizes.app.height,
		width: Sizes.app.width,
	},
	container: {
		backgroundColor: Colors.solid.grey.palette(1),
	},

	header: {
		paddingTop: 24,
		paddingBottom: 24,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
	},

	additionalHeader: {
		paddingTop: 24,
		paddingBottom: 0,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
	},

	subheader: {
		marginTop: 8,
		color: Colors.black.palette(4),
	},

	empty: {
		color: Colors.black.palette(2),
	},
})
