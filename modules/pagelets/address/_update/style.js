import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		backgroundColor: Colors.solid.grey.palette(1),
	},

	contentContainer: {
		flexGrow: 1,
	},

	content: {
		width: Sizes.screen.width,
	},

	form: {
		paddingTop: 0,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
		paddingBottom: 24,
		backgroundColor: Colors.white.primary,
	},

	footer: {
		paddingTop: 16,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
		paddingBottom: 64,
	},
})
