import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';

import AddressManager from 'app/managers/address';

import TrackingHandler from 'app/handlers/tracking';

import UtilitiesContext from 'coeur/contexts/utilities';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import HeaderBit from 'modules/bits/header';
import PageBit from 'modules/bits/page';

import ListCheckboxLego from 'modules/legos/list.checkbox';
import ListContainerLego from 'modules/legos/list.container';
import ListHeaderLego from 'modules/legos/list.header';

import FormPagelet from 'modules/pagelets/form';

import Styles from './style';

import _ from 'lodash';


export default ConnectHelper(
	class AddressUpdatePagelet extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				id: PropTypes.id,
				me: PropTypes.object,
				address: PropTypes.object,
				title: PropTypes.string,
				header: PropTypes.node,

				// temporary props as we must consider this as a modal instead of a page
				onRequestClose: PropTypes.func.isRequired,
			}
		}

		static stateToProps(state, oP) {
			if(oP.id) {
				const address = state.addresses.get(parseInt(oP.id, 10))

				return {
					id: oP.id,
					me: state.me,
					address,
					title: oP.title || 'UPDATE ADDRESS',
					header: oP.header,
				}
			} else {
				return {
					me: state.me,
					// create empty address
					address: new AddressManager.record(),
					title: oP.title || 'ADD ADDRESS',
					header: oP.header,
				}
			}
		}

		static contexts = [
			UtilitiesContext,
		]

		header = {
			leftActions: [{
				type: HeaderBit.TYPES.CLOSE,
				onPress: this.onRequestClose,
			}],
			rightActions: [{
				type: HeaderBit.TYPES.TEXT,
				data: 'SAVE',
				isActive: this.canSave,
				onPress: this.onSaveAddress,
			}],
		}

		constructor(p) {
			super(p, {
				data: {
					title: p.address.title,
					pic: p.address.pic,
					phone: p.address.phone,
					address: p.address.address,
					district: p.address.district,
					postal: p.address.postal,
					metadata: p.address.metadata,
				},
				completeds: {
					detail: false,
					address: false,
				},
				invalidPostal: [],
				isSaving: false,
				isUsingProfileDetail: p.address.pic === p.me.fullName,
				isDefaultAddress: p.address.id && p.address.id === p.me.defaultAddressId,
			}, [
				'canSave',
				'invalidPostalLength',
				'checkInvalidPostal',
				'validateAndRetrieveArea',
				'onRequestClose',
				'onSaveAddress',
				'onToggleUsingProfileDetail',
				'onToggleSetAsDefault',
				'onUpdateTitle',
				'onCompleteTitle',
				'onUpdateDetail',
				'onCompleteDetail',
				'onUpdateAddress',
				'onError',
			])

			this.validateAndRetrieveArea = _.debounce(this.validateAndRetrieveArea, 300)
		}

		componentDidMount() {
			TrackingHandler.trackPageView('address.update')
		}

		invalidPostalLength() {
			return this.state.invalidPostal.length
		}

		checkInvalidPostal(value) {
			return !this.state.invalidPostal.length || this.state.invalidPostal.indexOf(value) === -1
		}

		canSave() {
			return Object.values(this.state.completeds).findIndex(status => !status) === -1 &&
			(
				this.state.data.title !== (this.props.address.title || '')
				|| this.state.data.pic !== (this.props.address.pic || '')
				|| this.state.data.phone !== (this.props.address.phone || '')
				|| this.state.data.address !== (this.props.address.address || '')
				|| this.state.data.district !== (this.props.address.district || '')
				|| this.state.data.postal !== (this.props.address.postal || '')
				|| this.state.isDefaultAddress !== (this.props.address.id ? this.props.address.id === this.props.me.defaultAddressId : false)
			)
			&& !this.state.isSaving
		}

		onRequestClose() {
			this.props.onRequestClose &&
			this.props.onRequestClose()
		}

		onToggleUsingProfileDetail() {
			this.setState({
				data: {
					...this.state.data,
					pic: !this.state.isUsingProfileDetail ? this.props.me.fullName : this.state.data.pic,
				},
				isUsingProfileDetail: !this.state.isUsingProfileDetail,
			})
		}

		onToggleSetAsDefault() {
			this.setState({
				isDefaultAddress: !this.state.isDefaultAddress,
			})
		}

		onUpdateTitle(isDone, {
			title,
		}) {

			this.setState({
				completeds: {
					...this.state.completeds,
					title: isDone,
				},
				data: {
					...this.state.data,
					title: title.value,
				},
			})
		}

		onCompleteTitle() {}

		onUpdateDetail(isDone, {
			pic,
			phone,
		}) {

			this.setState({
				completeds: {
					...this.state.completeds,
					detail: isDone,
				},
				data: {
					...this.state.data,
					pic: pic.value,
					phone: phone.value && ('+62' + phone.value) || '',
				},
			})
		}

		onCompleteDetail() {}

		onUpdateAddress(isDone, {
			address,
			postal,
			// district,
		}) {
			if(postal.value !== this.state.data.postal && postal.isValid) {
				this.validateAndRetrieveArea(postal.value)
			}

			this.setState({
				completeds: {
					...this.state.completeds,
					address: isDone,
				},
				data: {
					...this.state.data,
					address: address.value,
					// district: district.value,
					postal: postal.value,
				},
			})
		}

		validateAndRetrieveArea(postal) {
			AddressManager.validatePostalAndRetrieveAreas(postal).then(areas => {
				if(areas.length > 1) {
					this.props.utilities.menu.show({
						title: 'Select area',
						actions: areas.map(area => {
							return {
								title: area.area_name,
								type: this.props.utilities.menu.TYPES.ACTION.RADIO,
								onPress: this.saveDistrict.bind(this, area),
							}
						}),
						closeOnPress: true,
					})
				} else if(areas.length === 1) {
					this.saveDistrict(areas[0])
				} else {
					throw new Error('invalid postal')
				}
			}).catch(() => {
				this.props.utilities.notification.show({
					title: 'Oops…',
					message: 'Postal code invalid',
					timeout: 3000,
				})

				this.setState({
					invalidPostal: [
						...this.state.invalidPostal,
						postal,
					],
				})
			})
		}

		saveDistrict(area) {
			this.setState({
				data: {
					...this.state.data,
					district: `${area.suburb_name}/${area.area_name}`,
					metadata: _.mapKeys(area, (value, key) => {
						return _.camelCase(key)
					}),
				},
			})
		}

		onSaveAddress() {
			// TODO: then must call onRequestClose
			this.setState({
				isSaving: true,
			}, () => {
				const data = {
					...this.state.data,
					setAsDefault: this.state.isDefaultAddress,
				}

				if(this.props.id) {
					AddressManager.updateAddress(this.props.id, data).then(this.props.onRequestClose).catch(this.onError)
				} else {
					AddressManager.addAddress(data).then(this.props.onRequestClose).catch(this.onError)
				}
			})
		}

		onError() {
			this.props.utilities.notification.show({
				title: 'Oops',
				message: 'Something went wrong, please try again later',
			})

			this.setState({
				isSaving: false,
			})
		}

		render() {
			return null
			return (
				<PageBit
					header={<HeaderBit { ...this.header } title={this.props.title} updater={ this.canSave() } />}
					style={ Styles.container }
				>
					{ this.props.header || (
						<FormPagelet ignoreNull
							updater={ this.state.isUsingProfileDetail }
							config={[{
								id: 'title',
								type: FormPagelet.TYPES.TITLE,
								title: 'ADDRESS NAME',
								placeholder: 'e.g. Home address',
								testOnMount: true,
								value: this.state.data.title,
							}]}
							onUpdate={ this.onUpdateTitle }
							onSubmit={ this.onCompleteTitle }
							style={Styles.form}
						/>
					) }
					<ListHeaderLego title="RECIPIENT DETAILS" />
					<ListCheckboxLego
						title="Use my profile details"
						isActive={ this.state.isUsingProfileDetail }
						onPress={ this.onToggleUsingProfileDetail }
					/>
					<FormPagelet
						updater={ this.state.isUsingProfileDetail }
						config={[{
							id: 'pic',
							key: this.state.isUsingProfileDetail,
							isRequired: true,
							// updater: this.state.isUsingProfileDetail,
							type: FormPagelet.TYPES.NAME,
							title: 'FULL NAME',
							disabled: this.state.isUsingProfileDetail,
							testOnMount: true,
							value: this.state.data.pic,
						}, {
							id: 'phone',
							type: FormPagelet.TYPES.PHONE,
							isRequired: true,
							title: 'PHONE NUMBER',
							value: this.state.data.phone,
							testOnMount: true,
							description: 'We will only contact you by phone if there is a problem with your order',
						}]}
						onUpdate={ this.onUpdateDetail }
						onSubmit={ this.onCompleteDetail }
						style={Styles.form}
					/>
					<ListHeaderLego title="DELIVERY ADDRESS" />
					<FormPagelet
						updater={ this.state.data.district || this.invalidPostalLength() }
						config={[{
							id: 'address',
							isRequired: true,
							type: FormPagelet.TYPES.ADDRESS,
							testOnMount: true,
							value: this.state.data.address,
						}, {
							id: 'postal',
							isRequired: true,
							updater: this.invalidPostalLength,
							type: FormPagelet.TYPES.POSTAL,
							validator: this.checkInvalidPostal,
							description: 'Edit your postal code if autofill shows incorrect code',
							testOnMount: true,
							value: this.state.data.postal,
						}, {
							id: 'district',
							disabled: true,
							isRequired: true,
							placeholder: 'autofill',
							type: FormPagelet.TYPES.TITLE,
							title: 'DISTRICT (KECAMATAN/KELURAHAN)',
							testOnMount: true,
							value: this.state.data.district,
						}]}
						onUpdate={ this.onUpdateAddress }
						onSubmit={ this.onSaveAddress }
						style={Styles.form}
					/>
					<ListContainerLego
						component={ListCheckboxLego}
						data={[{
							isFirst: true,
							title: 'Set as preferred shipping address',
							isActive: this.state.isDefaultAddress,
							onPress: this.onToggleSetAsDefault,
						}]}
					/>
					<BoxBit unflex style={Styles.footer}>
						<ButtonBit
							type={ ButtonBit.TYPES.SHAPES.PROGRESS }
							state={ this.canSave() ? ButtonBit.TYPES.STATES.NORMAL : this.state.isSaving && ButtonBit.TYPES.STATES.LOADING || ButtonBit.TYPES.STATES.DISABLED }
							title="SAVE ADDRESS"
							onPress={ this.onSaveAddress }
						/>
					</BoxBit>
				</PageBit>
			)
		}
	}
)
