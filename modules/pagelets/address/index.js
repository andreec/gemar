import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import ConnectedStatefulModel from 'coeur/models/stateful';

import MeManager from 'app/managers/me';

import TrackingHandler from 'app/handlers/tracking';

import PageContext from 'coeur/contexts/page';
import UtilitiesContext from 'coeur/contexts/utilities';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
import SourceSansBit from 'modules/bits/source.sans';
import HeaderBit from 'modules/bits/header';
import PageBit from 'modules/bits/page';

import CardAddressComponent from 'modules/components/card.address';

import UpdatePart from './_update';

import Styles from './style';


export default ConnectHelper(
	class AddressPagelet extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string,
				selectionMode: PropTypes.bool,
				selectedAddressId: PropTypes.id,
				onRequestClose: PropTypes.func,
				onSubmit: PropTypes.func,
				me: PropTypes.object,
			}
		}

		static stateToProps(state, oP) {
			return {
				title: oP.title || 'ADDRESS BOOK',
				selectionMode: oP.selectionMode,
				selectedAddressId: oP.selectedAddressId,
				onRequestClose: oP.onRequestClose,
				onSubmit: oP.onSubmit,
				address: state.me.address,
				me: state.me,
			}
		}

		static contexts = [
			PageContext,
			UtilitiesContext,
		]

		header = {
			leftActions: [{
				type: HeaderBit.TYPES.BACK,
				onPress: this.onRequestClose,
			}],
			rightActions: [this.props.selectionMode ? {
				type: HeaderBit.TYPES.TEXT,
				data: 'DONE',
				isActive: true,
				onPress: this.onSubmit,
			} : {
				type: HeaderBit.TYPES.BLANK,
			}],
		}

		constructor(p) {
			super(p, {
				selectedId: p.selectedAddressId || -1,
				activeIndex: 0,
			}, [
				'onRequestClose',
				'onSubmit',
				'onAddressPress',
				'onAddAddress',
				'onModalRequestClose',
				'addressCardRenderer',
			])
		}

		componentWillMount() {
			console.log(this.props);
			// if(!this.props.address.length && this.props.selectionMode) {
			// 	this.onAddAddress(null, true)
			// }
		}

		// componentDidMount() {
			// TrackingHandler.trackPageView('address')
			// this.refresh()
		// }

		onRequestClose() {
			this.props.onRequestClose &&
			this.props.onRequestClose()
		}

		onSubmit() {
			this.props.onSubmit &&
			this.props.onSubmit(this.state.selectedId)
		}

		onModalRequestClose(address) {
			if(address && this.props.selectionMode) {
				this.setState({
					selectedId: address.id,
				}, !this.props.me.addressIds.length && this.onSubmit || undefined)
			}

			this.props.utilities.alert.hide()

			this.refresh()
		}

		onAddressPress(id) {
			if(this.props.selectionMode) {
				this.setState({
					selectedId: id,
				})
			} else {
				this.props.utilities.alert.modal({
					component: (
						<UpdatePart
							id={ id }
							onRequestClose={ this.onModalRequestClose }
						/>
					),
				})
			}
		}

		onAddAddress(e, isEmpty) {
			this.log(e, isEmpty)
			this.props.utilities.alert.modal({
				component: (
					<UpdatePart
						onRequestClose={ this.onModalRequestClose }
						header={ isEmpty && this.headerRenderer() }
						title={ isEmpty && 'SHIPPING ADDRESS' }
					/>
				),
			})
		}

		refresh() {
			MeManager.authenticate(this.props.me.token).catch(() => {
				this.props.utilities.notification.show({
					title: 'Oops…',
					message: 'Looks like you\'ve been logged out of your session. Try logging in.',
				})

				this.props.page.navigator.top()

				MeManager.logout()
			})
		}

		headerRenderer() {
			return (
				<BoxBit unflex style={Styles.additionalHeader}>
					<SourceSansBit type={SourceSansBit.TYPES.HEADER_4}>
						You currently have no shipping addresses saved
					</SourceSansBit>
					<SourceSansBit type={SourceSansBit.TYPES.PARAGRAPH_2} style={Styles.subheader}>
						Add an address for a faster checkout experience
					</SourceSansBit>
				</BoxBit>
			)
		}

		addressCardRenderer({id, ...aId}, i) {
			return (
				<CardAddressComponent
					id={id}
					key={id}
					divider={ i !== 0 }
					isActive={ this.props.selectionMode ? id === this.state.selectedId : undefined }
					isDefaultAddress={ id === this.props.me.defaultAddressId }
					selectionMode={ this.props.selectionMode }
					// onPress={ this.onAddressPress }
				/>
			)
		}


		render() {
			return (
				<PageBit
					header={ <HeaderBit { ...this.header } title={ this.props.title } /> }
					contentContainerStyle={ Styles.container }
					style={Styles.page}
				>
					<BoxBit unflex style={Styles.header}>
						<ButtonBit
							title="ADD NEW ADDRESS"
							theme={ButtonBit.TYPES.THEMES.SECONDARY}
							onPress={ this.onAddAddress }
						/>
					</BoxBit>
					{ this.props.address ? this.props.address.map(this.addressCardRenderer) : (
						<BoxBit centering>
							<SourceSansBit type={SourceSansBit.TYPES.SUBHEADER_1} weight="normal" style={Styles.empty}>No address found</SourceSansBit>
						</BoxBit>
					) }
				</PageBit>
			)
		}
	}
)
