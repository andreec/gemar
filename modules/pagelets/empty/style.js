import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		background: Colors.white.primary,
	},
	content: {
		marginBottom: 24,
	},
	image: {
		width: 240,
		height: 240,
		borderRadius: 120,
		overflow: 'hidden',
	},
	title: {
		textAlign: 'center',
		alignSelf: 'stretch',
	},
	desc: {
		textAlign: 'center',
		paddingTop: 16,
		paddingBottom: 24,
	},
	padder: {
		flexGrow: .2,
	},
})
