import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import BoxBit from 'modules/bits/box';
import SourceSansBit from 'modules/bits/source.sans';
import ImageBit from 'modules/bits/image';

import Styles from './style';


export default ConnectHelper(
	class EmptyPagelet extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				source: PropTypes.image,
				title: PropTypes.string,
				description: PropTypes.string,
				children: PropTypes.node,
				style: PropTypes.style,
			}
		}

		render() {
			return(
				<BoxBit type={BoxBit.TYPES.THICK} style={[Styles.container, this.props.style]}>
					<BoxBit />
					{ this.props.source && (
						<BoxBit unflex centering style={Styles.content}>
							<ImageBit source={ this.props.source } broken={ !this.props.source } style={Styles.image}/>
						</BoxBit>
					) }
					<BoxBit style={Styles.padder} />
					<BoxBit unflex centering>
						<SourceSansBit type={SourceSansBit.TYPES.HEADER_4} style={Styles.title}>{ this.props.title }</SourceSansBit>
						<SourceSansBit type={SourceSansBit.TYPES.PARAGRAPH_2} style={Styles.desc}>{ this.props.description }</SourceSansBit>
						{ this.props.children }
					</BoxBit>
					<BoxBit />
				</BoxBit>
			)
		}
	}
)
