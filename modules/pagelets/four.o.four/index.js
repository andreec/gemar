import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import TrackingHandler from 'app/handlers/tracking';

import PageContext from 'coeur/contexts/page';

import ButtonBit from 'modules/bits/button';

import EmptyPagelet from '../empty';


export default ConnectHelper(
	class FourOFourPagelet extends StatefulModel {

		static contexts = [
			PageContext,
		]

		constructor(p) {
			super(p, {}, [
				'onNavigateToHome',
				'onNavigateToBack',
			])
		}

		componentDidMount() {
			TrackingHandler.trackPageView('404')
		}

		onNavigateToHome() {
			this.props.page.navigator.top()
		}

		onNavigateToBack() {
			this.props.page.navigator.back()
		}

		render() {
			return(
				<EmptyPagelet
					source="//app/404.jpg"
					title="Uh oh, you’re lost…"
					description="But you’re looking for great styles in all the right places. Let’s find something right for you."
				>
					<ButtonBit
						title="BACK TO HOME"
						onPress={this.onNavigateToHome}
					/>
					<ButtonBit
						type={ButtonBit.TYPES.SHAPES.GHOST}
						theme={ButtonBit.TYPES.THEMES.BLACK}
						title="or Back to previous page"
						onPress={this.onNavigateToBack}
					/>
				</EmptyPagelet>
			)
		}
	}
)
