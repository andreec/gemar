import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import {
	ImagePart as CoreImagePart,
} from 'coeur/modules/pagelets/alert'

import BoxBit from 'modules/bits/box';
import SourceSansBit from 'modules/bits/source.sans';
import ImageBit from 'modules/bits/image';

import ButtonPart from '../__button';

import Styles from './style'


export default ConnectHelper(
	class ImagePart extends CoreImagePart({
		Styles,
		BoxBit,
		ImageBit,
		TextBit: SourceSansBit,
		ButtonPart,
	}) {
		titleRenderer(style) {
			return (
				<SourceSansBit type={SourceSansBit.TYPES.subheading2} style={style}>
					{ this.props.title }
				</SourceSansBit>
			)
		}

		messageRenderer(style) {
			return (
				<SourceSansBit type={SourceSansBit.TYPES.body1} style={style}>
					{ this.props.message }
				</SourceSansBit>
			)
		}
	}
)
