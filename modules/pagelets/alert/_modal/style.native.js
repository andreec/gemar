import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		height: Sizes.screen.height,
		backgroundColor: Colors.white.primary,
	},
})
