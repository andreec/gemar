import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import {
	ModalPart as CoreModalPart,
} from 'coeur/modules/pagelets/alert'

import BoxBit from 'modules/bits/box';
import SourceSansBit from 'modules/bits/source.sans';
import IconBit from 'modules/bits/icon';
import ImageBit from 'modules/bits/image';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style'


export default ConnectHelper(
	class ModalPart extends CoreModalPart({
		Styles,
		BoxBit,
		IconBit,
		ImageBit,
		TextBit: SourceSansBit,
		TouchableBit,
	}) {
		titleRenderer(style) {
			return (
				<SourceSansBit type={SourceSansBit.TYPES.HERO} style={style}>{ this.props.title }</SourceSansBit>
			)
		}

		descriptionRenderer(style) {
			return (
				<SourceSansBit type={SourceSansBit.TYPES.PARAGRAPH_3} style={style}>
					{ this.props.description }
				</SourceSansBit>
			)
		}
	}
)
