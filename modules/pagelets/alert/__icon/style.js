import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	icon: {
		width: 44,
		height: 44,
		borderRadius: 22,
		backgroundColor: Colors.red.palette(2),
	},
})
