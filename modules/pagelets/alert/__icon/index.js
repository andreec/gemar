import ConnectHelper from 'coeur/helpers/connect';
import {
	IconPart as CoreIconPart,
} from 'coeur/modules/pagelets/alert'

import BoxBit from 'modules/bits/box';

import Styles from './style'


export default ConnectHelper(
	class IconPart extends CoreIconPart({
		Styles,
		BoxBit,
	}) {}
)
