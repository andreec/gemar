import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import {
	RadioPart as CoreRadioPart,
} from 'coeur/modules/pagelets/alert'

import RadioBit from 'modules/bits/radio';
import SourceSansBit from 'modules/bits/source.sans';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style'


export default ConnectHelper(
	class RadioPart extends CoreRadioPart({
		Styles,
		RadioBit,
		TextBit: SourceSansBit,
		TouchableBit,
	}) {
		titleRenderer(style) {
			return (
				<SourceSansBit type={SourceSansBit.TYPES.CAPTION_1} style={style}>{ this.props.title }</SourceSansBit>
			)
		}
	}
)
