import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	text: {
		marginLeft: 6,
		color: Colors.grey.palette(5),
	},
})
