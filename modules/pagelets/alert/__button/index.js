import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import {
	ButtonPart as CoreButtonPart,
} from 'coeur/modules/pagelets/alert'

import SourceSansBit from 'modules/bits/source.sans';
import TouchableBit from 'modules/bits/touchable';

import Styles from './style'


export default ConnectHelper(
	class ButtonPart extends CoreButtonPart({
		Styles,
		TextBit: SourceSansBit,
		TouchableBit,
	}) {
		titleRenderer(title, style) {
			return (
				<SourceSansBit type={SourceSansBit.TYPES.PRIMARY_3} style={style}>
					{ title }
				</SourceSansBit>
			)
		}
	}
)
