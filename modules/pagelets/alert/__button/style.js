import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	ok: {
		color: Colors.blue.palette(3),
	},
	cancel: {
		color: Colors.red.palette(3),
	},
	info: {
		color: Colors.grey.palette(3),
	},
})
