import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import {
	DefaultPart as CoreDefaultPart,
} from 'coeur/modules/pagelets/alert'

import BoxBit from 'modules/bits/box';
import SourceSansBit from 'modules/bits/source.sans';

import ButtonPart from '../__button';
import IconPart from '../__icon';
import RadioPart from '../__radio';

import Styles from './style'


export default ConnectHelper(
	class DefaultPart extends CoreDefaultPart({
		Styles,
		BoxBit,
		TextBit: SourceSansBit,
		ButtonPart,
		IconPart,
		RadioPart,
	}) {
		titleRenderer(style) {
			return (
				<SourceSansBit type={SourceSansBit.TYPES.SUBHEADER_1} style={style}>
					{ this.props.title }
				</SourceSansBit>
			)
		}

		messageRenderer(style) {
			return (
				<SourceSansBit type={SourceSansBit.TYPES.PARAGRAPH_3} style={style}>
					{ this.props.message }
				</SourceSansBit>
			)
		}

		descriptionRenderer(style) {
			return (
				<SourceSansBit type={SourceSansBit.TYPES.CAPTION_2} style={style}>
					{ this.props.description }
				</SourceSansBit>
			)
		}
	}
)
