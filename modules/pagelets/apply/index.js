import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import ConnectedStatefulModel from 'coeur/models/connected.stateful';

import TrackingHandler from 'app/handlers/tracking';

import PageContext from 'coeur/contexts/page';
import UtilitiesContext from 'coeur/contexts/utilities';

import FormatHelper from 'coeur/helpers/format';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box';
import ButtonBit from 'modules/bits/button';
// import HeaderBit from 'modules/bits/header';
import IconBit from 'modules/bits/icon';
import InputValidatedBit from 'modules/bits/input.validated';
// import PageBit from 'modules/bits/page';
import SourceSansBit from 'modules/bits/source.sans';
import TouchableBit from 'modules/bits/touchable';

// import ListCheckboxLego from 'modules/legos/list.checkbox';
// import ListContainerLego from 'modules/legos/list.container';
// import ListHeaderLego from 'modules/legos/list.header';

// import FormPagelet from 'modules/pagelets/form';

import Styles from './style';

import {
	// eslint-disable-next-line import/named
	DocumentPicker,
	// eslint-disable-next-line import/named
	DocumentPickerUtil,
} from 'react-native-document-picker';


export default ConnectHelper(
	class ApplyPagelet extends ConnectedStatefulModel {

		static propTypes(PropTypes) {
			return {
				position: PropTypes.string,
				company: PropTypes.string,
				minSallary: PropTypes.number,
				maxSallary: PropTypes.number,
				availability: PropTypes.number,
				applied: PropTypes.number,

				onRequestClose: PropTypes.func,
			}
		}

		static contexts = [
			PageContext,
			UtilitiesContext,
		]

		constructor(p) {
			super(p, {
				isValid: false,
				isSending: false,
				attachmentUri: undefined,
				attachmentType: undefined,
				attachmentName: undefined,
				attachmentSize: undefined,
			}, [
				'onChangeCV',
				'onAddAttachment',
				'onApply',
			])

			this.cv = ''
			this.attachment = undefined
		}

		componentDidMount() {
			TrackingHandler.trackPageView('address.update')
		}

		onChangeCV(value) {
			this.cv = value

			if(value.length && this.state.attachmentUri && !this.state.isValid) {
				this.setState({
					isValid: true,
				})
			} else if(!(value.length && this.state.attachmentUri) && this.state.isValid) {
				this.setState({
					isValid: false,
				})
			}
		}

		onAddAttachment() {
			DocumentPicker.show({
				filetype: [DocumentPickerUtil.pdf()],
			}, (error, res) => {
				if(error) {
					this.warn(error)
				} else {
					this.setState({
						isValid: this.cv.length ? true : false,
						attachmentUri: res.uri,
						attachmentType: res.type,
						attachmentName: res.fileName,
						attachmentSize: res.fileSize,
					})
				}
			});
		}

		onApply() {
			// TODO
			if(this.state.isValid) {
				this.setState({
					isSending: true,
				}, () => {
					// TODO
					this.setTimeout(() => {
						Promise.resolve()
							.then(() => {

								this.props.utilities.notification.show({
									title: 'CV sent',
									message: 'Please check your application page for more info',
									type: this.props.utilities.notification.TYPES.SUCCESS,
									timeout: 5000,
								})

								this.props.onRequestClose &&
								this.props.onRequestClose()
							}).catch(err => {
								this.warn(err)

								// TODO
							}).finally(() => {
								this.setState({
									isSending: false,
								})
							})
					}, 2000)
				})
			}
		}

		render() {
			return (
				<React.Fragment>
					<BoxBit />
					<BoxBit unflex style={Styles.container}>
						<TouchableBit unflex style={Styles.closer} onPress={ this.props.onRequestClose }>
							<IconBit
								name="close"
								size={26}
								color={ Colors.new.grey.palette(6) }
							/>
						</TouchableBit>
						<BoxBit unflex type={BoxBit.TYPES.THICK} style={Styles.content}>
							<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_1_ALT_2} style={Styles.title}>Applying to:</SourceSansBit>
							<SourceSansBit type={SourceSansBit.TYPES.NEW_HEADER_2}>{this.props.position}</SourceSansBit>
							<SourceSansBit italic weight="light" type={SourceSansBit.TYPES.NEW_SUBHEADER_1}>{this.props.company}</SourceSansBit>
							<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_2} weight="semibold" style={Styles.sallary}>IDR { FormatHelper.currencyFormat(this.props.minSallary) } - { FormatHelper.currencyFormat(this.props.maxSallary) }</SourceSansBit>
							<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_2} weight="semibold">{ this.props.applied } / { this.props.availability } people applied</SourceSansBit>
						</BoxBit>
						<BoxBit unflex type={BoxBit.TYPES.THICK} style={Styles.inputs}>
							<InputValidatedBit
								type={InputValidatedBit.TYPES.TEXTAREA}
								title="Application Letter *"
								placeholder="Write your application letter here…"
								onChange={this.onChangeCV}
								style={Styles.input}
							/>
							<BoxBit row unflex style={Styles.cv}>
								<BoxBit unflex>
									<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_1_ALT_2}>{this.state.attachmentName ? 'Attachment:' : 'CV or Portfolio'}</SourceSansBit>
									{ this.state.attachmentName ? (
										<SourceSansBit ellipsis type={SourceSansBit.TYPES.NEW_SUBHEADER_2}>{ this.state.attachmentName }</SourceSansBit>
									) : (
										<SourceSansBit type={SourceSansBit.TYPES.NEW_NOTE} italic>*in PDF format</SourceSansBit>
									) }
								</BoxBit>
								<ButtonBit
									type={ButtonBit.TYPES.SHAPES.BADGE}
									// theme={ButtonBit.TYPES.THEMES.ACTIVATED}
									title="Attach File"
									onPress={this.onAddAttachment}
								/>
							</BoxBit>
						</BoxBit>
						<BoxBit unflex type={BoxBit.TYPES.THICK} style={Styles.footer}>
							<ButtonBit
								type={ButtonBit.TYPES.SHAPES.PROGRESS}
								state={ this.state.isSending ? ButtonBit.TYPES.STATES.LOADING : this.state.isValid && ButtonBit.TYPES.STATES.NORMAL || ButtonBit.TYPES.STATES.DISABLED }
								title="Send Application"
								onPress={this.onApply}
							/>
						</BoxBit>
					</BoxBit>
				</React.Fragment>
			)
		}
	}
)
