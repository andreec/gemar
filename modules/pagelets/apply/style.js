import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		backgroundColor: Colors.new.white.primary,
		width: Sizes.app.width,
	},

	closer: {
		padding: 12,
		borderBottomWidth: 1,
		borderBottomColor: Colors.new.grey.palette(1),
	},

	content: {
		paddingTop: 16,
		paddingBottom: 32,
		// borderBottomWidth: 1,
		// borderBottomColor: Colors.new.grey.palette(1),
	},

	title: {
		color: Colors.new.black.palette(6),
		marginBottom: 11,
	},

	sallary: {
		marginTop: 16,
	},

	input: {
		marginTop: 22,
		marginBottom: 22,
	},

	cv: {
		marginBottom: 64,
		justifyContent: 'space-between',
		alignItems: 'center',
	},

	inputs: {
		backgroundColor: Colors.solid.grey.palette(1),
	},

	footer: {
		paddingTop: 11,
		paddingBottom: 11,
	}
})
