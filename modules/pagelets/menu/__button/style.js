import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	selected: {
		backgroundColor: Colors.grey.palette(1),
	},

	circleContainer: {
		width: 32,
		height: 32,
		marginRight: 12,
		borderWidth: 1,
		borderColor: Colors.grey.palette(3),
		borderStyle: 'solid',
		borderRadius: 16,
	},

	circleContainerActive: {
		backgroundColor: Colors.black.primary,
	},

	colorActive: {
		color: Colors.black.primary,
	},
	colorInactive: {
		color: Colors.black.palette(4),
	},
	colorBright: {
		color: Colors.white.primary,
	},
	colorDark: {
		color: Colors.grey.palette(4),
	},
	colorRed: {
		color: Colors.red.primary,
	},
	colorBlue: {
		color: Colors.blue.primary,
	},
})
