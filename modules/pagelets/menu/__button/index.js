import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import {
	ButtonPart as CoreButtonPart,
} from 'coeur/modules/pagelets/menu';

import BoxBit from 'modules/bits/box'
import SourceSansBit from 'modules/bits/source.sans'
import IconBit from 'modules/bits/icon'
import ImageBit from 'modules/bits/image'
import LineBit from 'modules/bits/line'
import RadioBit from 'modules/bits/radio'
import SwitchBit from 'modules/bits/switch'
import TouchableBit from 'modules/bits/touchable'

import Styles from './style'


export default ConnectHelper(
	class ButtonPart extends CoreButtonPart({
		Styles,
		BoxBit,
		SourceSansBit,
		IconBit,
		ImageBit,
		LineBit,
		RadioBit,
		SwitchBit,
		TextBit: SourceSansBit,
		TouchableBit,
	}) {
		iconTextRenderer(title, style) {
			return (
				<SourceSansBit type={SourceSansBit.TYPES.PRIMARY_3} style={style}>{ title }</SourceSansBit>
			)
		}

		titleTextRenderer(title, style) {
			return (
				<SourceSansBit type={this.props.onTitlePress ? SourceSansBit.TYPES.SECONDARY_3 : SourceSansBit.TYPES.PARAGRAPH_3} style={style}>
					{ title }
				</SourceSansBit>
			)
		}

		typeTextRenderer(title, style) {
			return (
				<SourceSansBit type={SourceSansBit.TYPES.PARAGRAPH_3} style={style}>{ title }</SourceSansBit>
			)
		}
	}
)
