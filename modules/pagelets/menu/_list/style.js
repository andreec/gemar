import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		width: Sizes.screen.width,
		backgroundColor: Colors.white.primary,
		paddingBottom: Sizes.margin.default,
		// marginBottom: 100,
		height: 'auto',
	},
})
