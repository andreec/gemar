import ConnectHelper from 'coeur/helpers/connect';
import {
	SwitchPart as CoreSwitchPart,
} from 'coeur/modules/pagelets/menu';

import ButtonPart from '../__button';


export default ConnectHelper(
	class SwitchPart extends CoreSwitchPart({
		ButtonPart,
	}) {}
)
