import React from 'react'
import ConnectHelper from 'coeur/helpers/connect';
import {
	NotifierPart as CoreNotifierPart,
} from 'coeur/modules/pagelets/notification';

import Colors from 'coeur/constants/color';

import BoxBit from 'modules/bits/box'
import SourceSansBit from 'modules/bits/source.sans'
import IconBit from 'modules/bits/icon'
import TouchableBit from 'modules/bits/touchable'

import Styles from './style'


export default ConnectHelper(
	class NotifierPart extends CoreNotifierPart({
		Styles,
		BoxBit,
		IconBit,
		TextBit: SourceSansBit,
		TouchableBit,
		iconColor: Colors.white.primary,
	}) {

		titleRenderer(style) {
			return (
				<SourceSansBit type={SourceSansBit.TYPES.NEW_HEADER_2} style={style}>
					{ this.props.title }
				</SourceSansBit>
			)
		}

		messageRenderer(style) {
			return (
				<SourceSansBit type={SourceSansBit.TYPES.NEW_SUBHEADER_2} weight={'medium'} style={style}>
					{ this.props.message }
				</SourceSansBit>
			)
		}

		iconRenderer(color) {
			return (
				<IconBit
					name="close"
					size={16}
					color={color}
				/>
			)
		}

	}
)
