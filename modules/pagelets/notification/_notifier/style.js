import StyleSheet from 'coeur/libs/style.sheet'
// import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	background_DEFAULT: {
		backgroundColor: Colors.new.red.palette(2),
	},
	background_SUCCESS: {
		backgroundColor: Colors.primary,
	},
})
