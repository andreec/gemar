import StyleSheet from 'coeur/libs/style.sheet'
import Sizes from 'coeur/constants/size'
import Colors from 'coeur/constants/color'

export default StyleSheet.create({
	container: {
		paddingTop: 16,
		paddingLeft: Sizes.margin.thick,
		paddingRight: Sizes.margin.thick,
		paddingBottom: 40,
	},

	section: {
		marginBottom: 24,
	},

	image: {
	},

	header: {
		color: Colors.black.palette(5),
	},

	content: {
		color: Colors.black.palette(4),
	},
})
