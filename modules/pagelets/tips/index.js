import React from 'react';
import ConnectHelper from 'coeur/helpers/connect';
import StatefulModel from 'coeur/models/stateful';

import BoxBit from 'modules/bits/box';
import SourceSansBit from 'modules/bits/source.sans';
import HeaderBit from 'modules/bits/header';
import ImageBit from 'modules/bits/image';
import PageBit from 'modules/bits/page';

import Styles from './style'


export default ConnectHelper(
	class TipsPagelet extends StatefulModel {

		static propTypes(PropTypes) {
			return {
				title: PropTypes.string,
				image: PropTypes.image,
				data: PropTypes.arrayOf(PropTypes.shape({
					title: PropTypes.string,
					description: PropTypes.string,
				})).isRequired,
				onRequestClose: PropTypes.func,
			}
		}

		header = {
			leftActions: [{
				type: HeaderBit.TYPES.CLOSE,
				onPress: this.props.onRequestClose,
			}],
			rightActions: [{
				type: HeaderBit.TYPES.BLANK,
			}],
		}

		static defaultProps = {
			data: [],
		}

		shouldComponentUpdate() {
			return false
		}

		contentRenderer(data) {
			return (
				<BoxBit unflex style={Styles.section}>
					{ data.title && (
						<SourceSansBit type={SourceSansBit.TYPES.HEADER_5} style={Styles.header}>
							{ data.title }
						</SourceSansBit>
					) }
					{ data.description && (
						<SourceSansBit type={SourceSansBit.TYPES.PARAGRAPH_2} style={Styles.content}>
							{ data.description }
						</SourceSansBit>
					) }
				</BoxBit>
			)
		}

		render() {
			return (
				<PageBit header={ <HeaderBit { ...this.header } title={ this.props.title } /> } >
					{ this.props.image && (
						<ImageBit
							source={this.props.image}
							// style={Styles.image}
						/>
					) }
					<BoxBit unflex style={Styles.container}>
						{ this.props.data.map(this.contentRenderer) }
					</BoxBit>
				</PageBit>
			)
		}
	}
)
