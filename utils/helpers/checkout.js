import _ from 'lodash';
import php from 'js-php-serialize';


export default {
	generateFormAutoSender(link, data) {
		const form = document.createElement('form');

		// form.target = 'webviewer';
		form.target = '_self';
		form.method = 'POST';
		form.action = link;
		form.style.display = 'none';

		for (const key in data) {
			if(_.isArray(data[key])) {
				const input = document.createElement('input');
				input.type = 'hidden';

				input.name = `${key}`
				input.value = php.serialize(data[key])
				form.appendChild(input);
			} else {
				const input = document.createElement('input');
				input.type = 'hidden';
				input.name = key;
				input.value = data[key];
				form.appendChild(input);
			}
		}

		document.body.appendChild(form);
		form.submit();

		setTimeout(() => {
			document.body.removeChild(form);
		}, 1000)
	},
};
