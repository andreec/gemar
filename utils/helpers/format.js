import _ from 'lodash';

const ranges = [
	{ divider: 1e18, suffix: 'E' },
	{ divider: 1e15, suffix: 'P' },
	{ divider: 1e12, suffix: 'T' },
	{ divider: 1e9, suffix: 'B' },
	{ divider: 1e6, suffix: 'M' },
	{ divider: 1e3, suffix: 'k' },
];

function condenseNumber(n, trailing = 2) {
	for (let i = 0; i < ranges.length; i++) {
		if (n >= ranges[i].divider) {
			return (n / ranges[i].divider).toFixed(trailing).replace(/[.,]0*$/, '') + ranges[i].suffix;
		}
	}
	return n.toFixed(trailing).replace(/[.,]0*$/, '');
}
function snakeToCamelCase(input = {}) {
	return _.mapKeys(input, (value, key) => _.camelCase(key))
}

export default {
	condenseNumber,
	snakeToCamelCase,
}
