import ColorModel from 'coeur/models/color';

/*
 * 08 is .03
 * 33 is .2
 * 66 is .4
 * 80 is .5
 * B3 is .7
 * E5 is .9
 */

const White = new ColorModel([
		'FFFFFF33', // rgba(255, 255, 255, .2)
		'FFFFFF66', // rgba(255, 255, 255, .4)
		'FFFFFF80', // rgba(255, 255, 255, .5)
		'FFFFFFB3', // rgba(255, 255, 255, .7)
		'FFFFFFE5', // rgba(255, 255, 255, .9)
		'FFFFFF', // #FFFFFF
	], 6)
	, Black = new ColorModel([
		'0F0F1433', // rgba(15, 15, 20, .2)
		'0F0F1466', // rgba(15, 15, 20, .4)
		'0F0F1480', // rgba(15, 15, 20, .5)
		'0F0F14B3', // rgba(15, 15, 20, .7)
		'0F0F14E5', // rgba(15, 15, 20, .9)
		'0F0F14', // #0F0F14
	], 6)
	, Coal = new ColorModel([
		'27272B', // #27272B
		'27272B33', // #27272B33
		'27272B66', // #27272B66
		'27272B80', // #27272B80
		'27272BB3', // #27272BB3
	], 1)
	, Purple = new ColorModel([
		'F4E5FF', // #F4E5FF
		'DBBDFF', // #DBBDFF
		'8080FF', // #8080FF
		'5B5BFF', // #5B5BFF
		'3333FF', // #3333FF
	], 3)
	, Pink = new ColorModel([
		'FFF2F7', // #FFF2F7
		'FFD1E5', // #FFD1E5
		'FF99AA', // #FF99AA
		'FF667F', // #FF667F
		'E62E4C', // #E62E4C
	], 3)
	, Blue = new ColorModel([
		'D8F4FF', // #D8F4FF
		'5CBFFF', // #5CBFFF
		'0068FF', // #0068FF
		'0A4DDA', // #0A4DDA
		'003787', // #003787
	], 3, {
		facebook: '#3b5998', // #3b5998
	})
	, Green = new ColorModel([
		'CCFFEE', // #CCFFEE
		'00E6A6', // #00E6A6
		'00AA5A', // #00AA5A
		'008040', // #008040
		'006030', // #006030
	], 2)
	, Yellow = new ColorModel([
		'FFFCAA', // #FFFCAA
		'FFF420', // #FFF420
		'FFBF00', // #FFBF00
		'FF9400', // #FF9400
		'FF6000', // #FF6000
	], 3)
	, Red = new ColorModel([
		'FFE8E6', // #FFE8E6
		'FFABAB', // #FFABAB
		'FF6666', // #FF6666
		'E84040', // #E84040
		'C11C1C', // #C11C1C
		'E8404033', // rgba(232, 64, 64, .2)
	], 3)
	, Grey = new ColorModel([
		'8993A40A', // rgba(137, 147, 164, .04)
		'8993A433', // rgba(137, 147, 164, .2)
		'8993A466', // rgba(137, 147, 164, .4)
		'8993A480', // rgba(137, 147, 164, .5)
		'8993A4B3', // rgba(137, 147, 164, .7)
		'8993A4', // #8993A4
	], 4)
	, GreySolid = new ColorModel([
		'F4F6F8', // #F4F6F8
		'EBECF0', // #EBECF0
		'B4BAC6', // #B4BAC6
		'C1C7D0', // #C1C7D0
		'A5ADBA', // #A5ADBA
		'8993A4', // #8993A4
	], 4)


const COLORS = {
	transparent: 'transparent',

	theme: new ColorModel([
		'0F0F14', // text
		'FFFFFF',
		'FFFCAA', // progress bg
		'FFBF00', // primary
		'8993A433', // border
	], 4),

	_yellow: new ColorModel([
		'FFFCAA', // #FFFCAA
		'FFBF00', // #FFBF00
		'FF9F00', // #FF9F00
		'FFF855', // #FFF855
	], 2),

	_black: new ColorModel([
		'0F0F14',   // #0F0F14 text
		'0F0F1433', // #0F0F1433 pale
		'0F0F1408', // #0F0F1408 paler
		'0F0F14B3', // #0F0F14B3 strong
	], 1),

	_grey: new ColorModel([
		'8993A4', // #8993A4
	], 1),

	_red: new ColorModel([
		'C11C1C', // #C11C1C
		'E84040', // #E84040
		'FF6666', // #FF6666
		'FFABAB', // #FFABAB
	], 1),

	_blue: new ColorModel([
		'0A4DDA', // #0A4DDA
		'5CBFFF', // #5CBFFF
	], 1),

	primary: '#FFBF00',

	white: White,
	black: Black,
	coal: Coal,
	purple: Purple,
	pink: Pink,
	blue: Blue,
	green: Green,
	yellow: Yellow,
	red: Red,
	grey: Grey,

	new: {
		black: new ColorModel([
			'000000',
			'000000B3',
			'0000008A',
			'00000051',
			'3B3B3BBA',
			'3B3B3B',
		], 1),
		blue: new ColorModel([
			'38569F',
			'01ACEC',
			'2C4277',
			'08A0D8',
		], 1),
		grey: new ColorModel([
			'E0E0E0',
			'D8D3D380',
			'9C9C9C',
			'D8D8D8',
			'E0E0E0B3',
			'BDBDBD',
			'E0E0E044',
		], 1),
		yellow: new ColorModel([
			'F7821E', // #F7821E
			'FCC540', // #FCC540
			'F7931E', // #F7931E
			'F7931ED8', // #F7931E D8
			'F7831E',
		], 1),
		white: new ColorModel([
			'FFFFFF',
			'FFFFFFB3',
		], 1),
		red: new ColorModel([
			'C11C1C', // #C11C1C
			'E84040', // #E84040
			'FF6666', // #FF6666
			'FFABAB', // #FFABAB
		], 1),
		green: new ColorModel([
			'00AA5A', // #00AA5A
			'008040', // #008040
			'006030', // #006030
		], 1),
	},

	solid: {
		grey: GreySolid,
	},
	opacity: {
		black: {
			overlay: Black.palette(6, .03),
		},
	},
	gradient: {},
};


export default COLORS
