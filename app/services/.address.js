import ServiceModel from 'coeur/models/service';


class AddressService extends ServiceModel {

	static displayName = 'address'

	constructor() {
		super(process.env.API_URL + 'address/')
	}

	getAddresses(ids = [], token) {
		if(ids.length) {
			return this.get('', {
				query: {
					ids: ids.join(','),
				},
				token,
			})
		}

		return this.get('', {
			token,
		})
	}

	getAddress(id, token) {
		return this.get(id, {
			token,
		})
	}

	addAddress(data, token) {
		return this.post('', {
			data,
			token,
		})
	}

	updateAddress(id, data, token) {
		return this.post(id, {
			data,
			token,
		})
	}
}

export default new AddressService()
