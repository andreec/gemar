import ServiceModel from 'coeur/models/service';


class MediaService extends ServiceModel {

	static displayName = 'media'

	constructor() {
		super(process.env.API_URL + 'media/', 0)
	}

	getMedias(ids = [], token) {
		if(ids.length) {
			return this.get('', {
				query: {
					ids: ids.join(','),
				},
				token,
			})
		}

		return this.get('', {
			token,
		})
	}

	getMedia(id, token) {
		return this.get(id, {
			token,
		})
	}

	upload({
		image,
		type = 'image',
		nonce,
	}, token) {
		return this.post('', {
			data: {
				[type]: image,
				nonce,
			},
			token,
		})
	}
}

export default new MediaService()
