import ServiceModel from 'coeur/models/service';
import Defaults from 'coeur/constants/default';


class AccountService extends ServiceModel {

	static displayName = 'account'

	constructor() {
		super(Defaults.API_URL + 'account/')
	}

	sosmedLogin(data) {
		return this.post('login_by_socmed', {
			data: {
				socmed_email: data.email,
				socmed_type: data.type,
				socmed_id: data.id,
				access_token: data.accessToken,
			},
		})
	}

	login({
		email,
		password,
	}) {
		return this.post('login', {
			data: {
				email,
				password,
			},
		})
	}

	sosmedRegister(data) {
		return this.post('register_by_socmed', {
			data: {
				socmed_email: data.email,
				username: data.username,
				password: data.password,
				'hobby[]': data.hobbyIds[0],
				// eslint-disable-next-line no-dupe-keys
				'hobby[]': data.hobbyIds[1],
				socmed_type: data.type,
				socmed_id: data.id,
				access_token: data.accessToken,
			},
		})
	}

	register(data) {
		return this.post('register', {
			data: {
				email: data.email,
				username: data.username,
				password: data.password,
				'hobby[]': data.hobby1,
				// eslint-disable-next-line no-dupe-keys
				'hobby[]': data.hobby2,
				newsletter: data.newsletter,
				invite_status: data.inviteStatus,
			},
		})
	}

	verify(email, redirect) {
		return this.post('mail:verification', {
			data: {
				email,
				redirect,
			},
		})
	}

	resetPassword(email, redirect) {
		return this.post('mail:reset', {
			data: {
				email,
				redirect,
			},
		})
	}

	verifyToken({
		token,
		email,
	}) {
		return this.post('verify:token', {
			data: {
				token,
				email,
			},
		})
	}

	verifyUser({
		token,
		email,
	}) {
		return this.post('verify:user', {
			data: {
				token,
				email,
			},
		})
	}

	changePasswordByToken({
		password1,
		password2,
		token,
		email,
	}) {
		return this.post('change:password', {
			data: {
				password1,
				password2,
				token,
				email,
			},
		})
	}
}

export default new AccountService()
