import ServiceModel from 'coeur/models/service';

// import StringHelper from 'coeur/helpers/string'
import TimeHelper from 'coeur/helpers/time';

const MOCK = [{
	id: 1,
	number: 'GM-1405939',
	status: 'WAITING PAYMENT',
	seller_id: 1,
	createdAt: TimeHelper.moment().subtract(5, 'days').toDate(),
	updatedAt: TimeHelper.moment().subtract(1, 'days').toDate(),
	shipping: {
		address: 'Victor Sembadha\n085928491022\nJalan Prof Dr.Satrio\nKav 11, Lantai 52,\nKaret Semanggi\nJakarta Selatan, DKI Jakarta 12930',
		courier: 'JNE Reguler',
		awb: 'JNE00001293884',
		price: 20000,
	},
	productIds: [1],
	notes: [],
}, {
	id: 2,
	number: 'GM-8520192',
	status: 'ON GOING',
	seller_id: 2,
	createdAt: TimeHelper.moment().subtract(3, 'days').toDate(),
	updatedAt: TimeHelper.moment().subtract(2, 'days').toDate(),
	shipping: {
		address: 'Victor Sembadha\n085928491022\nJalan Prof Dr.Satrio\nKav 11, Lantai 52,\nKaret Semanggi\nJakarta Selatan, DKI Jakarta 12930',
		courier: 'JNE Reguler',
		awb: 'JNE00001293884',
		price: 20000,
	},
	productIds: [1, 2],
	notes: [],
}, {
	id: 3,
	number: 'GM-5594129',
	status: 'CANCELLED',
	seller_id: 3,
	createdAt: TimeHelper.moment().subtract(7, 'days').toDate(),
	updatedAt: TimeHelper.moment().subtract(7, 'days').toDate(),
	shipping: {
		address: 'Victor Sembadha\n085928491022\nJalan Prof Dr.Satrio\nKav 11, Lantai 52,\nKaret Semanggi\nJakarta Selatan, DKI Jakarta 12930',
		courier: 'JNE Reguler',
		awb: 'JNE00001293884',
		price: 20000,
	},
	productIds: [2],
	notes: [],
}, {
	id: 4,
	number: 'GM-2920201',
	status: 'COMPLETED',
	seller_id: 3,
	createdAt: TimeHelper.moment().subtract(15, 'days').toDate(),
	updatedAt: TimeHelper.moment().subtract(10, 'days').toDate(),
	shipping: {
		address: 'Victor Sembadha\n085928491022\nJalan Prof Dr.Satrio\nKav 11, Lantai 52,\nKaret Semanggi\nJakarta Selatan, DKI Jakarta 12930',
		courier: 'JNE Reguler',
		awb: 'JNE00001293884',
		price: 20000,
	},
	productIds: [1, 2],
	notes: ['Warna putih ya gan.. Tolong packing rapi.'],
}]


class OrderService extends ServiceModel {

	static displayName = 'order'

	constructor() {
		super(process.env.API_URL + 'order/')
	}

	orders({
		// offset = 0,
		count = 18,
	}) {
		return new Promise(res => {
			setTimeout(() => {
				res(Array(count).fill(undefined).map(() => {
					return MOCK[Math.floor(Math.random() * MOCK.length)]
				}))
			}, 100)
		})
	}
}

export default new OrderService()
