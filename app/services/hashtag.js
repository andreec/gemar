import ServiceModel from 'coeur/models/service';

import StringHelper from 'coeur/helpers/string'
// import TimeHelper from 'coeur/helpers/time';

const MOCK = [{
	id				: 1,
	hashtag			: 'truetothat',
	count			: 2091203,
}, {
	id				: 2,
	hashtag			: 'women',
	count			: 18210,
}, {
	id				: 3,
	hashtag			: 'kids',
	count			: 90192,
}, {
	id				: 4,
	hashtag			: 'boxing',
	count			: 6172,
}, {
	id				: 5,
	hashtag			: 'photography',
	count			: 19281,
}, {
	id				: 6,
	hashtag			: 'places',
	count			: 18291,
}, {
	id				: 7,
	hashtag			: 'food',
	count			: 190201,
}, {
	id				: 8,
	hashtag			: 'gardening',
	count			: 1820,
}, {
	id				: 9,
	hashtag			: 'fish',
	count			: 182010,
}, {
	id				: 10,
	hashtag			: 'quotes',
	count			: 165839,
}]


class HashtagService extends ServiceModel {

	static displayName = 'hashtag'

	constructor() {
		super(process.env.API_URL + 'hashtag/')
	}

	search({
		search = '',
		// offset = 0,
		// count = 18,
	}) {
		return new Promise(res => {
			setTimeout(() => {
				const regex = StringHelper.fuzzyRegex(search)

				res(search ? MOCK.filter(d => regex.test(d.hashtag)) : MOCK)
			}, 100)
		})
	}
}

export default new HashtagService()
