import ServiceModel from 'coeur/models/service';
import Defaults from 'coeur/constants/default';

import StringHelper from 'coeur/helpers/string'
// import TimeHelper from 'coeur/helpers/time';

// transformRequest: [
// 	(data, headers) => {
// 		const form = new FormData()
// 		for ( const key in data ) {
// 			form.append(key, data[key]);
// 		  }
// 		  return form
// 	},
// ]

const MOCK = [{
	id				: 1,
	name			: 'Anthony Diorgo',
	username		: 'fishingforlife',

	hobbyIds		: [7, 21],
	status			: 'I\'m ready for adventure',

	bio				: 'It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Why do we use it? It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).',
	job				: 'Fisherman',

	followerCount	: 1398,
	followingCount 	: 4592,
	likeCount		: 18290,
	postCount		: 190,

	profile			: 'https://res.cloudinary.com/hiandree/image/upload/v1539438198/dummies/dummy-user1.jpg',
	cover			: 'https://res.cloudinary.com/hiandree/image/upload/v1539438196/dummies/dummy-user1-bg.jpg',
}, {
	id				: 2,
	name			: 'Herman Pradipta',
	username		: 'architectural',

	hobbyIds		: [5, 24],
	status			: 'I\'m ready for adventure',

	bio				: 'It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Why do we use it? It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).',
	job				: 'Architect at Paramount Consulting',

	followerCount	: 54210,
	followingCount 	: 29022,
	likeCount		: 19172,
	postCount		: 184,

	profile			: 'https://res.cloudinary.com/hiandree/image/upload/v1539438201/dummies/dummy-user2.jpg',
	cover			: 'https://res.cloudinary.com/hiandree/image/upload/v1539438199/dummies/dummy-user2-bg.jpg',
}, {
	id				: 3,
	name			: 'Andrea Yukana',
	username		: 'catwomanizee',

	hobbyIds		: [15, 2],
	status			: 'I\'m ready for adventure',

	bio				: 'It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Why do we use it? It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).',
	job				: 'Social Blogger',

	followerCount	: 928100,
	followingCount	: 1920,
	likeCount		: 48091,
	postCount		: 391,

	profile			: 'https://res.cloudinary.com/hiandree/image/upload/v1539438201/dummies/dummy-user3.jpg',
	cover			: 'https://cloudinary.com/console/media_library/folders/all/dummies',
}]


class ProfileService extends ServiceModel {

	static displayName = 'profile'

	constructor() {
		super(Defaults.API_URL + 'profile/')
	}

	profile(id) {
		return this.post('', {
			data: {
				profile_id: id,
			},
		})
	}

	post({
		profile_id: profileId,
		page,
	}) {
		console.warn('PS ', profileId, page);
		return this.post('get_user_post', {
			data: {
				profileId,
				page,
			},
		})
	}

	postDetail(id) {
		return this.post('get_post_detail', {
			data: {
				post_id: id,
			}
		})
	}

	search({
		search = '',
		// offset = 0,
		// count = 18,
	}) {
		return new Promise(res => {
			setTimeout(() => {
				const regex = StringHelper.fuzzyRegex(search)

				res(search ? MOCK.filter(d => regex.test(d.name)) : MOCK)
			}, 100)
		})
	}

	followers({
		offset = 0,
		count = 18,
	}) {
		return new Promise(res => {
			setTimeout(() => {
				res(Array(count).fill(undefined).map(() => {
					return MOCK[Math.floor(Math.random() * MOCK.length)]
				}))
			}, 100)
		})
	}

	following({
		offset = 0,
		count = 18,
	}) {
		return new Promise(res => {
			setTimeout(() => {
				res(Array(count).fill(undefined).map(() => {
					return MOCK[Math.floor(Math.random() * MOCK.length)]
				}))
			}, 100)
		})
	}
}

export default new ProfileService()
