import ServiceModel from 'coeur/models/service';
import Defaults from 'coeur/constants/default';

import StringHelper from 'coeur/helpers/string'
// import TimeHelper from 'coeur/helpers/time';

const MOCK = [{
	id				: 1,
	name			: 'Anthony Diorgo',
	username		: 'fishingforlife',

	hobbyIds		: [7, 21],
	status			: 'I\'m ready for adventure',

	bio				: 'It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Why do we use it? It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).',
	job				: 'Fisherman',

	followerCount	: 1398,
	followingCount 	: 4592,
	likeCount		: 18290,
	postCount		: 190,

	profile			: 'https://res.cloudinary.com/hiandree/image/upload/v1539438198/dummies/dummy-user1.jpg',
	cover			: 'https://res.cloudinary.com/hiandree/image/upload/v1539438196/dummies/dummy-user1-bg.jpg',
}, {
	id				: 2,
	name			: 'Herman Pradipta',
	username		: 'architectural',

	hobbyIds		: [5, 24],
	status			: 'I\'m ready for adventure',

	bio				: 'It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Why do we use it? It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).',
	job				: 'Architect at Paramount Consulting',

	followerCount	: 54210,
	followingCount 	: 29022,
	likeCount		: 19172,
	postCount		: 184,

	profile			: 'https://res.cloudinary.com/hiandree/image/upload/v1539438201/dummies/dummy-user2.jpg',
	cover			: 'https://res.cloudinary.com/hiandree/image/upload/v1539438199/dummies/dummy-user2-bg.jpg',
}, {
	id				: 3,
	name			: 'Andrea Yukana',
	username		: 'catwomanizee',

	hobbyIds		: [15, 2],
	status			: 'I\'m ready for adventure',

	bio				: 'It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Why do we use it? It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).',
	job				: 'Social Blogger',

	followerCount	: 928100,
	followingCount	: 1920,
	likeCount		: 48091,
	postCount		: 391,

	profile			: 'https://res.cloudinary.com/hiandree/image/upload/v1539438201/dummies/dummy-user3.jpg',
	cover			: 'https://cloudinary.com/console/media_library/folders/all/dummies',
}]


class ProfileService extends ServiceModel {

	static displayName = 'profile'

	constructor() {
		super(Defaults.API_URL + 'profile/')
	}

	profile(id) {
		return this.post('', {
			data: {
				profile_id: id,
			},
		})
	}

	getPost({
		id,
		page,
	}) {
		return this.post('get_user_post', {
			data: {
				profile_id: id,
				page,
			},
		})
	}

	getPostDetail(id) {
		return this.post('get_post_detail', {
			data: {
				post_id: id,
			},
		})
	}

	search({
		search = '',
		// offset = 0,
		// count = 18,
	}) {
		return new Promise(res => {
			setTimeout(() => {
				const regex = StringHelper.fuzzyRegex(search)

				res(search ? MOCK.filter(d => regex.test(d.name)) : MOCK)
			}, 100)
		})
	}

	followers({
		offset = 0,
		count = 18,
	}) {
		return new Promise(res => {
			setTimeout(() => {
				res(Array(count).fill(undefined).map(() => {
					return MOCK[Math.floor(Math.random() * MOCK.length)]
				}))
			}, 100)
		})
	}

	following({
		offset = 0,
		count = 18,
	}) {
		return new Promise(res => {
			setTimeout(() => {
				res(Array(count).fill(undefined).map(() => {
					return MOCK[Math.floor(Math.random() * MOCK.length)]
				}))
			}, 100)
		})
	}

	postingJob({
		userId,
		token,
		caption,
		images,
		position,
		company,
		minSallary,
		maxSallary,
		availability,
	}) {
		return this.post('create_post', {
			data: {
				// ====== MANDATORY =======
				user_id: userId,
				login_token: token,
				caption,
				images,
				post_type: 3,	// mandatory. 1=social, 2=marketplace, 3=job
				'images[]': images,
				job_name: position,
				company_name: company,
				slot: availability,
				// ========================
				min_salary: minSallary,
				max_salary: maxSallary,
				additional_info: null,
			},
		})
	}

	postingSocial({
		userId,
		token,
		caption,
		images,
	}) {
		return this.post('create_post', {
			data: {
				// all mandatory
				user_id: userId,
				login_token: token,
				post_type: 1,	// 1=social, 2=marketplace, 3=job
				caption,
				'images[]': images,
			},
		})
	}

	postingMarketPlace({
		userId,
		token,
		caption,
		images,
		title,
		description,
		price,
		weight,
		width,
		length,
		height,
		stock,
	}) {
		return this.post('create_post', {
			data: {
				// ====== MANDATORY =======
				user_id: userId,
				login_token: token,
				post_type: 2,	// mandatory. 1=social, 2=marketplace, 3=job
				caption,
				'images[]': images,
				product_name: title,
				product_price: price,
				// ========================
				product_description: description,
				product_weight: weight,
				product_stock: stock,
			}
		})
	}
}

export default new ProfileService()
