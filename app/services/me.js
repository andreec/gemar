import ServiceModel from 'coeur/models/service';


class MeService extends ServiceModel {

	static displayName = 'me'

	constructor() {
		super(process.env.API_URL + 'user/me/')
	}

	authenticate(token) {
		return this.get('', {
			token,
		})
	}

	changePassword({
		currentPassword,
		password1,
		password2,
	}, token) {
		return this.post('change:password', {
			data: {
				currentPassword,
				password1,
				password2,
			},
			token,
		})
	}

	updateData({
		token,
		...data
	}) {
		return this.post('update', {
			data,
			token,
		})
	}
}

export default new MeService()
