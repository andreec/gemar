import ServiceModel from 'coeur/models/service';
import Defaults from 'coeur/constants/default';
// import StringHelper from 'coeur/helpers/string'
const config = {
	key: 'c06c0afcc08e3586d23d93fb0b756677',
	'ios-key': 'com.gemar.app',
}

class OrderService extends ServiceModel {

	static displayName = 'shipping'

	constructor() {
		super(Defaults.SHIPMENT_URL)
	}

	check({
		// offset = 0,
		count = 18,
	}) {
	}

	getProvince() {
		return this.get('province/', {
			headers: {
				...config,
			}
		})
	}

	getCity(id) {
		return this.get('city/', {
			headers: {
				...config,
			},
		})
	}

	getCost(originId, destinationId, weight, courier) {
		return this.post('')
	}
}

export default new OrderService()
