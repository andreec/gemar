import ServiceModel from 'coeur/models/service';


class HobbyService extends ServiceModel {

	static displayName = 'hobby'

	constructor() {
		super(process.env.API_URL + 'hobby/')
	}
}

export default new HobbyService()
