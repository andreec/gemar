import ServiceModel from 'coeur/models/service';


class BatchService extends ServiceModel {

	static displayName = 'batch'

	constructor() {
		super(process.env.API_URL + 'batch/')
	}

	getBatches(ids = [], token) {
		if(ids.length) {
			return this.get('', {
				query: {
					ids: ids.join(','),
				},
				token,
			})
		}

		return this.get('', {
			token,
		})
	}

	getBatch(id, token) {
		return this.get(id, {
			token,
		})
	}

	create(data, token) {
		return this.post('', {
			data,
			token,
		})
	}

	getMock(data, token) {
		return this.post('', {
			data: {
				...data,
				mock: true,
			},
			token,
		})
	}

	getCheckoutLink(id, token) {
		return this.get(id, {
			query: {
				link: true,
			},
			token,
		})
	}

	getBatchByFaspayNotification(query = {}, token) {
		return this.post('faspay/notification', {
			data: query,
			token,
		})
	}
}

export default new BatchService()
