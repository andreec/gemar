import ServiceModel from 'coeur/models/service';
import Defaults from 'coeur/constants/default';

import TimeHelper from 'coeur/helpers/time';

const MOCK = [{
	id				: 1,
	userId 			: 1,

	refId			: null,

	type			: 'SOCIAL',
	images			: [
		'https://res.cloudinary.com/hiandree/image/upload/v1539438197/dummies/dummy-user1-img3.jpg',
	],

	caption			: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
	commentCount 	: 21,
	likeCount		: 291,
	createdAt		: TimeHelper.moment().subtract(2, 'days').toDate(),
}, {
	id				: 2,
	userId 			: 1,

	refId			: null,

	type			: 'SOCIAL',
	images			: [
		'https://res.cloudinary.com/hiandree/image/upload/v1539438197/dummies/dummy-user1-img2.jpg',
	],

	caption			: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
	commentCount 	: 4,
	likeCount		: 78,
	createdAt		: TimeHelper.moment().subtract(5, 'days').toDate(),
}, {
	id				: 3,
	userId 			: 1,

	refId			: null,

	type			: 'SOCIAL',
	images			: [
		'https://res.cloudinary.com/hiandree/image/upload/v1539438197/dummies/dummy-user1-img1.jpg',
	],

	caption			: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
	commentCount 	: 1,
	likeCount		: 51,
	createdAt		: TimeHelper.moment().subtract(10, 'days').toDate(),
}, {
	id				: 4,
	userId 			: 2,

	refId			: null,

	type			: 'JOB',
	images			: [
		'https://res.cloudinary.com/hiandree/image/upload/v1539438204/dummies/dummy-user2-img1.jpg',
		'https://res.cloudinary.com/hiandree/image/upload/v1539438202/dummies/dummy-user2-img3.jpg',
		'https://res.cloudinary.com/hiandree/image/upload/v1539438199/dummies/dummy-user2-img2.jpg',
	],

	caption			: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
	commentCount 	: 127,
	likeCount		: 11291,
	createdAt		: TimeHelper.moment().subtract(1, 'days').toDate(),
}, {
	id				: 5,
	userId 			: 3,

	refId			: 2,

	type			: 'MARKET',
	images			: [
		'https://res.cloudinary.com/hiandree/image/upload/v1539438205/dummies/dummy-user3-img1.jpg',
	],

	caption			: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
	commentCount 	: 279,
	likeCount		: 942291,
	createdAt		: TimeHelper.moment().subtract(1, 'hours').toDate(),
}]


class PostService extends ServiceModel {

	static displayName = 'post'

	constructor() {
		super(Defaults.API_URL + 'post/')
	}

	explore({
		filter,
		// offset,
		count = 18,
	}) {
		return new Promise(res => {
			setTimeout(() => {
				res(new Array(count).fill().map(() => {
					const data = filter ? MOCK.filter(d => d.type === filter) : MOCK
					return data[Math.floor(Math.random() * data.length)]
				}))
			}, 2000)
		})
	}
}

export default new PostService()
