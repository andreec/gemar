/*
|--------------------------------------------------------------------------
| START JS
|--------------------------------------------------------------------------
|
| Put what needs to be loaded first here
|
*/

/*
| Extending constants
*/
import Colors from 'coeur/constants/color';
import Defaults from 'coeur/constants/default';
import Fonts from 'coeur/constants/font';
import Sizes from 'coeur/constants/size';

import LocalColors from 'utils/constants/color';


Colors.extend({
	...LocalColors,
})

Defaults.extend({
	DEBUG: true,
	STORE_KEY: 'gemar',
	API_URL: 'http://159.65.14.249:8072/gemar_prod/',
	SHIPMENT_URL: 'https://api.rajaongkir.com/starter/',
})

Fonts.extend({
	src: 'System',
})

Sizes.extend({
	screen: {
		width: Sizes.screen.width,
		height: Sizes.screen.height,
	},
	margin: {
		default: Sizes.screen.width < 375 ? 7 : 13,
		thick: Sizes.screen.width < 375 ? 13 : 17,
	},
})


/*
| Extending helpers
*/

import ErrorHelper from 'coeur/helpers/error';
import FormatHelper from 'coeur/helpers/format';
import TimeHelper from 'coeur/helpers/time';
import StringHelper from 'coeur/helpers/string';

import LocalFormatHelper from 'utils/helpers/format'

ErrorHelper
	.addCode('000', 'Connection timeout')
	.addCode('003', 'Facebook connect cancelled')

FormatHelper.extend({
	condenseNumber: LocalFormatHelper.condenseNumber,
})

StringHelper.extend({
	fuzzyRegex(search) {
		let matchTerm = '.*';

		// Split all the search terms
		const terms = search.split(' ');

		for(let i = 0; i < terms.length; i++) {
			matchTerm += '(?=.*' + terms[i] + '.*)';
		}

		matchTerm += '.*';

		return new RegExp(matchTerm, 'ig')
	},
})

TimeHelper.extend({
	shortAgo(timestamp) {
		const diff = TimeHelper.moment().diff(timestamp, 'm');

		if(diff < 60) {
			return `${diff}m`
		} else if(diff < 60 * 24) {
			return `${Math.floor(diff / 60)}h`
		} else if(diff < 60 * 24 * 7) {
			return `${Math.floor(diff / 60 / 24)}d`
		} else if(diff < 60 * 24 * 31) {
			return `${Math.floor(diff / 60 / 24 / 7 )}w`
		} else if(diff < 60 * 24 * 365) {
			return `${Math.floor(diff / 60 / 24 / 31)}mo`
		} else {
			return `${Math.floor(diff / 60 / 24 / 365)}y`
		}
	},
})
