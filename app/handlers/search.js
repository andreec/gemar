import HandlerModel from 'coeur/models/handler'

// import GA from 'ga-lite'
import HashtagService from 'app/services/hashtag'
import UserService from 'app/services/user'

import _ from 'lodash'


class SearchHandler extends HandlerModel {

	static displayName = 'SearchHandler'

	

	explore({
		search = '',
		filter = '',
		// offset = 0,
		count = 10,
	}) {
		switch(filter) {
		case 'People':
			return UserService.search({
				search,
				count,
			}).then(users => {
				return users.map(u => {
					return {
						__type: 'user',
						...u,
					}
				})
			})
		case 'Tags':
			return HashtagService.search({
				search,
				count,
			}).then(hashtags => {
				return hashtags.map(h => {
					return {
						__type: 'hashtag',
						...h,
					}
				})
			})
		default:
			const hashtagCount = Math.round(count / 2)
			return Promise.all([
				HashtagService.search({
					search,
					count: hashtagCount,
				}),
				UserService.search({
					search,
					count: count - hashtagCount,
				}),
			]).then(([hashtags, users]) => {
				return _.shuffle(hashtags.map(h => {
					return {
						__type: 'hashtag',
						...h,
					}
				}).concat(users.map(u => {
					return {
						__type: 'user',
						...u,
					}
				})))
			})
		}
	}
}

export default new SearchHandler()
