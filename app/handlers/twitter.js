import HandlerModel from 'coeur/models/handler'

import ErrorHelper from 'coeur/helpers/error'

import {
	NativeModules,
} from 'react-native'

const {
	RNTwitterSignIn,
} = NativeModules


class TwitterHandler extends HandlerModel {

	static displayName = 'twitter'

	init() {
		if (super.init()) {
			RNTwitterSignIn.init(
				'kDkOXEKMovqx69vNouYpw4hsh',
				'mhRECiS7xWiiuX1BbVY6f5W4bdmWUXVybvuJZt87TGnri5E8NZ'
			)
		}

		return Promise.resolve(true)
	}

	login() {
		return RNTwitterSignIn.logIn()
			.then(loginData => {
				this.log(loginData)
				const { authToken, authTokenSecret } = loginData
				if (authToken && authTokenSecret) {
					// this.setState({
					// 	isLoggedIn: true
					// })
				}

				return loginData
			})
			.catch(err => {
				this.set('loginStatus', 'unknown')

				const error = ErrorHelper.create('003', err)
	
				this.warn(err)
				this.warn(error)

				throw error
			})
	}

	logout() {
		RNTwitterSignIn.logOut()
	}
}

export default new TwitterHandler()
