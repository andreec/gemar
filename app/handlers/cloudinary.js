import HandlerModel from 'coeur/models/handler'

// import cloudinary from 'cloudinary-core';
import RNFetchBlob from 'react-native-fetch-blob'

const options = {
	cloud_name: 'hiandree',
	upload_preset: 'h3wsaxnr',
	secure: true,
}

// import GA from 'ga-lite'
const link = 'https://res.cloudinary.com/hiandree/image/asset/c_fill{{EFFECT}},h_{{HEIGHT}},q_100,w_{{WIDTH}}/e_blur:{{BLUR}}/{{FILE}}'

const EFFECTS = {
		'Normal': '',
		'Incognito': ',e_art:incognito',
		'Audrey': ',e_art:audrey',
		'Frost': ',e_art:frost',
		'Red Rock': ',e_art:red_rock',
		'Primavera': ',e_art:primavera',
		'Eucalyptus': ',e_art:eucalyptus',
		'Hokusai': ',e_art:hokusai',
		'Linen': ',e_art:linen',
		'Hairspray': ',e_art:hairspray',
		'Sonnet': ',e_art:sonnet',
		'Zorro': ',e_art:zorro',
		'Aurora': ',e_art:aurora',
		'Daguerre': ',e_art:daguerre',
		'Fes': ',e_art:fes',
		'Refresh': ',e_art:refresh',
		'Ukulele': ',e_art:ukulele',
		'Athena': ',e_art:athena',
		'Al Dente': ',e_art:al_dente',
		'Sizzle': ',e_art:sizzle',
		'Peacock': ',e_art:peacock',
		'Quartz': ',e_art:quartz',
	}, EFFECTS_KEYS = Object.keys(EFFECTS)


class CloudinaryHandler extends HandlerModel {

	static displayName = 'cloudinary'

	// constructor() {
	// 	super({
	// 	}, [
	// 	])
	// }

	// init() {
	// 	if(super.init()) {
	// 	}
	// }

	upload(image) {
		return RNFetchBlob.fetch('POST', `https://api.cloudinary.com/v1_1/${options.cloud_name}/upload`, {
			'Content-Type': 'multipart/form-data',
		}, [
			{name: 'file', filename: image.name, type: image.type, data: RNFetchBlob.wrap(image.uri)},
			{name: 'upload_preset', data: options.upload_preset},
		])
	}

	effects(file, {
		height = 80,
		width = 80,
		blur = 0,
	} = {}) {
		return EFFECTS_KEYS.reduce((sum, key) => {
			return {
				...sum,
				[key]: link.replace(/{{EFFECT}}/, EFFECTS[key]).replace(/{{FILE}}/, file).replace(/{{HEIGHT}}/, height).replace(/{{WIDTH}}/, width).replace(/{{BLUR}}/, blur),
			}
		}, {})
	}

	effect(key, file, {
		height = 80,
		width = 80,
		blur = 0,
	} = {}) {
		return (EFFECTS[key] || key === 'Normal') && link.replace(/{{EFFECT}}/, EFFECTS[key]).replace(/{{FILE}}/, file).replace(/{{HEIGHT}}/, height).replace(/{{WIDTH}}/, width).replace(/{{BLUR}}/, blur)
	}
}

export default new CloudinaryHandler()
