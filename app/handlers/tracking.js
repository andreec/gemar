import HandlerModel from 'coeur/models/handler'

// import GA from 'ga-lite'


class TrackingHandler extends HandlerModel {

	static displayName = 'TrackingHandler'

	// constructor() {
	// 	super({
	// 	}, [
	// 	])
	// }

	init() {
		super.init();

		// GA('create', process.env.GA_UA_ID, 'auto')
	}

	trackPageView(page, title) {
		// if(page && title) {
		// 	GA('send', 'pageview', '/' + page, {
		// 		title,
		// 	})
		// } else if(page) {
		// 	GA('send', 'pageview', '/' + page)
		// }
	}

	trackEvent(type, data) {
		// switch(type) {
		// case 'register':
		// 	GA('send', 'event', 'User', 'register', data)
		// 	break;
		// case 'login':
		// 	GA('send', 'event', 'User', 'login', data)
		// 	break;
		// case 'matchbox-order-intent':
		// 	GA('send', 'event', 'Matchbox', 'order-intent', data)
		// 	break;
		// case 'matchbox-order':
		// 	GA('send', 'event', 'Matchbox', 'order', data)
		// 	break;
		// case 'matchbox-checkout':
		// 	GA('send', 'event', 'Matchbox', 'checkout', 'amount', data)
		// 	break;
		// }
	}
}

export default new TrackingHandler()
