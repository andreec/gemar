import HandlerModel from 'coeur/models/handler'

import ErrorHelper from 'coeur/helpers/error'

import {
	LoginManager,
	AccessToken,

} from 'react-native-fbsdk';


class FacebookHandler extends HandlerModel {

	static displayName = 'facebook'

	constructor() {
		super({
			init: false,
			loginStatus: false,
			response: undefined,
			userData: undefined,
		}, [
		])
	}

	init() {
		super.init()
		return Promise.resolve(true)
	}

	login() {
		// const option = {
		// 	scope: 'public_profile,email',
		// }
		//
		// if(relogin) {
		// 	option.auth_type = 'rerequest'
		// }

		return LoginManager.logInWithReadPermissions([
			'public_profile',
			'email',
		]).then(result => {
			this.log(result)

			if(result.isCancelled) {
				throw result
			} else {
				return AccessToken.getCurrentAccessToken().then(data => {
					this.set('loginStatus', 'connected')
						.set('response', data)

					this.log(data)

					return {
						...data,
						...result,
					}
				})
			}
		}).catch(err => {
			this.set('loginStatus', 'unknown')

			const error = ErrorHelper.create('003', err)

			this.warn(err)
			this.warn(error)

			throw error
		})
	}

	logout() {
		LoginManager.logOut();
	}
}

export default new FacebookHandler()
