import RecordModel from 'coeur/models/record';
// import Defaults from 'utils/constants/default';

import TimeHelper from 'coeur/helpers/time';


class StatusRecord extends RecordModel('status', {
	code			: RecordModel.TYPES.STRING,
	createdAt		: RecordModel.TYPES.DATE,
}) {}

class BatchOrderDetailRecord extends RecordModel('orderDetail', {
	price			: RecordModel.TYPES.NUMBER,
	discount		: RecordModel.TYPES.NUMBER,
	type			: RecordModel.TYPES.STRING,
	product			: RecordModel.TYPES.OBJECT,
	metadata		: RecordModel.TYPES.OBJECT,
}) {
	get image() {
		return this.product.images && this.product.images[0]
	}

	get listPrice() {
		return this.price + this.discount
	}
}

class BatchOrderAddressRecord extends RecordModel('orderAddress', {
	pic				: RecordModel.TYPES.STRING,
	phone			: RecordModel.TYPES.STRING,
	address			: RecordModel.TYPES.STRING,
	district		: RecordModel.TYPES.STRING,
	postal			: RecordModel.TYPES.STRING,
}) {}

class BatchOrderRecord extends RecordModel('order', {
	statuses		: RecordModel.TYPES.ARRAY,
	details			: RecordModel.TYPES.ARRAY,

	// ADDRESS
	from			: new BatchOrderAddressRecord(),
	to				: new BatchOrderAddressRecord(),

	// FEE
	amount			: RecordModel.TYPES.NUMBER,
	shipping		: RecordModel.TYPES.NUMBER,
	insurance		: RecordModel.TYPES.NUMBER,
}) {
	get status() {
		return this.statuses[this.statuses.length - 1] || new StatusRecord()
	}

	get productCount() {
		return this.details.length
	}

	get listPrice() {
		return this.details.reduce((sum, detail) => sum + detail.listPrice, 0)
	}

	get shipmentStartDate() {
		return this.details.map(detail => {
			return Date.parse(detail.metadata.shipmentStartDate)
		}).sort().shift()
	}

	get shipmentEndDate() {
		return this.details.map(detail => {
			return Date.parse(detail.metadata.shipmentEndDate)
		}).sort().pop()
	}
}

function BatchRecordFactory(additionalConfig = {}) {
	return class BatchRecord extends RecordModel('batch', {
		id				: RecordModel.TYPES.ID,
		orderNumber		: RecordModel.TYPES.STRING,
		couponId		: RecordModel.TYPES.STRING,
		discount		: RecordModel.TYPES.NUMBER,
		additionalDiscount	: RecordModel.TYPES.NUMBER,
		statuses		: RecordModel.TYPES.ARRAY,
		orders			: RecordModel.TYPES.ARRAY,

		createdAt		: RecordModel.TYPES.DATE,

		...additionalConfig,
	}) {
		get isExpired() {
			const status = this.statuses[this.statuses.length - 1]

			return status && status.code === 'UNPAID' && Date.parse(this.createdAt) + (1000 * 60 * 60 * 24) < Date.now()
		}

		get productCount() {
			return this.orders.reduce((sum, order) => sum + order.productCount, 0)
		}

		get status() {
			// Check for difference
			let status = this.statuses[this.statuses.length - 1] || new StatusRecord()

			if(status.code === 'PAID') {
				status = this.orders.map(order => order.status).sort((a, b) => {
					return Date.parse(b.createdAt) - Date.parse(a.createdAt)
				})[0]
			} else if(this.isExpired) {
				return new StatusRecord({
					code: 'EXPIRED',
				})
			}

			return status
		}

		get estimateShipment() {
			const start = this.orders.sort((a, b) => {
					return a.shipmentStartDate - b.shipmentStartDate
				})
				, end = this.orders.sort((a, b) => {
					return b.shipmentEndDate - a.shipmentEndDate
				})

			if(start.length && end.length) {
				return TimeHelper.getRange(TimeHelper.moment(start[0].shipmentStartDate), TimeHelper.moment(end[0].shipmentEndDate))
			} else {
				return '-'
			}
		}

		get totalListPrice() {
			return this.orders.reduce((sum, order) => sum + order.listPrice, 0)
		}

		get totalAmount() {
			return this.orders.reduce((sum, order) => sum + order.amount, 0)
		}

		get totalShipping() {
			return this.orders.reduce((sum, order) => sum + order.shipping, 0)
		}

		get totalInsurance() {
			return this.orders.reduce((sum, order) => sum + order.insurance, 0)
		}

		get totalDiscount() {
			return this.discount + this.additionalDiscount
		}

		get total() {
			return this.totalAmount + this.totalShipping + this.totalInsurance - this.totalDiscount
		}
	}
}

BatchRecordFactory.orderRecord = BatchOrderRecord
BatchRecordFactory.detailRecord = BatchOrderDetailRecord
BatchRecordFactory.addressRecord = BatchOrderAddressRecord
BatchRecordFactory.statusRecord = StatusRecord
BatchRecordFactory.getModel = function(config = {}) {
	return {
		...config,
		statuses: config.statuses && config.statuses.map(status => new StatusRecord(status)),
		orders: config.orders && config.orders.map(order => new BatchOrderRecord({
			...order,
			statuses: order.statuses && order.statuses.map(status => new StatusRecord(status)),
			details: order.details && order.details.map(detail => new BatchOrderDetailRecord({
				...detail,
				type			: detail.type || detail.reference.type,
				product			: detail.product || detail.reference.detail,
				metadata		: {
					...(detail.metadata || {}),
					mediaIds: detail.metadata.mediaIds || detail.mediaIds || [],
				},
			})),
			from : new BatchOrderAddressRecord(order.from),
			to : new BatchOrderAddressRecord(order.to),
		})),
	}
}


export default BatchRecordFactory
