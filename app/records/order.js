import RecordModel from 'coeur/models/record';


function OrderRecordFactory(additionalConfig = {}) {
	return class OrderRecord extends RecordModel('order', {
		id				: RecordModel.TYPES.ID,
		number			: RecordModel.TYPES.STRING,
		status			: RecordModel.TYPES.STRING,
		seller_id		: RecordModel.TYPES.ID,
		createdAt		: RecordModel.TYPES.DATE,
		updatedAt		: RecordModel.TYPES.DATE,
		shipping		: RecordModel.TYPES.OBJECT,
		productIds		: RecordModel.TYPES.ARRAY,
		notes			: RecordModel.TYPES.ARRAY,
		...additionalConfig,
	}) {
	}
}


export default OrderRecordFactory
