import RecordModel from 'coeur/models/record';


function MediaRecordFactory(additionalConfig = {}) {
	return class MediaRecord extends RecordModel('media', {
		id				: RecordModel.TYPES.ID,
		type			: RecordModel.TYPES.STRING,
		url				: RecordModel.TYPES.STRING,

		width			: RecordModel.TYPES.NUMBER,
		height			: RecordModel.TYPES.NUMBER,

		metadata		: RecordModel.TYPES.OBJECT,

		...additionalConfig,
	}) {
	}
}


export default MediaRecordFactory
