import RecordModel from 'coeur/models/record'

export default function UserRecordFactory(additionalConfig = {}) {
	return class UserRecord extends RecordModel('user', {
		id				: RecordModel.TYPES.ID,
		username		: RecordModel.TYPES.STRING,
		name			: RecordModel.TYPES.STRING,
		email			: RecordModel.TYPES.STRING,
		phone			: RecordModel.TYPES.STRING,

		fbId			: RecordModel.TYPES.STRING,
		twitterId		: RecordModel.TYPES.STRING,
		fbToken			: RecordModel.TYPES.STRING,
		twitterToken	: RecordModel.TYPES.STRING,
		loginToken		: RecordModel.TYPES.STRING,

		isPrivate		: RecordModel.TYPES.BOOL.FALSE,

		...additionalConfig,
	}) {
		get fullName() {
			// return `${this.firstName} ${this.lastName}`
			return this.name
		}
	}
}
