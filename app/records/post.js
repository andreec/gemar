import RecordModel from 'coeur/models/record';


function PostRecordFactory(additionalConfig = {}) {
	return class PostRecord extends RecordModel('post', {
		id				: RecordModel.TYPES.ID,
		userId 			: RecordModel.TYPES.ID,

		refId			: RecordModel.TYPES.ID,

		type			: RecordModel.TYPES.STRING,
		images			: RecordModel.TYPES.ARRAY,
		tags			: RecordModel.TYPES.ARRAY,

		caption			: RecordModel.TYPES.STRING,
		commentCount 	: RecordModel.TYPES.NUMBER,
		likeCount		: RecordModel.TYPES.NUMBER,

		createdAt		: RecordModel.TYPES.DATE,

		...additionalConfig,
	}) {
		get image() {
			return this.images[0]
		}
	}
}


export default PostRecordFactory
