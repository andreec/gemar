import RecordModel from 'coeur/models/record'

export default function VolatileRecordFactory(additionalConfig = {}) {
	return class VolatileRecord extends RecordModel('volatile', additionalConfig) {
	}
}
