import RecordModel from 'coeur/models/record';


function ProductRecordFactory(additionalConfig = {}) {
	return class ProductRecord extends RecordModel('product', {
		id				: RecordModel.TYPES.ID,

		title			: RecordModel.TYPES.STRING,
		description		: RecordModel.TYPES.STRING,
		price			: RecordModel.TYPES.NUMBER,
		weight			: RecordModel.TYPES.NUMBER,
		width			: RecordModel.TYPES.NUMBER,
		length			: RecordModel.TYPES.NUMBER,
		height			: RecordModel.TYPES.NUMBER,
		stock			: RecordModel.TYPES.NUMBER,

		...additionalConfig,
	}) {
	}
}


export default ProductRecordFactory
