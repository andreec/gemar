import RecordModel from 'coeur/models/record'

export default function AddressRecordFactory(additionalConfig = {}) {
	return class AddressRecord extends RecordModel('address', {
		id				: RecordModel.TYPES.ID,
		title			: RecordModel.TYPES.STRING,
		pic				: RecordModel.TYPES.STRING,
		phone			: RecordModel.TYPES.STRING,
		district		: RecordModel.TYPES.STRING,
		address			: RecordModel.TYPES.STRING,
		postal			: RecordModel.TYPES.STRING,

		...additionalConfig,
	}) {
	}
}
