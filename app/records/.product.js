import RecordModel from 'coeur/models/record';


class PacketRecord extends RecordModel('packet', {
	weight				: RecordModel.TYPES.NUMBER,
	height				: RecordModel.TYPES.NUMBER,
	width				: RecordModel.TYPES.NUMBER,
	length				: RecordModel.TYPES.NUMBER,
}) {}


function ProductRecordFactory(additionalConfig = {}) {
	return class ProductRecord extends RecordModel('product', {
		id				: RecordModel.TYPES.ID,
		title			: RecordModel.TYPES.STRING,
		description		: RecordModel.TYPES.STRING,

		basePrice		: RecordModel.TYPES.NUMBER,
		price			: RecordModel.TYPES.NUMBER,

		availability	: RecordModel.TYPES.NUMBER,

		publishedAt		: RecordModel.TYPES.DATE,

		packet			: new PacketRecord(),

		...additionalConfig,
	}) {
		get image() {
			return this.images && this.images.length && this.images[0]
		}

		get isDiscounted() {
			return this.basePrice !== this.price
		}

		get isNew() {
			return this.publishedAt - Date.now() < 1000 * 60 * 60 * 24 * 30
		}
	}
}

ProductRecordFactory.packetRecord = PacketRecord


export default ProductRecordFactory
