import RecordModel from 'coeur/models/record';


function JobRecordFactory(additionalConfig = {}) {
	return class JobRecord extends RecordModel('job', {
		id				: RecordModel.TYPES.ID,

		position		: RecordModel.TYPES.STRING,
		company			: RecordModel.TYPES.STRING,
		minSallary		: RecordModel.TYPES.NUMBER,
		maxSallary		: RecordModel.TYPES.NUMBER,
		availability	: RecordModel.TYPES.NUMBER,
		applied 		: RecordModel.TYPES.NUMBER,

		...additionalConfig,
	}) {
	}
}


export default JobRecordFactory
