import RecordModel from 'coeur/models/record';


function HobbyRecordFactory(additionalConfig = {}) {
	return class HobbyRecord extends RecordModel('hobby', {
		id				: RecordModel.TYPES.ID,

		title			: RecordModel.TYPES.STRING,
		description		: RecordModel.TYPES.STRING,
		image			: RecordModel.TYPES.STRING,

		...additionalConfig,
	}) {
	}
}


export default HobbyRecordFactory
