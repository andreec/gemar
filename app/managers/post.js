import DictionaryModel from 'coeur/models/dictionary'

import MeManager from './me'
import JobManager from './job'
import ProductManager from './product'
import ProfileService from 'app/services/profile';

import PostRecord from 'app/records/post'

import PostService from 'app/services/post'

import TimeHelper from 'coeur/helpers/time'


class PostManager extends DictionaryModel {

	static displayName = 'posts'

	constructor() {
		super(PostRecord, {
			expiricy: 1000 * 60 * 60 * 24 * 30,
			additionalProperties: {
				myComment: '',
			},
			ordered: true,
			datas: [{
				id				: 1,
				userId 			: 1,

				refId			: null,

				type			: 'SOCIAL',
				images			: [
					'https://res.cloudinary.com/hiandree/image/upload/v1539438197/dummies/dummy-user1-img3.jpg',
				],

				caption			: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
				commentCount 	: 21,
				likeCount		: 291,
				createdAt		: TimeHelper.moment().subtract(2, 'days').toDate(),
			}, {
				id				: 2,
				userId 			: 1,

				refId			: null,

				type			: 'SOCIAL',
				images			: [
					'https://res.cloudinary.com/hiandree/image/upload/v1539438197/dummies/dummy-user1-img2.jpg',
				],

				caption			: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
				commentCount 	: 4,
				likeCount		: 78,
				createdAt		: TimeHelper.moment().subtract(5, 'days').toDate(),
			}, {
				id				: 3,
				userId 			: 1,

				refId			: null,

				type			: 'SOCIAL',
				images			: [
					'https://res.cloudinary.com/hiandree/image/upload/v1539438197/dummies/dummy-user1-img1.jpg',
				],

				caption			: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
				commentCount 	: 1,
				likeCount		: 51,
				createdAt		: TimeHelper.moment().subtract(10, 'days').toDate(),
			}, {
				id				: 4,
				userId 			: 2,

				refId			: 1,

				type			: 'JOB',
				images			: [
					'https://res.cloudinary.com/hiandree/image/upload/v1539438204/dummies/dummy-user2-img1.jpg',
					'https://res.cloudinary.com/hiandree/image/upload/v1539438202/dummies/dummy-user2-img3.jpg',
					'https://res.cloudinary.com/hiandree/image/upload/v1539438199/dummies/dummy-user2-img2.jpg',
				],

				caption			: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
				commentCount 	: 127,
				likeCount		: 11291,
				createdAt		: TimeHelper.moment().subtract(1, 'days').toDate(),
			}, {
				id				: 5,
				userId 			: 3,

				refId			: 2,

				type			: 'MARKET',
				images			: [
					'https://res.cloudinary.com/hiandree/image/upload/v1539438205/dummies/dummy-user3-img1.jpg',
				],

				caption			: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
				commentCount 	: 279,
				likeCount		: 942291,
				createdAt		: TimeHelper.moment().subtract(1, 'hours').toDate(),
			}],
			// TODO: ADD MORE MARKETPLACE
		})
	}

	updateOne({
		id,
		tags,
		product,
		job,
		caption,
	}) {
		product && ProductManager.update(product)
		job && JobManager.update(job)

		this.update({
			id,
			tags,
			caption,
		})
	}

	insert({
		type,
		images,
		tags,
		product,
		job,
		caption,
	}) {
		this.log(type, images, tags, product, job, caption)

		// let id = null

		if(type === 'MARKET') {
			// first insert product
			// const productRecord = ProductManager.insert(product)
			// id = productRecord.id

			ProfileService.postingMarketPlace({
				userId: MeManager.state.id,
				token: MeManager.state.loginToken,
				images,
				caption,
				...product,
			}).then(res => {
				this.log(res)
				return this.update({
					id: Math.floor(Math.random() * 10000),
					userId: MeManager.state.id,
					type,
					images,
					tags,
					caption,
					refId: res.id,
					createdAt: new Date(),
				})
			})
		} else if(type === 'JOB') {
			// first insert job
			// const jobRecord = JobManager.insert(job)
			// id = jobRecord.id

			ProfileService.postingJob({
				userId: MeManager.state.id,
				token: MeManager.state.loginToken,
				caption,
				images,
				...job,
			})

		} else if(type === 'SOCIAL') {
			ProfileService.postingSocial({
				userId: MeManager.state.id,
				token: MeManager.state.loginToken,
				caption,
				images,
			}).then(res => {
				this.log(res)
				return this.update({
					id: Math.floor(Math.random() * 10000),
					userId: MeManager.state.id,
					type,
					images,
					tags,
					caption,
					refId: res.id,
					createdAt: new Date(),
				})
			})
		}

		// return this.update({
		// 	id: Math.floor(Math.random() * 10000),
		// 	userId: MeManager.state.id,
		// 	type,
		// 	images,
		// 	tags,
		// 	caption,
		// 	refId: id,
		// 	createdAt: new Date(),
		// })
	}

	explorePost({
		filter = '',
		offset,
		count,
	}) {
		return PostService.explore({
			filter,
			offset,
			count,
		}).then(posts => {
			return posts && this.updateBulk(posts)
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	toggleLikePost(id, isLiked = true) {
		// TODO
		const post = this.state.get(id)

		if(post) {
			return Promise.resolve(true).then(() => {
				return this.update({
					id,
					likeCount: isLiked ? post.likeCount + 1 : post.likeCount - 1,
				})
			})
		} else {
			return Promise.reject(false)
		}
	}

	addDirectComment(id, comment = '') {
		// TODO
		const post = this.state.get(id)

		if(post) {
			return Promise.resolve(true).then(() => {
				return this.update({
					id,
					myComment: comment,
				})
			})
		} else {
			return Promise.reject(false)
		}
	}
}

export default new PostManager()
