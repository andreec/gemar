import DictionaryModel from 'coeur/models/dictionary';

import ProductRecord from 'app/records/product';
import ProfileService from 'app/services/profile';

// import HobbyService from 'app/services/media';

// import MeManager from './me';


class ProductManager extends DictionaryModel {

	static displayName = 'products'

	constructor() {
		super(ProductRecord, {
			expiricy: 1000 * 60 * 60 * 24 * 30,
			datas: [{
				id				: 1,
				title			: 'Adidas Sport J GT -32',
				description		: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse suscipit enim ac ante ultrices, ac pellentesque ante varius. Sed @gravida tortor quis augue semper mattis. Curabitur efficitur dictum massa, sit amet porta libero pretium nec. Cras',
				price			: 499000,
				weight			: .8,
				width			: 25,
				length			: 35,
				height			: 20,
				stock			: 2,
			}, {
				id				: 2,
				title			: 'Nike Tanjun Men\'s Shoe',
				description		: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse suscipit enim ac ante ultrices, ac pellentesque ante varius. Sed @gravida tortor quis augue semper mattis. Curabitur efficitur dictum massa, sit amet porta libero pretium nec. Cras',
				price			: 349000,
				weight			: .8,
				width			: 25,
				length			: 35,
				height			: 20,
				stock			: 2,
			}],
		})
	}

	insert({
		title,
		description,
		price,
		weight,
		width,
		length,
		height,
		stock,
	}) {
		// return this.update({
		// 	id: Math.floor(Math.random() * 10000),
		// 	title,
		// 	description,
		// 	price,
		// 	weight,
		// 	width,
		// 	length,
		// 	height,
		// 	stock,
		// })
		return ProfileService.postingMarketPlace({
			
		}).then(res => {
			if(res) {
				return this.update({
					user_id: null,
					login_token: '',
					post_type: null,	// mandatory. 1=social, 2=marketplace, 3=job
					caption: null,
					'images[]': null,
					product_name: null,
					product_description: null,
					product_price: null,
					product_weight: null,
					product_stock: null,
				})
			}

		})
	}

	// dataGetter(id) {
	// 	return HobbyService.getMedia(id, MeManager.state.token).then(media => {
	// 		return this.updateLater(media)
	// 	}).catch(err => {
	// 		this.warn(err)
	//
	// 		return this.updateLater({
	// 			id,
	// 			_isInvalid: true,
	// 		})
	// 	})
	// }
	//
	// datasGetter(ids) {
	// 	return HobbyService.getMedias(ids, MeManager.state.token).then(medias => {
	// 		return this.updateLater(medias)
	// 	}).catch(err => {
	// 		this.warn(err)
	//
	// 		throw err
	// 	})
	// }
}

export default new ProductManager()
