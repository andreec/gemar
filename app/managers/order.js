import DictionaryModel from 'coeur/models/dictionary'

import OrderRecord from 'app/records/order'

import OrderService from 'app/services/order'

import TimeHelper from 'coeur/helpers/time'


class OrderManager extends DictionaryModel {

	static displayName = 'orders'

	constructor() {
		super(OrderRecord, {
			expiricy: 1000 * 60 * 60 * 24 * 30,
			datas: [{
				id: 1,
				number: 'GM-1405939',
				status: 'WAITING PAYMENT',
				seller_id: 1,
				createdAt: TimeHelper.moment().subtract(5, 'days').toDate(),
				updatedAt: TimeHelper.moment().subtract(1, 'days').toDate(),
				shipping: {
					address: 'Victor Sembadha\n085928491022\nJalan Prof Dr.Satrio\nKav 11, Lantai 52,\nKaret Semanggi\nJakarta Selatan, DKI Jakarta 12930',
					courier: 'JNE Reguler',
					awb: 'JNE00001293884',
					price: 20000,
				},
				productIds: [1],
				notes: [],
			}, {
				id: 2,
				number: 'GM-8520192',
				status: 'ON GOING',
				seller_id: 2,
				createdAt: TimeHelper.moment().subtract(3, 'days').toDate(),
				updatedAt: TimeHelper.moment().subtract(2, 'days').toDate(),
				shipping: {
					address: 'Victor Sembadha\n085928491022\nJalan Prof Dr.Satrio\nKav 11, Lantai 52,\nKaret Semanggi\nJakarta Selatan, DKI Jakarta 12930',
					courier: 'JNE Reguler',
					awb: null,
					price: 20000,
				},
				productIds: [1, 2],
				notes: [],
			}, {
				id: 3,
				number: 'GM-5594129',
				status: 'CANCELLED',
				seller_id: 3,
				createdAt: TimeHelper.moment().subtract(7, 'days').toDate(),
				updatedAt: TimeHelper.moment().subtract(7, 'days').toDate(),
				shipping: {
					address: 'Victor Sembadha\n085928491022\nJalan Prof Dr.Satrio\nKav 11, Lantai 52,\nKaret Semanggi\nJakarta Selatan, DKI Jakarta 12930',
					courier: 'JNE Express',
					awb: 'JNE0000245011',
					price: 20000,
				},
				productIds: [2],
				notes: [],
			}, {
				id: 4,
				number: 'GM-2920201',
				status: 'WAITING PAYMENT',
				seller_id: 3,
				createdAt: TimeHelper.moment().subtract(15, 'days').toDate(),
				updatedAt: TimeHelper.moment().subtract(10, 'days').toDate(),
				shipping: {
					address: 'Victor Sembadha\n085928491022\nJalan Prof Dr.Satrio\nKav 11, Lantai 52,\nKaret Semanggi\nJakarta Selatan, DKI Jakarta 12930',
					courier: 'JNE ONS',
					awb: null,
					price: 20000,
				},
				productIds: [1, 2],
				notes: ['Warna putih ya gan.. Tolong packing rapi.'],
			}],
		})
	}

	getOrders({
		offset,
		count,
	}) {
		return OrderService.orders({ offset, count})
	}

	// transformConfig(config = {}) {
	// 	return super.transformConfig({
	// 		...config,
	// 		email: config.email || config.eail,
	// 	})
	// }
}

export default new OrderManager()
