import DictionaryModel from 'coeur/models/dictionary'

import UserRecord from 'app/records/user'

import PostService from 'app/services/post'
import ProfileService from 'app/services/profile'


class UserManager extends DictionaryModel {

	static displayName = 'users'

	constructor() {
		super(UserRecord, {
			expiricy: 1000 * 60 * 60 * 24 * 30,
			datas: [{
				id				: 1,
				name			: 'Anthony Diorgo',
				username		: 'fishingforlife',

				hobbyIds		: [7, 21],
				status			: 'I\'m ready for adventure',

				bio				: 'It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Why do we use it? It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).',
				job				: 'Fisherman',

				followerCount	: 1398,

				profile			: 'https://res.cloudinary.com/hiandree/image/upload/v1539438198/dummies/dummy-user1.jpg',
				cover			: 'https://res.cloudinary.com/hiandree/image/upload/v1539438196/dummies/dummy-user1-bg.jpg',
			}, {
				id				: 2,
				name			: 'Herman Pradipta',
				username		: 'architectural',

				hobbyIds		: [5, 24],
				status			: 'I\'m ready for adventure',

				bio				: 'It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Why do we use it? It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).',
				job				: 'Architect at Paramount Consulting',

				followerCount	: 54210,

				profile			: 'https://res.cloudinary.com/hiandree/image/upload/v1539438201/dummies/dummy-user2.jpg',
				cover			: 'https://res.cloudinary.com/hiandree/image/upload/v1539438199/dummies/dummy-user2-bg.jpg',
			}, {
				id				: 3,
				name			: 'Andrea Yukana',
				username		: 'catwomanizee',

				hobbyIds		: [15, 2],
				status			: 'I\'m ready for adventure',

				bio				: 'It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Why do we use it? It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).',
				job				: 'Social Blogger',

				followerCount	: 928100,

				profile			: 'https://res.cloudinary.com/hiandree/image/upload/v1539438201/dummies/dummy-user3.jpg',
				cover			: 'https://cloudinary.com/console/media_library/folders/all/dummies',
			}],
		})
	}

	getPost({
		// filter = '',
		// offset,
		// count,
		id,
		offset,
	}) {
		// return PostService.explore({
		// 	filter,
		// 	offset,
		// 	count,
		// }).then(posts => {
		// 	return posts && this.updateBulk(posts)
		// }).catch(err => {
		// 	this.warn(err)

		// 	throw err
		// })
		return ProfileService.getPost({
			profile_id: id,
			page: offset,
			// count,
		})
	}

	getFollowers({
		offset,
		count,
	}) {
		return ProfileService.followers({ offset, count})
	}

	getProfile(id) {
		return ProfileService.profile(id)
	}

	getFollowing({
		offset,
		count,
	}) {
		return ProfileService.following({ offset, count })
	}

	// transformConfig(config = {}) {
	// 	return super.transformConfig({
	// 		...config,
	// 		email: config.email || config.eail,
	// 	})
	// }
}

export default new UserManager()
