import ManagerModel from 'coeur/models/manager'

import ErrorHelper from 'coeur/helpers/error'

import PostManager from './post'
// import UserManager from './user'

import FacebookHandler from 'app/handlers/facebook';
import AccountService from 'app/services/account'
import MeService from 'app/services/me'

// import MediaService from 'app/services/media'
import TrackingHandler from 'app/handlers/tracking';

import UserRecord from 'app/records/user'

import FormatHelper from 'utils/helpers/format';

import _ from 'lodash'


class MeManager extends ManagerModel {

	static displayName = 'me'

	constructor() {
		super(UserRecord, {
			additionalProperties: {
				credit			: 0,
				postCount		: 0,
				followerCount	: 0,
				followingCount	: 0,
				title			: '',
				subtitle		: '',
				userDescription	: '',
				newsletter		: '',
				inviteStatus	: '',
				tokenExpired	: '',
				createdDate		: '',
				modifiedDate	: '',
				hobbies			: [],

				postIds				: [],
				followingUserIds	: [],
				appliedPostIds		: [],
				likedPostIds		: [],
				cartItems			: [],
			},
			expiricy: 1000 * 60 * 60 * 24 * 7,
		})
	}

	// ------------------------------------
	// Login & Register
	// ------------------------------------

	authenticate(token) {
		if(token) {
			return MeService.authenticate(token).then(response => {
				return this.update(response)
			})
		} else {
			return Promise.resolve(false)
		}
	}

	updateStore(data, callback) {
		return this.update(FormatHelper.snakeToCamelCase(data), () => callback())
	}

	login({
		email,
		password,
	}) {
		return AccountService
			.login({
				email,
				password,
			})
			.then(response => {
				if(response && response.status === 1) {
					return this.updateStore(response.data.user, () => new Promise.resolve(response.data))
				} else {
					return new Promise.reject({
						status: response.status,
						code: response.code,
						message: response.message,
						error: response.error,
					})
				}
			})
	}

	register({
		email,
		username,
		hobbyIds,
		followingUserIds,
		status,
		password,
	}) {
		// TODO
		// return Promise.resolve(this.update({
		// 	id: 999999,
		// 	email,
		// 	username,
		// 	hobbyIds,
		// 	status,
		// 	token: 'thisisatesttoken',
		// 	followingCount: followingUserIds.length,
		// 	followingUserIds,
		// })).then(result => {
		// 	return new Promise(res => {
		// 		setTimeout(() => {
		// 			res(result)
		// 		}, 1000)
		// 	})
		// }).catch(result => {
		// 	return new Promise((res, rej) => {
		// 		setTimeout(() => {
		// 			rej(result)
		// 		}, 1000)
		// 	})
		// })
		return AccountService
			.register({
				email,
				// firstName,
				// lastName,
				password,
			})
			.then(response => {
				TrackingHandler.trackEvent('register', email)
				return this.update(response)
			})
	}
	
	sosmedRegister(data) {
		return AccountService.sosmedRegister(data).then(response => {
			if(response && response.status === 1) {
				return this.updateStore(response.data.user, () => new Promise.resolve(response.data))
			} else {
				return new Promise.reject({
					status: response.status,
					code: response.code,
					message: response.message,
					error: response.error,
				})
			}
		})
	}

	fbLogin(data) {
		return AccountService
			.sosmedLogin({
				type: data.type,
				email: data.email,
				id: data.userID,
				accessToken: data.accessToken,
			})
			.then(response => {
				if(response && response.status === 1) {
					this.update({
						...FormatHelper.snakeToCamelCase(response.data.user),
						// TODO FACEBOOK LOGIN DOESN'T RETURN EMAIL
						email: data.email,
					})

					return new Promise.resolve(response.data)
				} else {
					console.log(response);
					return new Promise.reject({
						// status: response.status,
						// code: response.code,
						// message: response.message,
						// error: response.error,
					})
				}
			})
	}

	twLogin(data) {
		return AccountService
			.sosmedLogin({
				id: data.id,
				email: data.email,
				type: data.type,
				accessToken: data.accessToken,
			})
			.then(response => {
				if(response && response.status === 1) {
					this.update(FormatHelper.snakeToCamelCase(response.data.user))

					return new Promise.resolve(response.data)
				} else {
					return new Promise.reject({
						status: response.status,
						code: response.code,
						message: response.message,
						error: response.error,
					})
				}
			})

		// if (this.state.token) {
		// 	return Promise.resolve(true).then(result => {
		// 		return new Promise(res => {
		// 			setTimeout(() => {
		// 				res(result)
		// 			}, 1000)
		// 		})
		// 	})
		// } else {
		// 	return Promise.reject(false).catch(result => {
		// 		return new Promise((res, rej) => {
		// 			setTimeout(() => {
		// 				rej(result)
		// 			}, 1000)
		// 		})
		// 	})
		// }
	}

	// ------------------------------------
	// Data manipulation
	// ------------------------------------

	updateProfile(data) {
		return new Promise(res => {
			if(Object.keys(data).length > 2) {
				setTimeout(() => {
					res(this.update(data))
				}, 1000)
			} else {
				res(this.update(data))
			}
		})
		// return MeService
		// 	.updateData({
		// 		token: this.state.token,
		// 		...data,
		// 	})
		// 	.then(response => {
		// 		return this.update(response)
		// 	})
	}

	verifyToken({
		token,
		email,
	}) {
		return AccountService.verifyToken({
			token,
			email,
		})
	}

	verifyUser({
		token,
		email,
	}) {
		return AccountService.verifyUser({
			token,
			email,
		}).then(response => {
			return this.update(response)
		})
	}

	// ------------------------------------
	// Password manipulation
	// ------------------------------------

	changePassword({
		currentPassword,
		password1,
		password2,
	}) {
		if(password1 !== password2) {
			return Promise.reject(ErrorHelper.create('013'))
		} else {
			return new Promise(res => {
				setTimeout(() => {
					res(true)
				}, 500)
			})

			// return MeService
			// 	.changePassword({
			// 		currentPassword,
			// 		password1,
			// 		password2,
			// 	}, this.state.token)
			// 	.then(response => {
			// 		TrackingHandler.trackEvent('login')
			// 		return this.update(response)
			// 	})
		}
	}

	changePasswordByToken({
		password1,
		password2,
		token,
		email,
	}) {
		if(password1 && password2 && password1 !== password2) {
			return Promise.reject(ErrorHelper.create('013'))
		} else {
			return AccountService
				.changePasswordByToken({
					password1,
					password2,
					token,
					email,
				})
				.then(response => {
					return this.update(response)
				})
		}
	}

	// ------------------------------------
	// Send Email
	// ------------------------------------

	verify(email = this.state.email, redirect) {
		return AccountService.verify(email, redirect)
	}

	resetPassword(email, redirect) {
		return AccountService.resetPassword(email, redirect)
	}

	contact(title = '', message = '') {
		return new Promise(res => {
			setTimeout(() => {
				res(true)
			}, 500)
		})
	}

	// ------------------------------------
	// ACTION
	// ------------------------------------

	toggleLike(postId, isLiked = true) {
		const isTrulyLiked = this.state.likedPostIds.indexOf(postId) > -1

		if(
			(isLiked && !isTrulyLiked)
			|| (!isLiked && isTrulyLiked)
		) {
			return PostManager
				.toggleLikePost(postId, isLiked)
				.then(() => {
					return this.update({
						likedPostIds: isLiked ? this.state.likedPostIds.concat(postId) : _.without(this.state.likedPostIds, postId)
					})
				})
		}

		return Promise.reject(false)
	}

	applyJob(id) {
		// JOB APPLICATION CANNOT BE CANCELLED
		// TODO
		return Promise.resolve(true).then(() => {
			return this.state.get(id)
		})
	}

	comment(postId, comment = '') {}

	// ------------------------------------
	// CAART
	// ------------------------------------

	addToCart(id, quantity = 1, metadata = {}) {
		return Promise.resolve().then(() => {
			const cartItemIndex = this.state.cartItems.findIndex(cI => cI.id === id)

			if(cartItemIndex > -1) {
				const cartItem = {
					...this.state.cartItems[cartItemIndex],
				}

				cartItem.quantity = cartItem.quantity + 1

				return this.update({
					cartItems: [...this.state.cartItems],
				})
			} else {
				return this.update({
					cartItems: [...this.state.cartItems, {
						id,
						quantity,
						note: metadata.note,
					}],
				})
			}
		}).then(state => {
			return state.cartItems
		})
	}

	removeFromCart(id) {
		return Promise.resolve().then(() => {
			const cartItemIndex = this.state.cartItems.findIndex(cI => cI.id === id)

			if(cartItemIndex > -1) {
				const newCartItems = this.state.cartItems.slice()
				newCartItems.splice(cartItemIndex, 1)

				return this.update({
					cartItems: newCartItems,
				})
			} else {
				throw new Error('Cart item not found')
			}
		}).then(state => {
			return state.cartItems
		})
	}

	updateCartItems(id, update) {
		return Promise.resolve().then(() => {
			const cartItemIndex = this.state.cartItems.findIndex(cI => cI.id === id)
				, cartItem = this.state.cartItems[cartItemIndex]

			if(cartItemIndex > -1) {
				const newCartItems = this.state.cartItems.slice()

				newCartItems[cartItemIndex] = {
					...cartItem,
					...update,
				}

				return this.update({
					cartItems: newCartItems,
				})
			} else {
				throw new Error('Cart item not found')
			}
		}).then(state => {
			return state.cartItems
		})
	}

	clearCart() {
		this.update({
			cartItems: [],
		})
	}

	logout() {
		FacebookHandler.logout()
		
		this.clear()
		this.dispatch({
			type: this.constants.RESET,
		})
	}
}

export default new MeManager()
