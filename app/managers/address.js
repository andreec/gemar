import DictionaryModel from 'coeur/models/dictionary';

import AddressRecord from 'app/records/address';

// import AddressService from 'app/services/address';
// import ShipperService from 'app/services/shipper';

// import MeManager from './me';


class AddressManager extends DictionaryModel {

	static displayName = 'address'

	constructor() {
		super(AddressRecord, {
			expiricy: 1000 * 60 * 60 * 24 * 30,
			ordered: true,
			datas: [{
				id: 1,
				title: 'Home sweet home',
				pic: 'Mbak Mirnah',
				phone: '08595930289',
				district: 'Bekasi Timur / Margahayu',
				address: 'Villa Taman Kartini, Jalan Graha Elok II Blok H1 no 8, Bekasi',
				postal: '17113',
			}, {
				id: 2,
				title: 'Office',
				pic: 'Tes',
				phone: '2387496',
				district: 'Jawa Barat',
				address: 'Jalan Ketapang Raya',
				postal: '17148',
			}],
		})
	}

	// transformConfig(config = {}) {
	// 	return super.transformConfig({
	// 		...config,
	// 		email: config.email || config.eail,
	// 	})
	// }

	// dataGetter(id) {
	// 	return AddressService.getAddress(id, MeManager.state.token).then(address => {
	// 		return this.updateLater(address)
	// 	}).catch(err => {
	// 		this.warn(err)

	// 		return this.updateLater({
	// 			id,
	// 			_isInvalid: true,
	// 		})
	// 	})
	// }

	// datasGetter(ids) {
	// 	return AddressService.getAddresses(ids, MeManager.state.token).then(addresses => {
	// 		return this.updateLater(addresses)
	// 	}).catch(err => {
	// 		this.warn(err)

	// 		throw err
	// 	})
	// }

	// getAll() {
	// 	return AddressService.getAddresses(undefined, MeManager.state.token).then(addresses => {
	// 		return this.updateLater(addresses)
	// 	})
	// }

	addAddress(data) {
		return new Promise(res => {
			setTimeout(() => {
				res(this.update({
					id: Math.ceil(Math.random() * 1000),
					...data,
				}))
			}, 1000)
		})
		// return AddressService.addAddress(data, MeManager.state.token).then(address => {
		// 	return this.updateLater(address)
		// }).catch(err => {
		// 	this.warn(err)

		// 	throw err
		// })
	}

	updateAddress(id, data) {
		return new Promise(res => {
			setTimeout(() => {
				res(this.update({
					id,
					...data,
				}))
			}, 1000)
		})
		// return AddressService.updateAddress(id, data, MeManager.state.token).then(address => {
		// 	return this.updateLater(address)
		// }).catch(err => {
		// 	this.warn(err)

		// 	throw err
		// })
	}


	// validatePostalAndRetrieveAreas(postal) {
	// 	return ShipperService.getAreas(postal).then(result => {
	// 		return result && result.data && result.data.rows
	// 	})
	// }
}

export default new AddressManager()
