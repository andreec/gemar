import DictionaryModel from 'coeur/models/dictionary';

import HobbyRecord from 'app/records/hobby';

// import HobbyService from 'app/services/media';

// import MeManager from './me';


class HobbyManager extends DictionaryModel {

	static displayName = 'hobbies'

	constructor() {
		super(HobbyRecord, {
			// clearOnRehidrate: true,
			// clearOnReset: true,
			expiricy: 1000 * 60 * 5,
			datas: [{
				id				: 1,

				title			: 'Travelling',
				description		: '',
				image			: 'https://res.cloudinary.com/hiandree/image/upload/v1539451733/dummies/dummy_travelling.jpg',
			}, {
				id				: 2,

				title			: 'Night Life',
				description		: '',
				image			: 'https://res.cloudinary.com/hiandree/image/upload/v1539451873/dummies/dummy-night_life.jpg',
			}, {
				id				: 3,

				title			: 'Culinary',
				description		: '',
				image			: 'https://res.cloudinary.com/hiandree/image/upload/v1539451949/dummies/dummy-culinary.jpg',
			}, {
				id				: 4,

				title			: 'Cycling',
				description		: '',
				image			: 'https://res.cloudinary.com/hiandree/image/upload/v1539452020/dummies/dummy-cycling.jpg',
			}, {
				id				: 5,

				title			: 'Design',
				description		: '',
				image			: 'https://res.cloudinary.com/hiandree/image/upload/v1539452069/dummies/dummy-design.jpg',
			}, {
				id				: 6,

				title			: 'Running',
				description		: '',
				image			: 'https://res.cloudinary.com/hiandree/image/upload/v1539452249/dummies/dummy-run.jpg',
			}, {
				id				: 7,

				title			: 'Aquatic Activity',
				description		: '',
				image			: 'https://res.cloudinary.com/hiandree/image/upload/v1539452318/dummies/dummy-aquatic.jpg',
			}, {
				id				: 8,

				title			: 'Photography',
				description		: '',
				image			: 'https://res.cloudinary.com/hiandree/image/upload/v1539452397/dummies/dummy-photography.jpg',
			}, {
				id				: 9,

				title			: 'Soccer',
				description		: '',
				image			: 'https://res.cloudinary.com/hiandree/image/upload/v1539452517/dummies/dummy-soccer.jpg',
			}, {
				id				: 10,

				title			: 'Extreme games',
				description		: '',
				image			: 'https://res.cloudinary.com/hiandree/image/upload/v1539452648/dummies/dummy-extreme.jpg',
			}, {
				id				: 11,

				title			: 'MMA',
				description		: '',
				image			: 'https://res.cloudinary.com/hiandree/image/upload/v1539452711/dummies/dummy-mma.jpg',
			}, {
				id				: 12,

				title			: 'Drone',
				description		: '',
				image			: 'https://res.cloudinary.com/hiandree/image/upload/v1539452776/dummies/dummy-drone.jpg',
			}, {
				id				: 13,

				title			: 'Gardening',
				description		: '',
				image			: 'https://res.cloudinary.com/hiandree/image/upload/v1539452854/dummies/dummy-planting.jpg',
			}, {
				id				: 14,

				title			: 'Movie',
				description		: '',
				image			: 'https://res.cloudinary.com/hiandree/image/upload/v1539452932/dummies/dummy-movie.jpg',
			}, {
				id				: 15,

				title			: 'Fashion',
				description		: '',
				image			: 'https://res.cloudinary.com/hiandree/image/upload/v1539453010/dummies/dummy-fashion.jpg',
			}, {
				id				: 16,

				title			: 'Coding',
				description		: '',
				image			: 'https://res.cloudinary.com/hiandree/image/upload/v1539453079/dummies/dummy-coding.jpg',
			}, {
				id				: 17,

				title			: 'Electricity',
				description		: '',
				image			: 'https://res.cloudinary.com/hiandree/image/upload/v1539453160/dummies/dummy-electricity.jpg',
			}, {
				id				: 18,

				title			: 'Machinery',
				description		: '',
				image			: 'https://res.cloudinary.com/hiandree/image/upload/v1539453204/dummies/dummy-machinery.jpg',
			}, {
				id				: 19,

				title			: 'Robotic',
				description		: '',
				image			: 'https://res.cloudinary.com/hiandree/image/upload/v1539453352/dummies/dummy-robotics.jpg',
			}, {
				id				: 20,

				title			: 'Racing',
				description		: '',
				image			: 'https://res.cloudinary.com/hiandree/image/upload/v1539453412/dummies/dummy-racing.jpg',
			}, {
				id				: 21,

				title			: 'Music',
				description		: '',
				image			: 'https://res.cloudinary.com/hiandree/image/upload/v1539453524/dummies/dummy-music.jpg',
			}, {
				id				: 22,

				title			: 'Vaping',
				description		: '',
				image			: 'https://res.cloudinary.com/hiandree/image/upload/v1539453629/dummies/dummy-vaping.jpg',
			}, {
				id				: 23,

				title			: 'Flying',
				description		: '',
				image			: 'https://res.cloudinary.com/hiandree/image/upload/v1539453742/dummies/dummy-flying.jpg',
			}, {
				id				: 24,

				title			: 'Art',
				description		: '',
				image			: 'https://res.cloudinary.com/hiandree/image/upload/v1539453796/dummies/dummy-art.jpg',
			}, {
				id				: 25,

				title			: 'Make up',
				description		: '',
				image			: 'https://res.cloudinary.com/hiandree/image/upload/v1539453858/dummies/dummy-makeup.jpg',
			}],
		})
	}

	// dataGetter(id) {
	// 	return HobbyService.getMedia(id, MeManager.state.token).then(media => {
	// 		return this.updateLater(media)
	// 	}).catch(err => {
	// 		this.warn(err)
	//
	// 		return this.updateLater({
	// 			id,
	// 			_isInvalid: true,
	// 		})
	// 	})
	// }
	//
	// datasGetter(ids) {
	// 	return HobbyService.getMedias(ids, MeManager.state.token).then(medias => {
	// 		return this.updateLater(medias)
	// 	}).catch(err => {
	// 		this.warn(err)
	//
	// 		throw err
	// 	})
	// }
}

export default new HobbyManager()
