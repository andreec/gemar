import ManagerModel from 'coeur/models/manager'

import TimeHelper from 'coeur/helpers/time'

import VolatileRecord from 'app/records/volatile'


class VolatileManager extends ManagerModel {

	static displayName = 'volatile'

	constructor() {
		super(VolatileRecord, {
			additionalProperties: {
				matchbox: null,
				me: null,
				order: null,
			},
			clearOnReset: true,
			expiricy: 1000 * 60 * 5,
		})
	}

	isExpired(key) {
		const me = this.state[key]

		return me === null ? true : +TimeHelper.moment(me) + this._expiricy < Date.now()
	}

	mark(key) {
		return this.update({
			[key]: Date.now(),
		})
	}
}

export default new VolatileManager()
