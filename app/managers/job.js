import DictionaryModel from 'coeur/models/dictionary';

import JobRecord from 'app/records/job';

// import HobbyService from 'app/services/media';

// import MeManager from './me';


class JobManager extends DictionaryModel {

	static displayName = 'jobs'

	constructor() {
		super(JobRecord, {
			expiricy: 1000 * 60 * 60 * 24 * 30,
			datas: [{
				id				: 1,
				position		: 'Senior Interior Designer',
				company			: 'PT. Interior Superior',
				minSallary		: 8000000,
				maxSallary		: 10000000,
				availability	: 2,
				applied 		: 0,
			}],
		})
	}

	insert({
		position,
		company,
		minSallary,
		maxSallary,
		availability,
	}) {
		return this.update({
			id: Math.floor(Math.random() * 10000),
			position,
			company,
			minSallary,
			maxSallary,
			availability,
			applied: 0,
		})
	}

	// dataGetter(id) {
	// 	return HobbyService.getMedia(id, MeManager.state.token).then(media => {
	// 		return this.updateLater(media)
	// 	}).catch(err => {
	// 		this.warn(err)
	//
	// 		return this.updateLater({
	// 			id,
	// 			_isInvalid: true,
	// 		})
	// 	})
	// }
	//
	// datasGetter(ids) {
	// 	return HobbyService.getMedias(ids, MeManager.state.token).then(medias => {
	// 		return this.updateLater(medias)
	// 	}).catch(err => {
	// 		this.warn(err)
	//
	// 		throw err
	// 	})
	// }
}

export default new JobManager()
