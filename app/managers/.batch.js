import DictionaryModel from 'coeur/models/dictionary'

import BatchRecord from 'app/records/batch';

import BatchService from 'app/services/batch';

import MeManager from './me';


class BatchManager extends DictionaryModel {

	static displayName = 'batches'

	constructor() {
		super(BatchRecord, {
			expiricy: 1000 * 60 * 5,
		})
	}

	dataGetter(id) {
		return BatchService.getBatch(id, MeManager.state.token).then(batch => {
			return this.updateLater(batch)
		}).catch(err => {
			this.warn(err)

			return this.updateLater({
				id,
				_isInvalid: true,
			})
		})
	}

	datasGetter(ids) {
		return BatchService.getBatches(ids, MeManager.state.token).then(batches => {
			return this.updateLater(batches)
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	getAll() {
		return BatchService.getBatches(undefined, MeManager.state.token).then(batches => {
			return this.updateLater(batches)
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}

	getMockFromCart({
		couponCode,
		products,
		addressId,
	}) {
		return BatchService.getMock({
			couponCode,
			products,
			addressId,
		}, MeManager.state.token)
	}

	create({
		couponCode,
		products,
		addressId,
	}) {
		return BatchService.create({
			couponCode,
			products,
			addressId,
		}, MeManager.state.token).then(batch => {
			return this.updateLater(batch)
		})
	}

	getCheckoutLink(id) {
		return BatchService.getCheckoutLink(id, MeManager.state.token)
	}

	getBatchByFaspayNotification(query) {
		return BatchService.getBatchByFaspayNotification(query, MeManager.state.token).then(batch => {
			return this.updateLater(batch)
		}).catch(err => {
			this.warn(err)

			throw err
		})
	}
}

export default new BatchManager()
