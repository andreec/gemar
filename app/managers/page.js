import DictionaryModel from 'coeur/models/dictionary'

import VolatileRecord from 'app/records/volatile'

import AddressManager from './address';
import BatchManager from './batch';
import MatchboxManager from './matchbox';


class PageManager extends DictionaryModel {

	static displayName = 'pages'

	constructor() {
		super(VolatileRecord, {
			additionalProperties: {
				id: null,
				ids: [],
			},
			clearOnRehidrate: true,
			clearOnReset: true,
			expiricy: 1000 * 60 * 5,
		})
	}

	dataGetter(id) {
		switch(id) {
		case 'matchbox':
			return MatchboxManager.getAll().then(matchboxes => {
				return this.updateLater({
					id,
					ids: matchboxes.map(matchbox => matchbox.id),
				})
			})
		case 'order':
			return BatchManager.getAll().then(batches => {
				return this.updateLater({
					id,
					ids: batches.map(batch => batch.id),
				})
			})
		case 'address':
			return AddressManager.getAll().then(addresses => {
				return this.updateLater({
					id,
					ids: addresses.map(address => address.id),
				})
			})
		default:
			throw new Error('cannot get page data of ' + id)
		}
	}
}

export default new PageManager()
