import React from 'react';
import {
	// DrawerNavigator,
	createAppContainer,
	createStackNavigator,
	createBottomTabNavigator,
	createSwitchNavigator,
} from 'react-navigation';

import Colors from 'coeur/constants/color';

import AuthScreen from 'modules/pages/auth';
import AddressPage from 'modules/pages/address';
import AddressEditPage from 'modules/pages/address.edit';
import CartPage from 'modules/pages/cart';
import CommentPage from 'modules/pages/comment';
import ContactPage from 'modules/pages/contact';
import GalleryPage from 'modules/pages/gallery';
import HomePage from 'modules/pages/home';
import ExplorePage from 'modules/pages/explore';
import NotificationPage from 'modules/pages/notification';
import OrderPage from 'modules/pages/order';
import ProfilePage from 'modules/pages/profile';
import PostPage from 'modules/pages/post';
import PostEditPage from 'modules/pages/post.edit';
import PostsPage from 'modules/pages/posts';
import PostingPage from 'modules/pages/posting';
import ProfileEditPage from 'modules/pages/profile.edit';
import ProfileEditPasswordPage from 'modules/pages/profile.edit.password';
import ProfileLikePage from 'modules/pages/profile.like';
import ProfileFollowerPage from 'modules/pages/profile.follower';
import ProfileFollowingPage from 'modules/pages/profile.following';
import ProfileOrderPage from 'modules/pages/profile.order';
import RegisterPage from 'modules/pages/register';
import SettingPage from 'modules/pages/setting';
import SignupPage from 'modules/pages/signup';
import TagPeoplePage from 'modules/pages/tag.people';
import TagProductPage from 'modules/pages/tag.product';
import CheckoutPage from 'modules/pages/new.checkout';
import JobApplicationPage from 'modules/pages/job.application';
import JobApplicationDetailPage from 'modules/pages/job.detail';
import TrackingPage from 'modules/pages/tracking';

import BoxBit from 'modules/bits/box';
import IconBit from 'modules/bits/icon';

function getConfig(Pages = []) {
	return Pages.reduce((sum, page) => {
		return {
			...sum,
			[page.routeName]: {
				path: page.routeName,
				screen: page,
			},
		}
	}, {})
}

const LandingNavigator = createBottomTabNavigator(getConfig([
	HomePage,
	ExplorePage,
	GalleryPage,
	CartPage,
	ProfilePage,
]), {
	tabBarOptions: {
		showLabel: false,
		style: {
			borderTopWidth: 1,
			borderTopColor: '#E0E0E0',
			backgroundColor: '#F8F8F8',
		},
	},
	initialRouteName: 'landing',
	defaultNavigationOptions: ({ navigation }) => ({
		tabBarIcon: ({ focused }) => {
			const { routeName } = navigation.state;

			let iconName;

			switch(routeName) {
			case 'landing':
				iconName = focused ? 'home' : 'home-outline';
				break;
			case 'explore':
				iconName = focused ? 'explore' : 'explore-outline';
				break;
			case 'gallery':
				iconName = focused ? 'add' : 'add-outline';
				break;
			case 'cart':
				iconName = focused ? 'cart' : 'cart-outline';
				break;
			default:
				iconName = focused ? 'profile' : 'profile-outline';
				break;
			}

			return (
				<BoxBit centering unflex style={{
					width: 32,
					height: 32,
					// borderRadius: 16,
					// backgroundColor: focused ? Colors.theme.palette(3) : undefined,
				}}>
					<IconBit
						size={24}
						name={iconName}
						color={ !focused ? Colors.new.black.palette(1) : Colors.new.yellow.palette(4) }
					/>
				</BoxBit>
			)
			// You can return any component that you like here! We usually use an
			// icon component from react-native-vector-icons
			// return <Ionicons name={iconName} size={horizontal ? 20 : 25} color={trackColor} />;
		},
	}),
})

console.disableYellowBox = true

const RootNavigator = createStackNavigator({
	'home' : LandingNavigator,
	'user.profile' : { screen: ProfilePage },
	...getConfig([
		AddressPage,
		AddressEditPage,
		CommentPage,
		ContactPage,
		NotificationPage,
		OrderPage,
		PostPage,
		PostsPage,
		PostEditPage,
		PostingPage,
		ProfileEditPage,
		ProfileEditPasswordPage,
		ProfileLikePage,
		ProfileFollowerPage,
		ProfileFollowingPage,
		ProfileOrderPage,
		SettingPage,
		TagPeoplePage,
		TagProductPage,
		CheckoutPage,
		JobApplicationPage,
		JobApplicationDetailPage,
		TrackingPage,
	]),
}, {
	// initialRouteName: OrderPage.routeName,
	initialRouteName: 'home',
	navigationOptions: {
		header: null,
	},
	headerMode: 'none',
})


const AuthNavigator = createStackNavigator({
	...getConfig([
		SignupPage,
		RegisterPage,
	]),
}, {
	navigationOptions: {
		header: null,
	},
	headerMode: 'none',
})

export default {
	Navigator: createAppContainer(createSwitchNavigator({
		AuthScreen,
		RootNavigator,
		Auth: AuthNavigator,
		// LandingNavigator,
	}, {
		initialRouteName: 'AuthScreen',
	})),
}
