import './start'
import {
	AppRegistry,
	// YellowBox,
} from 'react-native';
import Root from 'app/root';

// TODO: REMOVE WHEN NO LONGER BUGGING
// YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated'])


export default {
	init() {
		AppRegistry.registerComponent('gemar', () => Root);
	},
};
