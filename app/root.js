// import React from 'react'
import Root from 'coeur/kernel/root'

// import AddressManager from 'app/managers/address';
// import BatchManager from 'app/managers/batch';
// import MatchboxManager from 'app/managers/matchbox';
// import MeManager from 'app/managers/me';
// import MediaManager from 'app/managers/media';
// import PageManager from 'app/managers/page';
// import UserManager from 'app/managers/user';
// import VolatileManager from 'app/managers/volatile';

import AddressManager from 'app/managers/address';
import HobbyManager from 'app/managers/hobby';
import MeManager from 'app/managers/me';
import JobManager from 'app/managers/job';
import OrderManager from 'app/managers/order';
import PostManager from 'app/managers/post';
import ProductManager from 'app/managers/product';
import UserManager from 'app/managers/user';

import BoxBit from 'modules/bits/box';
// import SourceSansBit from 'modules/bits/source.sans';
import LoaderBit from 'modules/bits/loader';

import AlertPagelet from 'modules/pagelets/alert';
import MenuPagelet from 'modules/pagelets/menu';
import NotificationPagelet from 'modules/pagelets/notification';

import Router from './router'

import {
	AppState,
	NetInfo,
	Dimensions,
} from 'react-native'


export default class Gemar extends Root {

	initialize(Store) {
		// if(window) {
		// 	window.screen
		// 	&& window.screen.orientation
		// 	&& window.screen.orientation.lock
		// 	&& window.screen.orientation.lock('portrait').catch(err => {
		// 		if(process.env.DEBUG === 'true') {
		// 			console.warn(err)
		// 		}
		// 	})
		//
		// 	this.state.isPortrait = window.orientation !== -90 && window.orientation !== 90
		// }

		return new Store([
			AddressManager,
			HobbyManager,
			MeManager,
			JobManager,
			OrderManager,
			PostManager,
			ProductManager,
			UserManager,
		])
	}

	onAppStateChange(nextAppState) {
		super.onAppStateChange(nextAppState === 'active' ? 'visible' : 'hidden')
	}

	onConnectivityChange(connection) {
		super.onConnectivityChange(connection.type !== 'none' && connection.type !== 'unknown')
	}

	onOrientationChange() {
		// TODO: FIX, it isn't working
		super.onOrientationChange(Dimensions.get('windows').width > Dimensions.get('windows').height ? 90 : 0)
	}

	componentDidMount() {
		AppState.addEventListener('change', this.onAppStateChange);
		NetInfo.addEventListener('connectionChange', this.onConnectivityChange);
		Dimensions.addEventListener('change', this.onOrientationChange);
	}

	componentWillUnmount() {
		AppState.removeEventListener('change', this.onAppStateChange);
		NetInfo.removeEventListener('connectionChange', this.onConnectivityChange);
		Dimensions.removeEventListener('change', this.onOrientationChange);
	}

	registerUtilities() {
		super.registerUtilities({
			MENU_TYPES: MenuPagelet.TYPES,
			NOTIFICATION_TYPES: NotificationPagelet.TYPES,
		})
	}

	render() {
		return super.render({
			Navigator: Router.Navigator,
			AlertPagelet,
			MenuPagelet,
			NotificationPagelet,
			BoxBit,
			LoaderBit,
		})
	}
}
